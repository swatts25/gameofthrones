package client;

import java.io.File;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

import utils.EmailMessage;
import utils.GameOfThronesMarshaller;
import utils.SavedGameData;
import client.model.GameContext;
import client.model.House;
import client.model.Player;
import client.model.Turn;
import client.model.cards.WildlingCard;
import client.model.zones.Zone;
import controllers.FootmanRemoverPanelController;
import controllers.FootmanUpgraderPanelController;
import controllers.HouseCardChooserPanelController;
import controllers.JavaFXController;
import controllers.MainScreenPanelController;
import controllers.MarchOrderResolverPanelController;
import controllers.MusterPanelController;
import controllers.OrderEditorPanelController;
import controllers.OrderRemoverPanelController;
import controllers.RaidOrderResolverPanelController;
import controllers.RetreatPanelController;
import controllers.TrackBiddingPanelController;
import controllers.UnitDestroyerPanelController;
import controllers.WildlingCardViewerPanelController;

public class TurnHandler {

    public TurnHandler() {
        // TODO Auto-generated constructor stub
    }

    public static void takeTurn(Turn currentTurn,
            MainScreenPanelController mainScreenPanelController) {
        // Figure out what panel to show based on turn type
        String panelResource = null;
        String title = null;
        String opponentName;
        JavaFXController controller = null;
        GameContext context = GameContext.getMyGameContext();
        Combat combat;
        int choice;
        switch (currentTurn.getType()) {
            case Turn.PLANNING_PHASE:
                GameContext.getMyGameContext().setValyrianBladeAvailable(true);
                panelResource = "/forms/BaseOrderEditorPanel.fxml";
                title = "Planning Phase";
                controller = new OrderEditorPanelController(
                        mainScreenPanelController, currentTurn);
                break;
            case Turn.RAVEN:
                PlanningPhase planningPhase = new PlanningPhase(
                        mainScreenPanelController);
                choice = JavaFXLauncher
                        .showRavenChoiceDialog(
                                "Kings Court",
                                "You are the holder of the Three Eyed Raven!",
                                "Please choose to either edit ONE of your commands or view the next Wildling Card.");
                if (choice == 2) {
                    if (!GameContext.getMyGameContext().getWildlingDeck()
                            .isEmpty()) {
                        WildlingCard wildlingCard = context.getWildlingDeck()
                                .get(0);
                        JavaFXLauncher.showPanel(
                                "/forms/BaseWildlingCardViewerPanel.fxml",
                                "Wildling Card",
                                new WildlingCardViewerPanelController(
                                        wildlingCard));
                        planningPhase.submitCommands(null);
                        return;
                    } else {
                        Client.error("Wildling deck is empty!");
                        return;
                    }
                } else if (choice == 1) {
                    Client.getLogger().info("Edit Command");
                    panelResource = "/forms/BaseOrderEditorPanel.fxml";
                    title = "Raven Phase";
                    controller = new OrderEditorPanelController(
                            mainScreenPanelController, currentTurn);
                    break;
                } else {
                    return;
                }
            case Turn.VALYRIAN_BLADE:
                combat = currentTurn.getCombat();
                combat.setContext(context);
                choice = JavaFXLauncher
                        .showYesNoDialog(
                                "Valyrian Blade",
                                "Holder of the Valyrian Blade!",
                                "If you use the Valyrian Blade it will be unavailable for all remaining combats this phase and you will win this combat, otherwise you will lose this combat.");
                if (choice == 1) {
                    // Yes
                    context.getTurns().remove(currentTurn);
                    context.setValyrianBladeAvailable(false);
                    combat.setPlayerUsingValyrianBlade(currentTurn
                            .getPlayerName());
                    combat.resolveCombatBetweenPlayers(mainScreenPanelController);
                } else if (choice == 0) {
                    // No
                    context.getTurns().remove(currentTurn);
                    combat.resolveCombatBetweenPlayers(mainScreenPanelController);
                } else {
                    // Cancel
                    return;
                }
                break;
            case Turn.RESOLVE_RAID_ORDER:
                Client.getLogger().info("Resolving Raid Order");
                panelResource = "/forms/BaseRaidOrderResolverPanel.fxml";
                title = "Resolve Raid Order";
                controller = new RaidOrderResolverPanelController(
                        mainScreenPanelController, currentTurn);
                break;
            case Turn.RESOLVE_MARCH_ORDER:
                Client.getLogger().info("Resolving March Order");
                panelResource = "/forms/BaseRaidOrderResolverPanel.fxml";
                title = "Resolve March Order";
                controller = new MarchOrderResolverPanelController(
                        mainScreenPanelController, currentTurn);
                break;
            case Turn.AERON_DAMPHAIR:
                // after viewing opponent house card, card holder may pay 2
                // influence to pick a new card from their available hand
                Client.getLogger().info("Resolving Aeron Damphair House Card");
                combat = currentTurn.getCombat();
                combat.setContext(context);
                AeronDamphairSpecialAbility aeronHouseCardAbility = new AeronDamphairSpecialAbility(
                        currentTurn, mainScreenPanelController, combat, context);
                aeronHouseCardAbility.showOpponetsCard();
                choice = JavaFXLauncher
                        .showYesNoDialog("Change your House Card",
                                "Aeron Damphair",
                                "Would you like to pay 2 influence tokens to change your house card?");
                if (choice == 1) {
                    aeronHouseCardAbility.changeCard();
                } else if (choice == -1) {
                    return;
                } else {
                    context.getTurns().remove(currentTurn);
                    TurnHandler.saveAndDistributeGameContext(currentTurn,
                            context);
                    TurnHandler.UpdateUI(mainScreenPanelController);
                    return;
                }
                break;
            case Turn.TYRION_LANNISTER_CARDHOLDER:
                // Card holder can force the attacker to choose a new house
                // card from their available hand.
                // If their hand is empty they cannot play a card
                // their original chosen house card is returned to their
                // hand after combat
                Client.getLogger()
                        .info("Resolving Tyrion Lannister House Card");
                combat = currentTurn.getCombat();
                combat.setContext(context);
                TyrionLannisterSpecialAbility tyrionHouseCardAbilityCardHolder = new TyrionLannisterSpecialAbility(
                        currentTurn, mainScreenPanelController, combat, context);
                tyrionHouseCardAbilityCardHolder.showOpponetsCard();
                choice = JavaFXLauncher
                        .showYesNoDialog("Change opponents House Card",
                                "Tyrion Lannister",
                                "Would you like to force your opponent to chose a new card?");
                if (choice == 1) {
                    context.getTurns().remove(currentTurn);
                    tyrionHouseCardAbilityCardHolder
                            .forceOpponentToChangeCard();
                    return;
                } else if (choice == 0) {
                    context.getTurns().remove(0);
                    tyrionHouseCardAbilityCardHolder
                            .doNotForceOpponentToChangeCard();
                    return;
                } else {
                    return;
                }
            case Turn.TYRION_LANNISTER_OPPONENT:
                Client.getLogger()
                        .info("Resolving Tyrion Lannister House Card");
                combat = currentTurn.getCombat();
                combat.setContext(context);
                TyrionLannisterSpecialAbility tyrionHouseCardAbilityOpponent = new TyrionLannisterSpecialAbility(
                        currentTurn, mainScreenPanelController, combat, context);
                tyrionHouseCardAbilityOpponent.changeCard();
                break;
            case Turn.DORAN_MARTELL:
                Client.getLogger().info("Resolving Doran Martell House Card");
                // Card holder may move their opponent to the bottom of one
                // influence track of card holders choice
                // occurs before combat
                combat = currentTurn.getCombat();
                combat.setContext(context);
                DoranMartellSpecialAbility doranHouseCardAbility = new DoranMartellSpecialAbility(
                        currentTurn, mainScreenPanelController, combat, context);
                if (!doranHouseCardAbility.changeOpponentsTracks()) {
                    return;
                }
                if (doranHouseCardAbility.isAttacker()) {
                    combat.setAttackerHouseCardPlayed(true);
                } else {
                    combat.setDefenderHouseCardPlayed(true);
                }
                context.getTurns().remove(currentTurn);
                if (context.getTurns().isEmpty()) {
                    combat.resolveCombatBetweenPlayers(mainScreenPanelController);
                }
                TurnHandler.saveAndDistributeGameContext(currentTurn, context);
                TurnHandler.UpdateUI(mainScreenPanelController);
                break;
            case Turn.QUEEN_OF_THORNS:
                // card holder can remove 1 of their opponents order tokens
                // in a zone adjacent to the target zone
                // card holder may not remove the march order which started
                // combat (add flag in march order, set true if started
                // combat)
                combat = currentTurn.getCombat();
                combat.setContext(context);
                HouseCardSpecialAbility queenHouseCardAbility = new HouseCardSpecialAbility(
                        currentTurn, mainScreenPanelController, combat, context);
                if (queenHouseCardAbility.isAttacker()) {
                    opponentName = combat.getDefendingPlayerName();
                } else {
                    opponentName = combat.getAttackingPlayerName();
                }
                JavaFXLauncher.showPanel("/forms/BaseOrderRemoverPanel.fxml",
                        "Opponents Orders", new OrderRemoverPanelController(
                                combat.getTargetZoneName(), opponentName,
                                queenHouseCardAbility,
                                mainScreenPanelController, currentTurn));
                return;
            case Turn.MACE_TYRELL:
                // card hold can immediately destroy an opponents footman
                combat = currentTurn.getCombat();
                combat.setContext(context);
                HouseCardSpecialAbility maceHouseCardAbility = new HouseCardSpecialAbility(
                        currentTurn, mainScreenPanelController, combat, context);
                if (maceHouseCardAbility.isAttacker()) {
                    opponentName = combat.getDefendingPlayerName();
                } else {
                    opponentName = combat.getAttackingPlayerName();
                }
                JavaFXLauncher.showPanel("/forms/BaseUnitEditorPanel.fxml",
                        "Opponents Footmen", new FootmanRemoverPanelController(
                                combat.getTargetZoneName(), opponentName,
                                maceHouseCardAbility,
                                mainScreenPanelController, currentTurn));
                break;
            case Turn.PATCHFACE:
                // card holder gets to discard a card from attackers deck
                // after
                // combat
                combat = currentTurn.getCombat();
                combat.setContext(context);
                PatchfaceSpecialAbility patchfaceHouseCardAbility = new PatchfaceSpecialAbility(
                        currentTurn, mainScreenPanelController, combat, context);
                patchfaceHouseCardAbility.discardCard();
                return;
            case Turn.RENLY_BARATHEON:
                // IF card holder wins combat, defender may upgrade one
                // participating (including support) footman to a knight
                combat = currentTurn.getCombat();
                combat.setContext(context);
                HouseCardSpecialAbility renlyHouseCardAbility = new HouseCardSpecialAbility(
                        currentTurn, mainScreenPanelController, combat, context);
                if (renlyHouseCardAbility.isAttacker()) {
                    opponentName = combat.getDefendingPlayerName();
                } else {
                    opponentName = combat.getAttackingPlayerName();
                }
                JavaFXLauncher.showPanel(
                        "/forms/BaseUnitEditorPanel.fxml",
                        "Opponents Footmen",
                        new FootmanUpgraderPanelController(combat
                                .getTargetZoneName(), renlyHouseCardAbility,
                                mainScreenPanelController, currentTurn));
                break;
            case Turn.CERSEI_LANNISTER:
                // card holder can remove 1 of their opponents order tokens
                // from any zone
                combat = currentTurn.getCombat();
                combat.setContext(context);
                HouseCardSpecialAbility cerseiHouseCardAbility = new HouseCardSpecialAbility(
                        currentTurn, mainScreenPanelController, combat, context);
                if (cerseiHouseCardAbility.isAttacker()) {
                    opponentName = combat.getDefendingPlayerName();
                } else {
                    opponentName = combat.getAttackingPlayerName();
                }
                JavaFXLauncher.showPanel("/forms/BaseOrderRemoverPanel.fxml",
                        "Opponents Orders", new OrderRemoverPanelController(
                                combat.getTargetZoneName(), opponentName,
                                cerseiHouseCardAbility,
                                mainScreenPanelController, currentTurn));
                return;
            case Turn.RETREAT:
                combat = currentTurn.getCombat();
                combat.setContext(context);
                panelResource = "/forms/BaseRetreatPanel.fxml";
                title = "Retreat";
                RetreatPanelController retreatontroller = new RetreatPanelController(
                        combat.getTargetZoneName(), combat.getVictor(),
                        combat.getAttackerMarchedFromZoneName(),
                        mainScreenPanelController, currentTurn);
                if (retreatontroller.getNumRetreatingUnits() > 0) {
                    JavaFXLauncher.showPanel(panelResource, title,
                            retreatontroller);
                } else {
                    retreatontroller.endTurn(false);
                    JavaFXLauncher
                            .showInformationDialog("No Units",
                                    "All your units were destroyed in combat, you have none left to retreat");
                    return;
                }
                break;
            case Turn.CHOOSE_HOUSE_CARD:
                Client.getLogger().info("Choosing House Card");
                panelResource = "/forms/BaseHouseCardChooserPanel.fxml";
                title = "Choose House Card";
                List hand = null;
                switch (currentTurn.getPlayer().getHouse()) {
                    case House.BARATHEON:
                        hand = context.getBaratheonHouseDeck();
                        break;
                    case House.GREYJOY:
                        hand = context.getGreyjoyHouseDeck();
                        break;
                    case House.LANNISTER:
                        hand = context.getLannisterHouseDeck();
                        break;
                    case House.MARTELL:
                        hand = context.getMartellHouseDeck();
                        break;
                    case House.STARK:
                        hand = context.getStarkHouseDeck();
                        break;
                    case House.TYRELL:
                        hand = context.getTyrellHouseDeck();
                        break;
                    default:
                        break;
                }
                controller = new HouseCardChooserPanelController(hand,
                        mainScreenPanelController, currentTurn);
                break;
            case Turn.A_THRONE_OF_BLADES:
                choice = JavaFXLauncher
                        .showAThroneOfBladesChooserDialog("A Throne of Blades",
                                "Holder of the Iron Throne",
                                "You may choose to update supply tracks, muster units, or do nothing.");
                if (choice == 1) {
                    // supply
                    context.getTurns().remove(currentTurn);
                    WesterosPhase.rectifySupply(context);
                } else if (choice == 2) {
                    // muster
                    // add musters turns for each player, add them to head
                    context.getTurns().remove(currentTurn);
                    WesterosPhase.addMusterTurns(context);
                } else if (choice == 3) {
                    // nothing
                    context.getTurns().remove(currentTurn);
                } else {
                    return;
                }
                TurnHandler.saveAndDistributeGameContext(currentTurn, context);
                TurnHandler.UpdateUI(mainScreenPanelController);
                break;
            case Turn.RECTIFY_SUPPLY:
                JavaFXLauncher.showPanel("/forms/BaseUnitEditorPanel.fxml",
                        "Destroy Units", new UnitDestroyerPanelController(
                                currentTurn, mainScreenPanelController));
                break;
            case Turn.MUSTER:
                JavaFXLauncher.showPanel("/forms/BaseMusterPanel.fxml",
                        "Muster Units", new MusterPanelController(currentTurn,
                                mainScreenPanelController));
                break;
            case Turn.DARK_WINGS_DARK_WORDS:
                choice = JavaFXLauncher
                        .showDarkWingsDarkWordsChooserDialog(
                                "Dark Wings Dark Words",
                                "Holder of the Raven",
                                "You may choose whether everyone bids on the influence tracks or everyone collects an influence token for crowns printed on zones they control");
                if (choice == 1) {
                    // Bid on influence tracks
                    context.getTurns().remove(currentTurn);
                    WesterosPhase.addInfluenceTrackBiddingTurns(context);
                } else if (choice == 2) {
                    // Collect Influence Tokens
                    context.getTurns().remove(currentTurn);
                    WesterosPhase.collectInfluenceTokens(context);
                    TurnHandler.UpdateUI(mainScreenPanelController);
                } else if (choice == 3) {
                    // nothing
                    context.getTurns().remove(currentTurn);
                } else {
                    return;
                }
                TurnHandler.saveAndDistributeGameContext(currentTurn, context);
                TurnHandler.UpdateUI(mainScreenPanelController);
                break;
            case Turn.INFLUENCE_TRACK_BIDDING:
                JavaFXLauncher.showPanel("/forms/BaseTrackBiddingPanel.fxml",
                        "Influence Track Bidding",
                        new TrackBiddingPanelController(currentTurn,
                                mainScreenPanelController));
                break;
            case Turn.PUT_TO_THE_SWORD:
                choice = JavaFXLauncher
                        .showPutToTheSwordChooserDialog("Put to the Sword",
                                "Holder of the Valyrian Blade",
                                "You may choose to restrict defense or march +1 orders");
                if (choice == 1) {
                    // Defense
                    context.getTurns().remove(currentTurn);
                    context.setRestrictDefenseOrders(true);
                } else if (choice == 2) {
                    // March +1
                    context.getTurns().remove(currentTurn);
                    context.setRestrictMarchPlusOneOrders(true);
                } else if (choice == 3) {
                    // nothing
                    context.getTurns().remove(currentTurn);
                } else {
                    return;
                }
                TurnHandler.saveAndDistributeGameContext(currentTurn, context);
                TurnHandler.UpdateUI(mainScreenPanelController);
                break;
            case Turn.GAME_OVER:
                JavaFXLauncher
                        .showInformationDialog(
                                "New King",
                                currentTurn.getPlayerName()
                                        + " has captured 7 castles or strongholds and has won the game. All hail the new King, "
                                        + currentTurn.getPlayerName()
                                        + " "
                                        + Player.getPlayer(
                                                currentTurn.getPlayerId())
                                                .getHouse()
                                        + " King of the first men, Lord of the Seven Kingdoms, and Protector of the Realm.");
                break;
            default:
                Client.error("Invalid turn type: " + currentTurn.getType());
                return;
        }
        if (panelResource != null && controller != null) {
            JavaFXLauncher.showPanel(panelResource, title, controller);
        }
    }

    public static void addResolveTurnsBasedOnFieldoms(String orderToResolve,
            String turnToAdd) {
        GameContext context = GameContext.getMyGameContext();
        List<Zone> zones = new ArrayList<Zone>(GameContext.getMyGameContext()
                .getZones().values());
        List<Zone> zonesWithOrder = new ArrayList<Zone>();
        List<Player> players = context.getPlayers();
        players.sort(new Comparator<Player>() {

            @Override
            public int compare(Player o1, Player o2) {
                if (o1.getFieldomsTrack() < o2.getFieldomsTrack()) {
                    return -1;
                } else if (o1.getFieldomsTrack() > o2.getFieldomsTrack()) {
                    return 1;
                } else {
                    Client.error("Two players are on the same fieldoms track!");
                    return 0;
                }
            }
        });
        for (Zone zone : zones) {
            if (zone.getOrder() != null
                    && zone.getOrder().getType().equals(orderToResolve)
                    && zone.isOrderResolved() == false
                    && zone.getControlledByName() != null) {
                zonesWithOrder.add(zone);
            }
        }
        while (!zonesWithOrder.isEmpty()) {
            for (Player player : players) {
                for (Zone zone : zonesWithOrder) {
                    if (player.equals(zone.getControlledBy())) {
                        Turn newTurn = new Turn(player);
                        newTurn.setType(turnToAdd);
                        context.getTurns().add(newTurn);
                        zonesWithOrder.remove(zone);
                        break;
                    }
                }
            }
        }
    }

    public static void saveAndDistributeGameContext(Turn currentTurn,
            GameContext context) {
        context.setContextid(context.getContextid() + 1);
        if (MainScreenPanelController.VERSION.compareTo(context
                .getGreatesVersion()) > 0) {
            context.setGreatesVersion(MainScreenPanelController.VERSION);
        }
        File updatedGameContext = GameOfThronesMarshaller.marshall(context);
        sendEmail(currentTurn, context, updatedGameContext);
    }

    public static void resendGameContext(GameContext context) {
        File updatedGameContext = GameOfThronesMarshaller.marshall(context);
        sendEmail(null, context, updatedGameContext);
    }

    private static void sendEmail(Turn currentTurn, GameContext context,
            File updatedGameContext) {
        if (SavedGameData.getProperty(SavedGameData.DISABLE_EMAILS) == null) {
            Turn nextTurn = null;
            List<Player> spectatingPlayers = new ArrayList<Player>(
                    context.getPlayers());
            // Remove sender
            for (Player player : spectatingPlayers) {
                if (player.isMe()) {
                    spectatingPlayers.remove(player);
                    break;
                }
            }
            if (!context.getTurns().isEmpty()) {
                nextTurn = context.getTurns().get(0);
                // Remove next turn player
                spectatingPlayers.remove(nextTurn.getPlayer());
            }
            EmailMessage spectatorMessage = getSpectatorMessage(currentTurn,
                    context, updatedGameContext, nextTurn, spectatingPlayers);
            spectatorMessage.send();
            EmailMessage nextTurnPlayerMessage = getNextTurnPlayerMessage(
                    currentTurn, context, updatedGameContext, nextTurn);
            if (nextTurnPlayerMessage != null) {
                nextTurnPlayerMessage.send();
            }
        }
    }

    private static EmailMessage getSpectatorMessage(Turn currentTurn,
            GameContext context, File updatedGameContext, Turn nextTurn,
            List<Player> spectatingPlayers) {
        EmailMessage spectatorMessage;
        if (currentTurn != null && nextTurn != null) {
            spectatorMessage = new EmailMessage(
                    spectatingPlayers,
                    Player.getMyName() + " has sent their "
                            + currentTurn.getType(),
                    "gameContext_" + context.getContextid() + ".xml",
                    updatedGameContext.getAbsolutePath(),
                    Player.getMyName()
                            + " has sent his/her turn to all players. Now it is "
                            + nextTurn.getPlayerName() + "'s turn to complete "
                            + nextTurn.getType());
        } else if (nextTurn != null) {
            spectatorMessage = new EmailMessage(
                    spectatingPlayers,
                    Player.getMyName() + " has sent their turn",
                    "gameContext_" + context.getContextid() + ".xml",
                    updatedGameContext.getAbsolutePath(),
                    Player.getMyName()
                            + " has sent his/her turn to all players. Now it is "
                            + nextTurn.getPlayerName() + "'s turn to complete "
                            + nextTurn.getType());
        } else {
            spectatorMessage = new EmailMessage(spectatingPlayers,
                    Player.getMyName() + " has sent their turn", "gameContext_"
                            + context.getContextid() + ".xml",
                    updatedGameContext.getAbsolutePath(),
                    "Next turn was not found, game is over.");
        }
        return spectatorMessage;
    }

    private static EmailMessage getNextTurnPlayerMessage(Turn currentTurn,
            GameContext context, File updatedGameContext, Turn nextTurn) {
        EmailMessage nextTurnPlayerMessage = null;
        if (nextTurn != null) {
            List<Player> nextTurnPlayerList = new ArrayList<Player>();
            nextTurnPlayerList.add(nextTurn.getPlayer());
            if (currentTurn != null) {
                nextTurnPlayerMessage = new EmailMessage(
                        nextTurnPlayerList,
                        "ITS YOUR TURN to complete " + nextTurn.getType(),
                        "gameContext_" + context.getContextid() + ".xml",
                        updatedGameContext.getAbsolutePath(),
                        Player.getMyName()
                                + " has sent his/her "
                                + currentTurn.getType()
                                + " to all players. Now it is your turn to complete "
                                + nextTurn.getType());
            } else {
                nextTurnPlayerMessage = new EmailMessage(
                        nextTurnPlayerList,
                        "ITS YOUR TURN to complete " + nextTurn.getType(),
                        "gameContext_" + context.getContextid() + ".xml",
                        updatedGameContext.getAbsolutePath(),
                        Player.getMyName()
                                + " has sent his/her turn to all players. Now it is "
                                + nextTurn.getPlayerName()
                                + "'s turn to complete " + nextTurn.getType());
            }
        }
        return nextTurnPlayerMessage;
    }

    public static void UpdateUI(MainScreenPanelController mainscreen) {
        if (mainscreen != null) {
            GameContext context = GameContext.getMyGameContext();
            mainscreen.updateZonesTable(Player.getMyName());
            mainscreen.updateInfluenceTokens();
            mainscreen.updateButtons();
            mainscreen.refreshMap(context);
            if (!context.getTurns().isEmpty()) {
                mainscreen.getStatusLabel().setText(
                        "Next Turn: Waiting for "
                                + context.getTurns().get(0).getPlayer()
                                        .getName() + " to complete "
                                + context.getTurns().get(0).getType()
                                + ". Load GameContext_"
                                + (context.getContextid() + 1));
                if (!context.getTurns().get(0).getPlayer().isMe()) {
                    mainscreen.getBeginTurnButton().setDisable(true);
                }
            } else {
                mainscreen.getStatusLabel().setText("Next Turn was not found.");
            }
        }
    }
}
