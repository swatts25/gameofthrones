package client;

import java.io.File;
import java.io.IOException;
import java.util.Optional;

import client.model.GameContext;
import client.model.House;
import client.model.Player;
import controllers.JavaFXController;
import controllers.MainScreenPanelController;
import javafx.application.Application;
import javafx.application.Platform;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.ButtonBar.ButtonData;
import javafx.scene.control.ButtonType;
import javafx.scene.control.TextInputDialog;
import javafx.scene.image.Image;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import utils.GameOfThronesMarshaller;
import utils.SavedGameData;
import utils.StringHelper;

public class JavaFXLauncher extends Application {

    private static final String RESOURCES_LOGO_JPG = "/resources/logo.png";

    private static JavaFXController controller;

    private static String title;

    public void show(JavaFXController controller, String title) {
        JavaFXLauncher.controller = controller;
        JavaFXLauncher.title = title;
        if (getClass().getResourceAsStream(controller.getFxmlPanel()) != null) {
            launch(controller.getFxmlPanel());
        } else {
            Client.error("jrxml resource not found");
        }
    }

    @Override
    public void start(Stage primaryStage) throws IOException {
        Parameters params = getParameters();
        FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource(
                params.getUnnamed().get(0)));
        fxmlLoader.setController(controller);
        Parent root = (Parent) fxmlLoader.load();
        Scene scene = new Scene(root);
        primaryStage.setTitle(title);
        primaryStage.setScene(scene);
        controller.setScene(scene);
        primaryStage.setResizable(false);
        primaryStage.getIcons().add(new Image(getClass().getResourceAsStream(RESOURCES_LOGO_JPG)));
        primaryStage.show();
        if (controller instanceof MainScreenPanelController) {
            checkForGameData();
        }
    }

    public static void checkForGameData() {
        File currentGameFile = new File(
                GameOfThronesMarshaller.DEFAULT_GAME_COTEXT_FILENAME);
        if (currentGameFile.exists() && !currentGameFile.isDirectory()) {
            int loadGame = showYesNoDialog("Game in Progress",
                    "Game Data Found",
                    "Game data was found, would you like to load the game in progress?");
            if (loadGame == 1) {
                GameContext loadedGameContext = GameOfThronesMarshaller
                        .unMarshallGameContext(currentGameFile);
                ((MainScreenPanelController) controller)
                        .loadGame(loadedGameContext);
            }
        }
    }

    public static void showInformationDialog(String title, String message) {
        Alert alert = new Alert(AlertType.INFORMATION);
        alert.setTitle(title);
        alert.setHeaderText(null);
        alert.setContentText(message);
        alert.showAndWait();
    }

    public static void showConfirmationDialog(String title, String message) {
        Alert alert = new Alert(AlertType.CONFIRMATION);
        alert.setTitle(title);
        alert.setHeaderText(null);
        alert.setContentText(message);
        alert.showAndWait();
    }

    public static int showYesNoDialog(String title, String header,
            String message) {
        Alert alert = new Alert(AlertType.CONFIRMATION);
        alert.setTitle(title);
        alert.setHeaderText(header);
        alert.setContentText(message);
        ButtonType buttonTypeOne = new ButtonType("Yes");
        ButtonType buttonTypeTwo = new ButtonType("No");
        ButtonType buttonTypeCancel = new ButtonType("Cancel",
                ButtonData.CANCEL_CLOSE);
        alert.getButtonTypes().setAll(buttonTypeOne, buttonTypeTwo,
                buttonTypeCancel);
        Optional<ButtonType> result = alert.showAndWait();
        if (result.get() == buttonTypeOne) {
            return 1;
        } else if (result.get() == buttonTypeTwo) {
            return 0;
        } else {
            return -1;
        }
    }
    
    public static int showTrackChooserDialog(String title, String header,
            String message) {
        Alert alert = new Alert(AlertType.CONFIRMATION);
        alert.setTitle(title);
        alert.setHeaderText(header);
        alert.setContentText(message);
        ButtonType buttonTypeOne = new ButtonType("Iron Throne");
        ButtonType buttonTypeTwo = new ButtonType("Fiefdoms");
        ButtonType buttonTypeThree = new ButtonType("King's Court");
        ButtonType buttonTypeCancel = new ButtonType("Cancel",
                ButtonData.CANCEL_CLOSE);
        alert.getButtonTypes().setAll(buttonTypeOne, buttonTypeTwo, buttonTypeThree,
                buttonTypeCancel);
        Optional<ButtonType> result = alert.showAndWait();
        if (result.get() == buttonTypeOne) {
            return 1;
        } else if (result.get() == buttonTypeTwo) {
            return 2;
        } else if (result.get() == buttonTypeThree) {
            return 3;
        } else {
            return -1;
        }
    }
    
    public static int showAThroneOfBladesChooserDialog(String title, String header,
            String message) {
        Alert alert = new Alert(AlertType.CONFIRMATION);
        alert.setTitle(title);
        alert.setHeaderText(header);
        alert.setContentText(message);
        ButtonType buttonTypeOne = new ButtonType("Supply");
        ButtonType buttonTypeTwo = new ButtonType("Muster");
        ButtonType buttonTypeThree = new ButtonType("Nothing");
        ButtonType buttonTypeCancel = new ButtonType("Cancel",
                ButtonData.CANCEL_CLOSE);
        alert.getButtonTypes().setAll(buttonTypeOne, buttonTypeTwo, buttonTypeThree,
                buttonTypeCancel);
        Optional<ButtonType> result = alert.showAndWait();
        if (result.get() == buttonTypeOne) {
            return 1;
        } else if (result.get() == buttonTypeTwo) {
            return 2;
        } else if (result.get() == buttonTypeThree) {
            return 3;
        } else {
            return -1;
        }
    }
    
    public static int showDarkWingsDarkWordsChooserDialog(String title, String header,
            String message) {
        Alert alert = new Alert(AlertType.CONFIRMATION);
        alert.setTitle(title);
        alert.setHeaderText(header);
        alert.setContentText(message);
        ButtonType buttonTypeOne = new ButtonType("Bid on Influence Tracks");
        ButtonType buttonTypeTwo = new ButtonType("Collect Infleunce Tokens");
        ButtonType buttonTypeThree = new ButtonType("Do Nothing");
        ButtonType buttonTypeCancel = new ButtonType("Cancel",
                ButtonData.CANCEL_CLOSE);
        alert.getButtonTypes().setAll(buttonTypeOne, buttonTypeTwo, buttonTypeThree,
                buttonTypeCancel);
        Optional<ButtonType> result = alert.showAndWait();
        if (result.get() == buttonTypeOne) {
            return 1;
        } else if (result.get() == buttonTypeTwo) {
            return 2;
        } else if (result.get() == buttonTypeThree) {
            return 3;
        } else {
            return -1;
        }
    }
    
    public static int showPutToTheSwordChooserDialog(String title, String header,
            String message) {
        Alert alert = new Alert(AlertType.CONFIRMATION);
        alert.setTitle(title);
        alert.setHeaderText(header);
        alert.setContentText(message);
        ButtonType buttonTypeOne = new ButtonType("Defense Orders");
        ButtonType buttonTypeTwo = new ButtonType("March +1 Orders");
        ButtonType buttonTypeThree = new ButtonType("No Restrictions");
        ButtonType buttonTypeCancel = new ButtonType("Cancel",
                ButtonData.CANCEL_CLOSE);
        alert.getButtonTypes().setAll(buttonTypeOne, buttonTypeTwo, buttonTypeThree,
                buttonTypeCancel);
        Optional<ButtonType> result = alert.showAndWait();
        if (result.get() == buttonTypeOne) {
            return 1;
        } else if (result.get() == buttonTypeTwo) {
            return 2;
        } else if (result.get() == buttonTypeThree) {
            return 3;
        } else {
            return -1;
        }
    }

    public static int showRavenChoiceDialog(String title, String header,
            String messgae) {
        Alert alert = new Alert(AlertType.CONFIRMATION);
        alert.setTitle(title);
        alert.setHeaderText(header);
        alert.setContentText(messgae);
        ButtonType buttonTypeOne = new ButtonType("Edit Command");
        ButtonType buttonTypeTwo = new ButtonType("Wildling Card");
        ButtonType buttonTypeCancel = new ButtonType("Cancel",
                ButtonData.CANCEL_CLOSE);
        alert.getButtonTypes().setAll(buttonTypeOne, buttonTypeTwo,
                buttonTypeCancel);
        Optional<ButtonType> result = alert.showAndWait();
        if (result.get() == buttonTypeOne) {
            return 1;
        } else if (result.get() == buttonTypeTwo) {
            return 2;
        } else {
            return -1;
        }
    }

    public static void showPromptForName(String myName) {
        TextInputDialog dialog = new TextInputDialog();
        dialog.setTitle("Game of Thrones Board Game");
        dialog.setHeaderText("Welcome!");
        dialog.setContentText("Please enter your name:");
        // Traditional way to get the response value.
        Optional<String> result = dialog.showAndWait();
        if (result.isPresent()) {
            String enteredName = result.get();
            if (StringHelper.isNotNullOrEmpty(enteredName)) {
                SavedGameData.saveProperty(SavedGameData.MYNAME_PROPERTY,
                        enteredName);
                Player.setMyName(result.get());
                return;
            }
        }
        Platform.exit();
    }

    public static void showPanel(String panelResource, String title,
            JavaFXController controller) {
        Parent root;
        try {
            FXMLLoader fxmlLoader = new FXMLLoader(
                    MainScreenPanelController.class.getResource(panelResource));
            fxmlLoader.setController(controller);
            root = (Parent) fxmlLoader.load();
            Scene scene = new Scene(root);
            Stage stage = new Stage();
            stage.setTitle(title);
            stage.setScene(scene);
            stage.setResizable(false);
            stage.getIcons().add(new Image(JavaFXLauncher.class.getResourceAsStream(RESOURCES_LOGO_JPG)));
            stage.show();
            if (Player.getMyHouse() != null) {
                scene.getStylesheets().add(House.getStyle(Player.getMyHouse()));
            }
        } catch (IOException e) {
            Client.error(e);
        }
    }
}
