package client;

import java.util.ArrayList;
import java.util.List;

import client.model.GameContext;
import client.model.House;
import client.model.Turn;
import client.model.cards.HouseCard;
import client.model.cards.LannisterHouseCard;
import controllers.HouseCardChooserPanelController;
import controllers.MainScreenPanelController;

public class TyrionLannisterSpecialAbility extends HouseCardSpecialAbility {

    public TyrionLannisterSpecialAbility(Turn turn,
            MainScreenPanelController mainscreen, Combat combat,
            GameContext context) {
        super(turn, mainscreen, combat, context);
        // TODO Auto-generated constructor stub
    }

    private Turn getOpponentTurn() {
        Turn turn;
        if (getCombat().getAttackingHouseCard().getName()
                .equals(LannisterHouseCard.TYRION_LANNISTER)) {
            turn = new Turn(getPlayer(getCombat().getDefendingPlayerName()));
            turn.setType(Turn.TYRION_LANNISTER_OPPONENT);
            getCombat().setAttackerHouseCardPlayed(true);
        } else {
            turn = new Turn(getPlayer(getCombat().getAttackingPlayerName()));
            turn.setType(Turn.TYRION_LANNISTER_OPPONENT);
            getCombat().setDefenderHouseCardPlayed(true);
        }
        turn.setCombatId(getCombat().getId());
        return turn;
    }

    public void doNotForceOpponentToChangeCard() {
        if (getCombat().getAttackingHouseCard().getName()
                .equals(LannisterHouseCard.TYRION_LANNISTER)) {
            getCombat().setAttackerHouseCardPlayed(true);
        } else {
            getCombat().setDefenderHouseCardPlayed(true);
        }
        getCombat().resolveCombatBetweenPlayers(getMainscreencontroller());
    }

    public void forceOpponentToChangeCard() {
        Turn tyrionLannisterTurn = getOpponentTurn();
        getContext().getTurns().add(0, tyrionLannisterTurn);
        TurnHandler.saveAndDistributeGameContext(getTurn(), getContext());
        TurnHandler.UpdateUI(getMainscreencontroller());
    }

    public void changeCard() {
        String opponentHouse = getTurn().getPlayer().getHouse();
        HouseCard myHouseCard;
        if (getCombat().getAttackingPlayerName().equals(
                getTurn().getPlayer().getName())) {
            myHouseCard = getCombat().getAttackingHouseCard();
        } else {
            myHouseCard = getCombat().getDefendingHouseCard();
        }
        List houseCardHand = null;
        switch (opponentHouse) {
            case House.BARATHEON:
                houseCardHand = new ArrayList(getContext()
                        .getBaratheonHouseDeck());
                break;
            case House.GREYJOY:
                houseCardHand = new ArrayList(getContext()
                        .getGreyjoyHouseDeck());
                break;
            case House.LANNISTER:
                houseCardHand = new ArrayList(getContext()
                        .getLannisterHouseDeck());
                break;
            case House.MARTELL:
                houseCardHand = new ArrayList(getContext()
                        .getMartellHouseDeck());
                break;
            case House.STARK:
                houseCardHand = new ArrayList(getContext().getStarkHouseDeck());
                break;
            case House.TYRELL:
                houseCardHand = new ArrayList(getContext().getTyrellHouseDeck());
                break;
            default:
                break;
        }
        for (Object card : houseCardHand) {
            if (card instanceof HouseCard) {
                HouseCard houseCard = (HouseCard) card;
                if (houseCard.getName().equals(myHouseCard.getName())) {
                    houseCardHand.remove(card);
                    break;
                }
            }
        }
        JavaFXLauncher.showPanel("/forms/BaseHouseCardChooserPanel.fxml",
                "Change House Card", new HouseCardChooserPanelController(
                        houseCardHand, getMainscreencontroller(), getTurn()));
    }
}
