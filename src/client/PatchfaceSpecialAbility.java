package client;

import java.util.List;

import client.model.GameContext;
import client.model.House;
import client.model.Player;
import client.model.Turn;
import controllers.HouseCardChooserPanelController;
import controllers.MainScreenPanelController;

public class PatchfaceSpecialAbility extends HouseCardSpecialAbility {

    public PatchfaceSpecialAbility(Turn turn,
            MainScreenPanelController mainscreen, Combat combat,
            GameContext context) {
        super(turn, mainscreen, combat, context);
        // TODO Auto-generated constructor stub
    }

    public void discardCard() {
        String opponentHouse = null;
        for (Player player : getContext().getPlayers()) {
            if (isAttacker() && player.equals(getCombat().getDefendingPlayerName())) {
                opponentHouse = player.getHouse();
            } else if (!isAttacker()
                    && player.equals(getCombat().getAttackingPlayerName())) {
                opponentHouse = player.getHouse();
            }
        }
        if (opponentHouse != null) {
            List houseCardHand = null;
            switch (opponentHouse) {
                case House.BARATHEON:
                    houseCardHand = getContext().getBaratheonHouseDeck();
                    break;
                case House.GREYJOY:
                    houseCardHand = getContext().getGreyjoyHouseDeck();
                    break;
                case House.LANNISTER:
                    houseCardHand = getContext().getLannisterHouseDeck();
                    break;
                case House.MARTELL:
                    houseCardHand = getContext().getMartellHouseDeck();
                    break;
                case House.STARK:
                    houseCardHand = getContext().getStarkHouseDeck();
                    break;
                case House.TYRELL:
                    houseCardHand = getContext().getTyrellHouseDeck();
                    break;
                default:
                    break;
            }
            if (houseCardHand != null && !houseCardHand.isEmpty()) {
                JavaFXLauncher.showPanel(
                        "/forms/BaseHouseCardChooserPanel.fxml",
                        "Discard Opponent House Card",
                        new HouseCardChooserPanelController(houseCardHand,
                                getMainscreencontroller(), getTurn()));
            } else {
                Client.error(opponentHouse
                        + " deck is empty, make sure to reset deck if no cards are left after combat");
            }
        } else {
            Client.error("Cannot determine opponents house in PatchfaceSpecialAbility");
        }
    }
}
