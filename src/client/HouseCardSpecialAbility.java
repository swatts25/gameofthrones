package client;

import client.model.GameContext;
import client.model.Player;
import client.model.Turn;
import client.model.cards.HouseCard;
import controllers.MainScreenPanelController;
import controllers.WildlingCardViewerPanelController;

public class HouseCardSpecialAbility {

    private Turn turn;

    private MainScreenPanelController mainscreencontroller;

    private GameContext context;

    private Combat combat;

    public HouseCardSpecialAbility(Turn turn,
            MainScreenPanelController mainscreen, Combat combat,
            GameContext context) {
        this.turn = turn;
        this.combat = combat;
        this.mainscreencontroller = mainscreen;
        this.context = context;
    }

    public void showOpponetsCard() {
        HouseCard opponentCard;
        if (getCombat().getAttackingPlayerName().equals(getTurn().getPlayer())) {
            opponentCard = getCombat().getDefendingHouseCard();
        } else {
            opponentCard = getCombat().getAttackingHouseCard();
        }
        JavaFXLauncher.showPanel("/forms/BaseWildlingCardViewerPanel.fxml",
                "Opponents House Card",
                new WildlingCardViewerPanelController(opponentCard));
    }

    public boolean isAttacker() {
        if (turn.getPlayer().equals(combat.getAttackingPlayerName())) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * @return the turn
     */
    public Turn getTurn() {
        return turn;
    }

    /**
     * @return the mainscreencontroller
     */
    public MainScreenPanelController getMainscreencontroller() {
        return mainscreencontroller;
    }

    /**
     * @return the context
     */
    public GameContext getContext() {
        return context;
    }

    /**
     * @return the combat
     */
    public Combat getCombat() {
        return combat;
    }

    public Player getPlayer(String playerName) {
        for (Player player : context.getPlayers()) {
            if (player.getName().equalsIgnoreCase(playerName)) {
                return player;
            }
        }
        return null;
    }
}
