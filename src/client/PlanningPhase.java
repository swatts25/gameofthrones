package client;

import java.util.ArrayList;
import java.util.List;

import javafx.stage.Stage;
import client.model.GameContext;
import client.model.Order;
import client.model.Player;
import client.model.Turn;
import client.model.zones.Land;
import client.model.zones.Zone;
import controllers.MainScreenPanelController;
import controllers.OrderEditorPanelController;

public class PlanningPhase {

    private MainScreenPanelController mainscreen;

    public PlanningPhase(MainScreenPanelController mainScreenPanelController) {
        mainscreen = mainScreenPanelController;
    }

    public void submitCommands(OrderEditorPanelController commandsPanel) {
        GameContext context = GameContext.getMyGameContext();
        Turn completedTurn = context.getTurns().remove(0);
        AddNextTurns(completedTurn);
        TurnHandler.saveAndDistributeGameContext(completedTurn, context);
        TurnHandler.UpdateUI(mainscreen);
        if (commandsPanel != null) {
            Stage stage = (Stage) commandsPanel.getZonesTable().getScene()
                    .getWindow();
            stage.close();
        }
        Client.getLogger().info(completedTurn.getType() + " complete.");
    }

    public void AddNextTurns(Turn turn) {
        GameContext context = GameContext.getMyGameContext();
        if (context.getTurns().isEmpty()) {
            if (turn.getType().equals(Turn.RAVEN)) {
                Client.getLogger().info("Adding resolve raid order turns.");
                TurnHandler.addResolveTurnsBasedOnFieldoms(Order.RAID_ORDER,
                        Turn.RESOLVE_RAID_ORDER);
                Client.getLogger().info("Adding resolve march order turns.");
                TurnHandler.addResolveTurnsBasedOnFieldoms(Order.MARCH_ORDER,
                        Turn.RESOLVE_MARCH_ORDER);
                if (context.getTurns().isEmpty()) {
                    PlanningPhase.resolveConsolodatePowerOrders(context);
                    Client.getLogger().info("No march orders, skipping to westeros phase");
                    WesterosPhase newPhase = new WesterosPhase(mainscreen);
                    newPhase.drawWesterosCards();
                }
            } else if (turn.getType().equals(Turn.PLANNING_PHASE)) {
                addRavenTurnToContext();
            }
        }
    }
    
    public static void clearOrderTokens(GameContext context){
        for(Zone zone : context.getZones().values()){
            zone.setOrder(null);
        }
    }
    
    public static void resolveConsolodatePowerOrders(GameContext context) {
        List<Zone> zones = new ArrayList<Zone>(context.getZones().values());
        for (Zone zone : zones) {
            if (zone.getOrder() != null
                    && zone.getOrder().getType()
                            .equals(Order.CONSOLODATE_ORDER)) {
                if (zone instanceof Land) {
                    int influenceGain = 1;
                    influenceGain += ((Land) zone).getCrowns();
                    if (zone.getOrder().isStar()) {
                        influenceGain++;
                    }
                    List<Player> players = context.getPlayers();
                    for (Player player : players) {
                        if (player.equals(zone.getControlledBy())) {
                            player.setInfluence(player.getInfluence()
                                    + influenceGain);
                        }
                    }
                }
            }
        }
    }

    public static void addRavenTurnToContext() {
        List<Player> players = GameContext.getMyGameContext().getPlayers();
        int kingsCourt = 10;
        Player playerWithKingsCourt = null;
        for (Player player : players) {
            if (player.getKingsCourtTrack() < kingsCourt) {
                playerWithKingsCourt = player;
                kingsCourt = player.getKingsCourtTrack();
            }
        }
        // Decide if context id needs to be updated
        Turn nextTurn = new Turn(playerWithKingsCourt);
        nextTurn.setType(Turn.RAVEN);
        List<Turn> turns = GameContext.getMyGameContext().getTurns();
        turns.add(nextTurn);
    }
}
