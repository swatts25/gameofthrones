package client;

import utils.StringHelper;
import client.model.GameContext;
import client.model.Player;
import client.model.Turn;
import client.model.cards.AgeOneWesterosCard;
import client.model.cards.AgeThreeWesterosCard;
import client.model.cards.AgeTwoWesterosCard;
import client.model.cards.Deck;
import client.model.zones.Land;
import client.model.zones.Zone;
import controllers.MainScreenPanelController;

/**
 * @author swatts
 *
 */
public class WesterosPhase {

    private AgeOneWesterosCard ageOneCard;

    private AgeThreeWesterosCard ageThreeCard;

    private AgeTwoWesterosCard ageTwoCard;

    MainScreenPanelController mainscreen;

    public WesterosPhase(MainScreenPanelController controller) {
        GameContext context = GameContext.getMyGameContext();
        this.mainscreen = controller;
        removeOrderToekns(context);
        ageOneCard = context.getAgeOneDeck().get(0);
        context.setCurrentAgeOneCard(ageOneCard);
        ageTwoCard = context.getAgeTwoDeck().get(0);
        context.setCurrentAgeTwoCard(ageTwoCard);
        ageThreeCard = context.getAgeThreeDeck().get(0);
        context.setCurrentAgeThreeCard(ageThreeCard);
    }

    private void removeOrderToekns(GameContext context) {
        for(Zone zone : context.getZones().values()){
            zone.setOrder(null);
        }
        mainscreen.refreshMap(context);
    }

    public static void addInfluenceTrackBiddingTurns(GameContext context) {
        for (Player player : context.getPlayers()) {
            Turn bidTurn = new Turn(player);
            bidTurn.setType(Turn.INFLUENCE_TRACK_BIDDING);
            context.getTurns().add(bidTurn);
        }
    }

    public static void addMusterTurns(GameContext context) {
        for (Player player : context.getPlayers()) {
            Turn musterTurn = new Turn(player);
            musterTurn.setType(Turn.MUSTER);
            context.getTurns().add(musterTurn);
        }
    }

    public static void rectifySupply(GameContext context) {
        for (Player player : context.getPlayers()) {
            player.setSupplyTrack(Board.getNumSupplyBarrels(context,
                    player.getName()));
            int unitsToDestroy = Board.validateCurrentSupplyLimit(player,
                    context.getZones());
            if (unitsToDestroy > 0) {
                Turn destroyUnitsTurn = new Turn(player);
                destroyUnitsTurn.setType(Turn.RECTIFY_SUPPLY);
                context.getTurns().add(destroyUnitsTurn);
            }
        }
    }

    private void addAgeOneTurns() {
        GameContext context = GameContext.getMyGameContext();
        switch (ageOneCard.getName()) {
            case AgeOneWesterosCard.A_THRONE_OF_BLADES:
                Turn aThroneOfBladesTurn = new Turn(
                        Player.getHolderOfTheIronThrone());
                aThroneOfBladesTurn.setType(Turn.A_THRONE_OF_BLADES);
                context.getTurns().add(aThroneOfBladesTurn);
                break;
            case AgeOneWesterosCard.LAST_DAYS_OF_SUMMER:
                // Nothing happens
                break;
            case AgeOneWesterosCard.MUSTERING:
                addMusterTurns(context);
                break;
            case AgeOneWesterosCard.SUPPLY:
                rectifySupply(context);
                break;
            case AgeOneWesterosCard.WINTER_IS_COMING:
                Deck<AgeOneWesterosCard> deck = new Deck<AgeOneWesterosCard>(
                        context.getAgeOneDeck());
                deck.shuffle();
                ageOneCard = deck.seeTopCard();
                addAgeOneTurns();
                break;
            default:
                break;
        }
    }

    private void addAgeThreeTurns() {
        GameContext context = GameContext.getMyGameContext();
        switch (ageThreeCard.getName()) {
            case AgeThreeWesterosCard.FEAST_FOR_CROWS:
                context.setRestrictConsolodateOrders(true);
                break;
            case AgeThreeWesterosCard.PUT_TO_THE_SWORD:
                Turn putToTheSwordTurn = new Turn(
                        Player.getHolderOfTheValyrianBlade());
                putToTheSwordTurn.setType(Turn.PUT_TO_THE_SWORD);
                context.getTurns().add(putToTheSwordTurn);
                break;
            case AgeThreeWesterosCard.RAINS_OF_AUTUMN:
                context.setRestrictMarchPlusOneOrders(true);
                break;
            case AgeThreeWesterosCard.SEA_OF_STORMS:
                context.setRestrictRaidOrders(true);
                break;
            case AgeThreeWesterosCard.STORM_OF_SWORDS:
                context.setRestrictDefenseOrders(true);
                break;
            case AgeThreeWesterosCard.WEB_OF_LIES:
                context.setRestrictSupportOrders(true);
                break;
            case AgeThreeWesterosCard.WILDLINGS_ATTACK:
                // Not yet implemented
                break;
            default:
                break;
        }
    }

    private void addAgeTwoTurns() {
        GameContext context = GameContext.getMyGameContext();
        switch (ageTwoCard.getName()) {
            case AgeTwoWesterosCard.DARK_WINGS_DARK_WORDS:
                Turn darkWingsDarkWordsTurn = new Turn(
                        Player.getHolderOfTheRaven());
                darkWingsDarkWordsTurn.setType(Turn.DARK_WINGS_DARK_WORDS);
                context.getTurns().add(darkWingsDarkWordsTurn);
                break;
            case AgeTwoWesterosCard.LAST_DAYS_OF_SUMMER:
                // Nothing happens
                break;
            case AgeTwoWesterosCard.CLASH_OF_KINGS:
                addInfluenceTrackBiddingTurns(context);
                break;
            case AgeTwoWesterosCard.GAME_OF_THRONES:
                collectInfluenceTokens(context);
                TurnHandler.UpdateUI(mainscreen);
                break;
            case AgeTwoWesterosCard.WINTER_IS_COMING:
                Deck<AgeTwoWesterosCard> deck = new Deck<AgeTwoWesterosCard>(
                        context.getAgeTwoDeck());
                deck.shuffle();
                ageTwoCard = deck.seeTopCard();
                addAgeTwoTurns();
                break;
            default:
                break;
        }
    }

    private void addPlanningPhaseTurns(GameContext context) {
        for (Player player : context.getPlayers()) {
            Turn planningPhaseTurn = new Turn(player);
            planningPhaseTurn.setType(Turn.PLANNING_PHASE);
            context.getTurns().add(planningPhaseTurn);
        }
    }

    private void advanceWildlingCounter() {
        GameContext context = GameContext.getMyGameContext();
        if (ageOneCard.isWildlingCounter()) {
            context.setWildlingCounter(context.getWildlingCounter() + 1);
            Client.getLogger().info("Wildling counter advanced in Age One, new value: "
                    + context.getWildlingCounter());
        }
        if (ageTwoCard.isWildlingCounter()) {
            context.setWildlingCounter(context.getWildlingCounter() + 1);
            Client.getLogger().info("Wildling counter advanced in Age Two, new value: "
                    + context.getWildlingCounter());
        }
        if (ageThreeCard.isWildlingCounter()) {
            context.setWildlingCounter(context.getWildlingCounter() + 1);
            Client.getLogger().info("Wildling counter advanced in Age Three, new value: "
                    + context.getWildlingCounter());
        }
    }

    public static void collectInfluenceTokens(GameContext context) {
        for (Zone zone : context.getZones().values()) {
            if (zone instanceof Land) {
                Land land = (Land) zone;
                if (land.getCrowns() > 0
                        && StringHelper.isNotNullOrEmpty(land
                                .getControlledByName())) {
                    Player player = land.getControlledBy();
                    player.setInfluence(player.getInfluence()
                            + land.getCrowns());
                }
            }
        }
    }

    public void drawWesterosCards() {
        Client.getLogger().info("Starting Westeros Phase");
        advanceWildlingCounter();
        resetPhase();
        addAgeOneTurns();
        addAgeTwoTurns();
        addAgeThreeTurns();
        endWesterosPhase();
    }

    public void endWesterosPhase() {
        GameContext context = GameContext.getMyGameContext();
        pushTopCardsToBottom(context);
        addPlanningPhaseTurns(context);
        TurnHandler.saveAndDistributeGameContext(null, context);
        TurnHandler.UpdateUI(mainscreen);
    }

    private void pushTopCardsToBottom(GameContext context) {
        AgeOneWesterosCard card1 = context.getAgeOneDeck().remove(0);
        context.getAgeOneDeck().add(card1);
        AgeTwoWesterosCard card2 = context.getAgeTwoDeck().remove(0);
        context.getAgeTwoDeck().add(card2);
        AgeThreeWesterosCard card3 = context.getAgeThreeDeck().remove(0);
        context.getAgeThreeDeck().add(card3);
    }

    private void resetPhase() {
        GameContext context = GameContext.getMyGameContext();
        context.setRestrictConsolodateOrders(false);
        context.setRestrictDefenseOrders(false);
        context.setRestrictMarchPlusOneOrders(false);
        context.setRestrictRaidOrders(false);
        context.setRestrictSupportOrders(false);
        PlanningPhase.clearOrderTokens(context);
    }
}
