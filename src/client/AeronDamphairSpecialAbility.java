package client;

import java.util.List;

import client.model.GameContext;
import client.model.House;
import client.model.Player;
import client.model.Turn;
import controllers.HouseCardChooserPanelController;
import controllers.MainScreenPanelController;

public class AeronDamphairSpecialAbility extends HouseCardSpecialAbility {

    public AeronDamphairSpecialAbility(Turn turn,
            MainScreenPanelController mainscreen, Combat combat, GameContext context) {
        super(turn, mainscreen, combat, context);
    }

    public void changeCard() {
        if (getTurn().getPlayer().getInfluence() >= 2) {
            for (Player player : getContext().getPlayers()) {
                if (player.getName().equalsIgnoreCase(
                        getTurn().getPlayer().getName())) {
                    player.setInfluence(player.getInfluence() - 2);
                }
            }
        } else {
            JavaFXLauncher.showInformationDialog("Insufficient Funds",
                    "You do not have two influence tokens to spare");
            return;
        }
        String cardHolderHouse = getTurn().getPlayer().getHouse();
        List houseCardHand = null;
        switch (cardHolderHouse) {
            case House.BARATHEON:
                houseCardHand = getContext().getBaratheonHouseDeck();
                break;
            case House.GREYJOY:
                houseCardHand = getContext().getGreyjoyHouseDeck();
                break;
            case House.LANNISTER:
                houseCardHand = getContext().getLannisterHouseDeck();
                break;
            case House.MARTELL:
                houseCardHand = getContext().getMartellHouseDeck();
                break;
            case House.STARK:
                houseCardHand = getContext().getStarkHouseDeck();
                break;
            case House.TYRELL:
                houseCardHand = getContext().getTyrellHouseDeck();
                break;
            default:
                break;
        }
        JavaFXLauncher.showPanel("/forms/BaseHouseCardChooserPanel.fxml",
                "Change House Card", new HouseCardChooserPanelController(
                        houseCardHand, getMainscreencontroller(), getTurn()));
    }
}
