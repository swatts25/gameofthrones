package client;

import java.io.IOException;
import java.util.logging.FileHandler;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;

import controllers.MainScreenPanelController;

/**
 * @author zggis
 */
public class Client {

    private static final String LOG_FILE = "log.txt";

    private static Logger logger = Logger.getLogger(Logger.GLOBAL_LOGGER_NAME);

    public static void main(String[] args) {
        initLogger();
        Client.getLogger().info("Game Starting");
        JavaFXLauncher launcher = new JavaFXLauncher();
        launcher.show(new MainScreenPanelController(),
                "Game of Thrones Board Game");
    }

    private static void initLogger() {
        try {
            FileHandler fileTxt = new FileHandler(Client.LOG_FILE, true);
            SimpleFormatter formatterTxt = new SimpleFormatter();
            fileTxt.setFormatter(formatterTxt);
            logger.addHandler(fileTxt);
        } catch (SecurityException | IOException e) {
            e.printStackTrace();
        }
    }

    public static Logger getLogger() {
        return logger;
    }

    public static void error(Exception ex) {
        logger.log(Level.SEVERE, ex.toString(), ex);
    }

    public static void error(String s) {
        logger.log(Level.WARNING, s);
    }

    public static void info(String s) {
        logger.info(s);
    }
}
