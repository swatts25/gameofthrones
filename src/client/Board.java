package client;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import client.model.Footman;
import client.model.GameContext;
import client.model.House;
import client.model.Knight;
import client.model.MarchMovement;
import client.model.Player;
import client.model.Ship;
import client.model.Turn;
import client.model.Unit;
import client.model.zones.Land;
import client.model.zones.Sea;
import client.model.zones.Zone;

public class Board {

    public static Map<String, Zone> zones;

    public static final int[] SUPPLY_ZERO = { 2, 2 }; // 4

    public static final int[] SUPPLY_ONE = { 3, 2 }; // 5

    public static final int[] SUPPLY_TWO = { 3, 2, 2 }; // 7

    public static final int[] SUPPLY_THREE = { 3, 2, 2, 2 }; // 9

    public static final int[] SUPPLY_FOUR = { 3, 3, 2, 2 }; // 10

    public static final int[] SUPPLY_FIVE = { 4, 3, 2, 2 }; // 11

    public static final int[] SUPPLY_SIX = { 4, 3, 2, 2, 2 }; // 13;

    public static void initializeZones(List<Player> players) {
        zones = new HashMap<String, Zone>();
        // Castle Black
        Land castleBlack = new Land(Land.CASTLE_BLACK);
        castleBlack.setCastle(false);
        castleBlack.setStrongHold(false);
        castleBlack.setPort(false);
        castleBlack.setCrowns(1);
        castleBlack.setBarrels(0);
        castleBlack.addAdjacentZone(Sea.BAY_OF_ICE);
        castleBlack.addAdjacentZone(Land.WINTERFELL);
        castleBlack.addAdjacentZone(Land.KARHOLD);
        castleBlack.addAdjacentZone(Sea.THE_SHIVERING_SEA);
        zones.put(Land.CASTLE_BLACK, castleBlack);
        // Winterfell
        Land winterfell = new Land(Land.WINTERFELL);
        winterfell.setCastle(true);
        winterfell.setStrongHold(false);
        winterfell.setPort(false);
        winterfell.setCrowns(1);
        winterfell.setBarrels(1);
        winterfell.addAdjacentZone(Sea.BAY_OF_ICE);
        winterfell.addAdjacentZone(Land.CASTLE_BLACK);
        winterfell.addAdjacentZone(Land.KARHOLD);
        winterfell.addAdjacentZone(Sea.THE_SHIVERING_SEA);
        winterfell.addAdjacentZone(Land.WHITE_HARBOR);
        winterfell.addAdjacentZone(Land.MOAT_CALIN);
        winterfell.addAdjacentZone(Land.THE_STONY_SHORE);
        zones.put(Land.WINTERFELL, winterfell);
        // Karhold
        Land karhold = new Land(Land.KARHOLD);
        karhold.setCastle(false);
        karhold.setStrongHold(false);
        karhold.setPort(false);
        karhold.setCrowns(1);
        karhold.setBarrels(0);
        karhold.addAdjacentZone(Land.WINTERFELL);
        karhold.addAdjacentZone(Land.CASTLE_BLACK);
        karhold.addAdjacentZone(Sea.THE_SHIVERING_SEA);
        zones.put(Land.KARHOLD, karhold);
        // The Stony Shore
        Land theStonyShore = new Land(Land.THE_STONY_SHORE);
        theStonyShore.setCastle(false);
        theStonyShore.setStrongHold(false);
        theStonyShore.setPort(false);
        theStonyShore.setCrowns(0);
        theStonyShore.setBarrels(1);
        theStonyShore.addAdjacentZone(Land.WINTERFELL);
        theStonyShore.addAdjacentZone(Sea.BAY_OF_ICE);
        zones.put(Land.THE_STONY_SHORE, theStonyShore);
        // White Harbor
        Land whiteHarbor = new Land(Land.WHITE_HARBOR);
        whiteHarbor.setCastle(false);
        whiteHarbor.setStrongHold(true);
        whiteHarbor.setPort(true);
        whiteHarbor.setCrowns(0);
        whiteHarbor.setBarrels(0);
        whiteHarbor.addAdjacentZone(Land.WINTERFELL);
        whiteHarbor.addAdjacentZone(Sea.THE_SHIVERING_SEA);
        whiteHarbor.addAdjacentZone(Land.WIDOWS_WATCH);
        whiteHarbor.addAdjacentZone(Land.MOAT_CALIN);
        zones.put(Land.WHITE_HARBOR, whiteHarbor);
        // Widows Watch
        Land widowsWatch = new Land(Land.WIDOWS_WATCH);
        widowsWatch.setCastle(false);
        widowsWatch.setStrongHold(false);
        widowsWatch.setPort(false);
        widowsWatch.setCrowns(0);
        widowsWatch.setBarrels(1);
        widowsWatch.addAdjacentZone(Land.WHITE_HARBOR);
        widowsWatch.addAdjacentZone(Sea.THE_SHIVERING_SEA);
        zones.put(Land.WIDOWS_WATCH, widowsWatch);
        // Flint's Finger
        Land flintsFinger = new Land(Land.FLINTS_FINGER);
        flintsFinger.setCastle(false);
        flintsFinger.setStrongHold(true);
        flintsFinger.setPort(false);
        flintsFinger.setCrowns(0);
        flintsFinger.setBarrels(0);
        flintsFinger.addAdjacentZone(Land.GREYWATER_WATCH);
        flintsFinger.addAdjacentZone(Sea.BAY_OF_ICE);
        flintsFinger.addAdjacentZone(Sea.SUNSET_SEA);
        flintsFinger.addAdjacentZone(Sea.IRONMANS_BAY);
        zones.put(Land.FLINTS_FINGER, flintsFinger);
        // Greywater Watch
        Land greywaterWatch = new Land(Land.GREYWATER_WATCH);
        greywaterWatch.setCastle(false);
        greywaterWatch.setStrongHold(false);
        greywaterWatch.setPort(false);
        greywaterWatch.setCrowns(0);
        greywaterWatch.setBarrels(1);
        greywaterWatch.addAdjacentZone(Land.FLINTS_FINGER);
        greywaterWatch.addAdjacentZone(Sea.BAY_OF_ICE);
        greywaterWatch.addAdjacentZone(Sea.IRONMANS_BAY);
        greywaterWatch.addAdjacentZone(Land.MOAT_CALIN);
        greywaterWatch.addAdjacentZone(Land.SEAGARD);
        zones.put(Land.GREYWATER_WATCH, greywaterWatch);
        // Moat Calin
        Land moatCalin = new Land(Land.MOAT_CALIN);
        moatCalin.setCastle(false);
        moatCalin.setStrongHold(true);
        moatCalin.setPort(false);
        moatCalin.setCrowns(0);
        moatCalin.setBarrels(0);
        moatCalin.addAdjacentZone(Land.WINTERFELL);
        moatCalin.addAdjacentZone(Sea.THE_NARROW_SEA);
        moatCalin.addAdjacentZone(Land.GREYWATER_WATCH);
        moatCalin.addAdjacentZone(Land.SEAGARD);
        moatCalin.addAdjacentZone(Land.THE_TWINS);
        zones.put(Land.MOAT_CALIN, moatCalin);
        // Pyke
        Land pyke = new Land(Land.PYKE);
        pyke.setCastle(true);
        pyke.setStrongHold(false);
        pyke.setPort(true);
        pyke.setCrowns(1);
        pyke.setBarrels(1);
        pyke.addAdjacentZone(Sea.IRONMANS_BAY);
        zones.put(Land.PYKE, pyke);
        // Seagard
        Land seagard = new Land(Land.SEAGARD);
        seagard.setCastle(true);
        seagard.setStrongHold(false);
        seagard.setPort(false);
        seagard.setCrowns(1);
        seagard.setBarrels(1);
        seagard.addAdjacentZone(Sea.IRONMANS_BAY);
        seagard.addAdjacentZone(Land.GREYWATER_WATCH);
        seagard.addAdjacentZone(Land.MOAT_CALIN);
        seagard.addAdjacentZone(Land.THE_TWINS);
        seagard.addAdjacentZone(Land.RIVERRUN);
        zones.put(Land.SEAGARD, seagard);
        // The Twins
        Land theTwins = new Land(Land.THE_TWINS);
        theTwins.setCastle(false);
        theTwins.setStrongHold(false);
        theTwins.setPort(false);
        theTwins.setCrowns(1);
        theTwins.setBarrels(0);
        theTwins.addAdjacentZone(Sea.THE_NARROW_SEA);
        theTwins.addAdjacentZone(Land.MOAT_CALIN);
        theTwins.addAdjacentZone(Land.SEAGARD);
        theTwins.addAdjacentZone(Land.THE_MOUNTAINS_OF_THE_MOON);
        theTwins.addAdjacentZone(Land.THE_FINGERS);
        zones.put(Land.THE_TWINS, theTwins);
        // The Fingers
        Land theFingers = new Land(Land.THE_FINGERS);
        theFingers.setCastle(false);
        theFingers.setStrongHold(false);
        theFingers.setPort(false);
        theFingers.setCrowns(0);
        theFingers.setBarrels(1);
        theFingers.addAdjacentZone(Sea.THE_NARROW_SEA);
        theFingers.addAdjacentZone(Land.THE_TWINS);
        theFingers.addAdjacentZone(Land.THE_MOUNTAINS_OF_THE_MOON);
        zones.put(Land.THE_FINGERS, theFingers);
        // Lannisport
        Land lannisport = new Land(Land.LANNISPORT);
        lannisport.setCastle(true);
        lannisport.setStrongHold(false);
        lannisport.setPort(true);
        lannisport.setCrowns(0);
        lannisport.setBarrels(2);
        lannisport.addAdjacentZone(Sea.THE_GOLDEN_SOUND);
        lannisport.addAdjacentZone(Land.RIVERRUN);
        lannisport.addAdjacentZone(Land.STONEY_SEPT);
        lannisport.addAdjacentZone(Land.SEAROADMARCHES);
        zones.put(Land.LANNISPORT, lannisport);
        // Riverrun
        Land riverrun = new Land(Land.RIVERRUN);
        riverrun.setCastle(true);
        riverrun.setStrongHold(false);
        riverrun.setPort(false);
        riverrun.setCrowns(1);
        riverrun.setBarrels(1);
        riverrun.addAdjacentZone(Sea.IRONMANS_BAY);
        riverrun.addAdjacentZone(Sea.THE_GOLDEN_SOUND);
        riverrun.addAdjacentZone(Land.SEAGARD);
        riverrun.addAdjacentZone(Land.LANNISPORT);
        riverrun.addAdjacentZone(Land.STONEY_SEPT);
        riverrun.addAdjacentZone(Land.HARRENHAL);
        zones.put(Land.RIVERRUN, riverrun);
        // The Mountains of the Moon
        Land theMountainsOfTheMoon = new Land(Land.THE_MOUNTAINS_OF_THE_MOON);
        theMountainsOfTheMoon.setCastle(false);
        theMountainsOfTheMoon.setStrongHold(false);
        theMountainsOfTheMoon.setPort(false);
        theMountainsOfTheMoon.setCrowns(0);
        theMountainsOfTheMoon.setBarrels(1);
        theMountainsOfTheMoon.addAdjacentZone(Sea.THE_NARROW_SEA);
        theMountainsOfTheMoon.addAdjacentZone(Land.THE_FINGERS);
        theMountainsOfTheMoon.addAdjacentZone(Land.THE_TWINS);
        theMountainsOfTheMoon.addAdjacentZone(Land.CRACKLAW_POINT);
        theMountainsOfTheMoon.addAdjacentZone(Land.THE_EYRIE);
        zones.put(Land.THE_MOUNTAINS_OF_THE_MOON, theMountainsOfTheMoon);
        // The Eyrie
        Land theEyrie = new Land(Land.THE_EYRIE);
        theEyrie.setCastle(false);
        theEyrie.setStrongHold(true);
        theEyrie.setPort(false);
        theEyrie.setCrowns(1);
        theEyrie.setBarrels(1);
        theEyrie.addAdjacentZone(Sea.THE_NARROW_SEA);
        theEyrie.addAdjacentZone(Land.THE_MOUNTAINS_OF_THE_MOON);
        zones.put(Land.THE_EYRIE, theEyrie);
        // Searoad Marches
        Land searoadMarches = new Land(Land.SEAROADMARCHES);
        searoadMarches.setCastle(false);
        searoadMarches.setStrongHold(false);
        searoadMarches.setPort(false);
        searoadMarches.setCrowns(0);
        searoadMarches.setBarrels(1);
        searoadMarches.addAdjacentZone(Sea.THE_GOLDEN_SOUND);
        searoadMarches.addAdjacentZone(Sea.SUNSET_SEA);
        searoadMarches.addAdjacentZone(Sea.WEST_SUMMER_SEA);
        searoadMarches.addAdjacentZone(Land.LANNISPORT);
        searoadMarches.addAdjacentZone(Land.STONEY_SEPT);
        searoadMarches.addAdjacentZone(Land.BLACKWATER);
        searoadMarches.addAdjacentZone(Land.THE_REACH);
        searoadMarches.addAdjacentZone(Land.HIGHGARDEN);
        zones.put(Land.SEAROADMARCHES, searoadMarches);
        // Stoney Sept
        Land stoneySept = new Land(Land.STONEY_SEPT);
        stoneySept.setCastle(false);
        stoneySept.setStrongHold(false);
        stoneySept.setPort(false);
        stoneySept.setCrowns(1);
        stoneySept.setBarrels(0);
        stoneySept.addAdjacentZone(Land.RIVERRUN);
        stoneySept.addAdjacentZone(Land.LANNISPORT);
        stoneySept.addAdjacentZone(Land.SEAROADMARCHES);
        stoneySept.addAdjacentZone(Land.BLACKWATER);
        stoneySept.addAdjacentZone(Land.HARRENHAL);
        zones.put(Land.STONEY_SEPT, stoneySept);
        // Harrenhal
        Land harrenhal = new Land(Land.HARRENHAL);
        harrenhal.setCastle(false);
        harrenhal.setStrongHold(true);
        harrenhal.setPort(false);
        harrenhal.setCrowns(1);
        harrenhal.setBarrels(0);
        harrenhal.addAdjacentZone(Land.RIVERRUN);
        harrenhal.addAdjacentZone(Land.STONEY_SEPT);
        harrenhal.addAdjacentZone(Land.BLACKWATER);
        harrenhal.addAdjacentZone(Land.CRACKLAW_POINT);
        zones.put(Land.HARRENHAL, harrenhal);
        // Crackclaw Point
        Land crackclawPoint = new Land(Land.CRACKLAW_POINT);
        crackclawPoint.setCastle(false);
        crackclawPoint.setStrongHold(true);
        crackclawPoint.setPort(false);
        crackclawPoint.setCrowns(0);
        crackclawPoint.setBarrels(0);
        crackclawPoint.addAdjacentZone(Sea.THE_NARROW_SEA);
        crackclawPoint.addAdjacentZone(Sea.SHIPBREAKER_BAY);
        crackclawPoint.addAdjacentZone(Sea.BLACKWATER_BAY);
        crackclawPoint.addAdjacentZone(Land.THE_MOUNTAINS_OF_THE_MOON);
        crackclawPoint.addAdjacentZone(Land.HARRENHAL);
        crackclawPoint.addAdjacentZone(Land.BLACKWATER);
        crackclawPoint.addAdjacentZone(Land.KINGS_LANDING);
        zones.put(Land.CRACKLAW_POINT, crackclawPoint);
        // Dragonstone
        Land dragonstone = new Land(Land.DRAGONSTONE);
        dragonstone.setCastle(true);
        dragonstone.setStrongHold(false);
        dragonstone.setPort(true);
        dragonstone.setCrowns(1);
        dragonstone.setBarrels(1);
        dragonstone.addAdjacentZone(Sea.SHIPBREAKER_BAY);
        zones.put(Land.DRAGONSTONE, dragonstone);
        // Blackwater
        Land blackwater = new Land(Land.BLACKWATER);
        blackwater.setCastle(false);
        blackwater.setStrongHold(false);
        blackwater.setPort(false);
        blackwater.setCrowns(0);
        blackwater.setBarrels(2);
        blackwater.addAdjacentZone(Land.STONEY_SEPT);
        blackwater.addAdjacentZone(Land.SEAROADMARCHES);
        blackwater.addAdjacentZone(Land.THE_REACH);
        blackwater.addAdjacentZone(Land.KINGS_LANDING);
        blackwater.addAdjacentZone(Land.CRACKLAW_POINT);
        blackwater.addAdjacentZone(Land.HARRENHAL);
        zones.put(Land.BLACKWATER, blackwater);
        // Kings Landing
        Land kingsLanding = new Land(Land.KINGS_LANDING);
        kingsLanding.setCastle(true);
        kingsLanding.setStrongHold(false);
        kingsLanding.setPort(false);
        kingsLanding.setCrowns(2);
        kingsLanding.setBarrels(0);
        kingsLanding.addAdjacentZone(Land.CRACKLAW_POINT);
        kingsLanding.addAdjacentZone(Land.BLACKWATER);
        kingsLanding.addAdjacentZone(Land.THE_REACH);
        kingsLanding.addAdjacentZone(Land.KINGSWOOD);
        kingsLanding.addAdjacentZone(Sea.BLACKWATER_BAY);
        zones.put(Land.KINGS_LANDING, kingsLanding);
        // Highgarden
        Land highgarden = new Land(Land.HIGHGARDEN);
        highgarden.setCastle(true);
        highgarden.setStrongHold(false);
        highgarden.setPort(false);
        highgarden.setCrowns(0);
        highgarden.setBarrels(2);
        highgarden.addAdjacentZone(Land.SEAROADMARCHES);
        highgarden.addAdjacentZone(Land.THE_REACH);
        highgarden.addAdjacentZone(Land.DORNISH_MARCHES);
        highgarden.addAdjacentZone(Land.OLDTOWN);
        highgarden.addAdjacentZone(Sea.WEST_SUMMER_SEA);
        highgarden.addAdjacentZone(Sea.REDWYNE_STRAIGHTS);
        zones.put(Land.HIGHGARDEN, highgarden);
        // The Reach
        Land theReach = new Land(Land.THE_REACH);
        theReach.setCastle(false);
        theReach.setStrongHold(true);
        theReach.setPort(false);
        theReach.setCrowns(0);
        theReach.setBarrels(0);
        theReach.addAdjacentZone(Land.BLACKWATER);
        theReach.addAdjacentZone(Land.SEAROADMARCHES);
        theReach.addAdjacentZone(Land.HIGHGARDEN);
        theReach.addAdjacentZone(Land.DORNISH_MARCHES);
        theReach.addAdjacentZone(Land.THE_BONEWAY);
        theReach.addAdjacentZone(Land.KINGSWOOD);
        theReach.addAdjacentZone(Land.KINGS_LANDING);
        zones.put(Land.THE_REACH, theReach);
        // Kingswood
        Land kingswood = new Land(Land.KINGSWOOD);
        kingswood.setCastle(false);
        kingswood.setStrongHold(false);
        kingswood.setPort(false);
        kingswood.setCrowns(1);
        kingswood.setBarrels(1);
        kingswood.addAdjacentZone(Land.KINGS_LANDING);
        kingswood.addAdjacentZone(Land.THE_REACH);
        kingswood.addAdjacentZone(Land.THE_BONEWAY);
        kingswood.addAdjacentZone(Land.STORMS_END);
        kingswood.addAdjacentZone(Sea.BLACKWATER_BAY);
        kingswood.addAdjacentZone(Sea.SHIPBREAKER_BAY);
        zones.put(Land.KINGSWOOD, kingswood);
        // Oldtown
        Land oldtown = new Land(Land.OLDTOWN);
        oldtown.setCastle(true);
        oldtown.setStrongHold(false);
        oldtown.setPort(true);
        oldtown.setCrowns(0);
        oldtown.setBarrels(0);
        oldtown.addAdjacentZone(Land.HIGHGARDEN);
        oldtown.addAdjacentZone(Sea.REDWYNE_STRAIGHTS);
        oldtown.addAdjacentZone(Land.THREE_TOWERS);
        oldtown.addAdjacentZone(Land.DORNISH_MARCHES);
        zones.put(Land.OLDTOWN, oldtown);
        // Dornish Marches
        Land dornishMarches = new Land(Land.DORNISH_MARCHES);
        dornishMarches.setCastle(false);
        dornishMarches.setStrongHold(false);
        dornishMarches.setPort(false);
        dornishMarches.setCrowns(1);
        dornishMarches.setBarrels(0);
        dornishMarches.addAdjacentZone(Land.THE_REACH);
        dornishMarches.addAdjacentZone(Land.HIGHGARDEN);
        dornishMarches.addAdjacentZone(Land.OLDTOWN);
        dornishMarches.addAdjacentZone(Land.THREE_TOWERS);
        dornishMarches.addAdjacentZone(Land.PRINCES_PASS);
        dornishMarches.addAdjacentZone(Land.THE_BONEWAY);
        zones.put(Land.DORNISH_MARCHES, dornishMarches);
        // The Boneway
        Land theBoneway = new Land(Land.THE_BONEWAY);
        theBoneway.setCastle(false);
        theBoneway.setStrongHold(false);
        theBoneway.setPort(false);
        theBoneway.setCrowns(1);
        theBoneway.setBarrels(0);
        theBoneway.addAdjacentZone(Land.KINGSWOOD);
        theBoneway.addAdjacentZone(Land.THE_REACH);
        theBoneway.addAdjacentZone(Land.DORNISH_MARCHES);
        theBoneway.addAdjacentZone(Land.PRINCES_PASS);
        theBoneway.addAdjacentZone(Land.YRONWOOD);
        theBoneway.addAdjacentZone(Sea.SEA_OF_DORNE);
        theBoneway.addAdjacentZone(Land.STORMS_END);
        zones.put(Land.THE_BONEWAY, theBoneway);
        // Storms End
        Land stormsEnd = new Land(Land.STORMS_END);
        stormsEnd.setCastle(false);
        stormsEnd.setStrongHold(true);
        stormsEnd.setPort(true);
        stormsEnd.setCrowns(0);
        stormsEnd.setBarrels(0);
        stormsEnd.addAdjacentZone(Land.KINGSWOOD);
        stormsEnd.addAdjacentZone(Land.THE_BONEWAY);
        stormsEnd.addAdjacentZone(Sea.SEA_OF_DORNE);
        stormsEnd.addAdjacentZone(Sea.EAST_SUMMER_SEA);
        stormsEnd.addAdjacentZone(Sea.SHIPBREAKER_BAY);
        zones.put(Land.STORMS_END, stormsEnd);
        // The Arbor
        Land theArbor = new Land(Land.THE_ARBOR);
        theArbor.setCastle(false);
        theArbor.setStrongHold(false);
        theArbor.setPort(false);
        theArbor.setCrowns(1);
        theArbor.setBarrels(0);
        theArbor.addAdjacentZone(Sea.REDWYNE_STRAIGHTS);
        theArbor.addAdjacentZone(Sea.WEST_SUMMER_SEA);
        zones.put(Land.THE_ARBOR, theArbor);
        // Three Towers
        Land threeTowers = new Land(Land.THREE_TOWERS);
        threeTowers.setCastle(false);
        threeTowers.setStrongHold(false);
        threeTowers.setPort(false);
        threeTowers.setCrowns(0);
        threeTowers.setBarrels(1);
        threeTowers.addAdjacentZone(Land.OLDTOWN);
        threeTowers.addAdjacentZone(Sea.REDWYNE_STRAIGHTS);
        threeTowers.addAdjacentZone(Sea.WEST_SUMMER_SEA);
        threeTowers.addAdjacentZone(Land.PRINCES_PASS);
        threeTowers.addAdjacentZone(Land.DORNISH_MARCHES);
        zones.put(Land.THREE_TOWERS, threeTowers);
        // Princes Pass
        Land princesPass = new Land(Land.PRINCES_PASS);
        princesPass.setCastle(false);
        princesPass.setStrongHold(false);
        princesPass.setPort(false);
        princesPass.setCrowns(1);
        princesPass.setBarrels(1);
        princesPass.addAdjacentZone(Land.DORNISH_MARCHES);
        princesPass.addAdjacentZone(Land.THREE_TOWERS);
        princesPass.addAdjacentZone(Land.STARFALL);
        princesPass.addAdjacentZone(Land.YRONWOOD);
        princesPass.addAdjacentZone(Land.THE_BONEWAY);
        zones.put(Land.PRINCES_PASS, princesPass);
        // Starfall
        Land starfall = new Land(Land.STARFALL);
        starfall.setCastle(false);
        starfall.setStrongHold(true);
        starfall.setPort(false);
        starfall.setCrowns(0);
        starfall.setBarrels(1);
        starfall.addAdjacentZone(Land.PRINCES_PASS);
        starfall.addAdjacentZone(Sea.WEST_SUMMER_SEA);
        starfall.addAdjacentZone(Sea.EAST_SUMMER_SEA);
        starfall.addAdjacentZone(Land.SALT_SHORE);
        starfall.addAdjacentZone(Land.YRONWOOD);
        zones.put(Land.STARFALL, starfall);
        // Yronwood
        Land yronwood = new Land(Land.YRONWOOD);
        yronwood.setCastle(false);
        yronwood.setStrongHold(true);
        yronwood.setPort(false);
        yronwood.setCrowns(0);
        yronwood.setBarrels(0);
        yronwood.addAdjacentZone(Land.THE_BONEWAY);
        yronwood.addAdjacentZone(Land.PRINCES_PASS);
        yronwood.addAdjacentZone(Land.STARFALL);
        yronwood.addAdjacentZone(Land.SALT_SHORE);
        yronwood.addAdjacentZone(Sea.SEA_OF_DORNE);
        zones.put(Land.YRONWOOD, yronwood);
        // Salt Shore
        Land saltShore = new Land(Land.SALT_SHORE);
        saltShore.setCastle(false);
        saltShore.setStrongHold(false);
        saltShore.setPort(false);
        saltShore.setCrowns(0);
        saltShore.setBarrels(1);
        saltShore.addAdjacentZone(Land.YRONWOOD);
        saltShore.addAdjacentZone(Land.STARFALL);
        saltShore.addAdjacentZone(Sea.EAST_SUMMER_SEA);
        saltShore.addAdjacentZone(Land.SUNSPEAR);
        zones.put(Land.SALT_SHORE, saltShore);
        // Sunspear
        Land sunspear = new Land(Land.SUNSPEAR);
        sunspear.setCastle(true);
        sunspear.setStrongHold(false);
        sunspear.setPort(true);
        sunspear.setCrowns(1);
        sunspear.setBarrels(1);
        sunspear.addAdjacentZone(Sea.SEA_OF_DORNE);
        sunspear.addAdjacentZone(Land.YRONWOOD);
        sunspear.addAdjacentZone(Land.SALT_SHORE);
        sunspear.addAdjacentZone(Sea.EAST_SUMMER_SEA);
        zones.put(Land.SUNSPEAR, sunspear);
        // Bay of Ice
        Sea bayOfIce = new Sea(Sea.BAY_OF_ICE);
        bayOfIce.addAdjacentZone(Sea.SUNSET_SEA);
        bayOfIce.addAdjacentZone(Land.FLINTS_FINGER);
        bayOfIce.addAdjacentZone(Land.GREYWATER_WATCH);
        bayOfIce.addAdjacentZone(Land.WINTERFELL);
        bayOfIce.addAdjacentZone(Land.THE_STONY_SHORE);
        bayOfIce.addAdjacentZone(Land.CASTLE_BLACK);
        zones.put(Sea.BAY_OF_ICE, bayOfIce);
        // Sunset Sea
        Sea sunsetSea = new Sea(Sea.SUNSET_SEA);
        sunsetSea.addAdjacentZone(Sea.BAY_OF_ICE);
        sunsetSea.addAdjacentZone(Sea.WEST_SUMMER_SEA);
        sunsetSea.addAdjacentZone(Sea.THE_GOLDEN_SOUND);
        sunsetSea.addAdjacentZone(Land.SEAROADMARCHES);
        sunsetSea.addAdjacentZone(Sea.IRONMANS_BAY);
        sunsetSea.addAdjacentZone(Land.FLINTS_FINGER);
        zones.put(Sea.SUNSET_SEA, sunsetSea);
        // Ironman's Bay
        Sea ironmansBay = new Sea(Sea.IRONMANS_BAY);
        ironmansBay.addAdjacentZone(Land.FLINTS_FINGER);
        ironmansBay.addAdjacentZone(Sea.SUNSET_SEA);
        ironmansBay.addAdjacentZone(Sea.THE_GOLDEN_SOUND);
        ironmansBay.addAdjacentZone(Land.RIVERRUN);
        ironmansBay.addAdjacentZone(Land.SEAGARD);
        ironmansBay.addAdjacentZone(Land.GREYWATER_WATCH);
        ironmansBay.addAdjacentZone(Land.PYKE);
        zones.put(Sea.IRONMANS_BAY, ironmansBay);
        // The Golden Sound
        Sea theGoldenSound = new Sea(Sea.THE_GOLDEN_SOUND);
        theGoldenSound.addAdjacentZone(Sea.IRONMANS_BAY);
        theGoldenSound.addAdjacentZone(Sea.SUNSET_SEA);
        theGoldenSound.addAdjacentZone(Land.SEAROADMARCHES);
        theGoldenSound.addAdjacentZone(Land.LANNISPORT);
        theGoldenSound.addAdjacentZone(Land.RIVERRUN);
        zones.put(Sea.THE_GOLDEN_SOUND, theGoldenSound);
        // West Summer Sea
        Sea westSummerSea = new Sea(Sea.WEST_SUMMER_SEA);
        westSummerSea.addAdjacentZone(Sea.SUNSET_SEA);
        westSummerSea.addAdjacentZone(Sea.EAST_SUMMER_SEA);
        westSummerSea.addAdjacentZone(Land.STARFALL);
        westSummerSea.addAdjacentZone(Land.THREE_TOWERS);
        westSummerSea.addAdjacentZone(Sea.REDWYNE_STRAIGHTS);
        westSummerSea.addAdjacentZone(Land.HIGHGARDEN);
        westSummerSea.addAdjacentZone(Land.SEAROADMARCHES);
        westSummerSea.addAdjacentZone(Land.THE_ARBOR);
        zones.put(Sea.WEST_SUMMER_SEA, westSummerSea);
        // Redwyne Straights
        Sea redwyneStraights = new Sea(Sea.REDWYNE_STRAIGHTS);
        redwyneStraights.addAdjacentZone(Sea.WEST_SUMMER_SEA);
        redwyneStraights.addAdjacentZone(Land.THE_ARBOR);
        redwyneStraights.addAdjacentZone(Land.THREE_TOWERS);
        redwyneStraights.addAdjacentZone(Land.OLDTOWN);
        redwyneStraights.addAdjacentZone(Land.HIGHGARDEN);
        zones.put(Sea.REDWYNE_STRAIGHTS, redwyneStraights);
        // East Summer Sea
        Sea eastSummerSea = new Sea(Sea.EAST_SUMMER_SEA);
        eastSummerSea.addAdjacentZone(Land.SALT_SHORE);
        eastSummerSea.addAdjacentZone(Land.STARFALL);
        eastSummerSea.addAdjacentZone(Sea.WEST_SUMMER_SEA);
        eastSummerSea.addAdjacentZone(Sea.SHIPBREAKER_BAY);
        eastSummerSea.addAdjacentZone(Land.STORMS_END);
        eastSummerSea.addAdjacentZone(Sea.SEA_OF_DORNE);
        eastSummerSea.addAdjacentZone(Land.SUNSPEAR);
        zones.put(Sea.EAST_SUMMER_SEA, eastSummerSea);
        // Sea of Dorne
        Sea seaOfDorne = new Sea(Sea.SEA_OF_DORNE);
        seaOfDorne.addAdjacentZone(Land.STORMS_END);
        seaOfDorne.addAdjacentZone(Land.THE_BONEWAY);
        seaOfDorne.addAdjacentZone(Land.YRONWOOD);
        seaOfDorne.addAdjacentZone(Land.SUNSPEAR);
        seaOfDorne.addAdjacentZone(Sea.EAST_SUMMER_SEA);
        zones.put(Sea.SEA_OF_DORNE, seaOfDorne);
        // Shipbreaker Bay
        Sea shipbreakerBay = new Sea(Sea.SHIPBREAKER_BAY);
        shipbreakerBay.addAdjacentZone(Sea.THE_NARROW_SEA);
        shipbreakerBay.addAdjacentZone(Land.CRACKLAW_POINT);
        shipbreakerBay.addAdjacentZone(Sea.BLACKWATER_BAY);
        shipbreakerBay.addAdjacentZone(Sea.EAST_SUMMER_SEA);
        shipbreakerBay.addAdjacentZone(Land.KINGSWOOD);
        shipbreakerBay.addAdjacentZone(Land.STORMS_END);
        shipbreakerBay.addAdjacentZone(Land.DRAGONSTONE);
        zones.put(Sea.SHIPBREAKER_BAY, shipbreakerBay);
        // The Narrow Sea
        Sea theNarrowSea = new Sea(Sea.THE_NARROW_SEA);
        theNarrowSea.addAdjacentZone(Sea.THE_SHIVERING_SEA);
        theNarrowSea.addAdjacentZone(Land.WIDOWS_WATCH);
        theNarrowSea.addAdjacentZone(Land.WHITE_HARBOR);
        theNarrowSea.addAdjacentZone(Land.MOAT_CALIN);
        theNarrowSea.addAdjacentZone(Land.THE_TWINS);
        theNarrowSea.addAdjacentZone(Land.THE_FINGERS);
        theNarrowSea.addAdjacentZone(Land.THE_MOUNTAINS_OF_THE_MOON);
        theNarrowSea.addAdjacentZone(Land.THE_EYRIE);
        theNarrowSea.addAdjacentZone(Land.CRACKLAW_POINT);
        theNarrowSea.addAdjacentZone(Sea.SHIPBREAKER_BAY);
        zones.put(Sea.THE_NARROW_SEA, theNarrowSea);
        // The Shivering Sea
        Sea theShiveringSea = new Sea(Sea.THE_SHIVERING_SEA);
        theShiveringSea.addAdjacentZone(Land.CASTLE_BLACK);
        theShiveringSea.addAdjacentZone(Land.KARHOLD);
        theShiveringSea.addAdjacentZone(Land.WINTERFELL);
        theShiveringSea.addAdjacentZone(Land.WHITE_HARBOR);
        theShiveringSea.addAdjacentZone(Land.WIDOWS_WATCH);
        theShiveringSea.addAdjacentZone(Sea.THE_NARROW_SEA);
        zones.put(Sea.THE_SHIVERING_SEA, theShiveringSea);
        // Blackwater Bay
        Sea blackwaterBay = new Sea(Sea.BLACKWATER_BAY);
        blackwaterBay.addAdjacentZone(Land.CRACKLAW_POINT);
        blackwaterBay.addAdjacentZone(Land.KINGS_LANDING);
        blackwaterBay.addAdjacentZone(Land.KINGSWOOD);
        blackwaterBay.addAdjacentZone(Sea.SHIPBREAKER_BAY);
        zones.put(Sea.BLACKWATER_BAY, blackwaterBay);
        initializeNeutralPowers(players.size());
        initializeHouseUnits(players);
        initializeTracks(players);
    }

    private static void initializeHouseUnits(List<Player> players) {
        if (players.size() >= 3) {
            for (Player player : players) {
                if (player.getHouse().equals(House.STARK)) {
                    Client.getLogger().info("House Stark initialized");
                    ((Sea) zones.get(Sea.THE_SHIVERING_SEA)).addShip(new Ship(
                            player.getName()));
                    ((Sea) zones.get(Sea.THE_SHIVERING_SEA))
                            .setControlledByName(player.getName());
                    ((Land) zones.get(Land.WINTERFELL)).addKnight(new Knight(
                            player.getName()));
                    ((Land) zones.get(Land.WINTERFELL)).addFootman(new Footman(
                            player.getName()));
                    ((Land) zones.get(Land.WINTERFELL))
                            .setControlledByName(player.getName());
                    ((Land) zones.get(Land.WHITE_HARBOR))
                            .addFootman(new Footman(player.getName()));
                    ((Land) zones.get(Land.WHITE_HARBOR))
                            .setControlledByName(player.getName());
                } else if (player.getHouse().equals(House.BARATHEON)) {
                    Client.getLogger().info("House Baratheon initialized");
                    ((Sea) zones.get(Sea.SHIPBREAKER_BAY)).addShip(new Ship(
                            player.getName()));
                    ((Sea) zones.get(Sea.SHIPBREAKER_BAY)).addShip(new Ship(
                            player.getName()));
                    ((Sea) zones.get(Sea.SHIPBREAKER_BAY))
                            .setControlledByName(player.getName());
                    ((Land) zones.get(Land.DRAGONSTONE)).addKnight(new Knight(
                            player.getName()));
                    ((Land) zones.get(Land.DRAGONSTONE))
                            .addFootman(new Footman(player.getName()));
                    ((Land) zones.get(Land.DRAGONSTONE))
                            .setControlledByName(player.getName());
                    ((Land) zones.get(Land.KINGSWOOD)).addFootman(new Footman(
                            player.getName()));
                    ((Land) zones.get(Land.KINGSWOOD))
                            .setControlledByName(player.getName());
                } else if (player.getHouse().equals(House.LANNISTER)) {
                    Client.getLogger().info("House Lannister initialized");
                    ((Land) zones.get(Land.LANNISPORT)).addFootman(new Footman(
                            player.getName()));
                    ((Land) zones.get(Land.LANNISPORT)).addKnight(new Knight(
                            player.getName()));
                    ((Land) zones.get(Land.LANNISPORT))
                            .setControlledByName(player.getName());
                    ((Land) zones.get(Land.STONEY_SEPT))
                            .addFootman(new Footman(player.getName()));
                    ((Land) zones.get(Land.STONEY_SEPT))
                            .setControlledByName(player.getName());
                    ((Sea) zones.get(Sea.THE_GOLDEN_SOUND)).addShip(new Ship(
                            player.getName()));
                    ((Sea) zones.get(Sea.THE_GOLDEN_SOUND))
                            .setControlledByName(player.getName());
                } else if (player.getHouse().equals(House.GREYJOY)) {
                    Client.getLogger().info("House Greyjoy initialized");
                    ((Sea) zones.get(Sea.IRONMANS_BAY)).addShip(new Ship(player
                            .getName()));
                    ((Sea) zones.get(Sea.IRONMANS_BAY))
                            .setControlledByName(player.getName());
                    ((Land) zones.get(Land.PYKE)).addShip(new Ship(player
                            .getName()));
                    ((Land) zones.get(Land.PYKE)).addFootman(new Footman(player
                            .getName()));
                    ((Land) zones.get(Land.PYKE)).addKnight(new Knight(player
                            .getName()));
                    ((Land) zones.get(Land.PYKE)).setControlledByName(player
                            .getName());
                    ((Land) zones.get(Land.GREYWATER_WATCH))
                            .addFootman(new Footman(player.getName()));
                    ((Land) zones.get(Land.GREYWATER_WATCH))
                            .setControlledByName(player.getName());
                } else if (player.getHouse().equals(House.TYRELL)) {
                    Client.getLogger().info("House Tyrell initialized");
                    ((Sea) zones.get(Sea.REDWYNE_STRAIGHTS)).addShip(new Ship(
                            player.getName()));
                    ((Sea) zones.get(Sea.REDWYNE_STRAIGHTS))
                            .setControlledByName(player.getName());
                    ((Land) zones.get(Land.HIGHGARDEN)).addFootman(new Footman(
                            player.getName()));
                    ((Land) zones.get(Land.HIGHGARDEN)).addKnight(new Knight(
                            player.getName()));
                    ((Land) zones.get(Land.HIGHGARDEN))
                            .setControlledByName(player.getName());
                    ((Land) zones.get(Land.DORNISH_MARCHES))
                            .addFootman(new Footman(player.getName()));
                    ((Land) zones.get(Land.DORNISH_MARCHES))
                            .setControlledByName(player.getName());
                } else if (player.getHouse().equals(House.MARTELL)) {
                    Client.getLogger().info("House Martel initialized");
                    ((Sea) zones.get(Sea.SEA_OF_DORNE)).addShip(new Ship(player
                            .getName()));
                    ((Sea) zones.get(Sea.SEA_OF_DORNE))
                            .setControlledByName(player.getName());
                    ((Land) zones.get(Land.SUNSPEAR)).addFootman(new Footman(
                            player.getName()));
                    ((Land) zones.get(Land.SUNSPEAR)).addKnight(new Knight(
                            player.getName()));
                    ((Land) zones.get(Land.SUNSPEAR))
                            .setControlledByName(player.getName());
                    ((Land) zones.get(Land.SALT_SHORE)).addFootman(new Footman(
                            player.getName()));
                    ((Land) zones.get(Land.SALT_SHORE))
                            .setControlledByName(player.getName());
                }
            }
        }
    }

    private static void initializeTracks(List<Player> players) {
        if (players.size() == 3) {
            for (Player player : players) {
                switch (player.getHouse()) {
                    case House.STARK:
                        player.setIronThroneTrack(3);
                        player.setFieldomsTrack(1);
                        player.setKingsCourtTrack(2);
                        break;
                    case House.BARATHEON:
                        player.setIronThroneTrack(1);
                        player.setFieldomsTrack(2);
                        player.setKingsCourtTrack(3);
                        break;
                    case House.LANNISTER:
                        player.setIronThroneTrack(2);
                        player.setFieldomsTrack(3);
                        player.setKingsCourtTrack(1);
                        break;
                    default:
                        Client.error("Invalid player house detected when assigning tracks: "
                                + player.getHouse());
                }
            }
        } else if (players.size() == 4) {
            for (Player player : players) {
                switch (player.getHouse()) {
                    case House.STARK:
                        player.setIronThroneTrack(3);
                        player.setFieldomsTrack(2);
                        player.setKingsCourtTrack(2);
                        break;
                    case House.BARATHEON:
                        player.setIronThroneTrack(1);
                        player.setFieldomsTrack(3);
                        player.setKingsCourtTrack(3);
                        break;
                    case House.LANNISTER:
                        player.setIronThroneTrack(2);
                        player.setFieldomsTrack(4);
                        player.setKingsCourtTrack(1);
                        break;
                    case House.GREYJOY:
                        player.setIronThroneTrack(4);
                        player.setFieldomsTrack(1);
                        player.setKingsCourtTrack(4);
                        break;
                    default:
                        Client.error("Invalid player house detected when assigning tracks");
                }
            }
        } else if (players.size() == 5) {
            for (Player player : players) {
                switch (player.getHouse()) {
                    case House.STARK:
                        player.setIronThroneTrack(3);
                        player.setFieldomsTrack(3);
                        player.setKingsCourtTrack(2);
                        break;
                    case House.BARATHEON:
                        player.setIronThroneTrack(1);
                        player.setFieldomsTrack(4);
                        player.setKingsCourtTrack(3);
                        break;
                    case House.LANNISTER:
                        player.setIronThroneTrack(2);
                        player.setFieldomsTrack(5);
                        player.setKingsCourtTrack(1);
                        break;
                    case House.GREYJOY:
                        player.setIronThroneTrack(4);
                        player.setFieldomsTrack(1);
                        player.setKingsCourtTrack(5);
                        break;
                    case House.TYRELL:
                        player.setIronThroneTrack(5);
                        player.setFieldomsTrack(2);
                        player.setKingsCourtTrack(4);
                        break;
                    default:
                        Client.error("Invalid player house detected when assigning tracks");
                }
            }
        } else if (players.size() == 6) {
            for (Player player : players) {
                switch (player.getHouse()) {
                    case House.STARK:
                        player.setIronThroneTrack(3);
                        player.setFieldomsTrack(4);
                        player.setKingsCourtTrack(2);
                        break;
                    case House.BARATHEON:
                        player.setIronThroneTrack(1);
                        player.setFieldomsTrack(5);
                        player.setKingsCourtTrack(4);
                        break;
                    case House.LANNISTER:
                        player.setIronThroneTrack(2);
                        player.setFieldomsTrack(6);
                        player.setKingsCourtTrack(1);
                        break;
                    case House.GREYJOY:
                        player.setIronThroneTrack(5);
                        player.setFieldomsTrack(1);
                        player.setKingsCourtTrack(6);
                        break;
                    case House.TYRELL:
                        player.setIronThroneTrack(6);
                        player.setFieldomsTrack(2);
                        player.setKingsCourtTrack(5);
                        break;
                    case House.MARTELL:
                        player.setIronThroneTrack(4);
                        player.setFieldomsTrack(3);
                        player.setKingsCourtTrack(3);
                        break;
                    default:
                        Client.error("Invalid player house detected when assigning tracks");
                }
            }
        }
    }

    private static void initializeNeutralPowers(int numPlayers) {
        if (numPlayers == 3) {
            ((Land) zones.get(Land.CASTLE_BLACK)).setNeutralDefensePower(0);
            ((Land) zones.get(Land.KARHOLD)).setNeutralDefensePower(0);
            ((Land) zones.get(Land.WINTERFELL)).setGarrisonDefensePower(2);
            ((Land) zones.get(Land.THE_STONY_SHORE)).setNeutralDefensePower(0);
            ((Land) zones.get(Land.WHITE_HARBOR)).setNeutralDefensePower(0);
            ((Land) zones.get(Land.WIDOWS_WATCH)).setNeutralDefensePower(0);
            ((Land) zones.get(Land.FLINTS_FINGER)).setNeutralDefensePower(0);
            ((Land) zones.get(Land.GREYWATER_WATCH)).setNeutralDefensePower(0);
            ((Land) zones.get(Land.MOAT_CALIN)).setNeutralDefensePower(0);
            ((Land) zones.get(Land.PYKE)).setNeutralDefensePower(1000);
            ((Land) zones.get(Land.SEAGARD)).setNeutralDefensePower(0);
            ((Land) zones.get(Land.THE_TWINS)).setNeutralDefensePower(0);
            ((Land) zones.get(Land.THE_FINGERS)).setNeutralDefensePower(0);
            ((Land) zones.get(Land.LANNISPORT)).setGarrisonDefensePower(2);
            ((Land) zones.get(Land.RIVERRUN)).setNeutralDefensePower(0);
            ((Land) zones.get(Land.THE_MOUNTAINS_OF_THE_MOON))
                    .setNeutralDefensePower(0);
            ((Land) zones.get(Land.THE_EYRIE)).setNeutralDefensePower(6);
            ((Land) zones.get(Land.SEAROADMARCHES)).setNeutralDefensePower(0);
            ((Land) zones.get(Land.STONEY_SEPT)).setNeutralDefensePower(0);
            ((Land) zones.get(Land.HARRENHAL)).setNeutralDefensePower(0);
            ((Land) zones.get(Land.CRACKLAW_POINT)).setNeutralDefensePower(0);
            ((Land) zones.get(Land.DRAGONSTONE)).setGarrisonDefensePower(2);
            ((Land) zones.get(Land.BLACKWATER)).setNeutralDefensePower(0);
            ((Land) zones.get(Land.KINGS_LANDING)).setNeutralDefensePower(5);
            ((Land) zones.get(Land.HIGHGARDEN)).setNeutralDefensePower(1000);
            ((Land) zones.get(Land.THE_REACH)).setNeutralDefensePower(0);
            ((Land) zones.get(Land.KINGSWOOD)).setNeutralDefensePower(0);
            ((Land) zones.get(Land.OLDTOWN)).setNeutralDefensePower(1000);
            ((Land) zones.get(Land.DORNISH_MARCHES))
                    .setNeutralDefensePower(1000);
            ((Land) zones.get(Land.THE_BONEWAY)).setNeutralDefensePower(1000);
            ((Land) zones.get(Land.STORMS_END)).setNeutralDefensePower(3);
            ((Land) zones.get(Land.THE_ARBOR)).setNeutralDefensePower(0);
            ((Land) zones.get(Land.THREE_TOWERS)).setNeutralDefensePower(1000);
            ((Land) zones.get(Land.PRINCES_PASS)).setNeutralDefensePower(1000);
            ((Land) zones.get(Land.STARFALL)).setNeutralDefensePower(1000);
            ((Land) zones.get(Land.YRONWOOD)).setNeutralDefensePower(1000);
            ((Land) zones.get(Land.SALT_SHORE)).setNeutralDefensePower(1000);
            ((Land) zones.get(Land.SUNSPEAR)).setNeutralDefensePower(1000);
        }
        if (numPlayers == 4) {
            ((Land) zones.get(Land.CASTLE_BLACK)).setNeutralDefensePower(0);
            ((Land) zones.get(Land.KARHOLD)).setNeutralDefensePower(0);
            ((Land) zones.get(Land.WINTERFELL)).setGarrisonDefensePower(2);
            ((Land) zones.get(Land.THE_STONY_SHORE)).setNeutralDefensePower(0);
            ((Land) zones.get(Land.WHITE_HARBOR)).setNeutralDefensePower(0);
            ((Land) zones.get(Land.WIDOWS_WATCH)).setNeutralDefensePower(0);
            ((Land) zones.get(Land.FLINTS_FINGER)).setNeutralDefensePower(0);
            ((Land) zones.get(Land.GREYWATER_WATCH)).setNeutralDefensePower(0);
            ((Land) zones.get(Land.MOAT_CALIN)).setNeutralDefensePower(0);
            ((Land) zones.get(Land.PYKE)).setGarrisonDefensePower(2);
            ((Land) zones.get(Land.SEAGARD)).setNeutralDefensePower(0);
            ((Land) zones.get(Land.THE_TWINS)).setNeutralDefensePower(0);
            ((Land) zones.get(Land.THE_FINGERS)).setNeutralDefensePower(0);
            ((Land) zones.get(Land.LANNISPORT)).setGarrisonDefensePower(2);
            ((Land) zones.get(Land.RIVERRUN)).setNeutralDefensePower(0);
            ((Land) zones.get(Land.THE_MOUNTAINS_OF_THE_MOON))
                    .setNeutralDefensePower(0);
            ((Land) zones.get(Land.THE_EYRIE)).setNeutralDefensePower(6);
            ((Land) zones.get(Land.SEAROADMARCHES)).setNeutralDefensePower(0);
            ((Land) zones.get(Land.STONEY_SEPT)).setNeutralDefensePower(0);
            ((Land) zones.get(Land.HARRENHAL)).setNeutralDefensePower(0);
            ((Land) zones.get(Land.CRACKLAW_POINT)).setNeutralDefensePower(0);
            ((Land) zones.get(Land.DRAGONSTONE)).setGarrisonDefensePower(2);
            ((Land) zones.get(Land.BLACKWATER)).setNeutralDefensePower(0);
            ((Land) zones.get(Land.KINGS_LANDING)).setNeutralDefensePower(5);
            ((Land) zones.get(Land.HIGHGARDEN)).setNeutralDefensePower(2);
            ((Land) zones.get(Land.THE_REACH)).setNeutralDefensePower(0);
            ((Land) zones.get(Land.KINGSWOOD)).setNeutralDefensePower(0);
            ((Land) zones.get(Land.OLDTOWN)).setNeutralDefensePower(4);
            ((Land) zones.get(Land.DORNISH_MARCHES)).setNeutralDefensePower(3);
            ((Land) zones.get(Land.THE_BONEWAY)).setNeutralDefensePower(3);
            ((Land) zones.get(Land.STORMS_END)).setNeutralDefensePower(4);
            ((Land) zones.get(Land.THE_ARBOR)).setNeutralDefensePower(0);
            ((Land) zones.get(Land.THREE_TOWERS)).setNeutralDefensePower(3);
            ((Land) zones.get(Land.PRINCES_PASS)).setNeutralDefensePower(3);
            ((Land) zones.get(Land.STARFALL)).setNeutralDefensePower(3);
            ((Land) zones.get(Land.YRONWOOD)).setNeutralDefensePower(3);
            ((Land) zones.get(Land.SALT_SHORE)).setNeutralDefensePower(3);
            ((Land) zones.get(Land.SUNSPEAR)).setNeutralDefensePower(5);
        }
        if (numPlayers == 5) {
            ((Land) zones.get(Land.CASTLE_BLACK)).setNeutralDefensePower(0);
            ((Land) zones.get(Land.KARHOLD)).setNeutralDefensePower(0);
            ((Land) zones.get(Land.WINTERFELL)).setGarrisonDefensePower(2);
            ((Land) zones.get(Land.THE_STONY_SHORE)).setNeutralDefensePower(0);
            ((Land) zones.get(Land.WHITE_HARBOR)).setNeutralDefensePower(0);
            ((Land) zones.get(Land.WIDOWS_WATCH)).setNeutralDefensePower(0);
            ((Land) zones.get(Land.FLINTS_FINGER)).setNeutralDefensePower(0);
            ((Land) zones.get(Land.GREYWATER_WATCH)).setNeutralDefensePower(0);
            ((Land) zones.get(Land.MOAT_CALIN)).setNeutralDefensePower(0);
            ((Land) zones.get(Land.PYKE)).setGarrisonDefensePower(2);
            ((Land) zones.get(Land.SEAGARD)).setNeutralDefensePower(0);
            ((Land) zones.get(Land.THE_TWINS)).setNeutralDefensePower(0);
            ((Land) zones.get(Land.THE_FINGERS)).setNeutralDefensePower(0);
            ((Land) zones.get(Land.LANNISPORT)).setGarrisonDefensePower(2);
            ((Land) zones.get(Land.RIVERRUN)).setNeutralDefensePower(0);
            ((Land) zones.get(Land.THE_MOUNTAINS_OF_THE_MOON))
                    .setNeutralDefensePower(0);
            ((Land) zones.get(Land.THE_EYRIE)).setNeutralDefensePower(6);
            ((Land) zones.get(Land.SEAROADMARCHES)).setNeutralDefensePower(0);
            ((Land) zones.get(Land.STONEY_SEPT)).setNeutralDefensePower(0);
            ((Land) zones.get(Land.HARRENHAL)).setNeutralDefensePower(0);
            ((Land) zones.get(Land.CRACKLAW_POINT)).setNeutralDefensePower(0);
            ((Land) zones.get(Land.DRAGONSTONE)).setGarrisonDefensePower(2);
            ((Land) zones.get(Land.BLACKWATER)).setNeutralDefensePower(0);
            ((Land) zones.get(Land.KINGS_LANDING)).setNeutralDefensePower(5);
            ((Land) zones.get(Land.HIGHGARDEN)).setGarrisonDefensePower(2);
            ((Land) zones.get(Land.THE_REACH)).setNeutralDefensePower(0);
            ((Land) zones.get(Land.KINGSWOOD)).setNeutralDefensePower(0);
            ((Land) zones.get(Land.OLDTOWN)).setNeutralDefensePower(0);
            ((Land) zones.get(Land.DORNISH_MARCHES)).setNeutralDefensePower(0);
            ((Land) zones.get(Land.THE_BONEWAY)).setNeutralDefensePower(3);
            ((Land) zones.get(Land.STORMS_END)).setNeutralDefensePower(0);
            ((Land) zones.get(Land.THE_ARBOR)).setNeutralDefensePower(0);
            ((Land) zones.get(Land.THREE_TOWERS)).setNeutralDefensePower(3);
            ((Land) zones.get(Land.PRINCES_PASS)).setNeutralDefensePower(3);
            ((Land) zones.get(Land.STARFALL)).setNeutralDefensePower(3);
            ((Land) zones.get(Land.YRONWOOD)).setNeutralDefensePower(3);
            ((Land) zones.get(Land.SALT_SHORE)).setNeutralDefensePower(3);
            ((Land) zones.get(Land.SUNSPEAR)).setNeutralDefensePower(5);
        }
        if (numPlayers == 6) {
            ((Land) zones.get(Land.CASTLE_BLACK)).setNeutralDefensePower(0);
            ((Land) zones.get(Land.KARHOLD)).setNeutralDefensePower(0);
            ((Land) zones.get(Land.WINTERFELL)).setGarrisonDefensePower(2);
            ((Land) zones.get(Land.THE_STONY_SHORE)).setNeutralDefensePower(0);
            ((Land) zones.get(Land.WHITE_HARBOR)).setNeutralDefensePower(0);
            ((Land) zones.get(Land.WIDOWS_WATCH)).setNeutralDefensePower(0);
            ((Land) zones.get(Land.FLINTS_FINGER)).setNeutralDefensePower(0);
            ((Land) zones.get(Land.GREYWATER_WATCH)).setNeutralDefensePower(0);
            ((Land) zones.get(Land.MOAT_CALIN)).setNeutralDefensePower(0);
            ((Land) zones.get(Land.PYKE)).setGarrisonDefensePower(2);
            ((Land) zones.get(Land.SEAGARD)).setNeutralDefensePower(0);
            ((Land) zones.get(Land.THE_TWINS)).setNeutralDefensePower(0);
            ((Land) zones.get(Land.THE_FINGERS)).setNeutralDefensePower(0);
            ((Land) zones.get(Land.LANNISPORT)).setNeutralDefensePower(2);
            ((Land) zones.get(Land.RIVERRUN)).setNeutralDefensePower(0);
            ((Land) zones.get(Land.THE_MOUNTAINS_OF_THE_MOON))
                    .setNeutralDefensePower(0);
            ((Land) zones.get(Land.THE_EYRIE)).setNeutralDefensePower(6);
            ((Land) zones.get(Land.SEAROADMARCHES)).setNeutralDefensePower(0);
            ((Land) zones.get(Land.STONEY_SEPT)).setNeutralDefensePower(0);
            ((Land) zones.get(Land.HARRENHAL)).setNeutralDefensePower(0);
            ((Land) zones.get(Land.CRACKLAW_POINT)).setNeutralDefensePower(0);
            ((Land) zones.get(Land.DRAGONSTONE)).setNeutralDefensePower(2);
            ((Land) zones.get(Land.BLACKWATER)).setNeutralDefensePower(0);
            ((Land) zones.get(Land.KINGS_LANDING)).setNeutralDefensePower(5);
            ((Land) zones.get(Land.HIGHGARDEN)).setGarrisonDefensePower(2);
            ((Land) zones.get(Land.THE_REACH)).setNeutralDefensePower(0);
            ((Land) zones.get(Land.KINGSWOOD)).setNeutralDefensePower(0);
            ((Land) zones.get(Land.OLDTOWN)).setNeutralDefensePower(0);
            ((Land) zones.get(Land.DORNISH_MARCHES)).setNeutralDefensePower(0);
            ((Land) zones.get(Land.THE_BONEWAY)).setNeutralDefensePower(0);
            ((Land) zones.get(Land.STORMS_END)).setNeutralDefensePower(0);
            ((Land) zones.get(Land.THE_ARBOR)).setNeutralDefensePower(0);
            ((Land) zones.get(Land.THREE_TOWERS)).setNeutralDefensePower(0);
            ((Land) zones.get(Land.PRINCES_PASS)).setNeutralDefensePower(0);
            ((Land) zones.get(Land.STARFALL)).setNeutralDefensePower(0);
            ((Land) zones.get(Land.YRONWOOD)).setNeutralDefensePower(0);
            ((Land) zones.get(Land.SALT_SHORE)).setNeutralDefensePower(0);
            ((Land) zones.get(Land.SUNSPEAR)).setGarrisonDefensePower(2);
        }
    }

    public static void revivieRoutedUnits(GameContext context) {
        for (Zone zone : context.getZones().values()) {
            for (Unit unit : zone.getUnits()) {
                if (unit.isRouted()) {
                    Client.getLogger()
                            .info("Reviveing Unit: " + unit.getName());
                    unit.setRouted(false);
                }
            }
        }
    }

    public static boolean allMarchOrdersResolved(GameContext context) {
        for (Turn turn : context.getTurns()) {
            if (turn.getType().equals(Turn.RESOLVE_MARCH_ORDER)) {
                return false;
            }
        }
        return true;
    }

    public static boolean remainingCombats(GameContext context) {
        for (Turn turns : context.getTurns()) {
            if (turns.getType().equals(Turn.CHOOSE_HOUSE_CARD)) {
                return true;
            }
        }
        return false;
    }

    public static int validateCurrentSupplyLimit(Player marchingPlayer,
            Map<String, Zone> zones) {
        int supply[];
        switch (marchingPlayer.getSupplyTrack()) {
            case 0:
                supply = SUPPLY_ZERO.clone();
                break;
            case 1:
                supply = SUPPLY_ONE.clone();
                break;
            case 2:
                supply = SUPPLY_TWO.clone();
                break;
            case 3:
                supply = SUPPLY_THREE.clone();
                break;
            case 4:
                supply = SUPPLY_FOUR.clone();
                break;
            case 5:
                supply = SUPPLY_FIVE.clone();
                break;
            case 6:
                supply = SUPPLY_SIX.clone();
                break;
            default:
                Client.error("Invalid supply track for "
                        + marchingPlayer.getName() + " "
                        + marchingPlayer.getSupplyTrack());
                return 0;
        }
        List<String> zonesValidated = new ArrayList<String>();
        List<String> portsValidated = new ArrayList<String>();
        for (Zone zone : zones.values()) {
            if (!zonesValidated.contains(zone.getName())) {
                int numUnitsInZone = getNumUnitsInZone(zones, marchingPlayer,
                        zone.getName());
                if (numUnitsInZone > 1) {
                    boolean supplyFlagFound = false;
                    for (int i = 0; i < supply.length; i++) {
                        if (supply[i] == numUnitsInZone) {
                            supply[i] = 0;
                            supplyFlagFound = true;
                            break;
                        }
                    }
                    if (!supplyFlagFound) {
                        return (numUnitsInZone - getLargestRemainingSupplyFlag(supply));
                    }
                    zonesValidated.add(zone.getName());
                }
            }
            if (!portsValidated.contains(zone.getName())) {
                if (zone instanceof Land && ((Land) zone).isPort()) {
                    int numShipsInPort = getNumShipsInPort(zones,
                            marchingPlayer, zone.getName());
                    if (numShipsInPort > 1) {
                        boolean supplyFlagFound = false;
                        for (int i = 0; i < supply.length; i++) {
                            if (supply[i] == numShipsInPort) {
                                supply[i] = 0;
                                supplyFlagFound = true;
                                break;
                            }
                        }
                        if (!supplyFlagFound) {
                            return (numShipsInPort - getLargestRemainingSupplyFlag(supply));
                        }
                        portsValidated.add(zone.getName());
                    }
                }
            }
        }
        return 0;
    }

    public static int getLargestRemainingSupplyFlag(int[] supply) {
        int largestSupplyFlag = 0;
        int index = 0;
        for (int i = 0; i < supply.length; i++) {
            if (supply[i] >= largestSupplyFlag) {
                largestSupplyFlag = supply[i];
                index = i;
            }
        }
        supply[index] = 0;
        return largestSupplyFlag;
    }

    public static int getNumUnitsInZone(Map<String, Zone> zones, Player player,
            String zoneName) {
        return getNumAclamatedUnitsInZone(zones, player, zoneName)
                + getNumMarchingUnitsInZone(zones, player, zoneName);
    }

    public static int getNumShipsInPort(Map<String, Zone> zones, Player player,
            String zoneName) {
        int result = 0;
        Zone zone = zones.get(zoneName);
        if (zone instanceof Land && ((Land) zone).isPort()) {
            result += ((Land) zone).getShips().size();
        }
        return result;
    }

    private static int getNumAclamatedUnitsInZone(Map<String, Zone> zones,
            Player player, String zoneName) {
        int result = 0;
        Zone zone = zones.get(zoneName);
        List<Unit> units = zone.getUnits();
        for (Unit unit : units) {
            if (unit.getPlayerName().equals(player.getName())) {
                // Do not count ships in port as part of supply, ports are
                // separate zones
                if (!(zone instanceof Land && ((Land) zone).isPort() && unit instanceof Ship)) {
                    result++;
                }
            }
        }
        return result;
    }

    private static int getNumMarchingUnitsInZone(Map<String, Zone> zones,
            Player player, String zoneName) {
        int result = 0;
        Zone zone = zones.get(zoneName);
        List<MarchMovement> zoneMarches = zone.getMarches();
        for (MarchMovement marchMovement : zoneMarches) {
            Unit unit = marchMovement.getUnit();
            if (unit.getPlayerName().equalsIgnoreCase(player.getName())) {
                // Do not count ships in port as part of supply, ports are
                // separate zones
                if (!(zone instanceof Land && ((Land) zone).isPort() && unit instanceof Ship)) {
                    result++;
                }
            }
        }
        return result;
    }

    public static int getNumSupplyBarrels(GameContext context, String playerName) {
        int retVal = 0;
        for (Zone zone : context.getZones().values()) {
            if (zone.getControlledByName() != null
                    && zone.getControlledByName().equalsIgnoreCase(playerName)
                    && zone instanceof Land) {
                retVal += ((Land) zone).getBarrels();
            }
        }
        return retVal;
    }

    public static void endGame(Player victor) {
        JavaFXLauncher
                .showInformationDialog(
                        "New King",
                        victor.getName()
                                + " has captured 7 castles or strongholds and has won the game. All hail the new King, "
                                + victor.getName()
                                + " "
                                + Player.getPlayer(victor.getId()).getHouse()
                                + " King of the first men, Lord of the Seven Kingdoms, and Protector of the Realm.");
        GameContext.getMyGameContext().getTurns().clear();
        Turn turn = new Turn(victor);
        turn.setType(Turn.GAME_OVER);
        GameContext.getMyGameContext().getTurns().add(turn);
        TurnHandler.saveAndDistributeGameContext(null,
                GameContext.getMyGameContext());
    }
}
