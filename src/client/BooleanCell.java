package client;

import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ContentDisplay;
import javafx.scene.control.TableCell;
import client.model.Knight;
import client.model.Ship;
import client.model.SiegeEngine;
import client.model.Unit;
import controllers.JavaFXController;
import controllers.MusterPanelController;

public class BooleanCell extends TableCell<Unit, Boolean> {

    private CheckBox checkBox;

    private JavaFXController controller;

    public BooleanCell(JavaFXController controller) {
        this.controller = controller;
        checkBox = new CheckBox();
        // checkBox.setDisable(true);
        checkBox.selectedProperty().addListener(new ChangeListener<Boolean>() {

            public void changed(ObservableValue<? extends Boolean> observable,
                    Boolean oldValue, Boolean newValue) {
                if (getTableView().getItems().size() > getTableRow()
                        .getIndex()) {
                    Unit unit = getTableView().getItems()
                            .get(getTableRow().getIndex());
                    if (isEditing())
                        commitEdit(newValue == null ? false : newValue);
                    if (unit != null) {
                        controller.markUnit(unit, newValue);
                    }
                }
            }
        });
        this.setGraphic(checkBox);
        this.setContentDisplay(ContentDisplay.GRAPHIC_ONLY);
        this.setEditable(true);
        checkBox.setVisible(false);
    }

    /*
     * (non-Javadoc)
     * @see javafx.scene.control.IndexedCell#updateIndex(int)
     */
    @Override
    public void updateIndex(int rowIndex) {
        if (rowIndex >= 0 && rowIndex < getTableView().getItems().size()) {
            Unit unit = getTableView().getItems().get(rowIndex);
            checkBox.setVisible(true);
            if (controller instanceof MusterPanelController
                    && (unit instanceof Knight || unit instanceof Ship
                            || unit instanceof SiegeEngine)) {
                checkBox.setVisible(false);
            }
        }else{
            checkBox.setVisible(false);
        }
    }

    @Override
    public void startEdit() {
        super.startEdit();
        if (isEmpty()) {
            return;
        }
        checkBox.requestFocus();
    }

    @Override
    public void cancelEdit() {
        super.cancelEdit();
    }

    public void commitEdit(Boolean value) {
        super.commitEdit(value);
    }

    @Override
    public void updateItem(Boolean item, boolean empty) {
        super.updateItem(item, empty);
        if (!isEmpty() && item != null) {
            checkBox.setSelected(item);
        }
    }
}
