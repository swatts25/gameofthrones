package client;

import java.util.List;

import client.model.GameContext;
import client.model.Player;
import client.model.Turn;
import controllers.MainScreenPanelController;

public class DoranMartellSpecialAbility extends HouseCardSpecialAbility {

    public DoranMartellSpecialAbility(Turn turn,
            MainScreenPanelController mainscreen, Combat combat,
            GameContext context) {
        super(turn, mainscreen, combat, context);
    }

    public boolean changeOpponentsTracks() {
        List<Player> allPlayers = getContext().getPlayers();
        int opponentTrack;
        Player opponent;
        if (isAttacker()) {
            opponent = getPlayer(getCombat().getDefendingPlayerName());
        } else {
            opponent = getPlayer(getCombat().getAttackingPlayerName());
        }
        int choice = JavaFXLauncher.showTrackChooserDialog(
                "Destroy Opponent Track", "Doran Martell",
                "Choose the game track you would like to reduce your opponent in");
        if (choice == 1) {
            // Iron Throne
            opponentTrack = opponent.getIronThroneTrack();
            for (Player player : allPlayers) {
                if (player.getIronThroneTrack() > opponentTrack) {
                    player.setIronThroneTrack(player.getIronThroneTrack() - 1);
                }
            }
            opponent.setIronThroneTrack(allPlayers.size());
            return true;
        } else if (choice == 2) {
            // fieldoms
            opponentTrack = opponent.getFieldomsTrack();
            for (Player player : allPlayers) {
                if (player.getFieldomsTrack() > opponentTrack) {
                    player.setFieldomsTrack(player.getFieldomsTrack() - 1);
                }
            }
            opponent.setFieldomsTrack(allPlayers.size());
            return true;
        } else if (choice == 3) {
            // kings court
            opponentTrack = opponent.getKingsCourtTrack();
            for (Player player : allPlayers) {
                if (player.getKingsCourtTrack() > opponentTrack) {
                    player.setKingsCourtTrack(player.getKingsCourtTrack() - 1);
                }
            }
            opponent.setKingsCourtTrack(allPlayers.size());
            return true;
        }
        // cancel
        return false;
    }
}
