package client.model;

public class Knight extends Unit {

    @Deprecated
    public Knight() {
        // used by marshaller
    }

    public Knight(String player) {
        setPlayerName(player);
        setId(GameContext.getMyGameContext().getUnitCount());
    }

    @Override
    public String getName() {
        return Player.getPlayer(getPlayerName()).getHouse() + " Knight "
                + getId();
    }
}
