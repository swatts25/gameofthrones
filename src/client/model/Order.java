package client.model;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlRootElement;

import utils.StringHelper;

@XmlRootElement
public class Order {

    public static final String MARCH_ORDER = "March";

    public static final String SUPPORT_ORDER = "Support";

    public static final String DEFENSE_ORDER = "Defense";

    public static final String CONSOLODATE_ORDER = "Consolodate";

    public static final String RAID_ORDER = "Raid";

    public static final String NONE = "(None)";

    public static final Order NONE_ORDER = new Order(Order.NONE, 0);

    private String type;

    private int bonus;

    private boolean star;

    public Order() {
        bonus = 0;
    }

    public Order(String type, int bonus) {
        this.bonus = bonus;
        this.type = type;
    }

    public Order(String type, int bonus, boolean star) {
        this.bonus = bonus;
        this.type = type;
        this.star = star;
    }

    public String getName() {
        if (StringHelper.isNotNullOrEmpty(type)) {
            if (type.equals(RAID_ORDER)) {
                if (star) {
                    return type + " (*)";
                } else {
                    return type;
                }
            }
            if (bonus < 0) {
                return type + " (" + bonus + ")";
            }
            if (bonus > 0) {
                return type + " (+" + bonus + ")";
            }
            if (type.equals(NONE)) {
                return type;
            }
            return type + " (0)";
        } else {
            return "";
        }
    }

    /**
     * @return the type
     */
    public String getType() {
        return type;
    }

    /**
     * @param type
     *            the type to set
     */
    public void setType(String type) {
        this.type = type;
    }

    /**
     * @return the bonus
     */
    public int getBonus() {
        return bonus;
    }

    /**
     * @param bonus
     *            the bonus to set
     */
    public void setBonus(int bonus) {
        this.bonus = bonus;
    }

    /*
     * (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return getName();
    }

    public boolean isStar() {
        return Boolean.TRUE.equals(star);
    }

    public void setStar(boolean star) {
        this.star = star;
    }

    public static List<Order> getOrders() {
        List<Order> orders = new ArrayList<Order>();
        orders.add(new Order(Order.MARCH_ORDER, 1, true));
        orders.add(new Order(Order.MARCH_ORDER, 0));
        orders.add(new Order(Order.MARCH_ORDER, -1));
        orders.add(new Order(Order.CONSOLODATE_ORDER, 1, true));
        orders.add(new Order(Order.CONSOLODATE_ORDER, 0));
        orders.add(new Order(Order.CONSOLODATE_ORDER, 0));
        orders.add(new Order(Order.SUPPORT_ORDER, 1, true));
        orders.add(new Order(Order.SUPPORT_ORDER, 0));
        orders.add(new Order(Order.SUPPORT_ORDER, 0));
        orders.add(new Order(Order.DEFENSE_ORDER, 2, true));
        orders.add(new Order(Order.DEFENSE_ORDER, 1));
        orders.add(new Order(Order.DEFENSE_ORDER, 1));
        orders.add(new Order(Order.RAID_ORDER, 0, true));
        orders.add(new Order(Order.RAID_ORDER, 0));
        orders.add(new Order(Order.RAID_ORDER, 0));
        return orders;
    }

    public static List<Order> getUniqueAvailableOrders(GameContext context) {
        List<Order> orders = new ArrayList<Order>();
        if (!context.isRestrictMarchPlusOneOrders()) {
            orders.add(new Order(Order.MARCH_ORDER, 1, true));
        }
        orders.add(new Order(Order.MARCH_ORDER, 0));
        orders.add(new Order(Order.MARCH_ORDER, -1));
        if (!context.isRestrictConsolodateOrders()) {
            orders.add(new Order(Order.CONSOLODATE_ORDER, 1, true));
            orders.add(new Order(Order.CONSOLODATE_ORDER, 0));
        }
        if (!context.isRestrictSupportOrders()) {
            orders.add(new Order(Order.SUPPORT_ORDER, 1));
            orders.add(new Order(Order.SUPPORT_ORDER, 0));
        }
        if (!context.isRestrictDefenseOrders()) {
            orders.add(new Order(Order.DEFENSE_ORDER, 2, true));
            orders.add(new Order(Order.DEFENSE_ORDER, 1));
        }
        if (!context.isRestrictRaidOrders()) {
            orders.add(new Order(Order.RAID_ORDER, 0, true));
            orders.add(new Order(Order.RAID_ORDER, 0));
        }
        return orders;
    }

    public static List<String> getOrderNames() {
        List<String> orders = new ArrayList<String>();
        orders.add(new Order(Order.MARCH_ORDER, 1, true).getName());
        orders.add(new Order(Order.MARCH_ORDER, 0).getName());
        orders.add(new Order(Order.MARCH_ORDER, -1).getName());
        orders.add(new Order(Order.CONSOLODATE_ORDER, 1, true).getName());
        orders.add(new Order(Order.CONSOLODATE_ORDER, 0).getName());
        orders.add(new Order(Order.CONSOLODATE_ORDER, 0).getName());
        orders.add(new Order(Order.SUPPORT_ORDER, 1).getName());
        orders.add(new Order(Order.SUPPORT_ORDER, 0).getName());
        orders.add(new Order(Order.SUPPORT_ORDER, 0).getName());
        orders.add(new Order(Order.DEFENSE_ORDER, 2, true).getName());
        orders.add(new Order(Order.DEFENSE_ORDER, 1).getName());
        orders.add(new Order(Order.DEFENSE_ORDER, 1).getName());
        orders.add(new Order(Order.RAID_ORDER, 0, true).getName());
        orders.add(new Order(Order.RAID_ORDER, 0).getName());
        orders.add(new Order(Order.RAID_ORDER, 0).getName());
        return orders;
    }
}
