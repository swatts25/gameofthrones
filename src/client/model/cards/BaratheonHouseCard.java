package client.model.cards;

import java.util.ArrayList;
import java.util.List;

import javafx.scene.image.Image;
import client.Client;

public class BaratheonHouseCard extends HouseCard {
	public static final String RENLY_BARATHEON = "Renly Baratheon";
	public static final String STANNIS_BARATHEON = "Stannis Baratheon";
	public static final String SER_DAVOS_SEAWORTH = "Ser Davos Seaworth";
	public static final String BRIENE_OF_TARTH = "Briene of Tarth";
	public static final String MELISANDRE = "Melisandre";
	public static final String PATCHFACE = "Patchface";
	public static final String SALLADHOR_SAAN = "Salladhor Saan";
	
	@Deprecated
	public BaratheonHouseCard() {
		// Used by marshaller only
	}

	public BaratheonHouseCard(String name) {
		setName(name);
		switch (name) {
            case RENLY_BARATHEON:
                setCombatStrength(3);
                setSwordIcons(0);
                setFortificationIcons(0);
                break;
            case BRIENE_OF_TARTH:
                setCombatStrength(2);
                setSwordIcons(1);
                setFortificationIcons(1);
                break;
            case MELISANDRE:
                setCombatStrength(1);
                setSwordIcons(1);
                setFortificationIcons(0);
                break;
            case PATCHFACE:
                setCombatStrength(0);
                setSwordIcons(0);
                setFortificationIcons(0);
                break;
            case SALLADHOR_SAAN:
                setCombatStrength(1);
                setSwordIcons(0);
                setFortificationIcons(0);
                break;
            case SER_DAVOS_SEAWORTH:
                setCombatStrength(2);
                setSwordIcons(0);
                setFortificationIcons(0);
                break;
            case STANNIS_BARATHEON:
                setCombatStrength(4);
                setSwordIcons(0);
                setFortificationIcons(0);
                break;
            default:
                break;
        }
	}
	
	@Override
	public Image getImage() {
        Image img = null;
        switch (getName()) {
        case BRIENE_OF_TARTH:
            img = new Image(getClass().getResourceAsStream("/resources/housecards/baratheon/BrienneOfTarth.jpg"));
            break;
        case MELISANDRE:
            img = new Image(getClass().getResourceAsStream("/resources/housecards/baratheon/Melisandre.jpg"));
            break;
        case PATCHFACE:
            img = new Image(getClass().getResourceAsStream("/resources/housecards/baratheon/Patchface.jpg"));
            break;
        case RENLY_BARATHEON:
            img = new Image(getClass().getResourceAsStream("/resources/housecards/baratheon/RenlyBaratheon.jpg"));
            break;
        case SALLADHOR_SAAN:
            img = new Image(getClass().getResourceAsStream("/resources/housecards/baratheon/SalladhorSaan.jpg"));
            break;
        case SER_DAVOS_SEAWORTH:
            img = new Image(getClass().getResourceAsStream("/resources/housecards/baratheon/SerDavosSeaworth.jpg"));
            break;
        case STANNIS_BARATHEON:
            img = new Image(getClass().getResourceAsStream("/resources/housecards/baratheon/StannisBaratheon.jpg"));
            break;
        default:
            Client.error(getName() + " is not a Lannister House Card.");
            break;
        }
        return img;
	}
	
	public static List<BaratheonHouseCard> getAllCards(){
		List<BaratheonHouseCard> cards = new ArrayList<BaratheonHouseCard>();
		cards.add(new BaratheonHouseCard(BaratheonHouseCard.BRIENE_OF_TARTH));
		cards.add(new BaratheonHouseCard(BaratheonHouseCard.MELISANDRE));
		cards.add(new BaratheonHouseCard(BaratheonHouseCard.PATCHFACE));
		cards.add(new BaratheonHouseCard(BaratheonHouseCard.RENLY_BARATHEON));
		cards.add(new BaratheonHouseCard(BaratheonHouseCard.SALLADHOR_SAAN));
		cards.add(new BaratheonHouseCard(BaratheonHouseCard.SER_DAVOS_SEAWORTH));
		cards.add(new BaratheonHouseCard(BaratheonHouseCard.STANNIS_BARATHEON));
		return cards;
	}
}
