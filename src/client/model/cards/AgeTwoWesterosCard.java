package client.model.cards;

import java.util.ArrayList;
import java.util.List;

import javafx.scene.image.Image;

public class AgeTwoWesterosCard extends WesterosCard {
	public static final String CLASH_OF_KINGS = "Clash of Kings";
	public static final String LAST_DAYS_OF_SUMMER = "Last Days of Summer";
	public static final String GAME_OF_THRONES = "Game of Thrones";
	public static final String WINTER_IS_COMING = "Winter is Coming";
	public static final String DARK_WINGS_DARK_WORDS = "Dark Wings, Dark Words";
	
	   @Deprecated
	    public AgeTwoWesterosCard(){
	        //Used by marshaller only
	    }

	public AgeTwoWesterosCard(String name) {
		setName(name);
	}

	@Override
	public boolean isWildlingCounter() {
		boolean result = false;
		switch (getName()) {
		case CLASH_OF_KINGS:
			result = false;
			break;
		case LAST_DAYS_OF_SUMMER:
			result = true;
			break;
		case GAME_OF_THRONES:
			result = false;
			break;
		case WINTER_IS_COMING:
			result = false;
			break;
		case DARK_WINGS_DARK_WORDS:
			result = true;
			break;
		default:
			break;
		}
		return result;
	}

	@Override
	public Image getImage() {
		Image img = null;
		switch (getName()) {
		case CLASH_OF_KINGS:
			img = new Image("/resources/agetwocards/ClashOfKings.jpg");
			break;
		case LAST_DAYS_OF_SUMMER:
		    img = new Image("/resources/agetwocards/LastDaysOfSummer.jpg");
			break;
		case GAME_OF_THRONES:
		    img = new Image("/resources/agetwocards/GameOfThrones.jpg");
			break;
		case WINTER_IS_COMING:
		    img = new Image("/resources/agetwocards/WinterIsComing.jpg");
			break;
		case DARK_WINGS_DARK_WORDS:
		    img = new Image("/resources/agetwocards/DarkWingsDarkWords.jpg");
			break;
		default:
			break;
		}
		return img;
	}

	public static List getAllCards() {
		List<AgeTwoWesterosCard> cards = new ArrayList<AgeTwoWesterosCard>();
		cards.add(new AgeTwoWesterosCard(AgeTwoWesterosCard.CLASH_OF_KINGS));
		cards.add(new AgeTwoWesterosCard(AgeTwoWesterosCard.GAME_OF_THRONES));
		cards.add(new AgeTwoWesterosCard(AgeTwoWesterosCard.DARK_WINGS_DARK_WORDS));
		cards.add(new AgeTwoWesterosCard(AgeTwoWesterosCard.LAST_DAYS_OF_SUMMER));
		cards.add(new AgeTwoWesterosCard(AgeTwoWesterosCard.WINTER_IS_COMING));
		cards.add(new AgeTwoWesterosCard(AgeTwoWesterosCard.GAME_OF_THRONES));
		cards.add(new AgeTwoWesterosCard(AgeTwoWesterosCard.CLASH_OF_KINGS));
		cards.add(new AgeTwoWesterosCard(AgeTwoWesterosCard.DARK_WINGS_DARK_WORDS));
		cards.add(new AgeTwoWesterosCard(AgeTwoWesterosCard.GAME_OF_THRONES));
		cards.add(new AgeTwoWesterosCard(AgeTwoWesterosCard.CLASH_OF_KINGS));
		return cards;
	}
}
