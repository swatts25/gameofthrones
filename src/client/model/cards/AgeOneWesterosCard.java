package client.model.cards;

import java.util.ArrayList;
import java.util.List;

import javafx.scene.image.Image;
import client.Client;

public class AgeOneWesterosCard extends WesterosCard {
	public static final String A_THRONE_OF_BLADES = "A Throne of Blades";
	public static final String LAST_DAYS_OF_SUMMER = "Last Days of Summer";
	public static final String MUSTERING = "Mustering";
	public static final String SUPPLY = "Supply";
	public static final String WINTER_IS_COMING = "Winter is Coming";
	
	@Deprecated
	public AgeOneWesterosCard(){
	    //Used by marshaller only
	}

	public AgeOneWesterosCard(String name) {
		setName(name);
	}

	@Override
	public boolean isWildlingCounter() {
		boolean result = false;
		switch (getName()) {
		case A_THRONE_OF_BLADES:
			result = true;
			break;
		case LAST_DAYS_OF_SUMMER:
			result = true;
			break;
		case MUSTERING:
			result = false;
			break;
		case SUPPLY:
			result = false;
			break;
		case WINTER_IS_COMING:
			result = false;
			break;
		default:
			Client.error(getName() + " is not an Age One Westeros Card.");
			break;
		}
		return result;
	}

	@Override
	public Image getImage() {
		Image img = null;
		switch (getName()) {
		case A_THRONE_OF_BLADES:
			img = new Image(getClass().getResourceAsStream("/resources/ageonecards/AThroneOfBlades.jpg"));
			break;
		case LAST_DAYS_OF_SUMMER:
			img = new Image(getClass().getResourceAsStream("/resources/ageonecards/LastDaysOfSummer.jpg"));
			break;
		case MUSTERING:
			img = new Image(getClass().getResourceAsStream("/resources/ageonecards/Mustering.jpg"));
			break;
		case SUPPLY:
			img = new Image(getClass().getResourceAsStream("/resources/ageonecards/Supply.jpg"));
			break;
		case WINTER_IS_COMING:
			img = new Image(getClass().getResourceAsStream("/resources/ageonecards/WinterIsComing.jpg"));
			break;
		default:
			Client.error(getName() + " is not an Age One Westeros Card.");
			break;
		}
		return img;
	}

	public static List<AgeOneWesterosCard> getAllCards() {
		List<AgeOneWesterosCard> cards = new ArrayList<AgeOneWesterosCard>();
		cards.add(new AgeOneWesterosCard(AgeOneWesterosCard.SUPPLY));
		cards.add(new AgeOneWesterosCard(AgeOneWesterosCard.MUSTERING));
		cards.add(new AgeOneWesterosCard(AgeOneWesterosCard.A_THRONE_OF_BLADES));
		cards.add(new AgeOneWesterosCard(AgeOneWesterosCard.WINTER_IS_COMING));
		cards.add(new AgeOneWesterosCard(AgeOneWesterosCard.SUPPLY));
		cards.add(new AgeOneWesterosCard(AgeOneWesterosCard.LAST_DAYS_OF_SUMMER));
		cards.add(new AgeOneWesterosCard(AgeOneWesterosCard.MUSTERING));
		cards.add(new AgeOneWesterosCard(AgeOneWesterosCard.A_THRONE_OF_BLADES));
		cards.add(new AgeOneWesterosCard(AgeOneWesterosCard.SUPPLY));
		cards.add(new AgeOneWesterosCard(AgeOneWesterosCard.MUSTERING));
		return cards;
	}
}
