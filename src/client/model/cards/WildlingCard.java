package client.model.cards;

import java.util.ArrayList;
import java.util.List;

import javafx.scene.image.Image;
import client.Client;

/**
 * @author swatts
 */
public class WildlingCard extends Card {
	public static final String SILENCE_AT_THE_WALL = "Silence at the Wall";
	public static final String SKINCHANGER_SCOUT = "Skin Changer Scout";
	public static final String A_KING_BEYOND_THE_WALL = "A King Beyond the Wall";
	public static final String RATTLESHIRTS_RAIDERS = "Rattleshirt's Raiders";
	public static final String PREEMPTIVE_RAID = "Preemptive Raid";
	public static final String CROW_KILLERS = "Crow Killers";
	public static final String THE_HORDE_DESCENDS = "The Horde Descends";
	public static final String MASSING_ON_THE_MILKWATER = "Massing on the Milkwater";
	public static final String MAMMOTH_RIDERS = "Mammoth Riders";

	public WildlingCard(String name) {
		setName(name);
	}

	@Deprecated
	public WildlingCard() {
		// Used by Marshaller only
	}

	public static List<WildlingCard> getAllCards() {
		List<WildlingCard> cards = new ArrayList<WildlingCard>();
		cards.add(new WildlingCard(WildlingCard.A_KING_BEYOND_THE_WALL));
		cards.add(new WildlingCard(WildlingCard.CROW_KILLERS));
		cards.add(new WildlingCard(WildlingCard.MAMMOTH_RIDERS));
		cards.add(new WildlingCard(WildlingCard.MASSING_ON_THE_MILKWATER));
		cards.add(new WildlingCard(WildlingCard.PREEMPTIVE_RAID));
		cards.add(new WildlingCard(WildlingCard.RATTLESHIRTS_RAIDERS));
		cards.add(new WildlingCard(WildlingCard.SILENCE_AT_THE_WALL));
		cards.add(new WildlingCard(WildlingCard.SKINCHANGER_SCOUT));
		cards.add(new WildlingCard(WildlingCard.THE_HORDE_DESCENDS));
		return cards;
	}

	@Override
	public Image getImage() {
		Image img = null;
		switch (getName()) {
		case SILENCE_AT_THE_WALL:
			img = new Image(getClass().getResourceAsStream("/resources/wildlingcards/SilenceAtTheWall.jpg"));
			break;
		case SKINCHANGER_SCOUT:
			img = new Image(getClass().getResourceAsStream("/resources/wildlingcards/SkinchangerScout.jpg"));
			break;
		case A_KING_BEYOND_THE_WALL:
			img = new Image(getClass().getResourceAsStream("/resources/wildlingcards/AKingBeyondTheWall.jpg"));
			break;
		case RATTLESHIRTS_RAIDERS:
			img = new Image(getClass().getResourceAsStream("/resources/wildlingcards/RattleshirtsRaiders.jpg"));
			break;
		case PREEMPTIVE_RAID:
			img = new Image(getClass().getResourceAsStream("/resources/wildlingcards/PreemptiveRaid.jpg"));
			break;
		case CROW_KILLERS:
			img = new Image(getClass().getResourceAsStream("/resources/wildlingcards/CrowKillers.jpg"));
			break;
		case THE_HORDE_DESCENDS:
			img = new Image(getClass().getResourceAsStream("/resources/wildlingcards/TheHordeDescends.jpg"));
			break;
		case MASSING_ON_THE_MILKWATER:
			img = new Image(getClass().getResourceAsStream("/resources/wildlingcards/MassingAtTheMilkwater.jpg"));
			break;
		case MAMMOTH_RIDERS:
			img = new Image(getClass().getResourceAsStream("/resources/wildlingcards/MammothRiders.jpg"));
			break;
		default:
			Client.error(getName() + " is not a Wildling Card.");
			break;
		}
		return img;
	}
}
