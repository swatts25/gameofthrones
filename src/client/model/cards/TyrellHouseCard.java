package client.model.cards;

import java.util.ArrayList;
import java.util.List;

import javafx.scene.image.Image;

public class TyrellHouseCard extends HouseCard {

    public static final String MACE_TYRELL = "Mace Tyrell";

    public static final String SER_LORAS_TYRELL = "Ser Loras Tyrell";

    public static final String SER_GARLAN_TYRELL = "Ser Garlan Tyrell";

    public static final String RANDYLL_TARLY = "Randyll Tarly";

    public static final String MARGAERY_TYRELL = "Margaery Tyrell";

    public static final String ALESTER_FLORENT = "Alester Florent";

    public static final String QUEEN_OF_THORNS = "Queen of Thorns";

    @Deprecated
    public TyrellHouseCard() {
        // used by marshaller
    }

    public TyrellHouseCard(String name) {
        setName(name);
        switch (name) {
            case ALESTER_FLORENT:
                setCombatStrength(1);
                setSwordIcons(0);
                setFortificationIcons(1);
                break;
            case MACE_TYRELL:
                setCombatStrength(4);
                setSwordIcons(0);
                setFortificationIcons(0);
                break;
            case MARGAERY_TYRELL:
                setCombatStrength(1);
                setSwordIcons(0);
                setFortificationIcons(1);
                break;
            case QUEEN_OF_THORNS:
                setCombatStrength(0);
                setSwordIcons(0);
                setFortificationIcons(0);
                break;
            case RANDYLL_TARLY:
                setCombatStrength(2);
                setSwordIcons(1);
                setFortificationIcons(0);
                break;
            case SER_GARLAN_TYRELL:
                setCombatStrength(2);
                setSwordIcons(2);
                setFortificationIcons(0);
                break;
            case SER_LORAS_TYRELL:
                setCombatStrength(3);
                setSwordIcons(0);
                setFortificationIcons(0);
                break;
            default:
                break;
        }
    }

    @Override
    public Image getImage() {
        Image img = null;
        switch (getName()) {
            case ALESTER_FLORENT:
                img = new Image(getClass().getResourceAsStream(
                        "/resources/housecards/tyrell/AlesterFlorent.jpg"));
                break;
            case MACE_TYRELL:
                img = new Image(getClass().getResourceAsStream(
                        "/resources/housecards/tyrell/MaceTyrell.jpg"));
                break;
            case MARGAERY_TYRELL:
                img = new Image(getClass().getResourceAsStream(
                        "/resources/housecards/tyrell/MargaeryTyrell.jpg"));
                break;
            case QUEEN_OF_THORNS:
                img = new Image(getClass().getResourceAsStream(
                        "/resources/housecards/tyrell/QueenOfThornes.jpg"));
                break;
            case RANDYLL_TARLY:
                img = new Image(getClass().getResourceAsStream(
                        "/resources/housecards/tyrell/RandyllTarly.jpg"));
                break;
            case SER_GARLAN_TYRELL:
                img = new Image(getClass().getResourceAsStream(
                        "/resources/housecards/tyrell/SerGarlanTyrell.jpg"));
                break;
            case SER_LORAS_TYRELL:
                img = new Image(getClass().getResourceAsStream(
                        "/resources/housecards/tyrell/SerLorasTyrell.jpg"));
                break;
            default:
                break;
        }
        return img;
    }

    public static List<TyrellHouseCard> getAllCards() {
        List<TyrellHouseCard> cards = new ArrayList<TyrellHouseCard>();
        cards.add(new TyrellHouseCard(TyrellHouseCard.ALESTER_FLORENT));
        cards.add(new TyrellHouseCard(TyrellHouseCard.MACE_TYRELL));
        cards.add(new TyrellHouseCard(TyrellHouseCard.MARGAERY_TYRELL));
        cards.add(new TyrellHouseCard(TyrellHouseCard.QUEEN_OF_THORNS));
        cards.add(new TyrellHouseCard(TyrellHouseCard.RANDYLL_TARLY));
        cards.add(new TyrellHouseCard(TyrellHouseCard.SER_GARLAN_TYRELL));
        cards.add(new TyrellHouseCard(TyrellHouseCard.SER_LORAS_TYRELL));
        return cards;
    }
}
