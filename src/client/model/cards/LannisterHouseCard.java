package client.model.cards;

import java.util.ArrayList;
import java.util.List;

import javafx.scene.image.Image;
import client.Client;

public class LannisterHouseCard extends HouseCard {
	public static final String CERSEI_LANNISTER = "Cersei Lannister";
	public static final String TYRION_LANNISTER = "Tyrion Lannister";
	public static final String SER_KEVAN_LANNISTER = "Ser Kevan Lannister";
	public static final String THE_HOUND = "The Hound";
	public static final String TYWIN_LANNISTER = "Tywin Lannister";
	public static final String SER_GREGOR_CLEGANE = "Ser Gregor Clegane";
	public static final String SER_JAIME_LANNISTER = "Ser Jaime Lannister";

	@Deprecated
	public LannisterHouseCard() {
		// Used by marshaller only
	}

	public LannisterHouseCard(String name) {
		setName(name);
		switch (name) {
            case CERSEI_LANNISTER:
                setCombatStrength(0);
                setSwordIcons(0);
                setFortificationIcons(0);
                break;
            case SER_GREGOR_CLEGANE:
                setCombatStrength(3);
                setSwordIcons(3);
                setFortificationIcons(0);
                break;
            case SER_JAIME_LANNISTER:
                setCombatStrength(2);
                setSwordIcons(1);
                setFortificationIcons(0);
                break;
            case SER_KEVAN_LANNISTER:
                setCombatStrength(1);
                setSwordIcons(0);
                setFortificationIcons(0);
                break;
            case THE_HOUND:
                setCombatStrength(2);
                setSwordIcons(0);
                setFortificationIcons(2);
                break;
            case TYRION_LANNISTER:
                setCombatStrength(1);
                setSwordIcons(0);
                setFortificationIcons(0);
                break;
            case TYWIN_LANNISTER:
                setCombatStrength(4);
                setSwordIcons(0);
                setFortificationIcons(0);
                break;
            default:
                break;
        }
	}

	@Override
	public Image getImage() {
        Image img = null;
        switch (getName()) {
        case CERSEI_LANNISTER:
            img = new Image(getClass().getResourceAsStream("/resources/housecards/lannister/CerseiLannister.jpg"));
            break;
        case SER_GREGOR_CLEGANE:
            img = new Image(getClass().getResourceAsStream("/resources/housecards/lannister/SerGregorClegane.jpg"));
            break;
        case SER_JAIME_LANNISTER:
            img = new Image(getClass().getResourceAsStream("/resources/housecards/lannister/SerJaimeLannister.jpg"));
            break;
        case SER_KEVAN_LANNISTER:
            img = new Image(getClass().getResourceAsStream("/resources/housecards/lannister/SerKevanLannister.jpg"));
            break;
        case THE_HOUND:
            img = new Image(getClass().getResourceAsStream("/resources/housecards/lannister/TheHound.jpg"));
            break;
        case TYRION_LANNISTER:
            img = new Image(getClass().getResourceAsStream("/resources/housecards/lannister/TyrionLannister.jpg"));
            break;
        case TYWIN_LANNISTER:
            img = new Image(getClass().getResourceAsStream("/resources/housecards/lannister/TywinLannister.jpg"));
            break;
        default:
            Client.error(getName() + " is not a Lannister House Card.");
            break;
        }
        return img;
	}

	public static List<LannisterHouseCard> getAllCards() {
		List<LannisterHouseCard> cards = new ArrayList<LannisterHouseCard>();
		cards.add(new LannisterHouseCard(LannisterHouseCard.CERSEI_LANNISTER));
		cards.add(new LannisterHouseCard(LannisterHouseCard.SER_GREGOR_CLEGANE));
		cards.add(new LannisterHouseCard(LannisterHouseCard.SER_JAIME_LANNISTER));
		cards.add(new LannisterHouseCard(LannisterHouseCard.SER_KEVAN_LANNISTER));
		cards.add(new LannisterHouseCard(LannisterHouseCard.THE_HOUND));
		cards.add(new LannisterHouseCard(LannisterHouseCard.TYRION_LANNISTER));
		cards.add(new LannisterHouseCard(LannisterHouseCard.TYWIN_LANNISTER));
		return cards;
	}
}
