package client.model.cards;

import java.util.ArrayList;
import java.util.List;

import javafx.scene.image.Image;

public class AgeThreeWesterosCard extends WesterosCard {

    public static final String WILDLINGS_ATTACK = "Wildlings Attack";

    public static final String FEAST_FOR_CROWS = "Feast for Crows";

    public static final String STORM_OF_SWORDS = "Storm of Swords";

    public static final String WEB_OF_LIES = "Web of Lies";

    public static final String RAINS_OF_AUTUMN = "Rains of Autumn";

    public static final String SEA_OF_STORMS = "Sea of Storms";

    public static final String PUT_TO_THE_SWORD = "Put to the Sword";

    @Deprecated
    public AgeThreeWesterosCard() {
        // Used by marshaller only
    }

    public AgeThreeWesterosCard(String name) {
        setName(name);
    }

    @Override
    public boolean isWildlingCounter() {
        boolean result = false;
        switch (getName()) {
            case WILDLINGS_ATTACK:
                result = false;
                break;
            case FEAST_FOR_CROWS:
                result = true;
                break;
            case PUT_TO_THE_SWORD:
                result = false;
                break;
            case RAINS_OF_AUTUMN:
                result = true;
                break;
            case SEA_OF_STORMS:
                result = true;
                break;
            case WEB_OF_LIES:
                result = true;
                break;
            default:
                break;
        }
        return result;
    }

    @Override
    public Image getImage() {
        Image img = null;
        switch (getName()) {
            case FEAST_FOR_CROWS:
                img = new Image("/resources/agethreecards/FeastForCrows.jpg");
                break;
            case PUT_TO_THE_SWORD:
                img = new Image("/resources/agethreecards/PutToTheSword.jpg");
                break;
            case RAINS_OF_AUTUMN:
                img = new Image("/resources/agethreecards/RainsOfAutum.jpg");
                break;
            case SEA_OF_STORMS:
                img = new Image("/resources/agethreecards/SeaOfStorms.jpg");
                break;
            case STORM_OF_SWORDS:
                img = new Image("/resources/agethreecards/StormOfSwords.jpg");
                break;
            case WEB_OF_LIES:
                img = new Image("/resources/agethreecards/WebOfLies.jpg");
                break;
            case WILDLINGS_ATTACK:
                img = new Image("/resources/agethreecards/WildlingAttack.jpg");
                break;
            default:
                break;
        }
        return img;
    }

    public static List<AgeThreeWesterosCard> getAllCards() {
        List<AgeThreeWesterosCard> cards = new ArrayList<AgeThreeWesterosCard>();
        cards.add(new AgeThreeWesterosCard(AgeThreeWesterosCard.FEAST_FOR_CROWS));
//        cards.add(new AgeThreeWesterosCard(
//                AgeThreeWesterosCard.WILDLINGS_ATTACK));
        cards.add(new AgeThreeWesterosCard(
                AgeThreeWesterosCard.PUT_TO_THE_SWORD));
        cards.add(new AgeThreeWesterosCard(AgeThreeWesterosCard.STORM_OF_SWORDS));
//        cards.add(new AgeThreeWesterosCard(
//                AgeThreeWesterosCard.WILDLINGS_ATTACK));
        cards.add(new AgeThreeWesterosCard(AgeThreeWesterosCard.WEB_OF_LIES));
        cards.add(new AgeThreeWesterosCard(AgeThreeWesterosCard.RAINS_OF_AUTUMN));
        cards.add(new AgeThreeWesterosCard(AgeThreeWesterosCard.SEA_OF_STORMS));
        cards.add(new AgeThreeWesterosCard(
                AgeThreeWesterosCard.PUT_TO_THE_SWORD));
//        cards.add(new AgeThreeWesterosCard(
//                AgeThreeWesterosCard.WILDLINGS_ATTACK));
        return cards;
    }
}
