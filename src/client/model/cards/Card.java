package client.model.cards;

import javafx.scene.image.Image;

import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;

/**
 * @author swatts
 */
@XmlSeeAlso({ WildlingCard.class, AgeOneWesterosCard.class,
        AgeTwoWesterosCard.class, AgeThreeWesterosCard.class,
        StarkHouseCard.class })
@XmlType
public abstract class Card {

    private String name;

    public abstract Image getImage();
    
    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name
     *            the name to set
     */
    public void setName(String name) {
        this.name = name;
    }
}
