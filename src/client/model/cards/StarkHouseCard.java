package client.model.cards;

import java.util.ArrayList;
import java.util.List;

import javafx.scene.image.Image;

/**
 * @author swatts
 */
public class StarkHouseCard extends HouseCard {

    public static final String EDDARD_STARK = "Eddard Stark";

    public static final String ROBB_STARK = "Robb Stark";

    public static final String SER_RODRICK_CASSEL = "Ser Rodrick Cassel";

    public static final String ROOSE_BOLTON = "Roose Bolton";

    public static final String GREATJON_UMBER = "Greatjon Umber";

    public static final String THE_BLACKFISH = "The Blackfish";

    public static final String CATELYN_STARK = "Catelyn Stark";

    @Deprecated
    public StarkHouseCard() {
        // Used by marshaller only
    }

    public StarkHouseCard(String name) {
        setName(name);
        switch (name) {
            case EDDARD_STARK:
                setCombatStrength(4);
                setSwordIcons(2);
                setFortificationIcons(0);
                break;
            case CATELYN_STARK:
                setCombatStrength(0);
                setSwordIcons(0);
                setFortificationIcons(0);
                break;
            case GREATJON_UMBER:
                setCombatStrength(2);
                setSwordIcons(1);
                setFortificationIcons(0);
                break;
            case ROBB_STARK:
                setCombatStrength(3);
                setSwordIcons(0);
                setFortificationIcons(1);
                break;
            case ROOSE_BOLTON:
                setCombatStrength(2);
                setSwordIcons(0);
                setFortificationIcons(0);
                break;
            case SER_RODRICK_CASSEL:
                setCombatStrength(1);
                setSwordIcons(0);
                setFortificationIcons(2);
                break;
            case THE_BLACKFISH:
                setCombatStrength(1);
                setSwordIcons(0);
                setFortificationIcons(0);
                break;
            default:
                break;
        }
    }

    @Override
    public Image getImage() {
        Image img = null;
        switch (getName()) {
            case EDDARD_STARK:
                img = new Image(getClass().getResourceAsStream(
                        "/resources/housecards/stark/EddardStark.jpg"));
                break;
            case CATELYN_STARK:
                img = new Image(getClass().getResourceAsStream(
                        "/resources/housecards/stark/CatelynStark.jpg"));
                break;
            case GREATJON_UMBER:
                img = new Image(getClass().getResourceAsStream(
                        "/resources/housecards/stark/GreatjonUmber.jpg"));
                break;
            case ROBB_STARK:
                img = new Image(getClass().getResourceAsStream(
                        "/resources/housecards/stark/RobbStark.jpg"));
                break;
            case ROOSE_BOLTON:
                img = new Image(getClass().getResourceAsStream(
                        "/resources/housecards/stark/RooseBolton.jpg"));
                break;
            case SER_RODRICK_CASSEL:
                img = new Image(getClass().getResourceAsStream(
                        "/resources/housecards/stark/SerRodrickCassel.jpg"));
                break;
            case THE_BLACKFISH:
                img = new Image(getClass().getResourceAsStream(
                        "/resources/housecards/stark/TheBlackfish.jpg"));
                break;
            default:
                break;
        }
        return img;
    }

    public static List<StarkHouseCard> getAllCards() {
        List<StarkHouseCard> cards = new ArrayList<StarkHouseCard>();
        cards.add(new StarkHouseCard(StarkHouseCard.EDDARD_STARK));
        cards.add(new StarkHouseCard(StarkHouseCard.CATELYN_STARK));
        cards.add(new StarkHouseCard(StarkHouseCard.ROBB_STARK));
        cards.add(new StarkHouseCard(StarkHouseCard.GREATJON_UMBER));
        cards.add(new StarkHouseCard(StarkHouseCard.ROOSE_BOLTON));
        cards.add(new StarkHouseCard(StarkHouseCard.SER_RODRICK_CASSEL));
        cards.add(new StarkHouseCard(StarkHouseCard.THE_BLACKFISH));
        return cards;
    }
}
