package client.model.cards;

import java.util.ArrayList;
import java.util.List;

import javafx.scene.image.Image;
import client.Client;

public class GreyjoyHouseCard extends HouseCard {
	public static final String BALON_GREYJOY = "Balon Greyjoy";
	public static final String EURON_CROWS_EYE = "Euron Crow's Eye";
	public static final String THEON_GREYJOY = "Theon Greyjoy";
	public static final String VICTARION_GREYJOY = "Victarion Greyjoy";
	public static final String AERON_DAMPHAIR = "Aeron Damphair";
	public static final String ASHA_GREYJOY = "Asha Greyjoy";
	public static final String DAGMER_CLEFTJAW = "Dagmer Cleftjaw";

	@Deprecated
	public GreyjoyHouseCard() {
		// Used by marshaller only
	}

	public GreyjoyHouseCard(String name) {
		setName(name);
        switch (name) {
            case AERON_DAMPHAIR:
                setCombatStrength(0);
                setSwordIcons(0);
                setFortificationIcons(0);
                break;
            case ASHA_GREYJOY:
                setCombatStrength(1);
                setSwordIcons(0);
                setFortificationIcons(0);
                break;
            case BALON_GREYJOY:
                setCombatStrength(2);
                setSwordIcons(0);
                setFortificationIcons(0);
                break;
            case DAGMER_CLEFTJAW:
                setCombatStrength(1);
                setSwordIcons(1);
                setFortificationIcons(1);
                break;
            case EURON_CROWS_EYE:
                setCombatStrength(4);
                setSwordIcons(1);
                setFortificationIcons(0);
                break;
            case THEON_GREYJOY:
                setCombatStrength(2);
                setSwordIcons(0);
                setFortificationIcons(0);
                break;
            case VICTARION_GREYJOY:
                setCombatStrength(3);
                setSwordIcons(0);
                setFortificationIcons(0);
                break;
            default:
                break;
        }
	}

	public Image getImage() {
	      Image img = null;
	        switch (getName()) {
	        case AERON_DAMPHAIR:
	            img = new Image(getClass().getResourceAsStream("/resources/housecards/greyjoy/AeronDamphair.jpg"));
	            break;
	        case ASHA_GREYJOY:
	            img = new Image(getClass().getResourceAsStream("/resources/housecards/greyjoy/AshaGreyjoy.jpg"));
	            break;
	        case BALON_GREYJOY:
	            img = new Image(getClass().getResourceAsStream("/resources/housecards/greyjoy/BalonGreyjoy.jpg"));
	            break;
	        case DAGMER_CLEFTJAW:
	            img = new Image(getClass().getResourceAsStream("/resources/housecards/greyjoy/DagmerCleftjaw.jpg"));
	            break;
	        case EURON_CROWS_EYE:
	            img = new Image(getClass().getResourceAsStream("/resources/housecards/greyjoy/EuronCrowsEye.jpg"));
	            break;
	        case THEON_GREYJOY:
	            img = new Image(getClass().getResourceAsStream("/resources/housecards/greyjoy/TheonGreyjoy.jpg"));
	            break;
	        case VICTARION_GREYJOY:
	            img = new Image(getClass().getResourceAsStream("/resources/housecards/greyjoy/VictarionGreyjoy.jpg"));
	            break;
	        default:
	            Client.error(getName() + " is not a Greyjoy House Card.");
	            break;
	        }
	        return img;
	}

	public static List<GreyjoyHouseCard> getAllCards() {
		List<GreyjoyHouseCard> cards = new ArrayList<GreyjoyHouseCard>();
		cards.add(new GreyjoyHouseCard(GreyjoyHouseCard.AERON_DAMPHAIR));
		cards.add(new GreyjoyHouseCard(GreyjoyHouseCard.ASHA_GREYJOY));
		cards.add(new GreyjoyHouseCard(GreyjoyHouseCard.BALON_GREYJOY));
		cards.add(new GreyjoyHouseCard(GreyjoyHouseCard.DAGMER_CLEFTJAW));
		cards.add(new GreyjoyHouseCard(GreyjoyHouseCard.EURON_CROWS_EYE));
		cards.add(new GreyjoyHouseCard(GreyjoyHouseCard.THEON_GREYJOY));
		cards.add(new GreyjoyHouseCard(GreyjoyHouseCard.VICTARION_GREYJOY));
		return cards;
	}
}
