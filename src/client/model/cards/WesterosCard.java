package client.model.cards;


/**
 * @author swatts
 */
public abstract class WesterosCard extends Card {

    public abstract boolean isWildlingCounter();
}
