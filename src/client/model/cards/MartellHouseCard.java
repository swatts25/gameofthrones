package client.model.cards;

import java.util.ArrayList;
import java.util.List;

import javafx.scene.image.Image;
import client.Client;

public class MartellHouseCard extends HouseCard {
	public static final String THE_RED_VIPER = "The Red Viper";
	public static final String DORAN_MARTELL = "Doran Martell";
	public static final String DARKSTAR = "Darkstar";
	public static final String OBARA_SAND = "Obara Sand";
	public static final String NYMERIA_SAND = "Nymeria Sand";
	public static final String AREO_HOTAH = "Areo_Hotah";
	public static final String ARIANNE_MARTELL = "Arianne Martell";

	@Deprecated
	public MartellHouseCard() {
		// Used by marshaller only
	}

	public MartellHouseCard(String name) {
		setName(name);
        switch (name) {
            case AREO_HOTAH:
                setCombatStrength(3);
                setSwordIcons(0);
                setFortificationIcons(1);
                break;
            case ARIANNE_MARTELL:
                setCombatStrength(1);
                setSwordIcons(0);
                setFortificationIcons(0);
                break;
            case DARKSTAR:
                setCombatStrength(2);
                setSwordIcons(1);
                setFortificationIcons(0);
                break;
            case DORAN_MARTELL:
                setCombatStrength(0);
                setSwordIcons(0);
                setFortificationIcons(0);
                break;
            case NYMERIA_SAND:
                setCombatStrength(1);
                setSwordIcons(0);
                setFortificationIcons(0);
                break;
            case OBARA_SAND:
                setCombatStrength(2);
                setSwordIcons(1);
                setFortificationIcons(0);
                break;
            case THE_RED_VIPER:
                setCombatStrength(4);
                setSwordIcons(2);
                setFortificationIcons(1);
                break;
            default:
                break;
        }
	}

	@Override
	public Image getImage() {
        Image img = null;
        switch (getName()) {
        case AREO_HOTAH:
            img = new Image(getClass().getResourceAsStream("/resources/housecards/martell/AreoHotah.jpg"));
            break;
        case ARIANNE_MARTELL:
            img = new Image(getClass().getResourceAsStream("/resources/housecards/martell/ArianneMartell.jpg"));
            break;
        case DARKSTAR:
            img = new Image(getClass().getResourceAsStream("/resources/housecards/martell/Darkstar.jpg"));
            break;
        case DORAN_MARTELL:
            img = new Image(getClass().getResourceAsStream("/resources/housecards/martell/DoranMartell.jpg"));
            break;
        case NYMERIA_SAND:
            img = new Image(getClass().getResourceAsStream("/resources/housecards/martell/NymeriaSand.jpg"));
            break;
        case OBARA_SAND:
            img = new Image(getClass().getResourceAsStream("/resources/housecards/martell/ObraSand.jpg"));
            break;
        case THE_RED_VIPER:
            img = new Image(getClass().getResourceAsStream("/resources/housecards/martell/TheRedViper.jpg"));
            break;
        default:
            Client.error(getName() + " is not a Martell House Card.");
            break;
        }
        return img;
	}

	public static List<MartellHouseCard> getAllCards() {
		List<MartellHouseCard> cards = new ArrayList<MartellHouseCard>();
		cards.add(new MartellHouseCard(MartellHouseCard.AREO_HOTAH));
		cards.add(new MartellHouseCard(MartellHouseCard.ARIANNE_MARTELL));
		cards.add(new MartellHouseCard(MartellHouseCard.DARKSTAR));
		cards.add(new MartellHouseCard(MartellHouseCard.DORAN_MARTELL));
		cards.add(new MartellHouseCard(MartellHouseCard.NYMERIA_SAND));
		cards.add(new MartellHouseCard(MartellHouseCard.OBARA_SAND));
		cards.add(new MartellHouseCard(MartellHouseCard.THE_RED_VIPER));
		return cards;
	}
}
