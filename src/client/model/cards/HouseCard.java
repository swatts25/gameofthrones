package client.model.cards;

import java.util.List;

import client.model.GameContext;
import client.model.House;

/**
 * @author swatts
 */
public abstract class HouseCard extends Card {

    private int combatStrength;

    private int swordIcons;

    private int fortificationIcons;

    /**
     * @return the combatStrength
     */
    public int getCombatStrength() {
        return combatStrength;
    }

    /**
     * @param combatStrength
     *            the combatStrength to set
     */
    public void setCombatStrength(int combatStrength) {
        this.combatStrength = combatStrength;
    }

    /**
     * @return the swordIcons
     */
    public int getSwordIcons() {
        return swordIcons;
    }

    /**
     * @param swordIcons
     *            the swordIcons to set
     */
    public void setSwordIcons(int swordIcons) {
        this.swordIcons = swordIcons;
    }

    /**
     * @return the fortificationIcons
     */
    public int getFortificationIcons() {
        return fortificationIcons;
    }

    /**
     * @param fortificationIcons
     *            the fortificationIcons to set
     */
    public void setFortificationIcons(int fortificationIcons) {
        this.fortificationIcons = fortificationIcons;
    }

    public static void resetDeck(GameContext context, String house) {
        switch (house) {
            case House.BARATHEON:
                context.setBaratheonHouseDeck(BaratheonHouseCard.getAllCards());
                break;
            case House.GREYJOY:
                context.setGreyjoyHouseDeck(GreyjoyHouseCard.getAllCards());
                break;
            case House.LANNISTER:
                context.setLannisterHouseDeck(LannisterHouseCard.getAllCards());
                break;
            case House.MARTELL:
                context.setMartellHouseDeck(MartellHouseCard.getAllCards());
                break;
            case House.STARK:
                context.setStarkHouseDeck(StarkHouseCard.getAllCards());
                break;
            case House.TYRELL:
                context.setTyrellHouseDeck(TyrellHouseCard.getAllCards());
                break;
            default:
                break;
        }
    }

    public static List getHouseDeck(GameContext context, String house) {
        switch (house) {
            case House.BARATHEON:
                return context.getBaratheonHouseDeck();
            case House.GREYJOY:
                return context.getGreyjoyHouseDeck();
            case House.LANNISTER:
                return context.getLannisterHouseDeck();
            case House.MARTELL:
                return context.getMartellHouseDeck();
            case House.STARK:
                return context.getStarkHouseDeck();
            case House.TYRELL:
                return context.getTyrellHouseDeck();
            default:
                return null;
        }
    }
}
