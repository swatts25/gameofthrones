package client.model.cards;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.LinkedBlockingQueue;

/**
 * @author swatts
 *
 */
public class Deck<e> {
	private LinkedBlockingQueue<e> cards;

	@Deprecated
	public Deck() {
		// Used by marshaller only
	}

	public Deck(List<e> cards) {
		this.cards = new LinkedBlockingQueue<e>();
		this.cards.addAll(cards);
		return;
	}

	public void shuffle() {
		List<e> cardList = new ArrayList<e>();
		cards.drainTo(cardList);
		Collections.shuffle(cardList);
		cards.addAll(cardList);
		return;
	}

	public e removeTopCard() {
		return cards.poll();
	}
	
	public e seeTopCard() {
		return cards.peek();
	}

	public void addToBottom(e card) {
		cards.offer(card);
	}

	public static Deck<WildlingCard> getNewWildlingDeck() {
		Deck<WildlingCard> deck = new Deck(WildlingCard.getAllCards());
		deck.shuffle();
		return deck;
	}
	
	public static Deck<AgeOneWesterosCard> getNewAgeOneDeck() {
		Deck<AgeOneWesterosCard> deck = new Deck(AgeOneWesterosCard.getAllCards());
		deck.shuffle();
		return deck;
	}

	public List<e> getCardsAsList() {
		List<e> cardList = new ArrayList<e>();
		cards.drainTo(cardList);
		return cardList;
	}

	public static Deck<AgeTwoWesterosCard> getNewAgeTwoDeck() {
		Deck<AgeTwoWesterosCard> deck = new Deck(AgeTwoWesterosCard.getAllCards());
		deck.shuffle();
		return deck;
	}

	public static Deck<AgeThreeWesterosCard> getNewAgeThreeDeck() {
		Deck<AgeThreeWesterosCard> deck = new Deck(AgeThreeWesterosCard.getAllCards());
		deck.shuffle();
		return deck;
	}

	public static Deck<StarkHouseCard> getNewStarkHouseDeck() {
		Deck<StarkHouseCard> deck = new Deck(StarkHouseCard.getAllCards());
		deck.shuffle();
		return deck;
	}

	public static Deck<BaratheonHouseCard> getNewBaratheonHouseDeck() {
		Deck<BaratheonHouseCard> deck = new Deck(BaratheonHouseCard.getAllCards());
		deck.shuffle();
		return deck;
	}

    public static Deck<LannisterHouseCard> getNewLannisterHouseDeck() {
        Deck<LannisterHouseCard> deck = new Deck(LannisterHouseCard.getAllCards());
        deck.shuffle();
        return deck;
    }
    
    public static Deck<GreyjoyHouseCard> getNewGreyjoyHouseDeck() {
        Deck<GreyjoyHouseCard> deck = new Deck(GreyjoyHouseCard.getAllCards());
        deck.shuffle();
        return deck;
    }
    
    public static Deck<TyrellHouseCard> getNewTyrellHouseDeck() {
        Deck<TyrellHouseCard> deck = new Deck(TyrellHouseCard.getAllCards());
        deck.shuffle();
        return deck;
    }
    
    public static Deck<MartellHouseCard> getNewMartellHouseDeck() {
        Deck<MartellHouseCard> deck = new Deck(MartellHouseCard.getAllCards());
        deck.shuffle();
        return deck;
    }
}
