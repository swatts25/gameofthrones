package client.model;


public class Footman extends Unit {

    @Deprecated
    public Footman() {
        // used by marshaller only
    }

    public Footman(String playerName) {
        setPlayerName(playerName);
        setId(GameContext.getMyGameContext().getUnitCount());
    }

    @Override
    public String getName() {
        return Player.getPlayer(getPlayerName()).getHouse() + " Footman "
                + getId();
    }
}
