package client.model;

import javax.xml.bind.annotation.XmlRootElement;

import utils.StringHelper;
import client.Combat;
import client.model.cards.HouseCard;
import client.model.zones.Zone;

@XmlRootElement
public class CombatHistory {

    private String targetZoneName;

    private String attackerName;

    private HouseCard attackerHouseCard;

    private int attackerMarchingPower;

    private int attackerSupportPower;

    private int attackerSpecialAbility;

    private String defenderName;

    private HouseCard defenderHouseCard;

    private int defenderDefensePower;

    private int defenderSupportPower;

    private int defenderSpecialAbility;
    
    private String log;
    
    private long combatId;

    @Deprecated
    public CombatHistory() {
        // Used by marshaller only
    }

    public CombatHistory(Combat combat, GameContext context) {
        targetZoneName = combat.getTargetZoneName();
        Zone targetZone = context.getZones().get(targetZoneName);
        combatId = combat.getId();
        log = combat.getLog();
        attackerName = combat.getAttackingPlayerName();
        attackerHouseCard = combat.getAttackingHouseCard();
        attackerMarchingPower = combat.getMarchingUnitAttackPower(attackerName,
                targetZone);
        attackerSupportPower = combat.getSupportedStrength(attackerName,
                targetZone);
        defenderName = combat.getDefendingPlayerName();
        defenderHouseCard = combat.getDefendingHouseCard();
        defenderDefensePower = combat.getZoneUnitDefensePower(defenderName,
                targetZone);
        defenderSupportPower = combat.getSupportedStrength(defenderName,
                targetZone);
        attackerSpecialAbility = combat.getCardSpecialAbilityCombatStrength(
                attackerHouseCard, attackerName, defenderHouseCard,
                defenderName);
        defenderSpecialAbility = combat.getCardSpecialAbilityCombatStrength(
                defenderHouseCard, defenderName, attackerHouseCard,
                attackerName);
    }

    /**
     * @return the targetZoneName
     */
    public String getTargetZoneName() {
        return targetZoneName;
    }

    /**
     * @param targetZoneName
     *            the targetZoneName to set
     */
    public void setTargetZoneName(String targetZoneName) {
        this.targetZoneName = targetZoneName;
    }

    /**
     * @return the attackerName
     */
    public String getAttackerName() {
        return attackerName;
    }

    /**
     * @param attackerName
     *            the attackerName to set
     */
    public void setAttackerName(String attackerName) {
        this.attackerName = attackerName;
    }

    /**
     * @return the attackerHouseCard
     */
    public HouseCard getAttackerHouseCard() {
        return attackerHouseCard;
    }

    /**
     * @param attackerHouseCard
     *            the attackerHouseCard to set
     */
    public void setAttackerHouseCard(HouseCard attackerHouseCard) {
        this.attackerHouseCard = attackerHouseCard;
    }

    /**
     * @return the attackerMarchingPower
     */
    public int getAttackerMarchingPower() {
        return attackerMarchingPower;
    }

    /**
     * @param attackerMarchingPower
     *            the attackerMarchingPower to set
     */
    public void setAttackerMarchingPower(int attackerMarchingPower) {
        this.attackerMarchingPower = attackerMarchingPower;
    }

    /**
     * @return the attackerSupportPower
     */
    public int getAttackerSupportPower() {
        return attackerSupportPower;
    }

    /**
     * @param attackerSupportPower
     *            the attackerSupportPower to set
     */
    public void setAttackerSupportPower(int attackerSupportPower) {
        this.attackerSupportPower = attackerSupportPower;
    }

    /**
     * @return the attackerSpecialAbility
     */
    public int getAttackerSpecialAbility() {
        return attackerSpecialAbility;
    }

    /**
     * @param attackerSpecialAbility
     *            the attackerSpecialAbility to set
     */
    public void setAttackerSpecialAbility(int attackerSpecialAbility) {
        this.attackerSpecialAbility = attackerSpecialAbility;
    }

    /**
     * @return the defenderName
     */
    public String getDefenderName() {
        return defenderName;
    }

    /**
     * @param defenderName
     *            the defenderName to set
     */
    public void setDefenderName(String defenderName) {
        this.defenderName = defenderName;
    }

    /**
     * @return the defenderHouseCard
     */
    public HouseCard getDefenderHouseCard() {
        return defenderHouseCard;
    }

    /**
     * @param defenderHouseCard
     *            the defenderHouseCard to set
     */
    public void setDefenderHouseCard(HouseCard defenderHouseCard) {
        this.defenderHouseCard = defenderHouseCard;
    }

    /**
     * @return the defenderSupportPower
     */
    public int getDefenderSupportPower() {
        return defenderSupportPower;
    }

    /**
     * @param defenderSupportPower
     *            the defenderSupportPower to set
     */
    public void setDefenderSupportPower(int defenderSupportPower) {
        this.defenderSupportPower = defenderSupportPower;
    }

    /**
     * @return the defenderSpecialAbility
     */
    public int getDefenderSpecialAbility() {
        return defenderSpecialAbility;
    }

    /**
     * @param defenderSpecialAbility
     *            the defenderSpecialAbility to set
     */
    public void setDefenderSpecialAbility(int defenderSpecialAbility) {
        this.defenderSpecialAbility = defenderSpecialAbility;
    }

    public int getDefenderDefensePower() {
        return defenderDefensePower;
    }

    public void setDefenderDefensePower(int defenderDefensePower) {
        this.defenderDefensePower = defenderDefensePower;
    }

    public String getLog() {
        return log;
    }

    public void setLog(String log) {
        this.log = log;
    }
    
    public void addLog(String log) {
        if(StringHelper.isNotNullOrEmpty(this.log)){
            this.log += ("\n" + log);
        }else{
            this.log += log;
        }
    }

    public long getCombatId() {
        return combatId;
    }

    public void setCombatId(long combatId) {
        this.combatId = combatId;
    }
}
