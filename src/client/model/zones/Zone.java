package client.model.zones;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;

import utils.StringHelper;
import client.model.MarchMovement;
import client.model.Order;
import client.model.Player;
import client.model.Unit;

/**
 * @author swatts
 */
@XmlSeeAlso({ Sea.class, Land.class })
@XmlType
public abstract class Zone {

    private List<String> adjacentZones;

    private int combatId;

    private String controlledByName;

    private List<MarchMovement> marches;

    private String name;

    private Order order;

    private boolean orderResolved;

    public Zone() {
        adjacentZones = new ArrayList<String>();
        marches = new ArrayList<MarchMovement>();
        order = Order.NONE_ORDER;
        combatId = 0;
    }

    public void addAdjacentZone(String zoneName) {
        adjacentZones.add(zoneName);
    }

    public abstract void addUnit(Unit unit);

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof Zone) {
            return ((Zone) obj).getName().equals(name);
        }
        return super.equals(obj);
    }

    @XmlElementWrapper
    public List<String> getAdjacentZones() {
        return adjacentZones;
    }

    public abstract int getAttackPower(Zone defendingTerritory);

    public int getCombatId() {
        return combatId;
    }

    public Player getControlledBy() {
        return Player.getPlayer(controlledByName);
    }

    /**
     * @return the controlledByName
     */
    public String getControlledByName() {
        return controlledByName;
    }

    public abstract int getDefensePower();

    @XmlElementWrapper
    public List<MarchMovement> getMarches() {
        return marches;
    }

    public String getName() {
        return name;
    }

    /**
     * @return the order
     */
    public Order getOrder() {
        return order;
    }

    public abstract List<Unit> getUnits();

    public boolean hasAcclimatedUnit(String playerName, Class<?> unitType) {
        List<Unit> units = getUnits();
        for (Unit unit : units) {
            if (unit.getPlayerName().equals(playerName)
                    && unit.getClass().equals(unitType)) {
                return true;
            }
        }
        return false;
    }

    public boolean hasMarchingUnit(String playerName, Class<?> unitType) {
        List<MarchMovement> marches = getMarches();
        for (MarchMovement marchMovement : marches) {
            Unit unit = marchMovement.getUnit();
            if (unit.getPlayerName().equals(playerName)
                    && unit.getClass().equals(unitType)) {
                return true;
            }
        }
        return false;
    }

    public boolean isOrderResolved() {
        return orderResolved;
    }

    protected void removeMarchingUnit(String playerName, Class<?> unitType) {
        for (MarchMovement march : marches) {
            if (march.getUnit().getPlayerName().equals(playerName)
                    && march.getUnit().getClass().equals(unitType)) {
                marches.remove(march);
                return;
            }
        }
    }

    public abstract boolean removeUnit(Unit unit);

    public abstract boolean removeWeakestUnit(String playerName);

    public void setAdjacentZones(List<String> adjacentZones) {
        this.adjacentZones = adjacentZones;
    }

    public void setCombatId(int combatId) {
        this.combatId = combatId;
    }

    /**
     * @param controlledByName
     *            the controlledByName to set
     */
    public void setControlledByName(String controlledByName) {
        this.controlledByName = controlledByName;
    }

    /**
     * @param marches
     *            the marches to set
     */
    public void setMarches(List<MarchMovement> marches) {
        this.marches = marches;
    }

    public void setName(String name) {
        this.name = name;
    }

    public abstract void removeAllUnits();

    /**
     * @param order
     *            the order to set
     */
    public void setOrder(Order order) {
        this.order = order;
        this.orderResolved = false;
    }

    public void setOrderResolved(boolean orderResolved) {
        this.orderResolved = orderResolved;
    }

    @Override
    public String toString() {
        if (StringHelper.isNotNullOrEmpty(name)) {
            return name;
        } else {
            return super.toString();
        }
    }

    public void cleanMarches() {
        List<MarchMovement> emptyMarches = new ArrayList<MarchMovement>();
        for (MarchMovement march : marches) {
            if (march.getFrom() == null || march.getTo() == null
                    || march.getUnit() == null) {
                emptyMarches.add(march);
            }
        }
        marches.removeAll(emptyMarches);
    }

    public abstract void cleanUnits();

    public boolean removeMarch(Unit unit) {
        for (MarchMovement march : marches) {
            if (march.getUnit().equals(unit)) {
                marches.remove(march);
                return true;
            }
        }
        return false;
    }

    public abstract Zone copyUnits();
}
