package client.model.zones;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlElementWrapper;

import client.Client;
import client.model.Ship;
import client.model.Unit;

/**
 * @author zggis
 */
public class Sea extends Zone {

    public static final String BAY_OF_ICE = "Bay of Ice";

    public static final String BLACKWATER_BAY = "Blackwater Bay";

    public static final String EAST_SUMMER_SEA = "East Summer Sea";

    public static final String IRONMANS_BAY = "Ironman's Bay";

    public static final String REDWYNE_STRAIGHTS = "Redwyne Straights";

    public static final String SEA_OF_DORNE = "Sea of Dorne";

    public static final String SHIPBREAKER_BAY = "Shipbreaker Bay";

    public static final String SUNSET_SEA = "Sunset Sea";

    public static final String THE_GOLDEN_SOUND = "The Golden Sound";

    public static final String THE_NARROW_SEA = "The Narrow Sea";

    public static final String THE_SHIVERING_SEA = "The Shivering Sea";

    public static final String WEST_SUMMER_SEA = "West Summer Sea";

    private List<Ship> ships;

    @Deprecated
    public Sea() {
        // Used by Marshaller only
    }

    public Sea(String name) {
        super();
        ships = new ArrayList<Ship>();
        setName(name);
    }

    public void addShip(Ship ship) {
        this.ships.add(ship);
    }

    @Override
    public int getAttackPower(Zone zone) {
        int attackPower = 0;
        if (zone instanceof Sea) {
            attackPower += (ships.size() * 1);
        } else if (zone instanceof Land) {
            Land defendingTerritory = (Land) zone;
            if (defendingTerritory.isPort()) {
                attackPower += (ships.size() * 1);
            }
        }
        return attackPower;
    }

    @XmlElementWrapper
    public List<Ship> getShips() {
        return ships;
    }

    /**
     * @param ships
     *            the ships to set
     */
    public void setShips(List<Ship> ships) {
        this.ships = ships;
    }

    @Override
    public List<Unit> getUnits() {
        List<Unit> units = new ArrayList<Unit>();
        units.addAll(getShips());
        return units;
    }

    @Override
    public int getDefensePower() {
        int defensePower = 0;
        defensePower = (ships.size() * 1);
        return defensePower;
    }

    @Override
    public boolean removeUnit(Unit unit) {
        if (unit instanceof Ship) {
            for (Ship ship : ships) {
                if (ship.getId() == unit.getId()) {
                    ships.remove(ship);
                    return true;
                }
            }
            return false;
        } else {
            Client.error(
                    "Non ship unit detected in Sea zone " + unit.getName());
            return false;
        }
    }

    @Override
    public void addUnit(Unit unit) {
        if (unit instanceof Ship) {
            ships.add((Ship) unit);
        }
    }

    @Override
    public boolean removeWeakestUnit(String playerName) {
        if (hasMarchingUnit(playerName, Ship.class)) {
            removeMarchingUnit(playerName, Ship.class);
            return true;
        } else if (hasAcclimatedUnit(playerName, Ship.class)) {
            for (Ship ship : ships) {
                if (ship.getPlayerName().equalsIgnoreCase(playerName)) {
                    ships.remove(ship);
                    return true;
                }
            }
        }
        return false;
    }

    @Override
    public void cleanUnits() {
        List<Ship> nullShips = new ArrayList<Ship>();
        for (Ship ship : ships) {
            if (ship == null) {
                nullShips.add(ship);
            }
        }
        ships.removeAll(nullShips);
    }

    @Override
    public void removeAllUnits() {
        ships.clear();
    }

    @Override
    public Zone copyUnits() {
        Sea sea = new Sea(getName());
        sea.setControlledByName(getControlledByName());
        sea.setShips(new ArrayList<Ship>(ships));
        return sea;
    }
}
