package client.model.zones;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlTransient;

import client.Board;
import client.model.Footman;
import client.model.Knight;
import client.model.Order;
import client.model.Player;
import client.model.Ship;
import client.model.SiegeEngine;
import client.model.Unit;

/**
 * @author zggis
 */
public class Land extends Zone {

    public static final String BLACKWATER = "Blackwater";

    public static final String CASTLE_BLACK = "Castle Black";

    public static final String CRACKLAW_POINT = "Crackclaw Point";

    public static final String DORNISH_MARCHES = "Dornish Marches";

    public static final String DRAGONSTONE = "Dragonstone";

    public static final String FLINTS_FINGER = "Flint's Finger";

    public static final String GREYWATER_WATCH = "Greywater Watch";

    public static final String HARRENHAL = "Harrenhal";

    public static final String HIGHGARDEN = "Highgarden";

    public static final String KARHOLD = "Karhold";

    public static final String KINGS_LANDING = "King's Landing";

    public static final String KINGSWOOD = "Kingswood";

    public static final String LANNISPORT = "Lannisport";

    public static final String MOAT_CALIN = "Moat Calin";

    public static final String OLDTOWN = "Oldtown";

    public static final String PRINCES_PASS = "Prince's Pass";

    public static final String PYKE = "Pyke";

    public static final String RIVERRUN = "Riverrun";

    public static final String SALT_SHORE = "Salt Shore";

    public static final String SEAGARD = "Seagard";

    public static final String SEAROADMARCHES = "Searoad Marches";

    public static final String STARFALL = "Starfall";

    public static final String STONEY_SEPT = "Stoney Sept";

    public static final String STORMS_END = "Storms End";

    public static final String SUNSPEAR = "Sunspear";

    public static final String THE_ARBOR = "The Arbor";

    public static final String THE_BONEWAY = "The Boneway";

    public static final String THE_EYRIE = "The Eyrie";

    public static final String THE_FINGERS = "The Fingers";

    public static final String THE_MOUNTAINS_OF_THE_MOON = "The Mountains of the Moon";

    public static final String THE_REACH = "The Reach";

    public static final String THE_STONY_SHORE = "The Stony Shore";

    public static final String THE_TWINS = "The Twins";

    public static final String THREE_TOWERS = "Three Towers";

    public static final String WHITE_HARBOR = "White Harbor";

    public static final String WIDOWS_WATCH = "Widow's Watch";

    public static final String WINTERFELL = "Winterfell";

    public static final String YRONWOOD = "Yronwood";

    private int barrels;

    private boolean castle;

    private int crowns;

    private List<Footman> footmen;

    private List<Knight> knights;

    private int neutralDefensePower;

    private int garrisonDefensePower;

    private boolean port;

    private List<Ship> ships;

    private List<SiegeEngine> siegeEngines;

    private boolean strongHold;

    @XmlTransient
    private int availableUpgradePower;

    @Deprecated
    public Land() {
        // Used by Marshaller only
    }

    public Land(String name) {
        super();
        footmen = new ArrayList<Footman>();
        knights = new ArrayList<Knight>();
        ships = new ArrayList<Ship>();
        siegeEngines = new ArrayList<SiegeEngine>();
        setName(name);
    }

    public void addFootman(Footman footman) {
        this.footmen.add(footman);
    }

    public void addKnight(Knight knight) {
        this.knights.add(knight);
    }

    public void addShip(Ship ship) {
        this.ships.add(ship);
    }

    public void addSiegeEngine(SiegeEngine siegeEngine) {
        this.siegeEngines.add(siegeEngine);
    }

    @Override
    public int getAttackPower(Zone zone) {
        int attackPower = 0;
        if (zone instanceof Land) {
            attackPower = (footmen.size() * 1) + (knights.size() * 2);
            Land defendingTerritory = (Land) zone;
            if (defendingTerritory.isCastle()
                    || defendingTerritory.isStrongHold()) {
                attackPower += (siegeEngines.size() * 4);
            }
        }
        return attackPower;
    }

    @Override
    public int getDefensePower() {
        int defensePower = 0;
        int defenseTokenPower = 0;
        if (getOrder() != null
                && getOrder().getType().equals(Order.DEFENSE_ORDER)) {
            defenseTokenPower = getOrder().getBonus();
        }
        defensePower = (footmen.size() * 1) + (knights.size() * 2)
                + garrisonDefensePower + neutralDefensePower
                + defenseTokenPower;
        return defensePower;
    }

    public int getBarrels() {
        return barrels;
    }

    public int getCrowns() {
        return crowns;
    }

    @XmlElementWrapper
    public List<Footman> getFootmen() {
        return footmen;
    }

    @XmlElementWrapper
    public List<Knight> getKnights() {
        return knights;
    }

    public int getNeutralDefensePower() {
        return neutralDefensePower;
    }

    @XmlElementWrapper
    public List<Ship> getShips() {
        return ships;
    }

    @XmlElementWrapper
    public List<SiegeEngine> getSiegeEngines() {
        return siegeEngines;
    }

    public boolean isCastle() {
        return castle;
    }

    public boolean isPort() {
        return port;
    }

    public boolean isStrongHold() {
        return strongHold;
    }

    public void setBarrels(int barrels) {
        this.barrels = barrels;
    }

    public void setCastle(boolean castle) {
        this.castle = castle;
    }

    /*
     * (non-Javadoc)
     * @see client.model.zones.Zone#setControlledByName(java.lang.String)
     */
    @Override
    public void setControlledByName(String newControlledByName) {
        Player newControlledBy = Player.getPlayer(newControlledByName);
        if (isCastle() || isStrongHold()) {
            if (getControlledBy() != null) {
                getControlledBy().reduceVictoryTrack();
            }
            if (newControlledBy != null) {
                newControlledBy.increaseVictoryTrack();
                if (newControlledBy.getVictoryTrack() >= 7) {
                    Board.endGame(newControlledBy);
                }
            }
        }
        super.setControlledByName(newControlledByName);
    }

    public void setCrowns(int crowns) {
        this.crowns = crowns;
    }

    /**
     * @param footmen
     *            the footmen to set
     */
    public void setFootmen(List<Footman> footmen) {
        this.footmen = footmen;
    }

    /**
     * @param knights
     *            the knights to set
     */
    public void setKnights(List<Knight> knights) {
        this.knights = knights;
    }

    public void setNeutralDefensePower(int power) {
        neutralDefensePower = power;
    }

    public void setPort(boolean port) {
        this.port = port;
    }

    /**
     * @param ships
     *            the ships to set
     */
    public void setShips(List<Ship> ships) {
        this.ships = ships;
    }

    /**
     * @param siegeEngines
     *            the siegeEngines to set
     */
    public void setSiegeEngines(List<SiegeEngine> siegeEngines) {
        this.siegeEngines = siegeEngines;
    }

    public void setStrongHold(boolean strongHold) {
        this.strongHold = strongHold;
    }

    @Override
    public List<Unit> getUnits() {
        List<Unit> result = new ArrayList<Unit>();
        result.addAll(getFootmen());
        result.addAll(getKnights());
        result.addAll(getShips());
        result.addAll(getSiegeEngines());
        return result;
    }

    @Override
    public boolean removeUnit(Unit unit) {
        if (unit instanceof Footman) {
            for (Footman footman : footmen) {
                if (footman.getId() == unit.getId()) {
                    footmen.remove(footman);
                    return true;
                }
            }
            return false;
        }
        if (unit instanceof Knight) {
            for (Knight knight : knights) {
                if (knight.getId() == unit.getId()) {
                    knights.remove(knight);
                    return true;
                }
            }
            return false;
        }
        if (unit instanceof SiegeEngine) {
            for (SiegeEngine siegeEngine : siegeEngines) {
                if (siegeEngine.getId() == unit.getId()) {
                    siegeEngines.remove(siegeEngine);
                    return true;
                }
            }
            return false;
        }
        if (unit instanceof Ship) {
            for (Ship ship : ships) {
                if (ship.getId() == unit.getId()) {
                    ships.remove(ship);
                    return true;
                }
            }
            return false;
        }
        return false;
    }

    @Override
    public void addUnit(Unit unit) {
        if (unit instanceof Footman) {
            footmen.add((Footman) unit);
        }
        if (unit instanceof Knight) {
            knights.add((Knight) unit);
        }
        if (unit instanceof SiegeEngine) {
            siegeEngines.add((SiegeEngine) unit);
        }
        if (unit instanceof Ship) {
            ships.add((Ship) unit);
        }
    }

    @Override
    public boolean removeWeakestUnit(String playerName) {
        if (hasMarchingUnit(playerName, Footman.class)) {
            removeMarchingUnit(playerName, Footman.class);
            return true;
        } else if (hasAcclimatedUnit(playerName, Footman.class)) {
            for (Footman footman : footmen) {
                if (footman.getPlayerName().equalsIgnoreCase(playerName)) {
                    footmen.remove(footman);
                    return true;
                }
            }
        } else if (hasMarchingUnit(playerName, Ship.class)) {
            removeMarchingUnit(playerName, Ship.class);
            return true;
        } else if (hasAcclimatedUnit(playerName, Ship.class)) {
            for (Ship ship : ships) {
                if (ship.getPlayerName().equalsIgnoreCase(playerName)) {
                    ships.remove(ship);
                    return true;
                }
            }
        } else if (hasMarchingUnit(playerName, Knight.class)) {
            removeMarchingUnit(playerName, Knight.class);
            return true;
        } else if (hasAcclimatedUnit(playerName, Knight.class)) {
            for (Knight knight : knights) {
                if (knight.getPlayerName().equalsIgnoreCase(playerName)) {
                    knights.remove(knight);
                    return true;
                }
            }
        } else if (hasMarchingUnit(playerName, SiegeEngine.class)) {
            removeMarchingUnit(playerName, SiegeEngine.class);
            return true;
        } else if (hasAcclimatedUnit(playerName, SiegeEngine.class)) {
            for (SiegeEngine siegeEngine : siegeEngines) {
                if (siegeEngine.getPlayerName().equalsIgnoreCase(playerName)) {
                    siegeEngines.remove(siegeEngine);
                    return true;
                }
            }
        }
        return false;
    }

    @Override
    public void cleanUnits() {
        List<Footman> nullFootmen = new ArrayList<Footman>();
        for (Footman footman : footmen) {
            if (footman == null) {
                nullFootmen.add(footman);
            }
        }
        footmen.removeAll(nullFootmen);
        List<Knight> nullKnights = new ArrayList<Knight>();
        for (Knight knight : knights) {
            if (knight == null) {
                nullKnights.add(knight);
            }
        }
        knights.removeAll(nullKnights);
        List<Ship> nullShips = new ArrayList<Ship>();
        for (Ship ship : ships) {
            if (ship == null) {
                nullShips.add(ship);
            }
        }
        ships.removeAll(nullShips);
        List<SiegeEngine> nullSiegeEngines = new ArrayList<SiegeEngine>();
        for (SiegeEngine siegeEngine : siegeEngines) {
            if (siegeEngine == null) {
                nullSiegeEngines.add(siegeEngine);
            }
        }
        ships.removeAll(nullSiegeEngines);
    }

    /**
     * @return the availableUpgradePower
     */
    public int getAvailableUpgradePower() {
        return availableUpgradePower;
    }

    /**
     * @param availableUpgradePower
     *            the availableUpgradePower to set
     */
    public void setAvailableUpgradePower(int availableUpgradePower) {
        this.availableUpgradePower = availableUpgradePower;
    }

    @Override
    public void removeAllUnits() {
        footmen.clear();
        knights.clear();
        siegeEngines.clear();
        ships.clear();
    }

    @Override
    public Zone copyUnits() {
        Land land = new Land(getName());
        land.setFootmen(new ArrayList<Footman>(footmen));
        land.setKnights(new ArrayList<Knight>(knights));
        land.setControlledByName(getControlledByName());
        land.setShips(new ArrayList<Ship>(ships));
        land.setPort(port);
        land.setSiegeEngines(new ArrayList<SiegeEngine>(siegeEngines));
        return land;
    }

    public int getGarrisonDefensePower() {
        return garrisonDefensePower;
    }

    public void setGarrisonDefensePower(int garrisonDefensePower) {
        this.garrisonDefensePower = garrisonDefensePower;
    }
}
