package client.model;

public class Ship extends Unit {

    @Deprecated
    public Ship() {
        // used by marshaller only
    }

    public Ship(String player) {
        setPlayerName(player);
        setId(GameContext.getMyGameContext().getUnitCount());
    }

    @Override
    public String getName() {
        return Player.getPlayer(getPlayerName()).getHouse() + " Ship "
                + getId();
    }
}
