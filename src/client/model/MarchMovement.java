package client.model;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlRootElement;

import client.JavaFXLauncher;
import client.model.zones.Land;
import client.model.zones.Sea;
import client.model.zones.Zone;

/**
 * @author zggis
 */
@XmlRootElement
public class MarchMovement {

    private String from;

    private String to;

    private Unit unit;

    private List<Zone> visitedZones;

    @Deprecated
    public MarchMovement() {
    }

    public MarchMovement(String from, String to, Unit unit) {
        this.from = from;
        this.to = to;
        this.unit = unit;
        visitedZones = new ArrayList<Zone>();
    }

    public boolean isValid(boolean retreating) {
        visitedZones.clear();
        if (getZone(from) instanceof Land && getZone(to) instanceof Land) {
            if (isAdjacentViaShip(getZone(from), getZone(to))) {
                return true;
            } else {
                if (!retreating) {
                    JavaFXLauncher
                            .showInformationDialog(
                                    "Invalid Movement",
                                    "No traversable path can be found between "
                                            + from
                                            + " and "
                                            + to
                                            + ". You must control or contest all Sea zones between them.");
                }
                return false;
            }
        } else if (getZone(from) instanceof Land && getZone(to) instanceof Sea) {
            if (unit instanceof Ship) {
                if (isAdjacent(getZone(from), getZone(to))) {
                    return true;
                } else {
                    if (!retreating) {
                        JavaFXLauncher.showInformationDialog(
                                "Invalid Movement", from
                                        + " is not adjacent to " + to);
                    }
                    return false;
                }
            } else {
                if (!retreating) {
                    JavaFXLauncher.showInformationDialog("Invalid Movement",
                            "You are attempting to drown " + unit.getName()
                                    + " in " + to);
                }
                return false;
            }
        } else if (getZone(from) instanceof Sea && getZone(to) instanceof Land) {
            if (((Land) getZone(to)).isPort()) {
                return isAdjacent(getZone(from), getZone(to));
            } else {
                if (!retreating) {
                    JavaFXLauncher.showInformationDialog("Invalid Movement",
                            "You are attempting to run " + unit.getName()
                                    + " ashore on " + to);
                }
                return false;
            }
        } else if (getZone(from) instanceof Sea && getZone(to) instanceof Sea) {
            if (isAdjacent(getZone(from), getZone(to))) {
                return true;
            } else {
                if (!retreating) {
                    JavaFXLauncher.showInformationDialog("Invalid Movement",
                            from + " is not adjacent to " + to);
                }
                return false;
            }
        }
        return false;
    }

    private Zone getZone(String zoneName) {
        GameContext context = GameContext.getMyGameContext();
        Zone zone = context.getZones().get(zoneName);
        return zone;
    }

    private boolean isAdjacentViaShip(Zone z1, Zone z2) {
        visitedZones.add(z1);
        if (isAdjacent(z1, z2)) {
            return true;
        }
        List<Sea> adjacentFriendlySeaZones = getAdjacentFriendlySeaZones(z1);
        for (Sea adjacentSeaZone : adjacentFriendlySeaZones) {
            if (isAdjacentViaShip(adjacentSeaZone, z2)) {
                return true;
            }
        }
        return false;
    }

    private List<Sea> getAdjacentFriendlySeaZones(Zone z1) {
        List<Sea> result = new ArrayList<Sea>();
        List<String> adjacentZones = z1.getAdjacentZones();
        for (String zoneName : adjacentZones) {
            Zone zone = GameContext.getMyGameContext().getZones().get(zoneName);
            if (zone instanceof Sea) {
                if (zone.getControlledBy() != null
                        && !visitedZones.contains(zone)
                        && (zone.getControlledBy().equals(Player.getPlayer(unit
                                .getPlayerName())))) {
                    result.add((Sea) zone);
                }
            }
        }
        return result;
    }

    // private boolean isContestedByMe(Zone zone) {
    // if (zone.getMarches() != null && !zone.getMarches().isEmpty()) {
    // List<MarchMovement> marches = zone.getMarches();
    // for (MarchMovement marchMovement : marches) {
    // if (marchMovement.getUnit().getPlayer().isMe()) {
    // return true;
    // }
    // }
    // }
    // return false;
    // }
    private boolean isAdjacent(Zone z1, Zone z2) {
        List<String> adjacentZones = z1.getAdjacentZones();
        for (String string : adjacentZones) {
            if (to.equals(string)) {
                return true;
            }
        }
        return false;
    }

    /**
     * @return the from
     */
    public String getFrom() {
        return from;
    }

    /**
     * @param from
     *            the from to set
     */
    public void setFrom(String from) {
        this.from = from;
    }

    /**
     * @return the to
     */
    public String getTo() {
        return to;
    }

    /**
     * @param to
     *            the to to set
     */
    public void setTo(String to) {
        this.to = to;
    }

    /**
     * @return the unit
     */
    public Unit getUnit() {
        return unit;
    }

    /**
     * @param unit
     *            the unit to set
     */
    public void setUnit(Unit unit) {
        this.unit = unit;
    }

    /*
     * (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        if (unit != null && from != null && to != null) {
            return unit.getName() + " -> " + from + " -> " + to;
        } else {
            return super.toString();
        }
    }
}
