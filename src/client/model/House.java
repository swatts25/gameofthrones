/**
 * 
 */
package client.model;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import client.Client;

/**
 * @author zggis
 */
public interface House {

    public static final String STARK = "Stark";

    public static final String GREYJOY = "Greyjoy";

    public static final String LANNISTER = "Lannister";

    public static final String BARATHEON = "Baratheon";

    public static final String TYRELL = "Tyrell";

    public static final String MARTELL = "Martel";

    // Add methods to return default setups for house
    // How many units/zones controlled
    // Positions on tracks etc
    public static void setHouses(List<Player> players) {
        int numPlayers = players.size();
        List<String> availableHouses = new ArrayList<String>();
        if (numPlayers >= 3) {
            availableHouses.add(STARK);
            availableHouses.add(LANNISTER);
            availableHouses.add(BARATHEON);
        }
        if (numPlayers >= 4) {
            availableHouses.add(GREYJOY);
        }
        if (numPlayers >= 5) {
            availableHouses.add(TYRELL);
        }
        if (numPlayers >= 6) {
            availableHouses.add(MARTELL);
        }
        if (numPlayers > 6) {
            Client.error("Invalid number of players: " + players.size());
            return;
        }
        Collections.shuffle(availableHouses);
        Collections.shuffle(players);
        for (int i = 0; i < numPlayers; i++) {
            players.get(i).setHouse(availableHouses.get(i));
        }
    }

    public static String getStyle(String house) {
        String style = "/forms/styles/defaultStyle.css";
        switch (house) {
            case TYRELL:
                style = "/forms/styles/houseTyrellStyle.css";
                break;
            case STARK:
                style = "/forms/styles/houseStarkStyle.css";
                break;
            case LANNISTER:
                style = "/forms/styles/houseLannisterStyle.css";
                break;
            case BARATHEON:
                style = "/forms/styles/houseBaratheonStyle.css";
                break;
            case MARTELL:
                style = "/forms/styles/houseMartelStyle.css";
                break;
            case GREYJOY:
                style = "/forms/styles/houseGreyjoyStyle.css";
                break;
            default:
                break;
        }
        return style;
    }
}
