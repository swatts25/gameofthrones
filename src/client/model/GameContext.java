/**
 * 
 */
package client.model;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;

import client.Combat;
import client.model.cards.AgeOneWesterosCard;
import client.model.cards.AgeThreeWesterosCard;
import client.model.cards.AgeTwoWesterosCard;
import client.model.cards.BaratheonHouseCard;
import client.model.cards.GreyjoyHouseCard;
import client.model.cards.LannisterHouseCard;
import client.model.cards.MartellHouseCard;
import client.model.cards.StarkHouseCard;
import client.model.cards.TyrellHouseCard;
import client.model.cards.WildlingCard;
import client.model.zones.Zone;

/**
 * @author zggis
 */
@XmlRootElement
public class GameContext {

    private List<Combat> combats;

    private List<CombatHistory> combatHistory;

    private List<AgeOneWesterosCard> ageOneDeck;
    
    private AgeOneWesterosCard currentAgeOneCard;

    private List<AgeThreeWesterosCard> ageThreeDeck;
    
    private AgeThreeWesterosCard currentAgeThreeCard;

    private List<AgeTwoWesterosCard> ageTwoDeck;
    
    private AgeTwoWesterosCard currentAgeTwoCard;

    private List<WildlingCard> wildlingDeck;

    private List<StarkHouseCard> starkHouseDeck;

    private List<BaratheonHouseCard> baratheonHouseDeck;

    private List<LannisterHouseCard> lannisterHouseDeck;

    private List<GreyjoyHouseCard> greyjoyHouseDeck;

    private List<TyrellHouseCard> tyrellHouseDeck;

    private List<MartellHouseCard> martellHouseDeck;

    private List<Player> players;

    private List<Turn> turns;

    private int wildlingCounter;

    private int gameid;

    private int contextid;

    private int unitCount;
    
    private String greatesVersion;

    private Map<String, Zone> zones;

    private static int combatId;

    private boolean valyrianBladeAvailable;

    private static GameContext myGameContext;

    private TrackBiddingData biddingData;

    private boolean restrictConsolodateOrders;

    private boolean restrictDefenseOrders;

    private boolean restrictMarchPlusOneOrders;

    private boolean restrictRaidOrders;

    private boolean restrictSupportOrders;

    public static GameContext getMyGameContext() {
        return myGameContext;
    }

    public static void setMyGameContext(GameContext context) {
        myGameContext = context;
    }

    public GameContext() {
        turns = new ArrayList<Turn>();
        players = new ArrayList<Player>();
        combats = new ArrayList<Combat>();
        combatHistory = new ArrayList<CombatHistory>();
        greatesVersion = "1.0.0";
    }

    @XmlElementWrapper
    public List<AgeOneWesterosCard> getAgeOneDeck() {
        return ageOneDeck;
    }

    @XmlElementWrapper
    public List<AgeThreeWesterosCard> getAgeThreeDeck() {
        return ageThreeDeck;
    }

    @XmlElementWrapper
    public List<AgeTwoWesterosCard> getAgeTwoDeck() {
        return ageTwoDeck;
    }

    @XmlElementWrapper
    public List<StarkHouseCard> getStarkHouseDeck() {
        return starkHouseDeck;
    }

    @XmlElementWrapper
    public List<BaratheonHouseCard> getBaratheonHouseDeck() {
        return baratheonHouseDeck;
    }

    @XmlElementWrapper
    public List<LannisterHouseCard> getLannisterHouseDeck() {
        return lannisterHouseDeck;
    }

    @XmlElementWrapper
    public List<GreyjoyHouseCard> getGreyjoyHouseDeck() {
        return greyjoyHouseDeck;
    }

    @XmlElementWrapper
    public List<TyrellHouseCard> getTyrellHouseDeck() {
        return tyrellHouseDeck;
    }

    @XmlElementWrapper
    public List<MartellHouseCard> getMartellHouseDeck() {
        return martellHouseDeck;
    }

    @XmlElementWrapper
    public List<Player> getPlayers() {
        return players;
    }

    public int getWildlingCounter() {
        return wildlingCounter;
    }

    public void setWildlingDeck(List<WildlingCard> wildlingDeck) {
        this.wildlingDeck = wildlingDeck;
    }

    public void setStarkHouseDeck(List<StarkHouseCard> starkHouseDeck) {
        this.starkHouseDeck = starkHouseDeck;
    }

    public void setBaratheonHouseDeck(
            List<BaratheonHouseCard> baratheonHouseDeck) {
        this.baratheonHouseDeck = baratheonHouseDeck;
    }

    public List<WildlingCard> getWildlingDeck() {
        return wildlingDeck;
    }

    @XmlElementWrapper
    public Map<String, Zone> getZones() {
        return zones;
    }

    public void setPlayers(List<Player> players) {
        this.players = players;
    }

    public void setWildlingCounter(int wildlingCounter) {
        this.wildlingCounter = wildlingCounter;
    }

    public void setZones(Map<String, Zone> zones) {
        this.zones = zones;
    }

    public void setAgeOneDeck(List<AgeOneWesterosCard> ageOneDeck) {
        this.ageOneDeck = ageOneDeck;
    }

    public void setAgeThreeDeck(List<AgeThreeWesterosCard> ageThreeDeck) {
        this.ageThreeDeck = ageThreeDeck;
    }

    public void setAgeTwoDeck(List<AgeTwoWesterosCard> ageTwoDeck) {
        this.ageTwoDeck = ageTwoDeck;
    }

    public void setLannisterHouseDeck(
            List<LannisterHouseCard> lannisterHouseDeck) {
        this.lannisterHouseDeck = lannisterHouseDeck;
    }

    public void setGreyjoyHouseDeck(List<GreyjoyHouseCard> greyjoyHouseDeck) {
        this.greyjoyHouseDeck = greyjoyHouseDeck;
    }

    public void setTyrellHouseDeck(List<TyrellHouseCard> tyrellHouseDeck) {
        this.tyrellHouseDeck = tyrellHouseDeck;
    }

    public void setMartellHouseDeck(List<MartellHouseCard> martellHouseDeck) {
        this.martellHouseDeck = martellHouseDeck;
    }

    public int getGameid() {
        return gameid;
    }

    public void setGameid(int gameid) {
        this.gameid = gameid;
    }

    public int getContextid() {
        return contextid;
    }

    public void setContextid(int contextid) {
        this.contextid = contextid;
    }

    @XmlElementWrapper
    public List<Turn> getTurns() {
        return turns;
    }

    public void setTurns(List<Turn> turns) {
        this.turns = turns;
    }

    public static int getNextCombatId() {
        return combatId++;
    }

    public int getUnitCount() {
        return ++unitCount;
    }

    public void setUnitCount(int unitCount) {
        this.unitCount = unitCount;
    }

    @XmlElementWrapper
    public List<Combat> getCombats() {
        return combats;
    }

    public void setCombats(List<Combat> combats) {
        this.combats = combats;
    }

    @XmlElementWrapper
    public List<CombatHistory> getCombatHistory() {
        return combatHistory;
    }

    public void setCombatHistory(List<CombatHistory> combatHistory) {
        this.combatHistory = combatHistory;
    }

    public boolean isValyrianBladeAvailable() {
        return Boolean.TRUE.equals(valyrianBladeAvailable);
    }

    public void setValyrianBladeAvailable(boolean valyrianBladeAvailable) {
        this.valyrianBladeAvailable = valyrianBladeAvailable;
    }

    public TrackBiddingData getBiddingData() {
        return biddingData;
    }

    public void setBiddingData(TrackBiddingData biddingData) {
        this.biddingData = biddingData;
    }

    /**
     * @return the restrictConsolodateOrders
     */
    public boolean isRestrictConsolodateOrders() {
        return Boolean.TRUE.equals(restrictConsolodateOrders);
    }

    /**
     * @param restrictConsolodateOrders
     *            the restrictConsolodateOrders to set
     */
    public void setRestrictConsolodateOrders(boolean restrictConsolodateOrders) {
        this.restrictConsolodateOrders = restrictConsolodateOrders;
    }

    /**
     * @return the restrictDefenseOrders
     */
    public boolean isRestrictDefenseOrders() {
        return Boolean.TRUE.equals(restrictDefenseOrders);
    }

    /**
     * @param restrictDefenseOrders
     *            the restrictDefenseOrders to set
     */
    public void setRestrictDefenseOrders(boolean restrictDefenseOrders) {
        this.restrictDefenseOrders = restrictDefenseOrders;
    }

    /**
     * @return the restrictMarchPlusOneOrders
     */
    public boolean isRestrictMarchPlusOneOrders() {
        return Boolean.TRUE.equals(restrictMarchPlusOneOrders);
    }

    /**
     * @param restrictMarchPlusOneOrders
     *            the restrictMarchPlusOneOrders to set
     */
    public void setRestrictMarchPlusOneOrders(boolean restrictMarchPlusOneOrders) {
        this.restrictMarchPlusOneOrders = restrictMarchPlusOneOrders;
    }

    /**
     * @return the restrictRaidOrders
     */
    public boolean isRestrictRaidOrders() {
        return Boolean.TRUE.equals(restrictRaidOrders);
    }

    /**
     * @param restrictRaidOrders
     *            the restrictRaidOrders to set
     */
    public void setRestrictRaidOrders(boolean restrictRaidOrders) {
        this.restrictRaidOrders = restrictRaidOrders;
    }

    /**
     * @return the restrictSupportOrders
     */
    public boolean isRestrictSupportOrders() {
        return Boolean.TRUE.equals(restrictSupportOrders);
    }

    /**
     * @param restrictSupportOrders
     *            the restrictSupportOrders to set
     */
    public void setRestrictSupportOrders(boolean restrictSupportOrders) {
        this.restrictSupportOrders = restrictSupportOrders;
    }

    
    /**
     * @return the currentAgeOneCard
     */
    public AgeOneWesterosCard getCurrentAgeOneCard() {
        return currentAgeOneCard;
    }

    
    /**
     * @param currentAgeOneCard the currentAgeOneCard to set
     */
    public void setCurrentAgeOneCard(AgeOneWesterosCard currentAgeOneCard) {
        this.currentAgeOneCard = currentAgeOneCard;
    }

    
    /**
     * @return the currentAgeThreeCard
     */
    public AgeThreeWesterosCard getCurrentAgeThreeCard() {
        return currentAgeThreeCard;
    }

    
    /**
     * @param currentAgeThreeCard the currentAgeThreeCard to set
     */
    public void setCurrentAgeThreeCard(AgeThreeWesterosCard currentAgeThreeCard) {
        this.currentAgeThreeCard = currentAgeThreeCard;
    }

    
    /**
     * @return the currentAgeTwoCard
     */
    public AgeTwoWesterosCard getCurrentAgeTwoCard() {
        return currentAgeTwoCard;
    }

    
    /**
     * @param currentAgeTwoCard the currentAgeTwoCard to set
     */
    public void setCurrentAgeTwoCard(AgeTwoWesterosCard currentAgeTwoCard) {
        this.currentAgeTwoCard = currentAgeTwoCard;
    }

    public String getGreatesVersion() {
        return greatesVersion;
    }

    public void setGreatesVersion(String greatesVersion) {
        this.greatesVersion = greatesVersion;
    }
}
