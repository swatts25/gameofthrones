package client.model;

public class SiegeEngine extends Unit {

    @Deprecated
    public SiegeEngine() {
        // used by marshaller
    }

    public SiegeEngine(String player) {
        setPlayerName(player);
        setId(GameContext.getMyGameContext().getUnitCount());
    }

    @Override
    public String getName() {
        return Player.getPlayer(getPlayerName()).getHouse() + " Siege Engine "
                + getId();
    }
}
