/**
 * 
 */
package client.model;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlRootElement;

import client.model.zones.Land;
import client.model.zones.Zone;

/**
 * @author zggis
 */
@XmlRootElement
public class Player {

    private static String myName;

    public static String getMyHouse() {
        if (GameContext.getMyGameContext() != null) {
            List<Player> players = GameContext.getMyGameContext().getPlayers();
            for (Player player : players) {
                if (player.isMe()) {
                    return player.getHouse();
                }
            }
        }
        return null;
    }

    public static String getMyName() {
        return myName;
    }

    public static void setMyName(String name) {
        myName = name;
    }

    private String emailAddress;

    private int fieldomsTrack;

    private int footmenControlled;
    
    private int id;

    private String house;

    private int influence;

    private int ironThroneTrack;

    private int kingsCourtTrack;

    private int knightsControlled;

    private String name;

    private int shipsControlled;

    private int siegeEnginesControlled;

    private int supplyTrack;

    private int victoryTrack;

    public Player() {
        id=-1;
        victoryTrack = 0;
        supplyTrack = 0;
        kingsCourtTrack = 10;
        ironThroneTrack = 10;
        shipsControlled = 0;
        siegeEnginesControlled = 0;
        footmenControlled = 0;
        knightsControlled = 0;
    }

    public Player(String name) {
        this.name = name;
        id=-1;
        victoryTrack = 0;
        supplyTrack = 0;
        kingsCourtTrack = 10;
        ironThroneTrack = 10;
        shipsControlled = 0;
        siegeEnginesControlled = 0;
        footmenControlled = 0;
        knightsControlled = 0;
    }

    /*
     * (non-Javadoc)
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object player) {
        if (player != null && name != null) {
            if (player instanceof Player) {
                if (name.equalsIgnoreCase(((Player) player).getName())) {
                    return true;
                } else {
                    return false;
                }
            } else if (player instanceof String) {
                return ((String) player).equalsIgnoreCase(name);
            }
        }
        return super.equals(player);
    }

    public int getCrowns() {
        List<Zone> zones = new ArrayList<Zone>(GameContext.getMyGameContext()
                .getZones().values());
        int crowns = 0;
        for (Zone zone : zones) {
            if (zone.getControlledBy().getName().equalsIgnoreCase(name)) {
                if (zone instanceof Land) {
                    crowns += ((Land) zone).getCrowns();
                }
            }
        }
        return crowns;
    }

    public String getEmailAddress() {
        return emailAddress;
    }

    public int getFieldomsTrack() {
        return fieldomsTrack;
    }

    /**
     * @return the footmenControlled
     */
    public int getFootmenControlled() {
        return footmenControlled;
    }

    public String getHouse() {
        return house;
    }

    public int getInfluence() {
        return influence;
    }

    public int getIronThroneTrack() {
        return ironThroneTrack;
    }

    public int getKingsCourtTrack() {
        return kingsCourtTrack;
    }

    /**
     * @return the knightsControlled
     */
    public int getKnightsControlled() {
        return knightsControlled;
    }

    public String getName() {
        return name;
    }

    /**
     * @return the shipsControlled
     */
    public int getShipsControlled() {
        return shipsControlled;
    }

    /**
     * @return the siegeEnginesControlled
     */
    public int getSiegeEnginesControlled() {
        return siegeEnginesControlled;
    }

    public int getSupplyTrack() {
        return supplyTrack;
    }

    public int getVictoryTrack() {
        return victoryTrack;
    }

    public void increaseVictoryTrack() {
        victoryTrack++;
    }

    public boolean isMe() {
        return name.equalsIgnoreCase(myName);
    }

    public void reduceVictoryTrack() {
        victoryTrack--;
    }

    public void setEmailAddress(String emailAddress) {
        this.emailAddress = emailAddress;
    }

    public void setFieldomsTrack(int fieldomsTrack) {
        this.fieldomsTrack = fieldomsTrack;
    }

    /**
     * @param footmenControlled
     *            the footmenControlled to set
     */
    public void setFootmenControlled(int footmenControlled) {
        this.footmenControlled = footmenControlled;
    }

    public void setHouse(String house) {
        this.house = house;
    }

    public void setInfluence(int influence) {
        this.influence = influence;
    }

    public void setIronThroneTrack(int ironThroneTrack) {
        this.ironThroneTrack = ironThroneTrack;
    }

    public void setKingsCourtTrack(int kingsCourtTrack) {
        this.kingsCourtTrack = kingsCourtTrack;
    }

    /**
     * @param knightsControlled
     *            the knightsControlled to set
     */
    public void setKnightsControlled(int knightsControlled) {
        this.knightsControlled = knightsControlled;
    }

    public void setName(String name) {
        this.name = name;
    }

    /**
     * @param shipsControlled
     *            the shipsControlled to set
     */
    public void setShipsControlled(int shipControlled) {
        this.shipsControlled = shipControlled;
    }

    /**
     * @param siegeEnginesControlled
     *            the siegeEnginesControlled to set
     */
    public void setSiegeEnginesControlled(int siegeEnginesControlled) {
        this.siegeEnginesControlled = siegeEnginesControlled;
    }

    public void setSupplyTrack(int supplyTrack) {
        this.supplyTrack = supplyTrack;
    }

    public void setVictoryTrack(int victoryTrack) {
        this.victoryTrack = victoryTrack;
    }

    /*
     * (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return name + " " + house;
    }

    @Deprecated
    public static Player getPlayer(String playerName) {
        if(playerName==null){
            return null;
        }
        GameContext context = GameContext.getMyGameContext();
        if (context != null) {
            for (Player player : context.getPlayers()) {
                if (player.getName().equalsIgnoreCase(playerName)) {
                    return player;
                }
            }
        }
        return null;
    }
    
    public static Player getPlayer(int playerId) {
        GameContext context = GameContext.getMyGameContext();
        if (context != null) {
            for (Player player : context.getPlayers()) {
                if (player.getId() == playerId) {
                    return player;
                }
            }
        }
        return null;
    }

    public static Player getHolderOfTheIronThrone() {
        GameContext context = GameContext.getMyGameContext();
        int lowestTrack = 10;
        Player holderOfTheThrone = null;
        for (Player player : context.getPlayers()) {
            if (player.getIronThroneTrack() < lowestTrack) {
                lowestTrack = player.getIronThroneTrack();
                holderOfTheThrone = player;
            }
        }
        return holderOfTheThrone;
    }

    public static Player getHolderOfTheRaven() {
        GameContext context = GameContext.getMyGameContext();
        int lowestTrack = 10;
        Player holderOfTheRaven = null;
        for (Player player : context.getPlayers()) {
            if (player.getKingsCourtTrack() < lowestTrack) {
                lowestTrack = player.getKingsCourtTrack();
                holderOfTheRaven = player;
            }
        }
        return holderOfTheRaven;
    }
    
    public static Player getHolderOfTheValyrianBlade() {
        GameContext context = GameContext.getMyGameContext();
        int lowestTrack = 10;
        Player holderOfTheValyrianBlade = null;
        for (Player player : context.getPlayers()) {
            if (player.getFieldomsTrack() < lowestTrack) {
                lowestTrack = player.getFieldomsTrack();
                holderOfTheValyrianBlade = player;
            }
        }
        return holderOfTheValyrianBlade;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}
