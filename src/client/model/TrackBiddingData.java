package client.model;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class TrackBiddingData {

    private Map<String, int[]> bidsMap;

    private Map<String, ArrayList<String>> playerOrderMap;

    public TrackBiddingData() {
        bidsMap = new HashMap<String, int[]>();
        playerOrderMap = new HashMap<String, ArrayList<String>>();
    }

    /**
     * @return the bidsMap
     */
    public Map<String, int[]> getBidsMap() {
        return bidsMap;
    }

    /**
     * @param bidsMap
     *            the bidsMap to set
     */
    public void setBidsMap(Map<String, int[]> bidsMap) {
        this.bidsMap = bidsMap;
    }

    /**
     * @return the playerOrderMap
     */
    public Map<String, ArrayList<String>> getPlayerOrderMap() {
        return playerOrderMap;
    }

    /**
     * @param playerOrderMap
     *            the playerOrderMap to set
     */
    public void setPlayerOrderMap(Map<String, ArrayList<String>> playerOrderMap) {
        this.playerOrderMap = playerOrderMap;
    }
}
