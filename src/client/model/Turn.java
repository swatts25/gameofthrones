package client.model;

import javax.xml.bind.annotation.XmlRootElement;

import client.Client;
import client.Combat;
import client.model.cards.BaratheonHouseCard;
import client.model.cards.GreyjoyHouseCard;
import client.model.cards.LannisterHouseCard;
import client.model.cards.MartellHouseCard;
import client.model.cards.TyrellHouseCard;

/**
 * @author zggis
 */
@XmlRootElement
public class Turn {

    public static final String AERON_DAMPHAIR = GreyjoyHouseCard.AERON_DAMPHAIR;

    public static final String CERSEI_LANNISTER = LannisterHouseCard.CERSEI_LANNISTER;

    public static final String CHOOSE_HOUSE_CARD = "Choose House Card";

    public static final String DORAN_MARTELL = MartellHouseCard.DORAN_MARTELL;

    public static final String MACE_TYRELL = TyrellHouseCard.MACE_TYRELL;

    public static final String PATCHFACE = BaratheonHouseCard.PATCHFACE;

    public static final String PLANNING_PHASE = "Planning Phase";

    public static final String QUEEN_OF_THORNS = TyrellHouseCard.QUEEN_OF_THORNS;

    public static final String RAVEN = "Raven Phase";

    public static final String RENLY_BARATHEON = BaratheonHouseCard.RENLY_BARATHEON;

    public static final String RESOLVE_MARCH_ORDER = "Resolve March Order";

    public static final String RESOLVE_RAID_ORDER = "Resolve Raid Order";

    public static final String TYRION_LANNISTER_CARDHOLDER = LannisterHouseCard.TYRION_LANNISTER
            + "_CARDHOLDER";

    public static final String TYRION_LANNISTER_OPPONENT = LannisterHouseCard.TYRION_LANNISTER
            + "_OPPONENT";

    public static final String RETREAT = "Retreat";

    public static final String VALYRIAN_BLADE = "Valyrian Blade";

    public static final String A_THRONE_OF_BLADES = "A Throne of Blades";

    public static final String RECTIFY_SUPPLY = "Rectify Supply";

    public static final String MUSTER = "Muster";

    public static final String INFLUENCE_TRACK_BIDDING = "Influence Track Bidding";

    public static final String DARK_WINGS_DARK_WORDS = "Dark Wings Dark Words";

    public static final String PUT_TO_THE_SWORD = "Put to the Sword";

    public static final String GAME_OVER = "Game Over";

    private long combatId;

    private int playerId;

    private String type;

    @Deprecated
    public Turn() {
        // Used by marshaller only
    }

    public Turn(Player player) {
        setPlayerId(player.getId());
    }

    public Player getPlayer() {
        return Player.getPlayer(playerId);
    }

    /**
     * @return the playerName
     */
    public String getPlayerName() {
        return getPlayer().getName();
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof Turn) {
            Turn turn = (Turn) obj;
            if (turn.getType().equals(type) && turn.getPlayerId() == playerId) {
                return true;
            }
        }
        return super.equals(obj);
    }

    public long getCombatId() {
        return combatId;
    }

    @Deprecated
    public Combat getCombat() {
        GameContext context = GameContext.getMyGameContext();
        if (context != null) {
            for (Combat combat : context.getCombats()) {
                if (combat.getId() == combatId) {
                    return combat;
                }
            }
        }
        Client.error("Combat id: " + combatId + " not found in context");
        return null;
    }

    public void setCombatId(long combatId) {
        this.combatId = combatId;
    }

    public int getPlayerId() {
        return playerId;
    }

    public void setPlayerId(int playerId) {
        this.playerId = playerId;
    }
}