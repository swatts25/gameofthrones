package client.model;

import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;

@XmlSeeAlso({ Footman.class, Knight.class, SiegeEngine.class, Ship.class })
@XmlType
public abstract class Unit {

    private String PlayerName;

    @XmlTransient
    private boolean combine;

    private boolean routed;

    private int id;

    public Unit() {
        id = 0;
        setRouted(false);
    }

    public String getName() {
        return Player.getPlayer(PlayerName).getHouse() + " Unit";
    }

    /**
     * @return the id
     */
    public int getId() {
        return id;
    }

    /**
     * @param id
     *            the id to set
     */
    public void setId(int id) {
        this.id = id;
    }

    public boolean isRouted() {
        return routed;
    }

    public void setRouted(boolean routed) {
        this.routed = routed;
    }

    public String getPlayerName() {
        return PlayerName;
    }

    public void setPlayerName(String playerName) {
        PlayerName = playerName;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof Unit) {
            return id == ((Unit) obj).getId();
        }
        return super.equals(obj);
    }

    /**
     * @return the combine
     */
    public boolean isCombine() {
        return combine;
    }

    /**
     * @param combine
     *            the combine to set
     */
    public void setCombine(boolean combine) {
        this.combine = combine;
    }

    /*
     * (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        if (this instanceof Footman) {
            return "Footman (cost 1)";
        } else if (this instanceof Knight) {
            return "Knight (cost 2)";
        } else if (this instanceof Ship) {
            return "Ship (cost 1)";
        } else {
            return super.toString();
        }
    }
}
