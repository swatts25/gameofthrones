package client;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import client.model.GameContext;
import client.model.House;
import client.model.MarchMovement;
import client.model.Player;
import client.model.Unit;
import client.model.zones.Land;
import client.model.zones.Sea;
import client.model.zones.Zone;

/**
 * @author zggis
 */
public class ResolveMarchOrder {

    private Zone marchingZone;

    private List<MarchMovement> myMarches;

    private GameContext context;

    private String marchingPlayerName;

    private int numUnitsToDestroy;

    public ResolveMarchOrder(GameContext context, Zone marchingZone,
            List<MarchMovement> marches, String marchingPlayerName) {
        this.marchingZone = marchingZone;
        this.myMarches = marches;
        this.context = context;
        this.marchingPlayerName = marchingPlayerName;
    }

    ResolveMarchOrder(GameContext context, String marchingPlayerName) {
        this.context = context;
        this.marchingPlayerName = marchingPlayerName;
    }

    public boolean validateMarchingOrder() {
        for (MarchMovement march : myMarches) {
            if (march.getFrom() != null && march.getTo() != null
                    && march.getUnit() != null) {
                if (!march.isValid(false)) {
                    return false;
                }
            }
        }
        return true;
    }

    public boolean isHomeZone() {
        boolean leavingInfluence = false;
        switch (marchingZone.getName()) {
            case Land.WINTERFELL:
                if (marchingZone.getControlledBy().getHouse()
                        .equals(House.STARK)) {
                    leavingInfluence = true;
                }
                break;
            case Land.LANNISPORT:
                if (marchingZone.getControlledBy().getHouse()
                        .equals(House.LANNISTER)) {
                    leavingInfluence = true;
                }
                break;
            case Land.PYKE:
                if (marchingZone.getControlledBy().getHouse()
                        .equals(House.GREYJOY)) {
                    leavingInfluence = true;
                }
                break;
            case Land.DRAGONSTONE:
                if (marchingZone.getControlledBy().getHouse()
                        .equals(House.BARATHEON)) {
                    leavingInfluence = true;
                }
                break;
            case Land.HIGHGARDEN:
                if (marchingZone.getControlledBy().getHouse()
                        .equals(House.TYRELL)) {
                    leavingInfluence = true;
                }
                break;
            default:
                break;
        }
        return leavingInfluence;
    }

    public boolean isOccupiedAfterMarch() {
        if (marchingZone.getUnits() != null
                && !marchingZone.getUnits().isEmpty()) {
            for (Unit unit : marchingZone.getUnits()) {
                if (unit.getPlayerName().equalsIgnoreCase(Player.getMyName())
                        && !isUnitMarching(unit)) {
                    return true;
                }
            }
        }
        return false;
    }

    public boolean isSeaZone() {
        return (marchingZone instanceof Sea);
    }

    private boolean isUnitMarching(Unit unit) {
        for (MarchMovement march : myMarches) {
            if (march.getUnit().equals(unit)) {
                return true;
            }
        }
        return false;
    }

    public void moveUnits() {
        List<String> conflictZones = new ArrayList<String>();
        for (MarchMovement march : myMarches) {
            if (march.getFrom() != null && march.getTo() != null
                    && march.getUnit() != null) {
                if (context.getZones().get(march.getTo())
                        .getControlledBy() != null
                        && context.getZones().get(march.getTo())
                                .getControlledBy()
                                .equals(march.getUnit().getPlayerName())) {
                    // Instant acclimate, since you control zone
                    context.getZones().get(march.getTo())
                            .addUnit(march.getUnit());
                } else {
                    // march units because the zone is not controlled by you
                    context.getZones().get(march.getTo()).getMarches()
                            .add(march);
                    if (!conflictZones.contains(march.getTo())) {
                        conflictZones.add(march.getTo());
                        Combat newCombat;
                        if (context.getZones().get(march.getTo())
                                .getControlledBy() != null) {
                            // Setup combat to take player controlled zone
                            newCombat = new Combat(march.getTo(),
                                    march.getUnit().getPlayerName(),
                                    context.getZones().get(march.getTo())
                                            .getControlledBy().getName(),
                                    march.getFrom());
                        } else {
                            // setup combat to take neutral zone
                            newCombat = new Combat(march.getTo(),
                                    march.getUnit().getPlayerName());
                        }
                        newCombat.setMarchingBonus(context.getZones()
                                .get(march.getFrom()).getOrder().getBonus());
                        context.getCombats().add(newCombat);
                    }
                }
                context.getZones().get(march.getTo())
                        .setCombatId(GameContext.getNextCombatId());
                marchingZone.removeUnit(march.getUnit());
            }
        }
        removeOrderTokens();
    }

    private void removeOrderTokens() {
        for (MarchMovement march : myMarches) {
            context.getZones().get(march.getFrom()).setOrder(null);
        }
    }

    public void retreatUnits() {
        for (MarchMovement march : myMarches) {
            if (march.getFrom() != null && march.getTo() != null
                    && march.getUnit() != null) {
                context.getZones().get(march.getTo()).addUnit(march.getUnit());
                march.getUnit().setRouted(true);
                context.getZones().get(march.getTo())
                        .setControlledByName(march.getUnit().getPlayerName());
                Client.getLogger().info("Retreated: " + march.getUnit().getName());
                if (!marchingZone.removeUnit(march.getUnit())) {
                    marchingZone.removeMarch(march.getUnit());
                }
            }
        }
    }

    public void returnUnits() {
        Map<String, Zone> zones = context.getZones();
        for (MarchMovement march : myMarches) {
            zones.get(march.getFrom()).addUnit(march.getUnit());
            List<MarchMovement> toMarches = zones.get(march.getTo())
                    .getMarches();
            for (MarchMovement marchMovement : toMarches) {
                if (marchMovement.getUnit().getId() == march.getUnit()
                        .getId()) {
                    toMarches.remove(marchMovement);
                    break;
                }
            }
        }
    }

    public void removeUnit(Unit unit) {
        for (MarchMovement march : myMarches) {
            if (march.getUnit().equals(unit)) {
                myMarches.remove(march);
                return;
            }
        }
    }

    public boolean validateSupplyLimitAfterMarch(boolean doNotDisplayPrompt) {
        int supply[];
        Player marchingPlayer = null;
        for (Player player : context.getPlayers()) {
            if (player.getName().equalsIgnoreCase(marchingPlayerName)) {
                marchingPlayer = player;
                break;
            }
        }
        if (marchingPlayer != null) {
            switch (marchingPlayer.getSupplyTrack()) {
                case 0:
                    supply = Board.SUPPLY_ZERO.clone();
                    break;
                case 1:
                    supply = Board.SUPPLY_ONE.clone();
                    break;
                case 2:
                    supply = Board.SUPPLY_TWO.clone();
                    break;
                case 3:
                    supply = Board.SUPPLY_THREE.clone();
                    break;
                case 4:
                    supply = Board.SUPPLY_FOUR.clone();
                    break;
                case 5:
                    supply = Board.SUPPLY_FIVE.clone();
                    break;
                case 6:
                    supply = Board.SUPPLY_SIX.clone();
                    break;
                default:
                    Client.error("Invalid supply track for "
                            + marchingPlayer.getName() + " "
                            + marchingPlayer.getSupplyTrack());
                    return false;
            }
            List<String> zonesValidated = new ArrayList<String>();
            List<String> portsValidated = new ArrayList<String>();
            for (MarchMovement march : myMarches) {
                Zone zone = context.getZones().get(march.getTo());
                if (!zonesValidated.contains(march.getTo())) {
                    int numUnitsInZone = Board.getNumUnitsInZone(
                            context.getZones(), marchingPlayer, march.getTo())
                            + getNumMarchingUnitsInRouteToZone(marchingPlayer,
                                    march.getTo());
                    if (numUnitsInZone > 1) {
                        boolean supplyFlagFound = false;
                        for (int i = 0; i < supply.length; i++) {
                            if (supply[i] == numUnitsInZone) {
                                supply[i] = 0;
                                supplyFlagFound = true;
                                break;
                            }
                        }
                        if (!supplyFlagFound) {
                            if (!doNotDisplayPrompt) {
                                JavaFXLauncher.showInformationDialog(
                                        "Insufficient Supply",
                                        "The movements you have selected cannot be complete due to your supply track limit");
                            }
                            numUnitsToDestroy = numUnitsInZone - Board
                                    .getLargestRemainingSupplyFlag(supply);
                            return false;
                        }
                        zonesValidated.add(march.getTo());
                    }
                }
                if (!portsValidated.contains(zone.getName())) {
                    if (zone instanceof Land && ((Land) zone).isPort()) {
                        int numShipsInPort = Board.getNumShipsInPort(
                                context.getZones(), marchingPlayer, march.getTo())
                                + getNumMarchingUnitsInRouteToZone(marchingPlayer,
                                        march.getTo());
                        if (numShipsInPort > 1) {
                            boolean supplyFlagFound = false;
                            for (int i = 0; i < supply.length; i++) {
                                if (supply[i] == numShipsInPort) {
                                    supply[i] = 0;
                                    supplyFlagFound = true;
                                    break;
                                }
                            }
                            if (!supplyFlagFound) {
                                if (!doNotDisplayPrompt) {
                                    JavaFXLauncher.showInformationDialog(
                                            "Insufficient Supply",
                                            "The movements you have selected cannot be complete due to your supply track limit");
                                }
                                numUnitsToDestroy = numShipsInPort - Board
                                        .getLargestRemainingSupplyFlag(supply);
                                return false;
                            }
                            portsValidated.add(zone.getName());
                        }
                    }
                }
            }
            return true;
        } else {
            Client.error(
                    "Player not found in context: " + marchingPlayerName);
            return false;
        }
    }

    public int getNumUnitsToDestroy() {
        return numUnitsToDestroy;
    }

    private int getNumMarchingUnitsInRouteToZone(Player player,
            String zoneName) {
        int result = 0;
        for (MarchMovement march : myMarches) {
            if (march.getTo().equals(zoneName) && march.getUnit()
                    .getPlayerName().equals(player.getName())) {
                result++;
            }
        }
        return result;
    }
}
