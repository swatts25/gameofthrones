package client;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

import utils.StringHelper;
import client.model.CombatHistory;
import client.model.Footman;
import client.model.GameContext;
import client.model.Knight;
import client.model.MarchMovement;
import client.model.Order;
import client.model.Player;
import client.model.Ship;
import client.model.SiegeEngine;
import client.model.Turn;
import client.model.Unit;
import client.model.cards.BaratheonHouseCard;
import client.model.cards.GreyjoyHouseCard;
import client.model.cards.HouseCard;
import client.model.cards.LannisterHouseCard;
import client.model.cards.MartellHouseCard;
import client.model.cards.StarkHouseCard;
import client.model.cards.TyrellHouseCard;
import client.model.zones.Land;
import client.model.zones.Zone;
import controllers.MainScreenPanelController;

@XmlRootElement
public class Combat {

    private boolean attackerAllowedToAcclimate;

    private boolean attackerHouseCardPlayed;

    private boolean attackerImmuneToCasualties;

    private HouseCard attackingHouseCard;

    private String attackingPlayerName;

    private boolean battleResolved;

    @XmlTransient
    private GameContext context;

    private boolean defenderAllowedToAcclimate;

    private boolean defenderHouseCardPlayed;

    private boolean defenderImmuneToCasualties;

    private HouseCard defendingHouseCard;

    private String defendingPlayerName;

    private String targetZoneName;

    private String victorName;

    private String loserName;

    private String log;

    private String playerUsingValyrianBlade;

    private String attackerMarchedFromZoneName;

    private long id;

    private int marchingBonus;

    public int getMarchingBonus() {
        return marchingBonus;
    }

    public String getLoserName() {
        return loserName;
    }

    public void setLoserName(String loserName) {
        this.loserName = loserName;
    }

    @Deprecated
    public Combat() {
        // TODO Auto-generated constructor stub
    }

    public Combat(String targetZoneName, String attackingPlayerName) {
        this.targetZoneName = targetZoneName;
        this.attackingPlayerName = attackingPlayerName;
        this.log = "";
        this.id = generateId();
    }

    public Combat(String targetZoneName, String attackingPlayerName,
            String defendingPlayerName, String attackerMarchedFromZone) {
        this.targetZoneName = targetZoneName;
        this.attackingPlayerName = attackingPlayerName;
        this.defendingPlayerName = defendingPlayerName;
        this.attackerImmuneToCasualties = false;
        this.defenderImmuneToCasualties = false;
        this.attackerHouseCardPlayed = false;
        this.defenderHouseCardPlayed = false;
        this.battleResolved = false;
        this.attackerAllowedToAcclimate = true;
        this.defenderAllowedToAcclimate = true;
        this.id = generateId();
        this.log = "";
        this.setAttackerMarchedFromZoneName(attackerMarchedFromZone);
    }

    private long generateId() {
        GameContext context = GameContext.getMyGameContext();
        if (context != null) {
            return context.getCombats().size() + 1;
        } else {
            Client.error("You have not set a game context, combat id cannot be generated.");
            return -1;
        }
    }

    private void acclimateUnits(Zone targetZone, String attackingPlayerName) {
        List<MarchMovement> acclimatedUnits = new ArrayList<MarchMovement>();
        for (MarchMovement march : targetZone.getMarches()) {
            if (march.getUnit().getPlayerName()
                    .equalsIgnoreCase(attackingPlayerName)) {
                targetZone.addUnit(march.getUnit());
                acclimatedUnits.add(march);
            }
        }
        targetZone.getMarches().removeAll(acclimatedUnits);
    }

    /**
     * @return the attackingHouseCard
     */
    public HouseCard getAttackingHouseCard() {
        return attackingHouseCard;
    }

    /**
     * @return the attackingPlayer
     */
    public String getAttackingPlayerName() {
        return attackingPlayerName;
    }

    public int getAttackingPlayerCombatPower(String player, Zone targetZone) {
        return getSupportedStrength(player, targetZone)
                + getMarchingUnitAttackPower(player, targetZone);
    }

    /**
     * @return the targetZoneName
     */
    public String getAttackingZoneName() {
        return targetZoneName;
    }

    // returns the combat power of the card from special ability only
    // adds additional attack power and icons to cards based on combat
    public int getCardSpecialAbilityCombatStrength(HouseCard card,
            String cardHolderName, HouseCard opponentCard, String opponentName) {
        if (card != null && opponentCard != null) {
            Zone targetZone = context.getZones().get(targetZoneName);
            switch (card.getName()) {
                case BaratheonHouseCard.SALLADHOR_SAAN:
                    // IF card holder is being supported by a ship, all non
                    // Baratheon ships reduce to 0
                    int opponentShipPower = 0;
                    boolean removeSupportBonus = false;
                    for (String adjacentZoneName : context.getZones()
                            .get(targetZoneName).getAdjacentZones()) {
                        Zone adjacentZone = context.getZones().get(
                                adjacentZoneName);
                        if (adjacentZone.getControlledBy() != null
                                && adjacentZone.getControlledBy().equals(
                                        opponentName)) {
                            if (adjacentZone.getOrder() != null
                                    && adjacentZone.getOrder().getType()
                                            .equals(Order.SUPPORT_ORDER)) {
                                for (Unit unit : adjacentZone.getUnits()) {
                                    if (unit instanceof Ship
                                            && unit.getPlayerName()
                                                    .equalsIgnoreCase(
                                                            opponentName)) {
                                        opponentShipPower++;
                                        if (adjacentZone.getOrder().isStar()
                                                && !removeSupportBonus) {
                                            opponentShipPower++;
                                            removeSupportBonus = true;
                                        }
                                    }
                                }
                            }
                        }
                        removeSupportBonus = false;
                    }
                    return opponentShipPower;
                case BaratheonHouseCard.SER_DAVOS_SEAWORTH:
                    // IF card holder's stannis card is in discard pile, add +1
                    // combat strength and sword icon to davos card
                    List<BaratheonHouseCard> deck = context
                            .getBaratheonHouseDeck();
                    for (HouseCard baratheonCard : deck) {
                        if (baratheonCard.getName().equals(
                                BaratheonHouseCard.STANNIS_BARATHEON)) {
                            return 0;
                        }
                    }
                    card.setCombatStrength(card.getCombatStrength() + 1);
                    return 0;
                case BaratheonHouseCard.STANNIS_BARATHEON:
                    // IF opponent is closer to iron throne on track, Stannis
                    // card gains +1 combat strength
                    if (getPlayer(opponentName).getIronThroneTrack() < getPlayer(
                            cardHolderName).getIronThroneTrack()) {
                        card.setCombatStrength(card.getCombatStrength() + 1);
                    }
                    return 0;
                case GreyjoyHouseCard.ASHA_GREYJOY:
                    // if card holder is NOT being supported in this combat, add
                    // 2 sword icons and 1 fortification icon to asha card
                    if (getSupportedStrength(cardHolderName, targetZone) == 0) {
                        card.setFortificationIcons(card.getFortificationIcons() + 1);
                        card.setSwordIcons(card.getSwordIcons() + 2);
                        addLog(card.getName()
                                + " gains 2 sword icons and 1 fortification icon");
                    }
                    return 0;
                case GreyjoyHouseCard.THEON_GREYJOY:
                    // if card holder is DEFENDING a zone with castle or strong
                    // hold, add +1 combat strength and sword icon to theon card
                    if (cardHolderName.equalsIgnoreCase(defendingPlayerName)) {
                        if (targetZone instanceof Land) {
                            Land targetLand = (Land) targetZone;
                            if (targetLand.isCastle()
                                    || targetLand.isStrongHold()) {
                                card.setSwordIcons(card.getSwordIcons() + 1);
                                card.setCombatStrength(card.getCombatStrength() + 1);
                                addLog(card.getName() + " gains 1 sword icon");
                            }
                        }
                    }
                    return 0;
                case GreyjoyHouseCard.BALON_GREYJOY:
                    // Opponents printed card strength is reduced to zero
                    return opponentCard.getCombatStrength();
                case GreyjoyHouseCard.VICTARION_GREYJOY:
                    // if card holder is ATTACKING all participating ships
                    // (including support) gain +1 (total of 2 instead of 1)
                    int cardHolderShipPower = 0;
                    if (cardHolderName.equalsIgnoreCase(attackingPlayerName)) {
                        // Add ships marched into targetZone
                        cardHolderShipPower += getUnitMarchingPower(
                                cardHolderName, targetZone, Ship.class);
                        // Add acclimated ships
                        cardHolderShipPower += getUnitAcclimatedPower(
                                cardHolderName, targetZone, Ship.class);
                        // Add ship support
                        cardHolderShipPower += getUnitSupportPower(
                                cardHolderName, targetZone, Ship.class);
                    }
                    return cardHolderShipPower;
                case LannisterHouseCard.SER_KEVAN_LANNISTER:
                    // If card holder is ATTACKING all participating footman
                    // (including support) gain +1 (total of 2 instead of 1)
                    int cardHolderFootmanPower = 0;
                    if (cardHolderName.equalsIgnoreCase(attackingPlayerName)) {
                        // Add footmen marched into targetZone
                        cardHolderFootmanPower += getUnitMarchingPower(
                                cardHolderName, targetZone, Footman.class);
                        // Add acclimated footmen
                        cardHolderFootmanPower += getUnitAcclimatedPower(
                                cardHolderName, targetZone, Footman.class);
                        // Add footmen support
                        cardHolderFootmanPower += getUnitSupportPower(
                                cardHolderName, targetZone, Footman.class);
                    }
                    return cardHolderFootmanPower;
                case MartellHouseCard.NYMERIA_SAND:
                    // If card holder is attacking, add sword icon to nymeria
                    // card
                    // If card holder is defending, add fortification icon to
                    // nymeria card
                    if (attackingPlayerName.equalsIgnoreCase(cardHolderName)) {
                        card.setSwordIcons(card.getSwordIcons() + 1);
                        addLog(card.getName() + " gains 1 sword icon");
                    } else {
                        card.setFortificationIcons(card.getFortificationIcons() + 1);
                        addLog(card.getName() + " gains 1 fortification icon");
                    }
                    return 0;
                case StarkHouseCard.CATELYN_STARK:
                    // if card holder has defense token in embattled area, its
                    // value is doubled
                    int defenseOrderBonus = 0;
                    if (targetZone.getOrder() != null
                            && targetZone.getOrder().getType()
                                    .equals(Order.DEFENSE_ORDER)
                            && targetZone.getControlledBy().getName()
                                    .equalsIgnoreCase(cardHolderName)) {
                        defenseOrderBonus = targetZone.getOrder().getBonus();
                    }
                    return defenseOrderBonus;
                default:
                    break;
            }
        }
        return 0;
    }

    @XmlTransient
    public GameContext getContext() {
        return context;
    }

    /**
     * @return the defendingHouseCard
     */
    public HouseCard getDefendingHouseCard() {
        return defendingHouseCard;
    }

    /**
     * @return the defendingPlayer
     */
    public String getDefendingPlayerName() {
        return defendingPlayerName;
    }

    public int getDefendingPlayerCombatPower(String player, Zone targetZone) {
        return getSupportedStrength(player, targetZone)
                + getZoneUnitDefensePower(player, targetZone);
    }

    public int getMarchingUnitAttackPower(String playerName, Zone targetZone) {
        int result = 0;
        if (targetZone.getMarches() != null
                && !targetZone.getMarches().isEmpty()) {
            result += marchingBonus;
            for (MarchMovement march : targetZone.getMarches()) {
                Unit unit = march.getUnit();
                if (unit.getPlayerName().equalsIgnoreCase(playerName)) {
                    if (unit instanceof Footman) {
                        result += 1;
                    } else if (unit instanceof Knight) {
                        result += 2;
                    } else if (unit instanceof Ship) {
                        Zone defendingZone = context.getZones().get(
                                targetZoneName);
                        if (defendingZone instanceof Land) {
                            Land defendingLand = (Land) defendingZone;
                            if (defendingLand.isPort()) {
                                result += 1;
                            }
                        } else {
                            result += 1;
                        }
                    } else if (unit instanceof SiegeEngine) {
                        Zone defendingZone = context.getZones().get(
                                targetZoneName);
                        if (defendingZone instanceof Land) {
                            Land defendingLand = (Land) defendingZone;
                            if (defendingLand.isCastle()
                                    || defendingLand.isStrongHold()) {
                                result += 4;
                            }
                        }
                    }
                }
            }
        }
        return result;
    }

    public Player getPlayer(String playerName) {
        for (Player player : context.getPlayers()) {
            if (player.getName().equalsIgnoreCase(playerName)) {
                return player;
            }
        }
        return null;
    }

    public int getSupportedStrength(String player, Zone targetZone) {
        int result = 0;
        for (String adjacentZoneName : targetZone.getAdjacentZones()) {
            Zone adjacentZone = context.getZones().get(adjacentZoneName);
            Order order = adjacentZone.getOrder();
            if (adjacentZone.getControlledBy() != null
                    && adjacentZone.getControlledBy().equals(player)
                    && order != null
                    && order.getType().equals(Order.SUPPORT_ORDER)) {
                result += getZoneUnitSupportPower(player, adjacentZone);
                if (order.isStar()) {
                    result++;
                }
            }
        }
        return result;
    }

    public String getTargetZoneName() {
        return targetZoneName;
    }

    private int getUnitAcclimatedPower(String cardHolder, Zone targetZone,
            Class<?> unitClass) {
        int acclimatedPower = 0;
        for (Unit unit : targetZone.getUnits()) {
            if (unit.getClass().equals(unitClass)
                    && unit.getPlayerName().equalsIgnoreCase(cardHolder)
                    && !unit.isRouted()) {
                acclimatedPower++;
            }
        }
        return acclimatedPower;
    }

    private int getUnitMarchingPower(String cardHolderName, Zone targetZone,
            Class<?> unitClass) {
        int marchingPower = 0;
        for (MarchMovement march : targetZone.getMarches()) {
            if (march.getUnit() instanceof Ship
                    && march.getUnit().getPlayerName()
                            .equalsIgnoreCase(cardHolderName)) {
                marchingPower++;
            }
        }
        return marchingPower;
    }

    private int getUnitSupportPower(String cardHolderName, Zone targetZone,
            Class<?> unitClass) {
        int cardHolderUnitSupportPower = 0;
        for (String adjacentZoneName : targetZone.getAdjacentZones()) {
            Zone adjacentZone = context.getZones().get(adjacentZoneName);
            if (adjacentZone.getControlledBy() != null
                    && adjacentZone.getControlledBy().equals(cardHolderName)) {
                if (adjacentZone.getOrder().getType()
                        .equals(Order.SUPPORT_ORDER)) {
                    for (Unit unit : adjacentZone.getUnits()) {
                        if (unit.getClass().equals(unitClass)
                                && unit.getPlayerName().equalsIgnoreCase(
                                        cardHolderName) && !unit.isRouted()) {
                            cardHolderUnitSupportPower++;
                        }
                    }
                }
            }
        }
        return cardHolderUnitSupportPower;
    }

    public String getVictor() {
        return victorName;
    }

    /**
     * @return the victorName
     */
    public String getVictorName() {
        return victorName;
    }

    public int getZoneUnitDefensePower(String playerName, Zone targetZone) {
        int result = 0;
        // Add acclimated units
        for (Unit unit : targetZone.getUnits()) {
            if (unit.getPlayerName().equalsIgnoreCase(playerName)
                    && !unit.isRouted()) {
                if (unit instanceof Footman) {
                    result += 1;
                } else if (unit instanceof Knight) {
                    result += 2;
                } else if (unit instanceof Ship) {
                    result += 1;
                }
            }
        }
        // Add units marched into zone this turn
        List<Order> marchingBonusGained = new ArrayList<Order>();
        for (MarchMovement march : targetZone.getMarches()) {
            Zone fromZone = context.getZones().get(march.getFrom());
            Order fromZoneOrder = fromZone.getOrder();
            if (fromZoneOrder.getType().equals(Order.MARCH_ORDER)
                    && !marchingBonusGained.contains(fromZoneOrder)
                    && march.getUnit().getPlayerName()
                            .equalsIgnoreCase(playerName)) {
                result += fromZoneOrder.getBonus();
                marchingBonusGained.add(fromZoneOrder);
            }
            Unit unit = march.getUnit();
            if (unit.getPlayerName().equalsIgnoreCase(playerName)) {
                if (unit instanceof Footman) {
                    result += 1;
                } else if (unit instanceof Knight) {
                    result += 2;
                } else if (unit instanceof Ship) {
                    result += 1;
                }
            }
        }
        // Add garrison defense if it exists
        if (targetZone instanceof Land) {
            result += ((Land) targetZone).getGarrisonDefensePower();
        }
        // Add defense order bonus
        if (targetZone.getOrder().getType().equals(Order.DEFENSE_ORDER)) {
            result += targetZone.getOrder().getBonus();
        }
        return result;
    }

    private int getZoneUnitSupportPower(String player, Zone adjacentZone) {
        int result = 0;
        if (adjacentZone.getUnits() != null
                && !adjacentZone.getUnits().isEmpty()) {
            for (Unit unit : adjacentZone.getUnits()) {
                if (unit.getPlayerName().equalsIgnoreCase(player)
                        && !unit.isRouted()) {
                    if (unit instanceof Footman) {
                        result += 1;
                    } else if (unit instanceof Knight) {
                        result += 2;
                    } else if (unit instanceof Ship) {
                        result += 1;
                    } else if (unit instanceof SiegeEngine) {
                        Zone defendingZone = context.getZones().get(
                                targetZoneName);
                        if (defendingZone instanceof Land) {
                            Land defendingLand = (Land) defendingZone;
                            if (defendingLand.isCastle()
                                    || defendingLand.isStrongHold()) {
                                result += 4;
                            }
                        }
                    }
                }
            }
        }
        return result;
    }

    public void handlePostCombatCardSpecialAbility(HouseCard card,
            String cardHolderName, HouseCard opponentCard,
            String attackingPlayerName, MainScreenPanelController mainscreen) {
        if (card != null && opponentCard != null) {
            Turn nextTurn = null;
            switch (card.getName()) {
                case BaratheonHouseCard.PATCHFACE:
                    // card holder gets to discard a card from attackers deck
                    // after
                    // combat
                    nextTurn = new Turn(getPlayer(cardHolderName));
                    nextTurn.setType(Turn.PATCHFACE);
                    break;
                case BaratheonHouseCard.RENLY_BARATHEON:
                    // IF card holder wins combat, defender may upgrade one
                    // participating (including support) footman to a knight
                    if (cardHolderName.equalsIgnoreCase(victorName)) {
                        nextTurn = new Turn(getPlayer(cardHolderName));
                        nextTurn.setType(Turn.RENLY_BARATHEON);
                    }
                    break;
                case LannisterHouseCard.CERSEI_LANNISTER:
                    // if card holder wins combat, they may remove one order
                    // token from the opponent from any zone.
                    if (cardHolderName.equalsIgnoreCase(victorName)) {
                        nextTurn = new Turn(getPlayer(cardHolderName));
                        nextTurn.setType(Turn.CERSEI_LANNISTER);
                    }
                    break;
                case LannisterHouseCard.TYWIN_LANNISTER:
                    // If card holder wins combat, they gain +2 influence tokens
                    if (cardHolderName.equalsIgnoreCase(victorName)) {
                        for (Player player : context.getPlayers()) {
                            if (player.getName().equalsIgnoreCase(victorName)) {
                                player.setInfluence(player.getInfluence() + 2);
                            }
                        }
                    }
                    break;
                case MartellHouseCard.ARIANNE_MARTELL:
                    // If card holder loses combat, opponent may not move units
                    // into target zone, they return to their original locations
                    // card holders units retreat normally
                    if (!cardHolderName.equalsIgnoreCase(victorName)) {
                        if (victorName.equalsIgnoreCase(attackingPlayerName)) {
                            attackerAllowedToAcclimate = false;
                        } else {
                            defenderAllowedToAcclimate = false;
                        }
                    }
                    break;
                case StarkHouseCard.ROOSE_BOLTON:
                    // If card holder loses combat, reset house deck to new deck
                    // with all cards
                    if (!cardHolderName.equalsIgnoreCase(victorName)) {
                        String house = null;
                        for (Player player : context.getPlayers()) {
                            if (player.getName().equalsIgnoreCase(
                                    cardHolderName)) {
                                house = player.getHouse();
                            }
                        }
                        if (house != null) {
                            HouseCard.resetDeck(context, house);
                        } else {
                            Client.error("Cannot determine " + cardHolderName
                                    + "'s house");
                        }
                    }
                    break;
                case StarkHouseCard.ROBB_STARK:
                    // If card holder wins combat, they may chose where opponent
                    // retreats.
                    // Retreat location must be a legal zone where opponent
                    // loses the least units
                    break;
                case TyrellHouseCard.SER_LORAS_TYRELL:
                    // if card holder is attacking and wins combat, move march
                    // order token to
                    // target zone which can be resolved later in round
                    // (add additional march order turn for player, place turn
                    // after remaining combats)
                    if (cardHolderName.equalsIgnoreCase(victorName)
                            && cardHolderName
                                    .equalsIgnoreCase(attackingPlayerName)) {
                        Map<String, Zone> zones = context.getZones();
                        Order order = null;
                        for (MarchMovement march : zones.get(targetZoneName)
                                .getMarches()) {
                            if (march.getUnit().getPlayerName()
                                    .equalsIgnoreCase(cardHolderName)) {
                                Zone fromZone = zones.get(march.getFrom());
                                if (fromZone.getOrder() != null
                                        && fromZone.getOrder().getType()
                                                .equals(Order.MARCH_ORDER)) {
                                    if (order == null
                                            || order.getBonus() < fromZone
                                                    .getOrder().getBonus()) {
                                        order = fromZone.getOrder();
                                    }
                                }
                            }
                        }
                        if (order != null
                                && order.getType().equals(Order.MARCH_ORDER)) {
                            zones.get(targetZoneName).setOrder(order);
                            nextTurn = new Turn(getPlayer(cardHolderName));
                            nextTurn.setType(Turn.RESOLVE_MARCH_ORDER);
                        }
                    }
                    break;
                default:
                    break;
            }
            if (nextTurn != null) {
                context.getTurns().add(0, nextTurn);
                TurnHandler.saveAndDistributeGameContext(nextTurn, context);
                TurnHandler.UpdateUI(mainscreen);
                // Prevent endless loop when turn is resolved and resolveCombat
                // is invoked again
                if (cardHolderName.equalsIgnoreCase(attackingPlayerName)) {
                    attackerHouseCardPlayed = true;
                } else {
                    defenderHouseCardPlayed = true;
                }
                // further action is needed before combat can be resolved
                // When turn is resolved resolveCombat will be invoked again
                return;
            }
        }
    }

    public boolean handlePreCombatCardSpecialAbility(HouseCard card,
            String cardHolderName, HouseCard opponentCard, String opponentName,
            MainScreenPanelController mainscreen) {
        if (card != null && opponentCard != null) {
            Turn nextTurn = null;
            switch (card.getName()) {
                case GreyjoyHouseCard.AERON_DAMPHAIR:
                    // after viewing opponent house card, card holder may pay 2
                    // influence to pick a new card from their available hand
                    nextTurn = new Turn(getPlayer(cardHolderName));
                    nextTurn.setType(Turn.AERON_DAMPHAIR);
                    break;
                case LannisterHouseCard.TYRION_LANNISTER:
                    // Card holder can force the attacker to choose a new house
                    // card from their available hand.
                    // If their hand is empty they cannot play a card
                    // their original chosen house card is returned to their
                    // hand after combat
                    nextTurn = new Turn(getPlayer(cardHolderName));
                    nextTurn.setType(Turn.TYRION_LANNISTER_CARDHOLDER);
                    break;
                case MartellHouseCard.DORAN_MARTELL:
                    // Card holder may move their opponent to the bottom of one
                    // influence track of card holders choice
                    // occurs before combat
                    nextTurn = new Turn(getPlayer(cardHolderName));
                    nextTurn.setType(Turn.DORAN_MARTELL);
                    break;
                case StarkHouseCard.THE_BLACKFISH:
                    // Card holder takes no casualties from combat due to house
                    // card abilities or icons
                    if (cardHolderName.equalsIgnoreCase(attackingPlayerName)) {
                        attackerImmuneToCasualties = true;
                    } else {
                        defenderImmuneToCasualties = true;
                    }
                    break;
                case TyrellHouseCard.QUEEN_OF_THORNS:
                    // card holder can remove 1 of their opponents order tokens
                    // in a zone adjacent to the target zone
                    // card holder may not remove the march order which started
                    // combat (add flag in march order, set true if started
                    // combat)
                    nextTurn = new Turn(getPlayer(cardHolderName));
                    nextTurn.setType(Turn.QUEEN_OF_THORNS);
                    break;
                case TyrellHouseCard.MACE_TYRELL:
                    // card hold can immediately destroy an opponents footman
                    // Be sure to handle blackfish card logic before this
                    if (cardHolderName.equalsIgnoreCase(attackingPlayerName)
                            && defendingHouseCard.getName().equals(
                                    StarkHouseCard.THE_BLACKFISH)) {
                        attackerHouseCardPlayed = true;
                        break;
                    } else if (cardHolderName
                            .equalsIgnoreCase(defendingPlayerName)
                            && attackingHouseCard.getName().equals(
                                    StarkHouseCard.THE_BLACKFISH)) {
                        defenderHouseCardPlayed = true;
                        break;
                    } else {
                        nextTurn = new Turn(getPlayer(cardHolderName));
                        nextTurn.setType(Turn.MACE_TYRELL);
                        break;
                    }
                default:
                    break;
            }
            if (nextTurn != null) {
                nextTurn.setCombatId(id);
                context.getTurns().add(0, nextTurn);
                TurnHandler.saveAndDistributeGameContext(nextTurn, context);
                TurnHandler.UpdateUI(mainscreen);
                // Prevent endless loop when turn is resolved and resolveCombat
                // is invoked again
                if (cardHolderName.equalsIgnoreCase(attackingPlayerName)) {
                    attackerHouseCardPlayed = true;
                } else {
                    defenderHouseCardPlayed = true;
                }
                // further action is needed before combat can be resolved
                // When turn is resolved resolveCombat will be invoked again
                return true;
            }
        }
        return false;
    }

    /**
     * @return the attackerAllowedToAcclimate
     */
    public boolean isAttackerAllowedToAcclimate() {
        return attackerAllowedToAcclimate;
    }

    /**
     * @return the attackerHouseCardPlayed
     */
    public boolean isAttackerHouseCardPlayed() {
        return attackerHouseCardPlayed;
    }

    public boolean isAttackerImmuneToCasualties() {
        return attackerImmuneToCasualties;
    }

    public boolean isBattleResolved() {
        return battleResolved;
    }

    /**
     * @return the defenderAllowedToAcclimate
     */
    public boolean isDefenderAllowedToAcclimate() {
        return defenderAllowedToAcclimate;
    }

    /**
     * @return the defenderHouseCardPlayed
     */
    public boolean isDefenderHouseCardPlayed() {
        return defenderHouseCardPlayed;
    }

    public boolean isDefenderImmuneToCasualties() {
        return defenderImmuneToCasualties;
    }

    public void resolveCombatAgainstNeutralForce(
            MainScreenPanelController mainscreen) {
        Zone targetZone = context.getZones().get(targetZoneName);
        int attackingPlayerPower = getAttackingPlayerCombatPower(
                attackingPlayerName, targetZone);
        if (targetZone instanceof Land) {
            Land targetLand = (Land) targetZone;
            if (attackingPlayerPower >= targetLand.getNeutralDefensePower()) {
                acclimateUnits(targetZone, attackingPlayerName);
                targetZone.setControlledByName(attackingPlayerName);
                Client.getLogger().info(targetZoneName + " has been captured.");
            } else {
                returnUnits(targetZone, attackingPlayerName);
                Client.getLogger().info(attackingPlayerName + " has failed to captured "
                        + targetZoneName);
            }
        } else {
            acclimateUnits(targetZone, attackingPlayerName);
            targetZone.setControlledByName(attackingPlayerName);
            Client.getLogger().info(targetZoneName + " has been captured.");
        }
    }

    public void resolveCombatBetweenPlayers(MainScreenPanelController mainscreen) {
        Zone targetZone = context.getZones().get(targetZoneName);
        if (!attackerHouseCardPlayed
                && handlePreCombatCardSpecialAbility(attackingHouseCard,
                        attackingPlayerName, defendingHouseCard,
                        defendingPlayerName, mainscreen)) {
            return;
        }
        if (!defenderHouseCardPlayed
                && handlePreCombatCardSpecialAbility(defendingHouseCard,
                        defendingPlayerName, attackingHouseCard,
                        attackingPlayerName, mainscreen)) {
            return;
        }
        if (!battleResolved) {
            int defendingPlayerPower = getDefendingPlayerCombatPower(
                    defendingPlayerName, targetZone);
            Client.getLogger().info("Defending Player Combat Power (Units, Orders, Support): "
                    + defendingPlayerPower);
            int attackingPlayerPower = getAttackingPlayerCombatPower(
                    attackingPlayerName, targetZone);
            Client.getLogger().info("Attacking Player Combat Power (Units, Orders, Support): "
                    + attackingPlayerPower);
            defendingPlayerPower += getCardSpecialAbilityCombatStrength(
                    defendingHouseCard, defendingPlayerName,
                    attackingHouseCard, attackingPlayerName);
            defendingPlayerPower += defendingHouseCard.getCombatStrength();
            Client.getLogger().info("Defending Player combat power with card : "
                    + defendingPlayerPower);
            attackingPlayerPower += getCardSpecialAbilityCombatStrength(
                    attackingHouseCard, attackingPlayerName,
                    defendingHouseCard, defendingPlayerName);
            attackingPlayerPower += attackingHouseCard.getCombatStrength();
            Client.getLogger().info("Attacking Player combat power with card : "
                    + attackingPlayerPower);
            if (StringHelper.isNotNullOrEmpty(playerUsingValyrianBlade)
                    && playerUsingValyrianBlade
                            .equalsIgnoreCase(attackingPlayerName)) {
                attackingPlayerPower += 1;
                addLog("Attacker gains 1 combat strength by using the Valyrian Blade");
                Client.getLogger().info("Attacking Player combat power with Blade : "
                        + attackingPlayerPower);
            } else if (StringHelper.isNotNullOrEmpty(playerUsingValyrianBlade)
                    && playerUsingValyrianBlade
                            .equalsIgnoreCase(defendingPlayerName)) {
                defendingPlayerPower += 1;
                addLog("Defender gains 1 combat strength by using the Valyrian Blade");
                Client.getLogger().info("Defender Player combat power with Blade : "
                        + defendingPlayerPower);
            }
            // Determine victor
            if (attackingPlayerPower > defendingPlayerPower) {
                Client.getLogger().info("Attack succeeds " + attackingPlayerPower
                        + " vs " + defendingPlayerPower);
                victorName = attackingPlayerName;
                loserName = defendingPlayerName;
            } else if (attackingPlayerPower < defendingPlayerPower) {
                Client.getLogger().info("Attack fails " + attackingPlayerPower + " vs "
                        + defendingPlayerPower);
                victorName = defendingPlayerName;
                loserName = attackingPlayerName;
            } else {
                Client.getLogger().info("Combat Tie " + attackingPlayerPower + " vs "
                        + defendingPlayerPower);
                if (getPlayer(attackingPlayerName).getFieldomsTrack() < getPlayer(
                        defendingPlayerName).getFieldomsTrack()) {
                    addLog(attackingPlayerName + " wins tie due to Fiefdom Track");
                    Client.getLogger().info(attackingPlayerName + " wins due to fieldoms");
                    victorName = attackingPlayerName;
                    loserName = defendingPlayerName;
                } else {
                    addLog(defendingPlayerName + " wins tie due to Fiefdom Track");
                    Client.getLogger().info(defendingPlayerName + " wins due to fieldoms");
                    victorName = defendingPlayerName;
                    loserName = attackingPlayerName;
                }
            }
            if (context.isValyrianBladeAvailable()
                    && valyrianBladeCanChangeOutcome(attackingPlayerPower,
                            defendingPlayerPower)) {
                // add valyrian blade turn
                Turn valyrianBladeTurn = new Turn(Player.getPlayer(loserName));
                valyrianBladeTurn.setType(Turn.VALYRIAN_BLADE);
                valyrianBladeTurn.setCombatId(id);
                context.getTurns().add(0, valyrianBladeTurn);
                Turn currentTurn = new Turn(
                        Player.getPlayer(Player.getMyName()));
                currentTurn.setType("Combat");
                TurnHandler.saveAndDistributeGameContext(currentTurn, context);
                TurnHandler.UpdateUI(mainscreen);
                return;
            }
            battleResolved = true;
        }
        if (defendingPlayerName.equalsIgnoreCase(victorName)) {
            setAttackerMarchedFromZoneName("N/A");
        }
        // Remove card played from context decks, no player house card deck
        // should ever be empty at this point, if so reset deck
        removePlayedCards();
        // Historize Combat
        CombatHistory combatHistory = new CombatHistory(this, getContext());
        getContext().getCombatHistory().add(combatHistory);
        // Determine casualties
        handleCasualties(combatHistory);
        if (!defenderHouseCardPlayed) {
            handlePostCombatCardSpecialAbility(defendingHouseCard,
                    defendingPlayerName, attackingHouseCard,
                    attackingPlayerName, mainscreen);
        }
        if (!attackerHouseCardPlayed) {
            handlePostCombatCardSpecialAbility(attackingHouseCard,
                    attackingPlayerName, defendingHouseCard,
                    defendingPlayerName, mainscreen);
        }
        // acclimate victor units (loser units acclimated through retreat)
        acclimateVictorUnits();
        // Remove order token from target zones, do this LAST post card actions
        // depend on them
        targetZone.setOrder(Order.NONE_ORDER);
        // handle retreats / routed units
        Turn retreatTurn = null;
        for (Player player : context.getPlayers()) {
            if (player.equals(loserName)) {
                retreatTurn = new Turn(player);
                retreatTurn.setCombatId(id);
                retreatTurn.setType(Turn.RETREAT);
            }
        }
        if (retreatTurn != null) {
            // Add retreat turn to be handled immediately
            context.getTurns().add(0, retreatTurn);
            Turn currentTurn = new Turn(Player.getPlayer(Player.getMyName()));
            currentTurn.setType("Combat");
            TurnHandler.saveAndDistributeGameContext(currentTurn, context);
            TurnHandler.UpdateUI(mainscreen);
            return;
        }
        // Post combat turn handled in retreat panel, currently setup to go to
        // planning phase
    }

    private boolean valyrianBladeCanChangeOutcome(int attackingPlayerPower,
            int defendingPlayerPower) {
        if (Math.abs(attackingPlayerPower - defendingPlayerPower) == 1
                && getPlayer(loserName).getFieldomsTrack() == 1) {
            return true;
        }
        return false;
    }

    private void acclimateVictorUnits() {
        Zone targetZone = context.getZones().get(targetZoneName);
        if (targetZone instanceof Land
                && !targetZone.getControlledByName().equalsIgnoreCase(
                        victorName)) {
            ((Land) targetZone).setGarrisonDefensePower(0);
        }
        List<MarchMovement> acclimatedMarches = new ArrayList<MarchMovement>();
        for (MarchMovement march : targetZone.getMarches()) {
            if (march.getUnit().getPlayerName().equalsIgnoreCase(victorName)) {
                targetZone.addUnit(march.getUnit());
                acclimatedMarches.add(march);
            }
        }
        targetZone.getMarches().removeAll(acclimatedMarches);
        for (Player player : context.getPlayers()) {
            if (player.getName().equalsIgnoreCase(victorName)) {
                targetZone.setControlledByName(player.getName());
            }
        }
    }

    private void handleCasualties(CombatHistory combatHistory) {
        HouseCard winningCard;
        HouseCard losingCard;
        String losingPlayerName;
        if (attackingPlayerName.equalsIgnoreCase(victorName)) {
            winningCard = attackingHouseCard;
            losingCard = defendingHouseCard;
            losingPlayerName = defendingPlayerName;
            if (defenderImmuneToCasualties) {
                Client.getLogger().info(losingPlayerName
                        + " is immune to casualties for this combat due to Blackfish card");
                combatHistory.addLog("The Blackfish saves " + losingPlayerName
                        + " units from being destroyed");
                return;
            }
        } else {
            winningCard = defendingHouseCard;
            losingCard = attackingHouseCard;
            losingPlayerName = attackingPlayerName;
            if (attackerImmuneToCasualties) {
                Client.getLogger().info(losingPlayerName
                        + " is immune to casualties for this combat due to Blackfish card");
                combatHistory.addLog("The Blackfish saves " + losingPlayerName
                        + " units from being destroyed");
                return;
            }
        }
        int casualties = winningCard.getSwordIcons()
                - losingCard.getFortificationIcons();
        if (casualties > 0) {
            combatHistory.addLog(losingPlayerName + " had " + casualties
                    + " unit(s) destroyed");
            for (int i = 0; i < casualties; i++) {
                if (!context.getZones().get(targetZoneName)
                        .removeWeakestUnit(losingPlayerName)) {
                    Client.getLogger().info(losingPlayerName + " has no units to destroy");
                    break;
                } else {
                    Client.getLogger().info(losingPlayerName + " unit destroyed");
                }
            }
        }
    }

    private void removePlayedCards() {
        for (Player player : context.getPlayers()) {
            if (player.equals(defendingPlayerName)) {
                removeCardFromDeck(defendingHouseCard, player.getHouse());
            }
        }
        for (Player player : context.getPlayers()) {
            if (player.equals(attackingPlayerName)) {
                removeCardFromDeck(attackingHouseCard, player.getHouse());
            }
        }
    }

    private void removeCardFromDeck(HouseCard removalCard, String house) {
        List houseCardHand = HouseCard.getHouseDeck(context, house);
        if (houseCardHand != null && !houseCardHand.isEmpty()) {
            for (Object card : houseCardHand) {
                HouseCard houseCard = (HouseCard) card;
                if (houseCard.getName().equals(removalCard.getName())) {
                    houseCardHand.remove(card);
                    break;
                }
            }
            if (houseCardHand.isEmpty()) {
                HouseCard.resetDeck(context, house);
            }
        }
    }

    private void returnUnits(Zone targetZone, String attackingPlayerName) {
        List<MarchMovement> returnedUnits = new ArrayList<MarchMovement>();
        for (MarchMovement march : targetZone.getMarches()) {
            if (march.getUnit().getPlayerName()
                    .equalsIgnoreCase(attackingPlayerName)) {
                context.getZones().get(march.getFrom())
                        .addUnit(march.getUnit());
                returnedUnits.add(march);
            }
        }
        targetZone.getMarches().removeAll(returnedUnits);
    }

    /**
     * @param attackerAllowedToAcclimate
     *            the attackerAllowedToAcclimate to set
     */
    public void setAttackerAllowedToAcclimate(boolean attackerAllowedToAcclimate) {
        this.attackerAllowedToAcclimate = attackerAllowedToAcclimate;
    }

    /**
     * @param attackerHouseCardPlayed
     *            the attackerHouseCardPlayed to set
     */
    public void setAttackerHouseCardPlayed(boolean attackerHouseCardPlayed) {
        this.attackerHouseCardPlayed = attackerHouseCardPlayed;
    }

    public void setAttackerImmuneToCasualties(boolean attackerImmuneToCasualties) {
        this.attackerImmuneToCasualties = attackerImmuneToCasualties;
    }

    /**
     * @param attackingHouseCard
     *            the attackingHouseCard to set
     */
    public void setAttackingHouseCard(HouseCard attackingHouseCard) {
        this.attackingHouseCard = attackingHouseCard;
    }

    public void setAttackingPlayer(String attackingPlayerName) {
        this.attackingPlayerName = attackingPlayerName;
    }

    /**
     * @param attackingPlayerName
     *            the attackingPlayerName to set
     */
    public void setAttackingPlayerName(String attackingPlayerName) {
        this.attackingPlayerName = attackingPlayerName;
    }

    public void setBattleResolved(boolean battleResolved) {
        this.battleResolved = battleResolved;
    }

    public void setContext(GameContext context) {
        this.context = context;
    }

    /**
     * @param defenderAllowedToAcclimate
     *            the defenderAllowedToAcclimate to set
     */
    public void setDefenderAllowedToAcclimate(boolean defenderAllowedToAcclimate) {
        this.defenderAllowedToAcclimate = defenderAllowedToAcclimate;
    }

    /**
     * @param defenderHouseCardPlayed
     *            the defenderHouseCardPlayed to set
     */
    public void setDefenderHouseCardPlayed(boolean defenderHouseCardPlayed) {
        this.defenderHouseCardPlayed = defenderHouseCardPlayed;
    }

    public void setDefenderImmuneToCasualties(boolean defenderImmuneToCasualties) {
        this.defenderImmuneToCasualties = defenderImmuneToCasualties;
    }

    /**
     * @param defendingHouseCard
     *            the defendingHouseCard to set
     */
    public void setDefendingHouseCard(HouseCard defendingHouseCard) {
        this.defendingHouseCard = defendingHouseCard;
    }

    public void setDefendingPlayer(String defendingPlayerName) {
        this.defendingPlayerName = defendingPlayerName;
    }

    /**
     * @param defendingPlayerName
     *            the defendingPlayerName to set
     */
    public void setDefendingPlayerName(String defendingPlayerName) {
        this.defendingPlayerName = defendingPlayerName;
    }

    public void setTargetZoneName(String targetZoneName) {
        this.targetZoneName = targetZoneName;
    }

    public void setVictor(String victor) {
        this.victorName = victor;
    }

    /**
     * @param victorName
     *            the victorName to set
     */
    public void setVictorName(String victorName) {
        this.victorName = victorName;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public void setMarchingBonus(int bonus) {
        this.marchingBonus = bonus;
    }

    public String getLog() {
        return log;
    }

    public void setLog(String log) {
        this.log = log;
    }

    public void addLog(String newlog) {
        if (StringHelper.isNullOrEmpty(this.log)) {
            log = newlog;
        } else if (!log.contains(newlog)) {
            log += ("\n" + newlog);
        }
    }

    public String getAttackerMarchedFromZoneName() {
        return attackerMarchedFromZoneName;
    }

    public void setAttackerMarchedFromZoneName(
            String attackerMarchedFromZoneName) {
        this.attackerMarchedFromZoneName = attackerMarchedFromZoneName;
    }

    public String getPlayerUsingValyrianBlade() {
        return playerUsingValyrianBlade;
    }

    public void setPlayerUsingValyrianBlade(String playerUsingValyrianBlade) {
        this.playerUsingValyrianBlade = playerUsingValyrianBlade;
    }
    
    public static Combat getCombat(long combatId) {
        GameContext context = GameContext.getMyGameContext();
        if (context != null) {
            for (Combat combat : context.getCombats()) {
                if (combat.getId() == combatId) {
                    return combat;
                }
            }
        }
        Client.error("Combat id: " + combatId + " not found in context");
        return null;
    }
}
