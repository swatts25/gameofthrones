The following is a list of changes in game play between the board game and the app:

Players cannot support players of another house, this will be implemented at a later time.

The Ser Loras Tyrell house card will choose the marching token of greatest power to be used
as a copy in the target zone.

The Valyrian Steel Blade can only be used when it would make a difference in determination of the victor. If the combat strength
deficit is greater than 1 neither parties will be prompted to use the blade

The special ability of the Robb Stark house card has been changed due to the coding complexity
of the original. The new card will have a sword icon, the card artwork has been updated accordingly.

Wildling attacks have been removed from the game, they will be implemented at a later time. All 'Wildlings Attack' cards
have been removed from the deck.

There is no round counter, If at any point a player controls 7 castles or strongholds, they will be crowned the victor, and the
game will end immediately. A round counter may or may not be implemented at a later time.

Ship units displayed on map reflect a players total number of ships in a zone (routed + unrouted)