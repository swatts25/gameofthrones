package test;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.junit.Test;

import client.Board;
import client.JavaFXLauncher;
import client.model.Footman;
import client.model.GameContext;
import client.model.House;
import client.model.Knight;
import client.model.MarchMovement;
import client.model.Player;
import client.model.Ship;
import client.model.SiegeEngine;
import client.model.Turn;
import client.model.cards.LannisterHouseCard;
import client.model.cards.StarkHouseCard;
import client.model.zones.Land;
import client.model.zones.Sea;
import client.model.zones.Zone;
import controllers.RetreatPanelController;

/**
 * @author zggis
 */
public class __RetreatPanelTest {

    @Test
    public void test() {
        GameContext context = new GameContext();
        List<Player> players = new ArrayList<Player>();
        Player player1 = new Player();
        player1.setName("Sean");
        player1.setHouse(House.STARK);
        player1.setInfluence(10);
        player1.setFieldomsTrack(1);
        player1.setEmailAddress("s@s.com");
        players.add(player1);
        context.setStarkHouseDeck(StarkHouseCard.getAllCards());
        Player player2 = new Player();
        player2.setName("Will");
        player2.setHouse(House.LANNISTER);
        players.add(player2);
        player2.setFieldomsTrack(2);
        player2.setSupplyTrack(0);
        player2.setEmailAddress("w@w.com");
        context.setLannisterHouseDeck(LannisterHouseCard.getAllCards());
        // Player player3 = new Player();
        // player3.setName("Chris");
        // player3.setHouse(House.LANNISTER);
        Board.initializeZones(players);
        player1.setSupplyTrack(0);
        context.setZones(Board.zones);
        context.setPlayers(players);
        Player.setMyName("Sean");
        GameContext.setMyGameContext(context);
        Map<String, Zone> zones = context.getZones();
        // DRAGONSTONE
        Land dragonstone = (Land) zones.get(Land.DRAGONSTONE);
        dragonstone.setControlledByName(player2.getName());
        dragonstone.addFootman(new Footman(player2.getName()));
        // BLACKWATER BAY
        Sea blackwaterBay = (Sea) zones.get(Sea.BLACKWATER_BAY);
        blackwaterBay.setControlledByName(player2.getName());
        blackwaterBay.addShip(new Ship(player2.getName()));
        // BLACKWATER BAY
        Sea shipbreakerBay = (Sea) zones.get(Sea.SHIPBREAKER_BAY);
        shipbreakerBay.setControlledByName(player2.getName());
        shipbreakerBay.addShip(new Ship(player2.getName()));
        // KINGS LANDING
        Land kingsLanding = (Land) zones.get(Land.KINGS_LANDING);
        kingsLanding.setControlledByName(player2.getName());
        kingsLanding.addFootman(new Footman(player2.getName()));
        kingsLanding.addFootman(new Footman(player2.getName()));
        kingsLanding.addFootman(new Footman(player2.getName()));
        kingsLanding.addKnight(new Knight(player2.getName()));
        kingsLanding.addSiegeEngine(new SiegeEngine(player2.getName()));
        kingsLanding.getMarches().add(new MarchMovement(Land.CRACKLAW_POINT, Land.WINTERFELL, new Footman(player1.getName())));
        // BLACKWATER
        JavaFXLauncher launcher = new JavaFXLauncher();
        RetreatPanelController controller = new RetreatPanelController(
                Land.KINGS_LANDING, player1.getName(), Land.CRACKLAW_POINT, null, new Turn(player2));
        launcher.show(controller, "Retreat Test");
    }
}
