package test;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.Test;

import client.JavaFXLauncher;
import client.model.GameContext;
import client.model.Order;
import client.model.Player;
import client.model.Turn;
import client.model.zones.Land;
import client.model.zones.Zone;
import controllers.OrderEditorPanelController;

public class __PlanningPhasePanelTest {
	@Test
	public void test() {
		JavaFXLauncher launcher = new JavaFXLauncher();
		GameContext context = new GameContext();
		Zone winterfell = new Land(Land.WINTERFELL);
		Zone kingsLanding = new Land(Land.KINGS_LANDING);
		Player p1 = new Player();
		p1.setName("Sean");
		winterfell.setControlledByName(p1.getName());
		kingsLanding.setControlledByName(p1.getName());
		Map<String, Zone> zones = new HashMap<String, Zone>();
		zones.put(Land.WINTERFELL, winterfell);
		zones.put(Land.KINGS_LANDING, kingsLanding);
		Order testOrder = new Order(Order.MARCH_ORDER, 1, true);
		winterfell.setOrder(testOrder);
		context.setZones(zones);
		List<Player> players = new ArrayList<Player>();
		players.add(p1);
		context.setPlayers(players);
		GameContext.setMyGameContext(context);
		Player.setMyName("Sean");
		Turn turn = new Turn(p1);
		turn.setType(Turn.PLANNING_PHASE);
		OrderEditorPanelController controller = new OrderEditorPanelController(null, turn);
		launcher.show(controller, "Planning Phase Test");
	}
}
