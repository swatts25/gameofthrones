package test;

import org.junit.Test;

import client.JavaFXLauncher;
import client.model.cards.WildlingCard;
import controllers.WildlingCardViewerPanelController;

public class __WildlingCardViewerPanelTest {
	@Test
	public void test() {
		JavaFXLauncher launcher = new JavaFXLauncher();
		launcher.show(new WildlingCardViewerPanelController(new WildlingCard(WildlingCard.SILENCE_AT_THE_WALL)),
				"Wildling Card View Test");
	}
}
