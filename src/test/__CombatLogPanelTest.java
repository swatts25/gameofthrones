package test;

import org.junit.Test;

import client.Combat;
import client.JavaFXLauncher;
import client.model.CombatHistory;
import controllers.CombatLogPanelController;

public class __CombatLogPanelTest {

    @Test
    public void test() {
        __CombatTest combatTest = new __CombatTest();
        combatTest.test();
        Combat combat = combatTest.simTwo();
        combat.resolveCombatBetweenPlayers(null);
        JavaFXLauncher launcher = new JavaFXLauncher();
        launcher.show(new CombatLogPanelController(new CombatHistory(combat, combat.getContext())), "Combat Log Test");
    }
}
