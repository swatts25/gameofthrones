package test;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.Test;

import client.JavaFXLauncher;
import client.model.GameContext;
import client.model.Order;
import client.model.Player;
import client.model.cards.WildlingCard;
import client.model.zones.Land;
import client.model.zones.Zone;
import controllers.OrderViewerPanelController;

public class __RavenPhasePanelTest {
	@Test
	public void test() {
		JavaFXLauncher launcher = new JavaFXLauncher();
		GameContext context = new GameContext();
		Zone winterfell = new Land(Land.WINTERFELL);
		Zone kingsLanding = new Land(Land.KINGS_LANDING);
		WildlingCard card = new WildlingCard(WildlingCard.A_KING_BEYOND_THE_WALL);
		List<WildlingCard> cards = new ArrayList<WildlingCard>();
		cards.add(card);
		context.setWildlingDeck(cards);
		Player p1 = new Player();
		p1.setName("Sean");
		Player p2 = new Player();
        p2.setName("Chris");
		winterfell.setControlledByName(p1.getName());
		kingsLanding.setControlledByName(p2.getName());
		Map<String, Zone> zones = new HashMap<String, Zone>();
		zones.put(Land.WINTERFELL, winterfell);
		zones.put(Land.KINGS_LANDING, kingsLanding);
		Order testOrder = new Order(Order.MARCH_ORDER, 1, true);
		winterfell.setOrder(testOrder);
		context.setZones(zones);
		List<Player> players = new ArrayList<Player>();
		players.add(p1);
		players.add(p2);
		context.setPlayers(players);
		GameContext.setMyGameContext(context);
		Player.setMyName("Sean");
		OrderViewerPanelController controller = new OrderViewerPanelController();
		launcher.show(controller, "Raven Phase Test");
	}
}
