package test;

import org.junit.Test;

import client.JavaFXLauncher;
import client.model.Player;
import client.model.Turn;
import client.model.cards.GreyjoyHouseCard;
import controllers.HouseCardChooserPanelController;

public class __HouseCardViewerPanelTest {

    @Test
    public void test() {
        JavaFXLauncher launcher = new JavaFXLauncher();
        launcher.show(
                new HouseCardChooserPanelController(
                        GreyjoyHouseCard.getAllCards(), null,
                        new Turn(new Player("Sean"))),
                "House Card Chooser Test");
    }
}