package test;

import java.awt.Desktop;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.junit.Test;

import utils.MapBuilder;
import utils.SavedGameData;
import client.Board;
import client.Client;
import client.Combat;
import client.model.Footman;
import client.model.GameContext;
import client.model.House;
import client.model.Knight;
import client.model.MarchMovement;
import client.model.Order;
import client.model.Player;
import client.model.Ship;
import client.model.SiegeEngine;
import client.model.cards.BaratheonHouseCard;
import client.model.cards.GreyjoyHouseCard;
import client.model.cards.LannisterHouseCard;
import client.model.cards.StarkHouseCard;
import client.model.zones.Land;
import client.model.zones.Sea;
import client.model.zones.Zone;

public class __CombatTest {

    private Player player1;

    private Player player2;

    private Player player3;

    private GameContext context;

    @Test
    public void test() {
        context = new GameContext();
        List<Player> players = new ArrayList<Player>();
        player1 = new Player();
        player1.setName("Sean");
        player1.setHouse(House.STARK);
        player1.setEmailAddress("s@s.com");
        player1.setInfluence(10);
        // player1.setFieldomsTrack(1);
        players.add(player1);
        context.setStarkHouseDeck(StarkHouseCard.getAllCards());
        player2 = new Player();
        player2.setName("Will");
        player2.setHouse(House.LANNISTER);
        players.add(player2);
        player2.setEmailAddress("w@w.com");
        // player2.setFieldomsTrack(2);
        context.setLannisterHouseDeck(LannisterHouseCard.getAllCards());
        player3 = new Player();
        player3.setName("Chris");
        player3.setHouse(House.BARATHEON);
        players.add(player3);
        player3.setEmailAddress("c@c.com");
        // player3.setFieldomsTrack(3);
        context.setBaratheonHouseDeck(BaratheonHouseCard.getAllCards());
        Board.initializeZones(players);
        context.setZones(Board.zones);
        context.setPlayers(players);
        Player.setMyName("Sean");
        GameContext.setMyGameContext(context);
        // Combat combat = simOne(context, player1, player2);
        // Combat combat = simTwo();
        // combat.resolveCombatBetweenPlayers(null);
        saveMap();
    }

    private Combat simOne() {
        Map<String, Zone> zones = context.getZones();
        // DRAGONSTONE
        Land dragonstone = (Land) zones.get(Land.DRAGONSTONE);
        dragonstone.setOrder(new Order(Order.MARCH_ORDER, 1));
        dragonstone.setControlledByName(player1.getName());
        // BLACKWATER BAY
        Sea blackwaterBay = (Sea) zones.get(Sea.BLACKWATER_BAY);
        blackwaterBay.setOrder(new Order(Order.SUPPORT_ORDER, 0, true));
        blackwaterBay.setControlledByName(player1.getName());
        blackwaterBay.addShip(new Ship(player1.getName()));
        // KINGS LANDING
        Land kingsLanding = (Land) zones.get(Land.KINGS_LANDING);
        kingsLanding.setControlledByName(player2.getName());
        kingsLanding.addFootman(new Footman(player2.getName()));
        kingsLanding.addKnight(new Knight(player2.getName()));
        kingsLanding.addSiegeEngine(new SiegeEngine(player2.getName()));
        kingsLanding.setOrder(new Order(Order.DEFENSE_ORDER, 2));
        // CRACKCLAW POINT
        Zone crackclawpoint = zones.get(Land.CRACKLAW_POINT);
        crackclawpoint.setControlledByName(player2.getName());
        crackclawpoint.setOrder(new Order(Order.SUPPORT_ORDER, 0, true));
        crackclawpoint.addUnit(new Knight(player2.getName()));
        // BLACKWATER
        Zone blackwater = zones.get(Land.BLACKWATER);
        blackwater.setControlledByName(player2.getName());
        blackwater.setOrder(new Order(Order.MARCH_ORDER, 1, true));
        MarchMovement march = new MarchMovement(Land.DRAGONSTONE,
                Land.KINGS_LANDING, new Knight(player1.getName()));
        kingsLanding.getMarches().add(march);
        MarchMovement march2 = new MarchMovement(Land.DRAGONSTONE,
                Land.KINGS_LANDING, new SiegeEngine(player1.getName()));
        kingsLanding.getMarches().add(march2);
        MarchMovement march3 = new MarchMovement(Land.BLACKWATER,
                Land.KINGS_LANDING, new Knight(player2.getName()));
        kingsLanding.getMarches().add(march3);
        Combat combat = new Combat(kingsLanding.getName(), player1.getName(),
                player2.getName(), Land.BLACKWATER);
        combat.setContext(context);
        combat.setDefendingHouseCard(
                new BaratheonHouseCard(BaratheonHouseCard.MELISANDRE));
        combat.setAttackingHouseCard(
                new BaratheonHouseCard(BaratheonHouseCard.BRIENE_OF_TARTH));
        return combat;
    }

    public Combat simTwo() {
        // Defending Player Combat Power (Units, Orders, Support): 16
        // Attacking Player Combat Power (Units, Orders, Support): 7
        // Defending Player combat power with card : 17
        // Attacking Player combat power with card : 11
        // Attack fails 11 vs 17
        Map<String, Zone> zones = context.getZones();
        // Lannisport
        // +1 footman
        // +2 knight
        // +2 garrison
        Land lannisport = (Land) zones.get(Land.LANNISPORT);
        lannisport.setOrder(new Order(Order.DEFENSE_ORDER, 1));
        lannisport.addKnight(new Knight(player2.getName()));
        lannisport.setControlledByName(player2.getName());
        // The Golden Sound
        // +1 ship
        Sea theGoldenSound = (Sea) zones.get(Sea.THE_GOLDEN_SOUND);
        theGoldenSound.setOrder(new Order(Order.SUPPORT_ORDER, 0, true));
        theGoldenSound.setControlledByName(player2.getName());
        theGoldenSound.addShip(new Ship(player2.getName()));
        // Riverrun
        Land riverrun = (Land) zones.get(Land.RIVERRUN);
        riverrun.setControlledByName(player2.getName());
        riverrun.setOrder(new Order(Order.SUPPORT_ORDER, 1, true));
        riverrun.addKnight(new Knight(player2.getName()));
        riverrun.addKnight(new Knight(player2.getName()));
        // ------------------------------------------------------------------------
        // Searoad Marches
        Land seaRoadMarches = (Land) zones.get(Land.SEAROADMARCHES);
        seaRoadMarches.setControlledByName(player1.getName());
        seaRoadMarches.setOrder(new Order(Order.MARCH_ORDER, 1, true));
        // Stony Sept
        Zone stoneySept = zones.get(Land.STONEY_SEPT);
        stoneySept.setControlledByName(player1.getName());
        stoneySept.setOrder(new Order(Order.SUPPORT_ORDER, 0));
        stoneySept.addUnit(new Knight(player1.getName()));
        stoneySept.addUnit(new Footman(player1.getName()));
        // Player 1 Marches
        MarchMovement march = new MarchMovement(Land.SEAROADMARCHES,
                Land.LANNISPORT, new Knight(player1.getName()));
        lannisport.getMarches().add(march);
        MarchMovement march2 = new MarchMovement(Land.SEAROADMARCHES,
                Land.LANNISPORT, new Footman(player1.getName()));
        lannisport.getMarches().add(march2);
        Combat combat = new Combat(lannisport.getName(), player1.getName(),
                player2.getName(), Land.SEAROADMARCHES);
        combat.setContext(context);
        combat.setDefendingHouseCard(
                new GreyjoyHouseCard(GreyjoyHouseCard.THEON_GREYJOY));
        combat.setAttackingHouseCard(
                new GreyjoyHouseCard(GreyjoyHouseCard.ASHA_GREYJOY));
        return combat;
    }

    private void saveMap() {
        try {
            final Path destination = Paths
                    .get(SavedGameData.BASE_BOARD_FILENAME);
            InputStream is = getClass()
                    .getResourceAsStream(SavedGameData.BASE_BOARD_LOCATION);
            File map = destination.toFile();
            if (!map.exists()) {
                Files.copy(is, destination);
            }
            if (GameContext.getMyGameContext() != null
                    && GameContext.getMyGameContext().getZones() != null) {
                MapBuilder boardmap = new MapBuilder(map);
                List<Zone> zones = new ArrayList<Zone>(
                        GameContext.getMyGameContext().getZones().values());
                for (Zone zone : zones) {
                    if (zone.getControlledBy() != null) {
                        boardmap.addInfluenceTokenToMap(zone.getControlledBy().getHouse(),
                                zone);
                    }
                }
                Desktop.getDesktop().open(boardmap.generateImage());
            } else {
                Desktop.getDesktop().open(map);
            }
        } catch (IOException e) {
            Client.error(e);
        }
    }
}
