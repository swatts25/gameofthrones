package test;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.junit.Test;

import controllers.OrderViewerPanelController;
import controllers.RaidOrderResolverPanelController;
import client.Board;
import client.JavaFXLauncher;
import client.model.GameContext;
import client.model.House;
import client.model.Order;
import client.model.Player;
import client.model.zones.Land;
import client.model.zones.Zone;

public class __ResolveRaidOrderPanelTest {

    @Test
    public void test() {
        List<Player> players = new ArrayList<Player>();
        Player player1 = new Player();
        player1.setName("Sean");
        player1.setHouse(House.STARK);
        Player player2 = new Player();
        player2.setName("Will");
        player2.setHouse(House.BARATHEON);
        Player player3 = new Player();
        player3.setName("Chris");
        player3.setHouse(House.LANNISTER);
        Board.initializeZones(players);
        GameContext context = new GameContext();
        context.setZones(Board.zones);
        context.setPlayers(players);
        Player.setMyName("Sean");
        GameContext.setMyGameContext(context);
        Map<String, Zone> zones = context.getZones();
        Zone winterfell = zones.get(Land.WINTERFELL);
        winterfell.setOrder(new Order(Order.RAID_ORDER, 0));
        winterfell.setControlledByName(player1.getName());
        Zone castleBlack = zones.get(Land.CASTLE_BLACK);
        castleBlack.setOrder(new Order(Order.CONSOLODATE_ORDER, 0));
        castleBlack.setControlledByName(player1.getName());
        Zone whiteHarbor = zones.get(Land.WHITE_HARBOR);
        whiteHarbor.setOrder(new Order(Order.RAID_ORDER, 0, true));
        whiteHarbor.setControlledByName(player1.getName());
        Zone theStonyShore = zones.get(Land.THE_STONY_SHORE);
        theStonyShore.setControlledByName(player3.getName());
        theStonyShore.setOrder(new Order(Order.SUPPORT_ORDER, 1, true));
        Zone moatCalin = zones.get(Land.MOAT_CALIN);
        moatCalin.setControlledByName(player2.getName());
        moatCalin.setOrder(new Order(Order.DEFENSE_ORDER, 2, true));
        JavaFXLauncher launcher = new JavaFXLauncher();
        RaidOrderResolverPanelController controller = new RaidOrderResolverPanelController(
                null, null);
        launcher.show(controller, "Raid Order Resolver Test");
    }
}
