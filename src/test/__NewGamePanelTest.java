package test;

import org.junit.Test;

import client.JavaFXLauncher;
import controllers.NewGamePanelController;

public class __NewGamePanelTest {
	@Test
	public void test() {
		JavaFXLauncher launcher = new JavaFXLauncher();
		NewGamePanelController controller = new NewGamePanelController(null);
		launcher.show(controller, "New Game Test");
	}
}
