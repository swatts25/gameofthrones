package test;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;

import client.Board;
import client.WesterosPhase;
import client.model.Footman;
import client.model.GameContext;
import client.model.House;
import client.model.Knight;
import client.model.Player;
import client.model.cards.AgeOneWesterosCard;
import client.model.cards.AgeThreeWesterosCard;
import client.model.cards.AgeTwoWesterosCard;
import client.model.cards.BaratheonHouseCard;
import client.model.cards.LannisterHouseCard;
import client.model.cards.StarkHouseCard;
import client.model.zones.Land;

/**
 * @author swatts
 *
 */
public class __WesterosPhaseTest {

    private GameContext context;

    private Player player1;

    private Player player2;

    private Player player3;

    @Test
    public void test() {
        context = new GameContext();
        GameContext.setMyGameContext(context);
        List<Player> players = new ArrayList<Player>();
        player1 = new Player();
        player1.setName("Sean");
        player1.setHouse(House.STARK);
        player1.setEmailAddress("s@s.com");
        player1.setInfluence(10);
        players.add(player1);
        context.setStarkHouseDeck(StarkHouseCard.getAllCards());
        player2 = new Player();
        player2.setName("Will");
        player2.setHouse(House.LANNISTER);
        players.add(player2);
        player2.setEmailAddress("w@w.com");
        context.setLannisterHouseDeck(LannisterHouseCard.getAllCards());
        player3 = new Player();
        player3.setName("Chris");
        player3.setHouse(House.BARATHEON);
        players.add(player3);
        player3.setEmailAddress("c@c.com");
        context.setBaratheonHouseDeck(BaratheonHouseCard.getAllCards());
        Board.initializeZones(players);
        context.setZones(Board.zones);
        context.setPlayers(players);
        Player.setMyName("Sean");
        context.setAgeOneDeck(AgeOneWesterosCard.getAllCards());
        context.setAgeTwoDeck(AgeTwoWesterosCard.getAllCards());
        context.setAgeThreeDeck(AgeThreeWesterosCard.getAllCards());
        GameContext.setMyGameContext(context);
        testAThroneOfBlades();
        testDarkWingsDarkWords();
        testPutToTheSword();
    }

    private void testAThroneOfBlades() {
        context.getAgeOneDeck().add(0,
                new AgeOneWesterosCard(AgeOneWesterosCard.A_THRONE_OF_BLADES));
        context.getZones().get(Land.DRAGONSTONE)
                .addUnit(new Footman(player3.getName()));
        context.getZones().get(Land.RIVERRUN)
                .addUnit(new Footman(player2.getName()));
        context.getZones().get(Land.RIVERRUN)
                .addUnit(new Knight(player2.getName()));
        WesterosPhase phase = new WesterosPhase(null);
        phase.drawWesterosCards();
    }

    private void testDarkWingsDarkWords() {
        context.getAgeTwoDeck()
                .add(0,
                        new AgeTwoWesterosCard(
                                AgeTwoWesterosCard.DARK_WINGS_DARK_WORDS));
        WesterosPhase phase = new WesterosPhase(null);
        for (Player player : context.getPlayers()) {
            player.setInfluence(10);
        }
        phase.drawWesterosCards();
    }

    private void testPutToTheSword() {
        context.getAgeThreeDeck()
                .add(0,
                        new AgeThreeWesterosCard(
                                AgeThreeWesterosCard.PUT_TO_THE_SWORD));
        WesterosPhase phase = new WesterosPhase(null);
        for (Player player : context.getPlayers()) {
            player.setInfluence(10);
        }
        phase.drawWesterosCards();
    }
}
