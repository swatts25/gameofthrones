package test;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;

import client.Board;
import client.JavaFXLauncher;
import client.model.GameContext;
import client.model.House;
import client.model.Player;
import controllers.MainScreenPanelController;

public class __MainScreenPanelTest {
	@Test
	public void test() {
		JavaFXLauncher launcher = new JavaFXLauncher();
		MainScreenPanelController controller = new MainScreenPanelController();
		Thread guiThread = new Thread(new Runnable() {
			@Override
			public void run() {
				launcher.show(controller, "Game of Thrones Board Game Test");
			}
		});
		guiThread.start();
		while (controller.getScene() == null) {
			//Wait for panel to initialize
		}
		List<Player> players = new ArrayList<Player>();
		GameContext context = new GameContext();
		GameContext.setMyGameContext(context);
		context.setZones(Board.zones);
		context.setPlayers(players);
		Player p1 = new Player("Sean");
		Player.setMyName("Sean");
		p1.setHouse(House.TYRELL);
		Player p2 = new Player("C");
		p2.setHouse(House.BARATHEON);
		Player p3 = new Player("W");
		p3.setHouse(House.GREYJOY);
		Player p4 = new Player("K");
		p4.setHouse(House.MARTELL);
		Player p5 = new Player("R");
		p5.setHouse(House.STARK);
		Player p6 = new Player("N");
		p6.setHouse(House.TYRELL);
		players.add(p1);
		players.add(p2);
		players.add(p3);
		players.add(p4);
		players.add(p5);
		players.add(p6);
		Board.initializeZones(players);
		controller.updateMyGUI();
		try {
			guiThread.join();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}






