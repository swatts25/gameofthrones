package test;

import java.awt.Desktop;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import org.junit.Test;

import utils.MapBuilder;
import utils.SavedGameData;
import client.model.House;
import client.model.Order;
import client.model.zones.Land;
import client.model.zones.Sea;

public class __GraphicsTest {

    @Test
    public void test() {
        final Path destination = Paths.get(SavedGameData.BASE_BOARD_FILENAME);
        InputStream is = getClass()
                .getResourceAsStream(SavedGameData.BASE_BOARD_LOCATION);
        File map = destination.toFile();
        if (!map.exists()) {
            try {
                Files.copy(is, destination);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        MapBuilder mapBoard = new MapBuilder(map);
//        addInfluenceTokens(mapBoard);
        mapBoard.placeKingsCourt();
        addOrderTokens(mapBoard);
        addHouseTokens(mapBoard);
        addSupplyTokens(mapBoard);
        addVictoryTokens(mapBoard);
        addShipTokens(mapBoard);
        try {
            Desktop.getDesktop().open(mapBoard.generateImage());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void addShipTokens(MapBuilder mapBoard) {
        mapBoard.addShipTokenToSea(House.LANNISTER, new Sea(Sea.BAY_OF_ICE), 1);
        mapBoard.addShipTokenToSea(House.LANNISTER, new Sea(Sea.BLACKWATER_BAY), 2);
        mapBoard.addShipTokenToSea(House.MARTELL, new Sea(Sea.EAST_SUMMER_SEA), 3);
        mapBoard.addShipTokenToSea(House.STARK, new Sea(Sea.IRONMANS_BAY), 4);
        mapBoard.addShipTokenToSea(House.BARATHEON, new Sea(Sea.REDWYNE_STRAIGHTS), 1);
        mapBoard.addShipTokenToSea(House.BARATHEON, new Sea(Sea.SEA_OF_DORNE), 2);
        mapBoard.addShipTokenToSea(House.GREYJOY, new Sea(Sea.SHIPBREAKER_BAY), 3);
        mapBoard.addShipTokenToSea(House.GREYJOY, new Sea(Sea.SUNSET_SEA), 4);
        mapBoard.addShipTokenToSea(House.TYRELL, new Sea(Sea.THE_GOLDEN_SOUND), 1);
        mapBoard.addShipTokenToSea(House.MARTELL, new Sea(Sea.THE_NARROW_SEA), 2);
        mapBoard.addShipTokenToSea(House.STARK, new Sea(Sea.THE_SHIVERING_SEA), 3);
        mapBoard.addShipTokenToSea(House.TYRELL, new Sea(Sea.WEST_SUMMER_SEA), 4);
    }

    public static MapBuilder addInfluenceTokens(MapBuilder mapBoard) {
        mapBoard.addInfluenceTokenToMap(House.STARK, new Land(Land.BLACKWATER));
        mapBoard.addInfluenceTokenToMap(House.STARK,
                new Land(Land.CASTLE_BLACK));
        mapBoard.addInfluenceTokenToMap(House.STARK,
                new Land(Land.CRACKLAW_POINT));
        mapBoard.addInfluenceTokenToMap(House.STARK,
                new Land(Land.DORNISH_MARCHES));
        mapBoard.addInfluenceTokenToMap(House.STARK,
                new Land(Land.DRAGONSTONE));
        mapBoard.addInfluenceTokenToMap(House.STARK,
                new Land(Land.FLINTS_FINGER));
        mapBoard.addInfluenceTokenToMap(House.STARK,
                new Land(Land.GREYWATER_WATCH));
        mapBoard.addInfluenceTokenToMap(House.STARK, new Land(Land.HARRENHAL));
        mapBoard.addInfluenceTokenToMap(House.BARATHEON,
                new Land(Land.HIGHGARDEN));
        mapBoard.addInfluenceTokenToMap(House.BARATHEON,
                new Land(Land.KARHOLD));
        mapBoard.addInfluenceTokenToMap(House.BARATHEON,
                new Land(Land.KINGS_LANDING));
        mapBoard.addInfluenceTokenToMap(House.BARATHEON,
                new Land(Land.KINGSWOOD));
        mapBoard.addInfluenceTokenToMap(House.BARATHEON,
                new Land(Land.LANNISPORT));
        mapBoard.addInfluenceTokenToMap(House.BARATHEON,
                new Land(Land.MOAT_CALIN));
        mapBoard.addInfluenceTokenToMap(House.BARATHEON,
                new Land(Land.OLDTOWN));
        mapBoard.addInfluenceTokenToMap(House.GREYJOY,
                new Land(Land.PRINCES_PASS));
        mapBoard.addInfluenceTokenToMap(House.GREYJOY, new Land(Land.PYKE));
        mapBoard.addInfluenceTokenToMap(House.GREYJOY, new Land(Land.RIVERRUN));
        mapBoard.addInfluenceTokenToMap(House.GREYJOY,
                new Land(Land.SALT_SHORE));
        mapBoard.addInfluenceTokenToMap(House.GREYJOY, new Land(Land.SEAGARD));
        mapBoard.addInfluenceTokenToMap(House.GREYJOY,
                new Land(Land.SEAROADMARCHES));
        mapBoard.addInfluenceTokenToMap(House.GREYJOY, new Land(Land.STARFALL));
        mapBoard.addInfluenceTokenToMap(House.LANNISTER,
                new Land(Land.STONEY_SEPT));
        mapBoard.addInfluenceTokenToMap(House.LANNISTER,
                new Land(Land.STORMS_END));
        mapBoard.addInfluenceTokenToMap(House.LANNISTER,
                new Land(Land.SUNSPEAR));
        mapBoard.addInfluenceTokenToMap(House.LANNISTER,
                new Land(Land.THE_ARBOR));
        mapBoard.addInfluenceTokenToMap(House.LANNISTER,
                new Land(Land.THE_BONEWAY));
        mapBoard.addInfluenceTokenToMap(House.LANNISTER,
                new Land(Land.THE_EYRIE));
        mapBoard.addInfluenceTokenToMap(House.MARTELL,
                new Land(Land.THE_FINGERS));
        mapBoard.addInfluenceTokenToMap(House.MARTELL,
                new Land(Land.THE_MOUNTAINS_OF_THE_MOON));
        mapBoard.addInfluenceTokenToMap(House.MARTELL,
                new Land(Land.THE_REACH));
        mapBoard.addInfluenceTokenToMap(House.MARTELL,
                new Land(Land.THE_STONY_SHORE));
        mapBoard.addInfluenceTokenToMap(House.MARTELL,
                new Land(Land.THE_TWINS));
        mapBoard.addInfluenceTokenToMap(House.MARTELL,
                new Land(Land.THREE_TOWERS));
        mapBoard.addInfluenceTokenToMap(House.TYRELL,
                new Land(Land.WHITE_HARBOR));
        mapBoard.addInfluenceTokenToMap(House.TYRELL,
                new Land(Land.WIDOWS_WATCH));
        mapBoard.addInfluenceTokenToMap(House.TYRELL,
                new Land(Land.WINTERFELL));
        mapBoard.addInfluenceTokenToMap(House.TYRELL, new Land(Land.YRONWOOD));
        mapBoard.addInfluenceTokenToMap(House.MARTELL, new Sea(Sea.BAY_OF_ICE));
        mapBoard.addInfluenceTokenToMap(House.MARTELL,
                new Sea(Sea.BLACKWATER_BAY));
        mapBoard.addInfluenceTokenToMap(House.MARTELL,
                new Sea(Sea.EAST_SUMMER_SEA));
        mapBoard.addInfluenceTokenToMap(House.MARTELL,
                new Sea(Sea.IRONMANS_BAY));
        mapBoard.addInfluenceTokenToMap(House.LANNISTER,
                new Sea(Sea.REDWYNE_STRAIGHTS));
        mapBoard.addInfluenceTokenToMap(House.LANNISTER,
                new Sea(Sea.SEA_OF_DORNE));
        mapBoard.addInfluenceTokenToMap(House.LANNISTER,
                new Sea(Sea.SHIPBREAKER_BAY));
        mapBoard.addInfluenceTokenToMap(House.GREYJOY, new Sea(Sea.SUNSET_SEA));
        mapBoard.addInfluenceTokenToMap(House.GREYJOY,
                new Sea(Sea.THE_GOLDEN_SOUND));
        mapBoard.addInfluenceTokenToMap(House.BARATHEON,
                new Sea(Sea.THE_NARROW_SEA));
        mapBoard.addInfluenceTokenToMap(House.BARATHEON,
                new Sea(Sea.THE_SHIVERING_SEA));
        mapBoard.addInfluenceTokenToMap(House.STARK,
                new Sea(Sea.WEST_SUMMER_SEA));
        return mapBoard;
    }

    public static MapBuilder addOrderTokens(MapBuilder mapBoard) {
        mapBoard.addOrderTokenToMap(new Order(Order.MARCH_ORDER, 1, true),
                new Land(Land.BLACKWATER));
        mapBoard.addOrderTokenToMap(new Order(Order.MARCH_ORDER, 1, true),
                new Land(Land.CASTLE_BLACK));
        mapBoard.addOrderTokenToMap(new Order(Order.MARCH_ORDER, 1, true),
                new Land(Land.CRACKLAW_POINT));
        mapBoard.addOrderTokenToMap(new Order(Order.MARCH_ORDER, 0),
                new Land(Land.DORNISH_MARCHES));
        mapBoard.addOrderTokenToMap(new Order(Order.MARCH_ORDER, 0),
                new Land(Land.DRAGONSTONE));
        mapBoard.addOrderTokenToMap(new Order(Order.MARCH_ORDER, 0),
                new Land(Land.FLINTS_FINGER));
        mapBoard.addOrderTokenToMap(new Order(Order.MARCH_ORDER, -1),
                new Land(Land.GREYWATER_WATCH));
        mapBoard.addOrderTokenToMap(new Order(Order.MARCH_ORDER, -1),
                new Land(Land.HARRENHAL));
        mapBoard.addOrderTokenToMap(new Order(Order.MARCH_ORDER, -1),
                new Land(Land.HIGHGARDEN));
        mapBoard.addOrderTokenToMap(new Order(Order.CONSOLODATE_ORDER, 0, true),
                new Land(Land.KARHOLD));
        mapBoard.addOrderTokenToMap(new Order(Order.CONSOLODATE_ORDER, 0, true),
                new Land(Land.KINGS_LANDING));
        mapBoard.addOrderTokenToMap(new Order(Order.CONSOLODATE_ORDER, 0, true),
                new Land(Land.KINGSWOOD));
        mapBoard.addOrderTokenToMap(new Order(Order.CONSOLODATE_ORDER, 0),
                new Land(Land.LANNISPORT));
        mapBoard.addOrderTokenToMap(new Order(Order.CONSOLODATE_ORDER, 0),
                new Land(Land.MOAT_CALIN));
        mapBoard.addOrderTokenToMap(new Order(Order.CONSOLODATE_ORDER, 0),
                new Land(Land.OLDTOWN));
        mapBoard.addOrderTokenToMap(new Order(Order.CONSOLODATE_ORDER, 0),
                new Land(Land.PRINCES_PASS));
        mapBoard.addOrderTokenToMap(new Order(Order.CONSOLODATE_ORDER, 0),
                new Land(Land.PYKE));
        mapBoard.addOrderTokenToMap(new Order(Order.CONSOLODATE_ORDER, 0),
                new Land(Land.RIVERRUN));
        mapBoard.addOrderTokenToMap(new Order(Order.CONSOLODATE_ORDER, 0),
                new Land(Land.SALT_SHORE));
        mapBoard.addOrderTokenToMap(new Order(Order.CONSOLODATE_ORDER, 0),
                new Land(Land.SEAGARD));
        mapBoard.addOrderTokenToMap(new Order(Order.DEFENSE_ORDER, 2, true),
                new Land(Land.SEAROADMARCHES));
        mapBoard.addOrderTokenToMap(new Order(Order.DEFENSE_ORDER, 2, true),
                new Land(Land.STARFALL));
        mapBoard.addOrderTokenToMap(new Order(Order.DEFENSE_ORDER, 2, true),
                new Land(Land.STONEY_SEPT));
        mapBoard.addOrderTokenToMap(new Order(Order.DEFENSE_ORDER, 1),
                new Land(Land.STORMS_END));
        mapBoard.addOrderTokenToMap(new Order(Order.DEFENSE_ORDER, 1),
                new Land(Land.SUNSPEAR));
        mapBoard.addOrderTokenToMap(new Order(Order.DEFENSE_ORDER, 1),
                new Land(Land.THE_ARBOR));
        mapBoard.addOrderTokenToMap(new Order(Order.DEFENSE_ORDER, 1),
                new Land(Land.THE_BONEWAY));
        mapBoard.addOrderTokenToMap(new Order(Order.DEFENSE_ORDER, 1),
                new Land(Land.THE_EYRIE));
        mapBoard.addOrderTokenToMap(new Order(Order.DEFENSE_ORDER, 1),
                new Land(Land.THE_FINGERS));
        mapBoard.addOrderTokenToMap(new Order(Order.RAID_ORDER, 0, true),
                new Land(Land.THE_MOUNTAINS_OF_THE_MOON));
        mapBoard.addOrderTokenToMap(new Order(Order.RAID_ORDER, 0, true),
                new Land(Land.THE_REACH));
        mapBoard.addOrderTokenToMap(new Order(Order.RAID_ORDER, 0),
                new Land(Land.THE_STONY_SHORE));
        mapBoard.addOrderTokenToMap(new Order(Order.RAID_ORDER, 0),
                new Land(Land.THE_TWINS));
        mapBoard.addOrderTokenToMap(new Order(Order.RAID_ORDER, 0),
                new Land(Land.THREE_TOWERS));
        mapBoard.addOrderTokenToMap(new Order(Order.SUPPORT_ORDER, 1, true),
                new Land(Land.WHITE_HARBOR));
        mapBoard.addOrderTokenToMap(new Order(Order.SUPPORT_ORDER, 1, true),
                new Land(Land.WIDOWS_WATCH));
        mapBoard.addOrderTokenToMap(new Order(Order.SUPPORT_ORDER, 0),
                new Land(Land.WINTERFELL));
        mapBoard.addOrderTokenToMap(new Order(Order.SUPPORT_ORDER, 0),
                new Land(Land.YRONWOOD));
        mapBoard.addOrderTokenToMap(new Order(Order.SUPPORT_ORDER, 0),
                new Sea(Sea.BAY_OF_ICE));
        mapBoard.addOrderTokenToMap(new Order(Order.SUPPORT_ORDER, 0),
                new Sea(Sea.BLACKWATER_BAY));
        mapBoard.addOrderTokenToMap(new Order(Order.SUPPORT_ORDER, 0),
                new Sea(Sea.EAST_SUMMER_SEA));
        mapBoard.addOrderTokenToMap(new Order(Order.MARCH_ORDER, 0),
                new Sea(Sea.IRONMANS_BAY));
        mapBoard.addOrderTokenToMap(new Order(Order.MARCH_ORDER, 0),
                new Sea(Sea.REDWYNE_STRAIGHTS));
        mapBoard.addOrderTokenToMap(new Order(Order.MARCH_ORDER, 0),
                new Sea(Sea.SEA_OF_DORNE));
        mapBoard.addOrderTokenToMap(new Order(Order.RAID_ORDER, 0),
                new Sea(Sea.SHIPBREAKER_BAY));
        mapBoard.addOrderTokenToMap(new Order(Order.RAID_ORDER, 0),
                new Sea(Sea.SUNSET_SEA));
        mapBoard.addOrderTokenToMap(new Order(Order.RAID_ORDER, 0),
                new Sea(Sea.THE_GOLDEN_SOUND));
        mapBoard.addOrderTokenToMap(new Order(Order.DEFENSE_ORDER, 0),
                new Sea(Sea.THE_NARROW_SEA));
        mapBoard.addOrderTokenToMap(new Order(Order.DEFENSE_ORDER, 0),
                new Sea(Sea.THE_SHIVERING_SEA));
        mapBoard.addOrderTokenToMap(new Order(Order.DEFENSE_ORDER, 0),
                new Sea(Sea.WEST_SUMMER_SEA));
        return mapBoard;
    }

    public static MapBuilder addHouseTokens(MapBuilder mapBoard) {
        mapBoard.addPlayerTokenToIronThroneTrack(House.BARATHEON, 1);
        mapBoard.addPlayerTokenToIronThroneTrack(House.GREYJOY, 2);
        mapBoard.addPlayerTokenToIronThroneTrack(House.LANNISTER, 3);
        mapBoard.addPlayerTokenToIronThroneTrack(House.MARTELL, 4);
        mapBoard.addPlayerTokenToIronThroneTrack(House.STARK, 5);
        mapBoard.addPlayerTokenToIronThroneTrack(House.TYRELL, 6);
        mapBoard.addPlayerTokenToFiefdomsTrack(House.BARATHEON, 1);
        mapBoard.addPlayerTokenToFiefdomsTrack(House.GREYJOY, 2);
        mapBoard.addPlayerTokenToFiefdomsTrack(House.LANNISTER, 3);
        mapBoard.addPlayerTokenToFiefdomsTrack(House.MARTELL, 4);
        mapBoard.addPlayerTokenToFiefdomsTrack(House.STARK, 5);
        mapBoard.addPlayerTokenToFiefdomsTrack(House.TYRELL, 6);
        mapBoard.addPlayerTokenToKingsCourtTrack(House.BARATHEON, 1);
        mapBoard.addPlayerTokenToKingsCourtTrack(House.GREYJOY, 2);
        mapBoard.addPlayerTokenToKingsCourtTrack(House.LANNISTER, 3);
        mapBoard.addPlayerTokenToKingsCourtTrack(House.MARTELL, 4);
        mapBoard.addPlayerTokenToKingsCourtTrack(House.STARK, 5);
        mapBoard.addPlayerTokenToKingsCourtTrack(House.TYRELL, 6);
        return mapBoard;
    }

    public static MapBuilder addSupplyTokens(MapBuilder mapBoard) {
        for (int i = 0; i < 7; i++) {
            mapBoard.addSupplyTokenToSupplyTrack(House.BARATHEON, i);
            mapBoard.addSupplyTokenToSupplyTrack(House.GREYJOY, i);
            mapBoard.addSupplyTokenToSupplyTrack(House.LANNISTER, i);
            mapBoard.addSupplyTokenToSupplyTrack(House.MARTELL, i);
            mapBoard.addSupplyTokenToSupplyTrack(House.STARK, i);
            mapBoard.addSupplyTokenToSupplyTrack(House.TYRELL, i);
        }
        return mapBoard;
    }
    
    public static MapBuilder addVictoryTokens(MapBuilder mapBoard) {
        for (int i = 1; i < 8; i++) {
            mapBoard.addVictoryTokenToVictoryTrack(House.BARATHEON, i);
            mapBoard.addVictoryTokenToVictoryTrack(House.GREYJOY, i);
            mapBoard.addVictoryTokenToVictoryTrack(House.LANNISTER, i);
            mapBoard.addVictoryTokenToVictoryTrack(House.MARTELL, i);
            mapBoard.addVictoryTokenToVictoryTrack(House.STARK, i);
            mapBoard.addVictoryTokenToVictoryTrack(House.TYRELL, i);
        }
        return mapBoard;
    }
}
