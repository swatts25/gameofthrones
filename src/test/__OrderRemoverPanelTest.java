package test;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;

import client.Board;
import client.JavaFXLauncher;
import client.model.GameContext;
import client.model.House;
import client.model.Order;
import client.model.Player;
import client.model.Turn;
import client.model.cards.LannisterHouseCard;
import client.model.cards.StarkHouseCard;
import client.model.zones.Land;
import client.model.zones.Sea;
import controllers.OrderRemoverPanelController;

public class __OrderRemoverPanelTest {

    @Test
    public void test() {
        GameContext context = new GameContext();
        List<Player> players = new ArrayList<Player>();
        Player player1 = new Player();
        player1.setName("Sean");
        player1.setHouse(House.STARK);
        player1.setInfluence(10);
        player1.setFieldomsTrack(1);
        players.add(player1);
        context.setStarkHouseDeck(StarkHouseCard.getAllCards());
        Player player2 = new Player();
        player2.setName("Will");
        player2.setHouse(House.LANNISTER);
        players.add(player2);
        player2.setFieldomsTrack(2);
        context.setLannisterHouseDeck(LannisterHouseCard.getAllCards());
        // Player player3 = new Player();
        // player3.setName("Chris");
        // player3.setHouse(House.LANNISTER);
        Board.initializeZones(players);
        context.setZones(Board.zones);
        context.setPlayers(players);
        Player.setMyName("Sean");
        context.getZones().get(Land.BLACKWATER)
                .setOrder(new Order(Order.CONSOLODATE_ORDER, 0, true));
         context.getZones().get(Land.BLACKWATER).setControlledByName(player2.getName());
        context.getZones().get(Land.CRACKLAW_POINT)
                .setOrder(new Order(Order.SUPPORT_ORDER, 0, true));
         context.getZones().get(Land.CRACKLAW_POINT).setControlledByName(player1.getName());
        context.getZones().get(Land.KINGSWOOD)
                .setOrder(new Order(Order.MARCH_ORDER, 0, true));
         context.getZones().get(Land.KINGSWOOD).setControlledByName(player2.getName());
        context.getZones().get(Sea.BLACKWATER_BAY)
                .setOrder(new Order(Order.DEFENSE_ORDER, 2, true));
         context.getZones().get(Sea.BLACKWATER_BAY).setControlledByName(player2.getName());
        GameContext.setMyGameContext(context);
        JavaFXLauncher launcher = new JavaFXLauncher();
        Turn turn = new Turn(player2);
        turn.setType(Turn.CERSEI_LANNISTER);
        OrderRemoverPanelController controller = new OrderRemoverPanelController(
                Land.KINGS_LANDING, player2.getName(), null, null, turn);
        launcher.show(controller, "Order Remover Test");
    }
}
