package test;

import static org.junit.Assert.assertEquals;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Stack;

import org.junit.Test;

import utils.GameOfThronesMarshaller;
import client.Combat;
import client.model.Footman;
import client.model.GameContext;
import client.model.Knight;
import client.model.Order;
import client.model.Player;
import client.model.Ship;
import client.model.SiegeEngine;
import client.model.Turn;
import client.model.cards.Deck;
import client.model.cards.WildlingCard;
import client.model.zones.Land;
import client.model.zones.Sea;
import client.model.zones.Zone;

public class __XMLMarshallerTest {

    private File testXmlFile;

    @Test
    public void test() {
        Map<String, Zone> zones = new HashMap<String, Zone>();
        Deck<WildlingCard> wildlingDeck = Deck.getNewWildlingDeck();
        WildlingCard topWildlingCard = wildlingDeck.seeTopCard();
        Land zone1 = new Land(Land.WINTERFELL);
        zones.put(Land.WINTERFELL, zone1);
        Sea zone2 = new Sea(Sea.BAY_OF_ICE);
        zones.put(Sea.BAY_OF_ICE, zone2);
        List<Player> players = new ArrayList<Player>();
        Player player1 = new Player();
        player1.setName("Sean");
        players.add(player1);
        Footman footman = new Footman(player1.getName());
        zone1.addFootman(footman);
        Knight knight = new Knight(player1.getName());
        zone1.addKnight(knight);
        SiegeEngine siegeEngine = new SiegeEngine(player1.getName());
        zone1.addSiegeEngine(siegeEngine);
        Order testOrder = new Order();
        zone1.setOrder(testOrder);
        Ship ship = new Ship(player1.getName());
        zone2.addShip(ship);
        GameContext context = new GameContext();
        context.setZones(zones);
        context.setPlayers(players);
        context.setWildlingDeck(wildlingDeck.getCardsAsList());
        Stack<Turn> turns = new Stack<Turn>();
        Turn turn = new Turn(player1);
        turn.setType(Turn.PLANNING_PHASE);
        List<Player> turnPlayers = new ArrayList<Player>();
        turnPlayers.add(player1);
        Turn turn2 = new Turn(player1);
        turn2.setType(Turn.PLANNING_PHASE);
        turns.push(turn);
        turns.push(turn2);
        context.setTurns(turns);
//        Combat combat = new Combat(Land.WINTERFELL, player1.getName(), player1.getName());
//        combat.setContext(context);
        testXmlFile = GameOfThronesMarshaller.marshall(context);
        GameContext result = GameOfThronesMarshaller
                .unMarshallGameContext(testXmlFile);
        assertEquals(Land.WINTERFELL,
                result.getZones().get(Land.WINTERFELL).getName());
        assertEquals(Sea.BAY_OF_ICE,
                result.getZones().get(Sea.BAY_OF_ICE).getName());
        assertEquals("Sean", result.getPlayers().get(0).getName());
        assertEquals(topWildlingCard.getName(),
                result.getWildlingDeck().get(0).getName());
        assertEquals(testOrder.getName(),
                result.getZones().get(Land.WINTERFELL).getOrder().getName());
    }
}
