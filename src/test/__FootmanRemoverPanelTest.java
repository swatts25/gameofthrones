package test;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;

import client.Board;
import client.JavaFXLauncher;
import client.model.Footman;
import client.model.GameContext;
import client.model.House;
import client.model.MarchMovement;
import client.model.Player;
import client.model.cards.LannisterHouseCard;
import client.model.cards.StarkHouseCard;
import client.model.zones.Land;
import client.model.zones.Sea;
import controllers.FootmanRemoverPanelController;

public class __FootmanRemoverPanelTest {

    @Test
    public void test() {
        GameContext context = new GameContext();
        List<Player> players = new ArrayList<Player>();
        Player player1 = new Player();
        player1.setName("Sean");
        player1.setHouse(House.STARK);
        player1.setInfluence(10);
        player1.setFieldomsTrack(1);
        players.add(player1);
        context.setStarkHouseDeck(StarkHouseCard.getAllCards());
        Player player2 = new Player();
        player2.setName("Will");
        player2.setHouse(House.LANNISTER);
        players.add(player2);
        player2.setFieldomsTrack(2);
        context.setLannisterHouseDeck(LannisterHouseCard.getAllCards());
        Board.initializeZones(players);
        context.setZones(Board.zones);
        context.setPlayers(players);
        Player.setMyName("Sean");
         context.getZones().get(Land.BLACKWATER).setControlledByName(player2.getName());
         MarchMovement march = new MarchMovement(Land.WINTERFELL, Land.BLACKWATER, new Footman(player2.getName()));
         context.getZones().get(Land.BLACKWATER).getMarches().add(march);
         context.getZones().get(Land.CRACKLAW_POINT).setControlledByName(player1.getName());
         context.getZones().get(Land.KINGSWOOD).setControlledByName(player2.getName());
         context.getZones().get(Land.KINGSWOOD).addUnit(new Footman(player2.getName()));
         context.getZones().get(Sea.BLACKWATER_BAY).setControlledByName(player2.getName());
         context.getZones().get(Sea.BLACKWATER_BAY).addUnit(new Footman(player1.getName()));
        GameContext.setMyGameContext(context);
        JavaFXLauncher launcher = new JavaFXLauncher();
        FootmanRemoverPanelController controller = new FootmanRemoverPanelController(
                Land.KINGS_LANDING, player2.getName(), null, null, null);
        launcher.show(controller, "Order Remover Test");
    }
}
