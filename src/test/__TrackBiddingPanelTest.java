package test;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import org.junit.Test;

import client.JavaFXLauncher;
import client.model.GameContext;
import client.model.Player;
import client.model.TrackBiddingData;
import client.model.Turn;
import controllers.TrackBiddingPanelController;

public class __TrackBiddingPanelTest {

    @Test
    public void test() {
        GameContext context = new GameContext();
        GameContext.setMyGameContext(context);
        TrackBiddingData data = new TrackBiddingData();
        Map<String, int[]> bids = new HashMap<String, int[]>();
        Map<String, ArrayList<String>> playerOrderPreference = new HashMap<String, ArrayList<String>>();
        ArrayList<Player> players = new ArrayList<Player>();
        Player player1 = new Player("Sean");
        player1.setInfluence(10);
        player1.setIronThroneTrack(1);
        players.add(player1);
        Player player2 = new Player("Chris");
        player2.setInfluence(10);
        player2.setIronThroneTrack(2);
        bids.put(player2.getName(), new int[] { 2, 0, 0 });
        ArrayList<String> player2Pref = new ArrayList<String>();
        player2Pref.add("Kat");
        player2Pref.add("Will");
        player2Pref.add("Sean");
        playerOrderPreference.put(player2.getName(), player2Pref);
        players.add(player2);
        Player player3 = new Player("Will");
        player3.setInfluence(10);
        player3.setIronThroneTrack(3);
        bids.put(player3.getName(), new int[] { 6, 0, 0 });
        ArrayList<String> player3Pref = new ArrayList<String>();
        player3Pref.add("Sean");
        player3Pref.add("Chris");
        player3Pref.add("Kat");
        playerOrderPreference.put(player3.getName(), player3Pref);
        players.add(player3);
        Player player4 = new Player("Kat");
        player4.setInfluence(10);
        player4.setIronThroneTrack(4);
        bids.put(player4.getName(), new int[] { 6, 0, 0 });
        ArrayList<String> player4Pref = new ArrayList<String>();
        player4Pref.add("Chris");
        player4Pref.add("Sean");
        player4Pref.add("Will");
        playerOrderPreference.put(player4.getName(), player4Pref);
        players.add(player4);
        data.setBidsMap(bids);
        data.setPlayerOrderMap(playerOrderPreference);
        context.setPlayers(players);
        context.setBiddingData(data);
        JavaFXLauncher launcher = new JavaFXLauncher();
        TrackBiddingPanelController controller = new TrackBiddingPanelController(
                new Turn(players.get(0)), null);
        launcher.show(controller, "Track Bidding Test");
    }
}
