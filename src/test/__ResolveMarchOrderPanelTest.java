package test;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.junit.Test;

import utils.MapBuilder;
import utils.SavedGameData;
import client.Board;
import client.Client;
import client.JavaFXLauncher;
import client.model.Footman;
import client.model.GameContext;
import client.model.House;
import client.model.Knight;
import client.model.Order;
import client.model.Player;
import client.model.Ship;
import client.model.zones.Land;
import client.model.zones.Sea;
import client.model.zones.Zone;
import controllers.MarchOrderResolverPanelController;

public class __ResolveMarchOrderPanelTest {

    @Test
    public void test() {
        List<Player> players = new ArrayList<Player>();
        Player player1 = new Player();
        player1.setName("Sean");
        player1.setHouse(House.STARK);
        players.add(player1);
        Player player2 = new Player();
        player2.setName("Will");
        player2.setHouse(House.BARATHEON);
        players.add(player2);
        Player player3 = new Player();
        player3.setName("Chris");
        player3.setHouse(House.LANNISTER);
        players.add(player3);
        Board.initializeZones(players);
        GameContext context = new GameContext();
        context.setZones(Board.zones);
        context.setPlayers(players);
        Player.setMyName("Sean");
        GameContext.setMyGameContext(context);
        Map<String, Zone> zones = context.getZones();
        Land winterfell = (Land) zones.get(Land.WINTERFELL);
        winterfell.setOrder(new Order(Order.MARCH_ORDER, 0));
        winterfell.setControlledByName(player1.getName());
        winterfell.addFootman(new Footman(player1.getName()));
        // winterfell.addFootman(new Footman(player1));
        winterfell.addKnight(new Knight(player1.getName()));
        // winterfell.addKnight(new Knight(player1));
        // winterfell.addKnight(new Knight(player1));
        // winterfell.addSiegeEngine(new SiegeEngine(player1));
        Zone castleBlack = zones.get(Land.CASTLE_BLACK);
        castleBlack.setOrder(new Order(Order.MARCH_ORDER, 0));
        castleBlack.setControlledByName(player1.getName());
        Zone whiteHarbor = zones.get(Land.WHITE_HARBOR);
        whiteHarbor.setOrder(new Order(Order.MARCH_ORDER, 1, true));
        whiteHarbor.setControlledByName(player1.getName());
        Sea theShiveringSea = (Sea) zones.get(Sea.THE_SHIVERING_SEA);
        theShiveringSea.setOrder(new Order(Order.MARCH_ORDER, -1, true));
        theShiveringSea.setControlledByName(player1.getName());
        theShiveringSea.addShip(new Ship(player1.getName()));
        Sea theNarrowSea = (Sea) zones.get(Sea.THE_NARROW_SEA);
        theNarrowSea.setOrder(new Order(Order.MARCH_ORDER, -1, true));
        theNarrowSea.setControlledByName(player1.getName());
        theNarrowSea.addShip(new Ship(player1.getName()));
        Sea shipbreakerBay = (Sea) zones.get(Sea.SHIPBREAKER_BAY);
        shipbreakerBay.setOrder(new Order(Order.MARCH_ORDER, -1, true));
        shipbreakerBay.setControlledByName(player1.getName());
        shipbreakerBay.addShip(new Ship(player1.getName()));
        Sea eastSummerSea = (Sea) zones.get(Sea.EAST_SUMMER_SEA);
        eastSummerSea.setOrder(new Order(Order.MARCH_ORDER, -1, true));
        eastSummerSea.setControlledByName(player1.getName());
        eastSummerSea.addShip(new Ship(player1.getName()));
        Zone theStonyShore = zones.get(Land.THE_STONY_SHORE);
        theStonyShore.setControlledByName(player3.getName());
        theStonyShore.setOrder(new Order(Order.MARCH_ORDER, 1, true));
        Zone moatCalin = zones.get(Land.MOAT_CALIN);
        moatCalin.setControlledByName(player2.getName());
        moatCalin.setOrder(new Order(Order.MARCH_ORDER, 2, true));
        saveMap();
        JavaFXLauncher launcher = new JavaFXLauncher();
        MarchOrderResolverPanelController controller = new MarchOrderResolverPanelController(
                null, null);
        launcher.show(controller, "March Order Resolver Test");
    }

    private void saveMap() {
        try {
            final Path destination = Paths
                    .get(SavedGameData.BASE_BOARD_FILENAME);
            InputStream is = getClass()
                    .getResourceAsStream(SavedGameData.BASE_BOARD_LOCATION);
            File map = destination.toFile();
            if (!map.exists()) {
                Files.copy(is, destination);
            }
            if (GameContext.getMyGameContext() != null
                    && GameContext.getMyGameContext().getZones() != null) {
                MapBuilder boardmap = new MapBuilder(map);
                List<Zone> zones = new ArrayList<Zone>(
                        GameContext.getMyGameContext().getZones().values());
                for (Zone zone : zones) {
                    if (zone.getControlledBy() != null) {
                        boardmap.addInfluenceTokenToMap(zone.getControlledBy().getHouse(),
                                zone);
                    }
                }
                boardmap.generateImage();
            }
        } catch (IOException e) {
            Client.error(e);
        }
    }
}
