package test;

import org.junit.Test;

import client.JavaFXLauncher;
import client.model.cards.AgeOneWesterosCard;
import controllers.WesterosCardsViewerPanelController;

public class __WesterosCardsViewerPanelTest {

    @Test
    public void test() {
        JavaFXLauncher launcher = new JavaFXLauncher();
        launcher.show(new WesterosCardsViewerPanelController(
                new AgeOneWesterosCard(AgeOneWesterosCard.MUSTERING),
                new AgeOneWesterosCard(AgeOneWesterosCard.MUSTERING),
                new AgeOneWesterosCard(AgeOneWesterosCard.MUSTERING)),
                "Westeros Cards View Test");
    }
}
