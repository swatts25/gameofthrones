package controllers;

import java.net.URL;
import java.util.ResourceBundle;

import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.image.ImageView;
import client.model.cards.Card;

public class WildlingCardViewerPanelController extends JavaFXController implements Initializable {
	private Card card;

	public WildlingCardViewerPanelController(Card card) {
		super("/forms/BaseWildlingCardViewerPanel.fxml");
		this.card = card;
	}

	@FXML
	ImageView imvCardImage;
	

	@Override
	public void initialize(URL arg0, ResourceBundle arg1) {
		imvCardImage.setImage(card.getImage());
	}
}
