package controllers;

import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableColumn.CellDataFeatures;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.Stage;
import javafx.util.Callback;
import client.Board;
import client.Client;
import client.Combat;
import client.JavaFXLauncher;
import client.ResolveMarchOrder;
import client.TurnHandler;
import client.WesterosPhase;
import client.model.GameContext;
import client.model.MarchMovement;
import client.model.Player;
import client.model.SiegeEngine;
import client.model.Turn;
import client.model.Unit;
import client.model.zones.Land;
import client.model.zones.Zone;

/**
 * @author zggis
 */
@SuppressWarnings({ "unchecked", "rawtypes" })
public class RetreatPanelController extends JavaFXController
        implements Initializable {

    private ObservableList<Zone> tblZoneData = FXCollections
            .observableArrayList();

    @FXML
    private TableView tblZones;

    @FXML
    private Button btnDone;

    private MainScreenPanelController mainScreenPanelController;

    private Turn currentTurn;

    private String retreatingZoneName;

    private String attackerMarchedFromZone;

    public RetreatPanelController(String retreatingZoneName, String victorName,
            String attackerMarchedFromZone,
            MainScreenPanelController mainScreenPanelController,
            Turn currentTurn) {
        super("/forms/BaseRetreatPanel.fxml");
        this.mainScreenPanelController = mainScreenPanelController;
        this.currentTurn = currentTurn;
        this.retreatingZoneName = retreatingZoneName;
        this.attackerMarchedFromZone = attackerMarchedFromZone;
    }

    @Override
    public void initialize(URL arg0, ResourceBundle arg1) {
        initTableView();
        btnDone.setDisable(true);
        destroySiegeEngines();
    }

    public int getNumRetreatingUnits() {
        GameContext context = GameContext.getMyGameContext();
        int retVal = 0;
        Zone retreatingZone = context.getZones().get(retreatingZoneName);
        for (Unit unit : retreatingZone.getUnits()) {
            if (unit.getPlayerName()
                    .equalsIgnoreCase(currentTurn.getPlayerName())) {
                retVal++;
            }
        }
        for (MarchMovement march : retreatingZone.getMarches()) {
            if (march.getUnit().getPlayerName()
                    .equalsIgnoreCase(currentTurn.getPlayerName())) {
                retVal++;
            }
        }
        return retVal;
    }

    private void destroySiegeEngines() {
        GameContext context = GameContext.getMyGameContext();
        Zone retreatingzone = context.getZones().get(retreatingZoneName);
        for (Unit unit : retreatingzone.getUnits()) {
            if (unit instanceof SiegeEngine && unit.getPlayerName()
                    .equalsIgnoreCase(currentTurn.getPlayer().getName())) {
                retreatingzone.removeUnit(unit);
                Client.getLogger().info(currentTurn.getPlayer().getName()
                        + "'s SiegeEngine was destroyed in the retreat");
            }
        }
        for (MarchMovement march : retreatingzone.getMarches()) {
            if (march.getUnit() instanceof SiegeEngine
                    && march.getUnit().getPlayerName().equalsIgnoreCase(
                            currentTurn.getPlayer().getName())) {
                Client.getLogger().info(currentTurn.getPlayer().getName()
                        + "'s SiegeEngine was destroyed in the retreat");
                march.setFrom(null);
                march.setTo(null);
                march.setUnit(null);
            }
        }
        retreatingzone.cleanMarches();
    }

    private void initTableView() {
        tblZones.setEditable(true);
        TableColumn zoneCol = new TableColumn("Zone");
        zoneCol.setPrefWidth(220);
        zoneCol.setCellValueFactory(
                new PropertyValueFactory<Zone, String>("name"));
        TableColumn controlledCol = new TableColumn("Controlled By");
        controlledCol.setCellValueFactory(
                new Callback<CellDataFeatures<Zone, String>, ObservableValue<String>>() {

                    public ObservableValue<String> call(
                            CellDataFeatures<Zone, String> cell) {
                        SimpleStringProperty selectedContinent = new SimpleStringProperty();
                        if (cell.getValue() instanceof Zone) {
                            Zone zone = (Zone) cell.getValue();
                            if (zone.getControlledByName() != null) {
                                selectedContinent
                                        .setValue(zone.getControlledByName());
                            } else {
                                selectedContinent.setValue("Neutral");
                            }
                        }
                        return selectedContinent;
                    }
                });
        controlledCol.setPrefWidth(123);
        TableColumn actionCol = new TableColumn("Action");
        actionCol.setCellValueFactory(new PropertyValueFactory<>("DUMMY"));
        actionCol.setCellFactory(
                new Callback<TableColumn<Zone, String>, TableCell<Zone, String>>() {

                    @Override
                    public TableCell call(
                            final TableColumn<Zone, String> param) {
                        final TableCell<Zone, String> cell = new TableCell<Zone, String>() {

                            final Button btn = new Button("Retreat To");

                            @Override
                            public void updateItem(String item, boolean empty) {
                                super.updateItem(item, empty);
                                if (empty) {
                                    setGraphic(null);
                                    setText(null);
                                } else {
                                    btn.setOnAction((ActionEvent event) -> {
                                        Zone zone = getTableView().getItems()
                                                .get(getIndex());
                                        selectionMade(zone);
                                    });
                                    setGraphic(btn);
                                    setText(null);
                                }
                            }
                        };
                        return cell;
                    }
                });
        actionCol.setPrefWidth(78);
        tblZones.getColumns().addAll(zoneCol, controlledCol, actionCol);
        updateZonesTable();
    }

    private void updateZonesTable() {
        GameContext context = GameContext.getMyGameContext();
        Zone retreatingZone = context.getZones().get(retreatingZoneName);
        List<Zone> zoneList = new ArrayList<Zone>(context.getZones().values());
        Player retreatingPlayer = null;
        for (Player player : context.getPlayers()) {
            if (player.getName()
                    .equalsIgnoreCase(currentTurn.getPlayerName())) {
                retreatingPlayer = player;
            }
        }
        tblZoneData.clear();
        for (Zone zone : zoneList) {
            if (zone instanceof Land) {
                if (!zone.getName().equals(attackerMarchedFromZone)
                        && !zone.getName().equals(retreatingZoneName)
                        && (zone.getControlledBy() == null || zone
                                .getControlledBy().equals(retreatingPlayer))
                        && !(zone instanceof Land && ((Land) zone)
                                .getNeutralDefensePower() > 0)) {
                    boolean isValid = true;
                    for (Unit unit : retreatingZone.getUnits()) {
                        MarchMovement march = new MarchMovement(
                                retreatingZoneName, zone.getName(), unit);
                        if (!march.isValid(true)) {
                            isValid = false;
                            break;
                        }
                    }
                    if (isValid) {
                        tblZoneData.add(zone);
                    }
                }
            }
        }
        if (tblZoneData.isEmpty()) {
            JavaFXLauncher.showInformationDialog("No Retreatable Location",
                    "There is no zone by which your units can retreat, all of your units will be destroyed.");
        } else {
            tblZones.setItems(tblZoneData);
        }
    }

    private void selectionMade(Zone zone) {
        GameContext context = GameContext.getMyGameContext();
        Zone retreatingZone = context.getZones().get(retreatingZoneName);
        Client.getLogger().info("Retreating to " + zone.getName());
        List<MarchMovement> retreatMarches = new ArrayList<MarchMovement>();
        for (Unit unit : retreatingZone.getUnits()) {
            if (unit.getPlayerName()
                    .equalsIgnoreCase(currentTurn.getPlayer().getName())) {
                retreatMarches.add(new MarchMovement(retreatingZoneName,
                        zone.getName(), unit));
            }
        }
        for (MarchMovement march : retreatingZone.getMarches()) {
            if (march.getUnit().getPlayerName()
                    .equalsIgnoreCase(currentTurn.getPlayer().getName())) {
                retreatMarches.add(new MarchMovement(retreatingZoneName,
                        zone.getName(), march.getUnit()));
            }
        }
        retreatingZone.cleanMarches();
        ResolveMarchOrder marchOrder = new ResolveMarchOrder(context,
                context.getZones().get(retreatingZoneName), retreatMarches,
                currentTurn.getPlayerName());
        if (marchOrder.validateSupplyLimitAfterMarch(true)) {
            Client.getLogger().info("Supply Limit is NOT violated.");
            // Move units
            // remove units from retreating zone and its marches
            marchOrder.retreatUnits();
            endTurn(true);
        } else {
            // Calculate how many units must be destroyed to retreat here
            Client.getLogger().info("Supply Limit IS violated. You must destroy "
                    + marchOrder.getNumUnitsToDestroy() + " units");
            // Prompt user if they would like to destroy the number of units to
            // necessary to retreat here
            JavaFXLauncher.showPanel("/forms/BaseUnitEditorPanel.fxml",
                    "Destroy Units", new UnitDestroyerPanelController(
                            retreatingZoneName, this, currentTurn, marchOrder));
        }
    }

    public void endTurn(boolean unitsDestroyed) {
        GameContext context = GameContext.getMyGameContext();
        for (Combat combat : context.getCombats()) {
            if (combat.getId() == currentTurn.getCombatId()) {
                context.getCombats().remove(combat);
                break;
            }
        }
        // Remove current turn
        context.getTurns().remove(currentTurn);
        // next turn handled by combat and resolve march order
        if (Board.allMarchOrdersResolved(context)
                && !Board.remainingCombats(context)) {
            Board.revivieRoutedUnits(context);
            WesterosPhase newPhase = new WesterosPhase(
                    mainScreenPanelController);
            newPhase.drawWesterosCards();
        } else {
            TurnHandler.saveAndDistributeGameContext(currentTurn, context);
            TurnHandler.UpdateUI(mainScreenPanelController);
        }
        if (unitsDestroyed) {
            Stage stage = (Stage) tblZones.getScene().getWindow();
            stage.close();
        }
    }

    public void doneClicked() {
        selectionMade(null);
    }
}
