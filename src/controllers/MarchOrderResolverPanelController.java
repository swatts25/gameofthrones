package controllers;

import java.net.URL;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.ResourceBundle;

import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableRow;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.ComboBoxTableCell;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.ClipboardContent;
import javafx.scene.input.DataFormat;
import javafx.scene.input.Dragboard;
import javafx.scene.input.TransferMode;
import javafx.stage.Stage;
import javafx.util.Callback;
import javafx.util.StringConverter;
import client.Board;
import client.Client;
import client.Combat;
import client.JavaFXLauncher;
import client.PlanningPhase;
import client.ResolveMarchOrder;
import client.TurnHandler;
import client.WesterosPhase;
import client.model.GameContext;
import client.model.MarchMovement;
import client.model.Order;
import client.model.Player;
import client.model.Turn;
import client.model.Unit;
import client.model.zones.Land;
import client.model.zones.Zone;

@SuppressWarnings({ "rawtypes", "unchecked" })
public class MarchOrderResolverPanelController extends JavaFXController
        implements Initializable {

    private static final String ZONE_STAY = "->Stay<-";

    private static final DataFormat SERIALIZED_MIME_TYPE = new DataFormat(
            "application/x-java-serialized-object");

    private ObservableList<Unit> tblUnitData = FXCollections
            .observableArrayList();

    private ObservableList<Zone> cboZoneData = FXCollections
            .observableArrayList();

    private List<MarchMovement> marches;

    private MainScreenPanelController mainscreen;

    private Turn currentTurn;

    private Zone marchingZone;

    public MarchOrderResolverPanelController(
            MainScreenPanelController mainScreenPanelController, Turn turn) {
        super("/forms/BaseRaidOrderResolverPanel.fxml");
        this.mainscreen = mainScreenPanelController;
        this.currentTurn = turn;
        this.marches = new ArrayList<MarchMovement>();
    }

    @FXML
    private TableView tblZones;

    @FXML
    private Button btnSubmit;

    @FXML
    private ComboBox cboZone;

    private TableColumn targetCol;

    protected Zone selectedTargetZone;

    @Override
    public void initialize(URL arg0, ResourceBundle arg1) {
        initTableView();
        initializeZoneComboBox();
    }

    private void initTableView() {
        tblZones.setEditable(true);
        marches = new ArrayList<MarchMovement>();
        TableColumn unitCol = new TableColumn("Unit");
        unitCol.setPrefWidth(170);
        unitCol.setCellValueFactory(new PropertyValueFactory<Unit, String>(
                "name"));
        targetCol = new TableColumn("March To");
        targetCol.setPrefWidth(322);
        tblZones.getColumns().addAll(unitCol, targetCol);
        tblZones.setRowFactory(tv -> {
            TableRow<Unit> row = new TableRow<>();
            row.setOnDragDetected(event -> {
                if (!row.isEmpty()) {
                    Integer index = row.getIndex();
                    Dragboard db = row.startDragAndDrop(TransferMode.MOVE);
                    db.setDragView(row.snapshot(null, null));
                    ClipboardContent cc = new ClipboardContent();
                    cc.put(SERIALIZED_MIME_TYPE, index);
                    db.setContent(cc);
                    event.consume();
                }
            });
            row.setOnDragOver(event -> {
                Dragboard db = event.getDragboard();
                if (db.hasContent(SERIALIZED_MIME_TYPE)) {
                    if (row.getIndex() != ((Integer) db
                            .getContent(SERIALIZED_MIME_TYPE)).intValue()) {
                        event.acceptTransferModes(TransferMode.COPY_OR_MOVE);
                        event.consume();
                    }
                }
            });
            row.setOnDragDropped(event -> {
                Dragboard db = event.getDragboard();
                if (db.hasContent(SERIALIZED_MIME_TYPE)) {
                    int draggedIndex = (Integer) db
                            .getContent(SERIALIZED_MIME_TYPE);
                    Unit draggedPerson = (Unit) tblZones.getItems().remove(
                            draggedIndex);
                    int dropIndex;
                    if (row.isEmpty()) {
                        dropIndex = tblZones.getItems().size();
                    } else {
                        dropIndex = row.getIndex();
                    }
                    tblZones.getItems().add(dropIndex, draggedPerson);
                    event.setDropCompleted(true);
                    tblZones.getSelectionModel().select(dropIndex);
                    event.consume();
                }
            });
            return row;
        });
    }

    private void updateZonesTable(Zone zone) {
        tblUnitData.clear();
        marches.clear();
        // Units that were marched to a zone controlled by that units player can
        // be added to zone without combat
        for (MarchMovement march : zone.getMarches()) {
            if (march.getUnit().getPlayerName()
                    .equalsIgnoreCase(Player.getMyName())) {
                zone.addUnit(march.getUnit());
            }
        }
        for (Unit unit : zone.getUnits()) {
            if (unit.getPlayerName().equalsIgnoreCase(Player.getMyName())
                    && !unit.isRouted()) {
                tblUnitData.add(unit);
            }
        }
        // Remove marches by player who already controlled zone, they have been
        // added to zone above
        for (Unit unit : tblUnitData) {
            for (MarchMovement march : zone.getMarches()) {
                if (march.getUnit().getId() == unit.getId()) {
                    zone.getMarches().remove(march);
                    break;
                }
            }
        }
        for (int i = 0; i < zone.getUnits().size(); i++) {
            marches.add(new MarchMovement(null, null, null));
        }
        ObservableList<Zone> targetChoice = FXCollections
                .observableArrayList(new ArrayList<Zone>());
        GameContext context = GameContext.getMyGameContext();
        targetChoice.addAll(new ArrayList<Zone>(context.getZones().values()));
        targetChoice.add(new Land(ZONE_STAY));
        targetChoice.sort(new Comparator<Zone>() {

            @Override
            public int compare(Zone o1, Zone o2) {
                return o1.getName().compareTo(o2.getName());
            }
        });
        targetCol
                .setCellFactory(new Callback<TableColumn<Zone, String>, TableCell<Zone, String>>() {

                    @Override
                    public TableCell<Zone, String> call(
                            TableColumn<Zone, String> p) {
                        ComboBoxTableCell cell = new ComboBoxTableCell(
                                FXCollections.observableArrayList(targetChoice)) {

                            @Override
                            public void updateItem(Object item, boolean empty) {
                                super.updateItem(item, empty);
                                if (item != null && item instanceof Zone) {
                                    Zone zone = ((Zone) item);
                                    if (zone.getControlledBy() != null) {
                                        setText(zone.getName()
                                                + " "
                                                + zone.getControlledBy()
                                                        .getHouse()
                                                + " Defense "
                                                + zone.getDefensePower());
                                    } else if (!zone.getName()
                                            .equals(ZONE_STAY)) {
                                        setText(zone.getName()
                                                + " Neutral Defense "
                                                + zone.getDefensePower());
                                    } else {
                                        setText(ZONE_STAY);
                                        marches.get(getIndex()).setFrom(null);
                                        marches.get(getIndex()).setTo(null);
                                        marches.get(getIndex()).setUnit(null);
                                        return;
                                    }
                                    MarchMovement currentMarch = marches
                                            .get(getIndex());
                                    currentMarch.setFrom(marchingZone.getName());
                                    currentMarch.setTo(zone.getName());
                                    currentMarch.setUnit(tblUnitData
                                            .get(getIndex()));
                                }
                            }
                        };
                        cell.setConverter(new StringConverter() {

                            @Override
                            public Object fromString(String arg0) {
                                // TODO Auto-generated method stub
                                return null;
                            }

                            @Override
                            public String toString(Object arg0) {
                                if (arg0 != null) {
                                    if (arg0 instanceof Zone) {
                                        Zone zone = ((Zone) arg0);
                                        return zone.getName();
                                    }
                                }
                                return null;
                            }
                        });
                        return cell;
                    }
                });
        tblZones.setItems(tblUnitData);
        tblZones.setDisable(false);
    }

    private void initializeZoneComboBox() {
        cboZone.setCellFactory(new Callback<ListView<Zone>, ListCell<Zone>>() {

            @Override
            public ListCell<Zone> call(ListView<Zone> param) {
                final ListCell<Zone> cell = new ListCell<Zone>() {

                    @Override
                    public void updateItem(Zone zone, boolean empty) {
                        super.updateItem(zone, empty);
                        if (zone != null) {
                            setText(zone.getName() + " - "
                                    + zone.getOrder().getName());
                        } else {
                            setText(null);
                        }
                    }
                };
                return cell;
            }
        });
        cboZone.setConverter(new StringConverter() {

            @Override
            public Object fromString(String arg0) {
                // TODO Auto-generated method stub
                return null;
            }

            @Override
            public String toString(Object arg0) {
                if (arg0 != null) {
                    if (arg0 instanceof Zone) {
                        Zone zone = ((Zone) arg0);
                        return zone.getName() + " - "
                                + zone.getOrder().getName();
                    }
                }
                return null;
            }
        });
        cboZone.valueProperty().addListener(new ChangeListener<Zone>() {

            @Override
            public void changed(ObservableValue<? extends Zone> arg0,
                    Zone oldSelection, Zone newSelection) {
                updateZonesTable(newSelection);
                marchingZone = newSelection;
            }
        });
        updateZoneCombobox();
    }

    private void updateZoneCombobox() {
        List<Zone> zones = new ArrayList<Zone>(GameContext.getMyGameContext()
                .getZones().values());
        List<Zone> zonesWithMarchOrders = new ArrayList<Zone>();
        for (Zone zone : zones) {
            if (zone.getControlledByName() != null
                    && zone.getControlledByName().equalsIgnoreCase(
                            Player.getMyName())
                    && zone.getOrder().getType().equals(Order.MARCH_ORDER)
                    && !zone.getUnits().isEmpty() && !zone.isOrderResolved()) {
                zonesWithMarchOrders.add(zone);
            }
        }
        cboZoneData.addAll(zonesWithMarchOrders);
        cboZone.setItems(cboZoneData);
        if (zonesWithMarchOrders != null && !zonesWithMarchOrders.isEmpty()) {
            cboZone.getSelectionModel().select(zonesWithMarchOrders.get(0));
            marchingZone = zonesWithMarchOrders.get(0);
        }
    }

    private void cleanUpMarches() {
        List<MarchMovement> result = new ArrayList<MarchMovement>();
        for (MarchMovement march : marches) {
            if (march != null && march.getTo() != null
                    && march.getFrom() != null && march.getUnit() != null) {
                result.add(march);
            }
        }
        marches.clear();
        marches = result;
    }

    public void submitClicked() {
        GameContext context = GameContext.getMyGameContext();
        cleanUpMarches();
        if (marches != null && !marches.isEmpty()) {
            ResolveMarchOrder marchOrder = new ResolveMarchOrder(context,
                    marchingZone, marches, Player.getMyName());
            // Validate movement is traversable
            if (!marchOrder.validateMarchingOrder()) {
                return;
            }
            // Validate supply
            if (!marchOrder.validateSupplyLimitAfterMarch(false)) {
                return;
            }
            // handel leaving influence
            if (!marchOrder.isHomeZone() && !marchOrder.isOccupiedAfterMarch()
                    && !marchOrder.isSeaZone()) {
                int influenceChoice = JavaFXLauncher.showYesNoDialog(
                        "Maintain Influence", marchingZone.getName()
                                + " is being vacated",
                        "Would you like to leave behind an influence token to maintain control of "
                                + marchingZone.getName() + "?");
                if (influenceChoice == 1) {
                    List<Player> players = context.getPlayers();
                    for (Player player : players) {
                        if (player.isMe()) {
                            if (player.getInfluence() > 0) {
                                player.setInfluence(player.getInfluence() - 1);
                            } else {
                                JavaFXLauncher
                                        .showInformationDialog(
                                                "Insufficient Funds",
                                                "You do not have an influence token to leave behind");
                                return;
                            }
                        }
                    }
                } else if (influenceChoice == 0) {
                    marchingZone.setControlledByName(null);
                } else {
                    return;
                }
            } else if (marchOrder.isSeaZone()
                    && !marchOrder.isOccupiedAfterMarch()) {
                marchingZone.setControlledByName(null);
            }
            // Remove units from marching territory
            // Remove Order from marching territory
            // Add march to destination
            marchOrder.moveUnits();
        }
        marchingZone.setOrderResolved(true);
        // marchingZone.setOrder(Order.NONE_ORDER);
        // Handle next turn
        if (context.getTurns() != null) {
            context.getTurns().remove(0);
        }
        if (context.getCombats() != null && !context.getCombats().isEmpty()) {
            resolveCombats(context);
        }
        if (Board.allMarchOrdersResolved(context)
                && !Board.remainingCombats(context)) {
            PlanningPhase.resolveConsolodatePowerOrders(context);
            Board.revivieRoutedUnits(context);
            startWesterosPhase(context);
        }
        if (mainscreen != null && currentTurn != null) {
            TurnHandler.saveAndDistributeGameContext(currentTurn, context);
            TurnHandler.UpdateUI(mainscreen);
            Stage stage = (Stage) tblZones.getScene().getWindow();
            stage.close();
        }
        Client.getLogger().info("March Order Resolved.");
    }

    private void startWesterosPhase(GameContext context) {
        WesterosPhase phase = new WesterosPhase(mainscreen);
        phase.drawWesterosCards();
    }

    private void resolveCombats(GameContext context) {
        List<Combat> combats = context.getCombats();
        List<Combat> resolvedCombats = new ArrayList<Combat>();
        for (Combat combat : combats) {
            Zone targetZone = context.getZones().get(combat.getTargetZoneName());
            if (combat.getDefendingPlayerName() == null
                    || (targetZone.getUnits().isEmpty()
                            && targetZone instanceof Land && ((Land) targetZone)
                            .getGarrisonDefensePower() == 0)) {
                combat.setContext(context);
                combat.resolveCombatAgainstNeutralForce(mainscreen);
                resolvedCombats.add(combat);
            } else {
                // Add two turns for each combat opponents to chose house cards
                Turn attackerChooseHouseCard = new Turn(getPlayer(
                        combat.getAttackingPlayerName(), context));
                attackerChooseHouseCard.setCombatId(combat.getId());
                attackerChooseHouseCard.setType(Turn.CHOOSE_HOUSE_CARD);
                attackerChooseHouseCard.setCombatId(combat.getId());
                context.getTurns().add(0, attackerChooseHouseCard);
                Turn defenderChooseHouseCard = new Turn(getPlayer(
                        combat.getDefendingPlayerName(), context));
                defenderChooseHouseCard.setType(Turn.CHOOSE_HOUSE_CARD);
                defenderChooseHouseCard.setCombatId(combat.getId());
                context.getTurns().add(0, defenderChooseHouseCard);
            }
        }
        combats.removeAll(resolvedCombats);
    }

    public Player getPlayer(String playerName, GameContext context) {
        for (Player player : context.getPlayers()) {
            if (player.getName().equalsIgnoreCase(playerName)) {
                return player;
            }
        }
        return null;
    }
}