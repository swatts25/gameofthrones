package controllers;

import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.ResourceBundle;

import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.ComboBox;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.Stage;
import javafx.util.Callback;
import client.Board;
import client.BooleanCell;
import client.Client;
import client.JavaFXLauncher;
import client.ResolveMarchOrder;
import client.TurnHandler;
import client.model.GameContext;
import client.model.MarchMovement;
import client.model.Turn;
import client.model.Unit;
import client.model.zones.Zone;

@SuppressWarnings({ "unchecked", "rawtypes" })
public class UnitDestroyerPanelController extends JavaFXController implements
        Initializable {

    private ObservableList<Unit> tblUnitData = FXCollections
            .observableArrayList();

    @FXML
    private TableView tblFootmen;

    @FXML
    private ComboBox cboZone;

    private String targetZoneName;

    private List<Unit> markedUnits;

    private RetreatPanelController retreatPanelController;

    private Turn currentTurn;

    private ResolveMarchOrder marchOrder;

    private UnitDestroyerPanelController instance;

    private ObservableList<Zone> cboPlayerData = FXCollections
            .observableArrayList();

    private int numUnitsToDestroy;

    private MainScreenPanelController mainscreen;

    public UnitDestroyerPanelController(String targetZoneName,
            RetreatPanelController retreatPanelController, Turn currentTurn,
            ResolveMarchOrder marchOrder) {
        // Retreat Destroy (Player can choose from retreating units ONLY)
        super("/forms/BaseUnitEditorPanel.fxml");
        this.targetZoneName = targetZoneName;
        this.retreatPanelController = retreatPanelController;
        this.currentTurn = currentTurn;
        this.marchOrder = marchOrder;
        this.markedUnits = new ArrayList<Unit>();
        this.instance = this;
        this.numUnitsToDestroy = marchOrder.getNumUnitsToDestroy();
        cboZone.setVisible(false);
    }

    public UnitDestroyerPanelController(Turn currentTurn,
            MainScreenPanelController mainscreen) {
        // Rectify supply (Player can choose from all units)
        super("/forms/BaseUnitEditorPanel.fxml");
        for (Zone zone : GameContext.getMyGameContext().getZones().values()) {
            if (zone.getControlledByName() != null
                    && zone.getControlledByName().equalsIgnoreCase(
                            currentTurn.getPlayerName())) {
                this.targetZoneName = zone.getName();
                break;
            }
        }
        this.mainscreen = mainscreen;
        this.currentTurn = currentTurn;
        this.markedUnits = new ArrayList<Unit>();
        this.instance = this;
        this.numUnitsToDestroy = Board.validateCurrentSupplyLimit(currentTurn
                .getPlayer(), GameContext.getMyGameContext().getZones());
    }

    @Override
    public void initialize(URL arg0, ResourceBundle arg1) {
        initTableView();
        initializeZoneComboBox();
    }

    private void initTableView() {
        tblFootmen.setEditable(true);
        TableColumn nameCol = new TableColumn("Name");
        nameCol.setPrefWidth(356);
        nameCol.setCellValueFactory(new PropertyValueFactory<Unit, String>(
                "name"));
        TableColumn actionCol = new TableColumn("Destroy");
        Callback<TableColumn<Unit, Boolean>, TableCell<Unit, Boolean>> booleanCellFactory = new Callback<TableColumn<Unit, Boolean>, TableCell<Unit, Boolean>>() {

            @Override
            public TableCell<Unit, Boolean> call(TableColumn<Unit, Boolean> p) {
                return new BooleanCell(instance);
            }
        };
        actionCol.setCellValueFactory(new PropertyValueFactory<Unit, Boolean>(
                "active"));
        actionCol.setCellFactory(booleanCellFactory);
        actionCol.setPrefWidth(65);
        tblFootmen.getColumns().addAll(nameCol, actionCol);
        updateZonesTable(targetZoneName);
    }

    private void updateZonesTable(String targetZoneName) {
        GameContext context = GameContext.getMyGameContext();
        Map<String, Zone> zones = context.getZones();
        Zone targetZone = zones.get(targetZoneName);
        tblUnitData.clear();
        for (Unit unit : targetZone.getUnits()) {
            if (unit.getPlayerName().equalsIgnoreCase(
                    currentTurn.getPlayer().getName())) {
                tblUnitData.add(unit);
            }
        }
        for (MarchMovement march : targetZone.getMarches()) {
            if (march.getUnit().getPlayerName()
                    .equals(currentTurn.getPlayer().getName())) {
                tblUnitData.add(march.getUnit());
            }
        }
        tblFootmen.setItems(tblUnitData);
    }

    private void initializeZoneComboBox() {
        cboZone.setCellFactory(new Callback<ListView<Zone>, ListCell<Zone>>() {

            @Override
            public ListCell<Zone> call(ListView<Zone> param) {
                final ListCell<Zone> cell = new ListCell<Zone>() {

                    @Override
                    public void updateItem(Zone zone, boolean empty) {
                        super.updateItem(zone, empty);
                        if (zone != null) {
                            setText(zone.getName());
                        } else {
                            setText(null);
                        }
                    }
                };
                return cell;
            }
        });
        cboZone.valueProperty().addListener(new ChangeListener<Zone>() {

            @Override
            public void changed(ObservableValue<? extends Zone> arg0,
                    Zone oldSelection, Zone newSelection) {
                updateZonesTable(newSelection.getName());
            }
        });
        updateZoneCombobox();
    }

    private void updateZoneCombobox() {
        List<Zone> myZones = new ArrayList<Zone>();
        for (Zone zone : GameContext.getMyGameContext().getZones().values()) {
            if (zone.getControlledByName() != null
                    && zone.getControlledByName().equalsIgnoreCase(
                            currentTurn.getPlayerName())) {
                myZones.add(zone);
            }
        }
        cboPlayerData.addAll(myZones);
        cboZone.setItems(cboPlayerData);
        cboZone.getSelectionModel().select(myZones.get(0));
        cboZone.setDisable(false);
    }

    public void doneClicked() {
        if (markedUnits.size() < numUnitsToDestroy) {
            JavaFXLauncher.showInformationDialog("Supply Violation",
                    "You have not selected anough units to destroy.");
        } else {
            for (Unit unit : markedUnits) {
                Client.getLogger().info("Destroyed: " + unit.getName());
                destroyUnit(unit);
            }
            endTurn();
        }
        Stage stage = (Stage) tblFootmen.getScene().getWindow();
        stage.close();
    }

    private void endTurn() {
        if (retreatPanelController != null && marchOrder != null) {
            marchOrder.retreatUnits();
            retreatPanelController.endTurn(true);
        } else {
            GameContext.getMyGameContext().getTurns().remove(currentTurn);
            TurnHandler.saveAndDistributeGameContext(currentTurn,
                    GameContext.getMyGameContext());
            TurnHandler.UpdateUI(mainscreen);
        }
    }

    private void destroyUnit(Unit unit) {
        GameContext context = GameContext.getMyGameContext();
        if (retreatPanelController != null && marchOrder != null) {
            Zone retreatingZone = context.getZones().get(targetZoneName);
            marchOrder.removeUnit(unit);
            if (!retreatingZone.removeUnit(unit)) {
                retreatingZone.removeMarch(unit);
            }
        } else {
            for (Zone zone : context.getZones().values()) {
                if (zone.removeUnit(unit)) {
                    Client.getLogger().info(unit.getName() + " removed from "
                            + zone.getName());
                    break;
                }
            }
        }
    }

    @Override
    public void markUnit(Unit unit, Boolean newValue) {
        if (newValue) {
            markedUnits.add(unit);
        } else {
            markedUnits.remove(unit);
        }
    }
}
