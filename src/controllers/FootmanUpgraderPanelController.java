package controllers;

import java.net.URL;
import java.util.List;
import java.util.Map;
import java.util.ResourceBundle;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.Stage;
import javafx.util.Callback;
import client.Client;
import client.HouseCardSpecialAbility;
import client.TurnHandler;
import client.model.Footman;
import client.model.GameContext;
import client.model.Knight;
import client.model.Order;
import client.model.Turn;
import client.model.Unit;
import client.model.zones.Zone;

@SuppressWarnings({ "unchecked", "rawtypes" })
public class FootmanUpgraderPanelController extends JavaFXController implements
        Initializable {

    private ObservableList<Zone> tblZoneData = FXCollections
            .observableArrayList();

    @FXML
    private TableView tblFootmen;

    private String targetZoneName;

    private HouseCardSpecialAbility renlyHouseCardAbility;

    private MainScreenPanelController mainScreenPanelController;

    private Turn currentTurn;

    public FootmanUpgraderPanelController(String targetZoneName,
            HouseCardSpecialAbility queenHouseCardAbility,
            MainScreenPanelController mainScreenPanelController,
            Turn currentTurn) {
        super("/forms/BaseUnitEditorPanel.fxml");
        this.targetZoneName = targetZoneName;
        this.renlyHouseCardAbility = queenHouseCardAbility;
        this.mainScreenPanelController = mainScreenPanelController;
        this.currentTurn = currentTurn;
    }

    @Override
    public void initialize(URL arg0, ResourceBundle arg1) {
        initTableView();
    }

    private void initTableView() {
        tblFootmen.setEditable(true);
        TableColumn zoneCol = new TableColumn("Zone");
        zoneCol.setPrefWidth(356);
        zoneCol.setCellValueFactory(new PropertyValueFactory<Zone, String>(
                "name"));
        TableColumn actionCol = new TableColumn("Action");
        actionCol.setCellValueFactory(new PropertyValueFactory<>("DUMMY"));
        actionCol
                .setCellFactory(new Callback<TableColumn<Zone, String>, TableCell<Zone, String>>() {

                    @Override
                    public TableCell call(final TableColumn<Zone, String> param) {
                        final TableCell<Zone, String> cell = new TableCell<Zone, String>() {

                            final Button btn = new Button("Upgrade");

                            @Override
                            public void updateItem(String item, boolean empty) {
                                super.updateItem(item, empty);
                                if (empty) {
                                    setGraphic(null);
                                    setText(null);
                                } else {
                                    btn.setOnAction((ActionEvent event) -> {
                                        Zone zone = getTableView().getItems()
                                                .get(getIndex());
                                        selectionMade(zone);
                                    });
                                    setGraphic(btn);
                                    setText(null);
                                }
                            }
                        };
                        return cell;
                    }
                });
        actionCol.setPrefWidth(65);
        tblFootmen.getColumns().addAll(zoneCol, actionCol);
        updateZonesTable(targetZoneName);
    }

    private boolean hasFootmen(Zone zone) {
        List<Unit> units = zone.getUnits();
        for (Unit unit : units) {
            if (unit instanceof Footman) {
                return true;
            }
        }
        return false;
    }

    private void updateZonesTable(String targetZoneName) {
        GameContext context = GameContext.getMyGameContext();
        Map<String, Zone> zones = context.getZones();
        Zone targetZone = context.getZones().get(targetZoneName);
        tblZoneData.clear();
        for (String adajacentZoneName : targetZone.getAdjacentZones()) {
            // Need to add some filters here, check house card
            Zone adjacentZone = zones.get(adajacentZoneName);
            if (adjacentZone.getControlledBy() != null
                    && adjacentZone.getControlledBy().equals(
                            currentTurn.getPlayer())
                    && hasFootmen(adjacentZone)
                    && adjacentZone.getOrder() != null
                    && adjacentZone.getOrder().equals(Order.SUPPORT_ORDER)) {
                tblZoneData.add(adjacentZone);
            }
        }
        if (hasFootmen(targetZone)) {
            tblZoneData.add(targetZone);
        }
        tblFootmen.setItems(tblZoneData);
    }

    private void selectionMade(Zone zone) {
        GameContext context = GameContext.getMyGameContext();
        if (zone != null) {
            Client.getLogger().info("Upgraded Footman in " + zone.getName()
                    + " to Knight");
            upgradeFootman(zone);
        } else {
            Client.getLogger().info("No Footman has been upgraded");
        }
        if (renlyHouseCardAbility != null && mainScreenPanelController != null
                && currentTurn != null) {
            if (renlyHouseCardAbility.isAttacker()) {
                renlyHouseCardAbility.getCombat().setAttackerHouseCardPlayed(
                        true);
            } else {
                renlyHouseCardAbility.getCombat().setDefenderHouseCardPlayed(
                        true);
            }
            context.getTurns().remove(0);
            if (context.getTurns().isEmpty()) {
                renlyHouseCardAbility.getCombat().resolveCombatBetweenPlayers(
                        mainScreenPanelController);
            }
            TurnHandler.saveAndDistributeGameContext(currentTurn, context);
            TurnHandler.UpdateUI(mainScreenPanelController);
        }
        Stage stage = (Stage) tblFootmen.getScene().getWindow();
        stage.close();
    }

    private void upgradeFootman(Zone zone) {
        List<Unit> aclimatedUnits = zone.getUnits();
        for (Unit unit : aclimatedUnits) {
            if (unit.getPlayerName().equalsIgnoreCase(
                    currentTurn.getPlayer().getName())
                    && unit instanceof Footman) {
                zone.removeUnit(unit);
                zone.addUnit(new Knight(currentTurn.getPlayer().getName()));
                return;
            }
        }
    }

    public void doneClicked() {
        selectionMade(null);
    }
}
