package controllers;

import java.net.URL;
import java.util.ResourceBundle;

import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.image.ImageView;
import client.model.cards.Card;

public class WesterosCardsViewerPanelController extends JavaFXController
        implements Initializable {

    private Card ageOneCard;

    private Card ageTwoCard;

    private Card ageThreeCard;

    public WesterosCardsViewerPanelController(Card ageOneCard, Card ageTwoCard,
            Card ageThreeCard) {
        super("/forms/BaseWesterosCardsViewerPanel.fxml");
        this.ageOneCard = ageOneCard;
        this.ageTwoCard = ageTwoCard;
        this.ageThreeCard = ageThreeCard;
    }

    @FXML
    ImageView imvAgeOneCard;

    @FXML
    ImageView imvAgeTwoCard;

    @FXML
    ImageView imvAgeThreeCard;

    @Override
    public void initialize(URL arg0, ResourceBundle arg1) {
        imvAgeOneCard.setImage(ageOneCard.getImage());
        imvAgeTwoCard.setImage(ageTwoCard.getImage());
        imvAgeThreeCard.setImage(ageThreeCard.getImage());
    }
}
