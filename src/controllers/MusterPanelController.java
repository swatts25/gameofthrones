package controllers;

import java.net.URL;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.ResourceBundle;

import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.Stage;
import javafx.util.Callback;
import client.Board;
import client.BooleanCell;
import client.Client;
import client.JavaFXLauncher;
import client.TurnHandler;
import client.model.Footman;
import client.model.GameContext;
import client.model.Knight;
import client.model.Ship;
import client.model.SiegeEngine;
import client.model.Turn;
import client.model.Unit;
import client.model.zones.Land;
import client.model.zones.Zone;

@SuppressWarnings({ "unchecked", "rawtypes" })
public class MusterPanelController extends JavaFXController implements
        Initializable {

    private Map<String, ObservableList<Unit>> unitMap = new HashMap<String, ObservableList<Unit>>();

    private ObservableList<Unit> newUnits = FXCollections.observableArrayList();

    private MusterPanelController instance;

    @FXML
    private TableView tblUnits;

    @FXML
    private ComboBox cboZone;

    @FXML
    private Label lblUpgradePower;

    @FXML
    private ComboBox cboNewUnit;

    @FXML
    private Button btnAdd;

    private ObservableList<Zone> cboZoneData = FXCollections
            .observableArrayList();

    private MainScreenPanelController mainscreen;

    private Turn currentTurn;

    public MusterPanelController(Turn currentTurn,
            MainScreenPanelController mainscreen) {
        super("/forms/BaseMusterPanel.fxml");
        this.mainscreen = mainscreen;
        this.currentTurn = currentTurn;
        instance = this;
    }

    @Override
    public void initialize(URL arg0, ResourceBundle arg1) {
        initTableView();
        initializeZoneComboBox();
        initializeUnitComboBox();
    }

    private void initTableView() {
        tblUnits.setEditable(true);
        TableColumn nameCol = new TableColumn("Name");
        nameCol.setPrefWidth(286);
        nameCol.setCellValueFactory(new PropertyValueFactory<Unit, String>(
                "name"));
        TableColumn actionCol = new TableColumn("Combine");
        Callback<TableColumn<Unit, Boolean>, TableCell<Unit, Boolean>> booleanCellFactory = new Callback<TableColumn<Unit, Boolean>, TableCell<Unit, Boolean>>() {

            @Override
            public TableCell<Unit, Boolean> call(TableColumn<Unit, Boolean> p) {
                return new BooleanCell(instance);
            }
        };
        actionCol.setCellValueFactory(new PropertyValueFactory<Unit, Boolean>(
                "active"));
        actionCol.setCellFactory(booleanCellFactory);
        actionCol.setPrefWidth(65);
        TableColumn upgradeCol = new TableColumn("Action");
        upgradeCol.setCellValueFactory(new PropertyValueFactory<>("DUMMY"));
        upgradeCol
                .setCellFactory(new Callback<TableColumn<Unit, String>, TableCell<Unit, String>>() {

                    @Override
                    public TableCell call(final TableColumn<Unit, String> param) {
                        final TableCell<Unit, String> cell = new TableCell<Unit, String>() {

                            final Button btn = new Button("Upgrade");

                            @Override
                            public void updateItem(String item, boolean empty) {
                                super.updateItem(item, empty);
                                if (empty) {
                                    setGraphic(null);
                                    setText(null);
                                } else {
                                    btn.setOnAction((ActionEvent event) -> {
                                        Unit unit = getTableView().getItems()
                                                .get(getIndex());
                                        upgradeUnit(unit);
                                    });
                                    setGraphic(btn);
                                    setText(null);
                                }
                            }

                            /*
                             * (non-Javadoc)
                             * @see
                             * javafx.scene.control.IndexedCell#updateIndex(int)
                             */
                            @Override
                            public void updateIndex(int arg0) {
                                super.updateIndex(arg0);
                                if (arg0 >= 0
                                        && arg0 < getTableView().getItems()
                                                .size()) {
                                    Unit unit = getTableView().getItems().get(
                                            arg0);
                                    if (unit instanceof SiegeEngine
                                            || unit instanceof Ship) {
                                        btn.setVisible(false);
                                    }
                                }
                            }
                        };
                        return cell;
                    }
                });
        upgradeCol.setPrefWidth(70);
        tblUnits.getColumns().addAll(nameCol, actionCol, upgradeCol);
    }

    private void updateUnitsTable(String targetZoneName, Zone oldSelection) {
        if (oldSelection != null) {
            ObservableList<Unit> units = unitMap.get(oldSelection.getName());
            for (Unit unit : units) {
                unit.setCombine(false);
            }
        }
        tblUnits.setItems(unitMap.get(targetZoneName));
        tblUnits.refresh();
    }

    public void resetClicked() {
        Land zone = (Land) cboZone.getSelectionModel().getSelectedItem();
        if (zone.isCastle()) {
            zone.setAvailableUpgradePower(2);
        } else {
            zone.setAvailableUpgradePower(1);
        }
        lblUpgradePower.setText("Remaining Upgrade Power: "
                + zone.getAvailableUpgradePower());
        ObservableList<Unit> units = unitMap.get(zone.getName());
        units.clear();
        tblUnits.refresh();
        for (Unit unit : zone.getUnits()) {
            units.add(unit);
            unit.setCombine(false);
        }
    }

    private void upgradeUnit(Unit unit) {
        Client.getLogger().info("Upgrading " + unit.getName());
        Land selectedZone = (Land) cboZone.getSelectionModel()
                .getSelectedItem();
        if (selectedZone.getAvailableUpgradePower() >= 1) {
            if (unit instanceof Footman) {
                unitMap.get(selectedZone.getName()).add(
                        new Knight(unit.getPlayerName()));
            } else if (unit instanceof Knight) {
                unitMap.get(selectedZone.getName()).add(
                        new SiegeEngine(unit.getPlayerName()));
            } else {
                JavaFXLauncher.showInformationDialog("Oops!",
                        "Ships and Siege Engines cannot be upgraded");
                return;
            }
            unitMap.get(selectedZone.getName()).remove(unit);
            selectedZone.setAvailableUpgradePower(selectedZone
                    .getAvailableUpgradePower() - 1);
            lblUpgradePower.setText("Remaining Upgrade Power: "
                    + selectedZone.getAvailableUpgradePower());
        } else {
            JavaFXLauncher.showInformationDialog("Insufficient Upgrade Power",
                    "You have already used all your upgrades for this Zone");
        }
    }

    private void initializeZoneComboBox() {
        cboZone.setCellFactory(new Callback<ListView<Zone>, ListCell<Zone>>() {

            @Override
            public ListCell<Zone> call(ListView<Zone> param) {
                final ListCell<Zone> cell = new ListCell<Zone>() {

                    @Override
                    public void updateItem(Zone zone, boolean empty) {
                        super.updateItem(zone, empty);
                        if (zone != null) {
                            setText(zone.getName());
                        } else {
                            setText(null);
                        }
                    }
                };
                return cell;
            }
        });
        cboZone.valueProperty().addListener(new ChangeListener<Zone>() {

            @Override
            public void changed(ObservableValue<? extends Zone> arg0,
                    Zone oldSelection, Zone newSelection) {
                updateUnitsTable(newSelection.getName(), oldSelection);
                lblUpgradePower.setText("Remaining Upgrade Power: "
                        + ((Land) newSelection).getAvailableUpgradePower());
            }
        });
        updateZoneCombobox();
    }

    private void initializeUnitComboBox() {
        cboNewUnit
                .setCellFactory(new Callback<ListView<Unit>, ListCell<Unit>>() {

                    @Override
                    public ListCell<Unit> call(ListView<Unit> param) {
                        final ListCell<Unit> cell = new ListCell<Unit>() {

                            @Override
                            public void updateItem(Unit unit, boolean empty) {
                                super.updateItem(unit, empty);
                                if (unit != null) {
                                    if (unit instanceof Footman) {
                                        setText("Footman (cost 1)");
                                    } else if (unit instanceof Knight) {
                                        setText("Knight (cost 2)");
                                    } else if (unit instanceof Ship) {
                                        setText("Ship (cost 1)");
                                    }
                                } else {
                                    setText(null);
                                }
                            }
                        };
                        return cell;
                    }
                });
        updateUnitCombobox();
    }

    private void updateUnitCombobox() {
        newUnits.add(new Footman(currentTurn.getPlayerName()));
        newUnits.add(new Ship(currentTurn.getPlayerName()));
        newUnits.add(new Knight(currentTurn.getPlayerName()));
        sortNewUnitList();
        cboNewUnit.setItems(newUnits);
        cboNewUnit.getSelectionModel().select(0);
    }

    public void addClicked() {
        Unit unitToAdd = (Unit) cboNewUnit.getSelectionModel()
                .getSelectedItem();
        Land targetZone = (Land) cboZone.getSelectionModel().getSelectedItem();
        if (unitToAdd instanceof Knight
                && targetZone.getAvailableUpgradePower() == 2) {
            unitMap.get(targetZone.getName()).add(unitToAdd);
            newUnits.remove(unitToAdd);
            newUnits.add(new Knight(currentTurn.getPlayerName()));
            targetZone.setAvailableUpgradePower(targetZone
                    .getAvailableUpgradePower() - 2);
        } else if (unitToAdd instanceof Footman
                && targetZone.getAvailableUpgradePower() >= 1) {
            unitMap.get(targetZone.getName()).add(unitToAdd);
            newUnits.remove(unitToAdd);
            newUnits.add(new Footman(currentTurn.getPlayerName()));
            targetZone.setAvailableUpgradePower(targetZone
                    .getAvailableUpgradePower() - 1);
        } else if (unitToAdd instanceof Ship
                && targetZone.getAvailableUpgradePower() >= 1) {
            unitMap.get(targetZone.getName()).add(unitToAdd);
            newUnits.remove(unitToAdd);
            newUnits.add(new Ship(currentTurn.getPlayerName()));
            targetZone.setAvailableUpgradePower(targetZone
                    .getAvailableUpgradePower() - 1);
        } else {
            JavaFXLauncher
                    .showInformationDialog("Insufficient Upgrade Power",
                            "You do not have enough available upgrade power to do that");
        }
        sortNewUnitList();
        cboNewUnit.getSelectionModel().select(0);
        lblUpgradePower.setText("Remaining Upgrade Power: "
                + targetZone.getAvailableUpgradePower());
    }

    private void sortNewUnitList() {
        newUnits.sort(new Comparator<Unit>() {

            @Override
            public int compare(Unit o1, Unit o2) {
                return o1.getName().compareTo(o2.getName());
            }
        });
    }

    private void updateZoneCombobox() {
        List<Zone> musterZones = new ArrayList<Zone>();
        for (Zone zone : GameContext.getMyGameContext().getZones().values()) {
            if (zone.getControlledByName() != null
                    && zone.getControlledByName().equalsIgnoreCase(
                            currentTurn.getPlayerName())
                    && zone instanceof Land) {
                Land land = (Land) zone;
                if (land.isCastle()) {
                    musterZones.add(zone);
                    land.setAvailableUpgradePower(2);
                } else if (land.isStrongHold()) {
                    musterZones.add(zone);
                    land.setAvailableUpgradePower(1);
                }
                ObservableList<Unit> tblUnitData = FXCollections
                        .observableArrayList();
                for (Unit unit : zone.getUnits()) {
                    tblUnitData.add(unit);
                }
                unitMap.put(zone.getName(), tblUnitData);
            }
        }
        cboZoneData.addAll(musterZones);
        cboZone.setItems(cboZoneData);
        if (!musterZones.isEmpty()) {
            cboZone.getSelectionModel().select(musterZones.get(0));
        }
        cboZone.setDisable(false);
    }

    public boolean validateSupply() {
        Map<String, Zone> newZoneMap = new HashMap<String, Zone>();
        for (Zone zone : GameContext.getMyGameContext().getZones().values()) {
            newZoneMap.put(zone.getName(), zone.copyUnits());
        }
        for (Zone zone : cboZoneData) {
            newZoneMap.get(zone.getName()).removeAllUnits();
            for (Unit unit : unitMap.get(zone.getName())) {
                newZoneMap.get(zone.getName()).addUnit(unit);
            }
        }
        return (Board.validateCurrentSupplyLimit(currentTurn.getPlayer(),
                newZoneMap) == 0);
    }

    public void doneClicked() {
        if (validateSupply()) {
            for (Zone zone : cboZoneData) {
                List<Unit> newUnits = unitMap.get(zone.getName());
                zone.removeAllUnits();
                for (Unit unit : newUnits) {
                    unit.setCombine(false);
                    zone.addUnit(unit);
                }
            }
            // Save and distribute
            GameContext.getMyGameContext().getTurns().remove(currentTurn);
            TurnHandler.saveAndDistributeGameContext(currentTurn,
                    GameContext.getMyGameContext());
            TurnHandler.UpdateUI(mainscreen);
            Stage stage = (Stage) tblUnits.getScene().getWindow();
            stage.close();
        } else {
            JavaFXLauncher.showInformationDialog("Supply Violation",
                    "Mustering these units violates your supply.");
        }
    }

    @Override
    public void markUnit(Unit unit, Boolean newValue) {
        if (newValue) {
            Zone selectedZone = (Zone) cboZone.getSelectionModel()
                    .getSelectedItem();
            ObservableList<Unit> units = unitMap.get(selectedZone.getName());
            List<Unit> removalUnits = new ArrayList<Unit>();
            for (Unit tblUnit : units) {
                if (!tblUnit.equals(unit) && tblUnit.isCombine()
                        && tblUnit.getClass().equals(unit.getClass())) {
                    if (unit instanceof Footman) {
                        // clear checks
                        tblUnits.refresh();
                        units.add(new Knight(unit.getPlayerName()));
                        removalUnits.add(tblUnit);
                        removalUnits.add(unit);
                        break;
                    }
                }
            }
            if (removalUnits.isEmpty()) {
                unit.setCombine(true);
            } else {
                units.removeAll(removalUnits);
            }
        } else {
            unit.setCombine(false);
        }
    }
}
