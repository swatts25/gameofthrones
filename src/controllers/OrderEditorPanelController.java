package controllers;

import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableColumn.CellDataFeatures;
import javafx.scene.control.TableColumn.CellEditEvent;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.ComboBoxTableCell;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.util.Callback;
import client.Client;
import client.JavaFXLauncher;
import client.PlanningPhase;
import client.model.GameContext;
import client.model.Order;
import client.model.Player;
import client.model.Turn;
import client.model.zones.Zone;

@SuppressWarnings({ "rawtypes", "unchecked" })
public class OrderEditorPanelController extends JavaFXController implements
        Initializable {

    private ObservableList<Zone> tblZoneData = FXCollections
            .observableArrayList();

    private ObservableList<Order> orderChoice = FXCollections
            .observableArrayList(Order.getUniqueAvailableOrders(GameContext.getMyGameContext()));

    private MainScreenPanelController mainscreen;

    private Turn currentTurn;

    private Order currentOrder;

    private Zone modifiedZone;

    public OrderEditorPanelController(
            MainScreenPanelController mainScreenPanelController, Turn turn) {
        super("/forms/BaseOrderEditorPanel.fxml");
        this.mainscreen = mainScreenPanelController;
        this.currentTurn = turn;
    }

    @FXML
    private TableView tblZones;
    
    @FXML
    private Label lblOrderRestriction;

    @FXML
    private Button btnSubmit;

    @Override
    public void initialize(URL arg0, ResourceBundle arg1) {
        GameContext context = GameContext.getMyGameContext();
        orderChoice.add(Order.NONE_ORDER);
        initTableView();
        if(context.isRestrictConsolodateOrders()){
            lblOrderRestriction.setText("Consolodate Orders cannot be played");
        }else if(context.isRestrictDefenseOrders()){
            lblOrderRestriction.setText("Defense Orders cannot be played");
        }else if(context.isRestrictMarchPlusOneOrders()){
            lblOrderRestriction.setText("March +1 Orders cannot be played");
        }else if(context.isRestrictRaidOrders()){
            lblOrderRestriction.setText("Raid Orders cannot be played");
        }else if(context.isRestrictSupportOrders()){
            lblOrderRestriction.setText("Support Orders cannot be played");
        }
        
    }

    private void initTableView() {
        tblZones.setEditable(true);
        TableColumn zoneCol = new TableColumn("Name");
        zoneCol.setPrefWidth(128);
        zoneCol.setCellValueFactory(new PropertyValueFactory<Zone, String>(
                "name"));
        TableColumn commandCol = new TableColumn("Order");
        commandCol.setPrefWidth(159);
        commandCol.setCellValueFactory(new PropertyValueFactory<Order, String>(
                "Order"));
        commandCol
                .setCellFactory(ComboBoxTableCell.forTableColumn(orderChoice));
        commandCol
                .setOnEditCommit(new EventHandler<CellEditEvent<Zone, Order>>() {

                    @Override
                    public void handle(CellEditEvent<Zone, Order> t) {
                        Zone selectedZone = ((Zone) t.getTableView().getItems()
                                .get(t.getTablePosition().getRow()));
                        modifiedZone = selectedZone;
                        currentOrder = selectedZone.getOrder();
                        Order newSelectedOrder = t.getNewValue();
                        selectedZone.setOrder(newSelectedOrder);
                        updateZonesTable();
                        if (currentTurn.getType().equals(Turn.RAVEN)) {
                            tblZones.setDisable(true);
                        }
                    };
                });
        TableColumn starCol = new TableColumn("Star");
        starCol.setPrefWidth(39);
        starCol.setStyle("-fx-alignment: CENTER;");
        starCol.setCellValueFactory(new Callback<CellDataFeatures<Zone, String>, ObservableValue<String>>() {

            public ObservableValue<String> call(
                    CellDataFeatures<Zone, String> cell) {
                SimpleStringProperty selectedContinent = new SimpleStringProperty();
                if (cell.getValue().getOrder() != null
                        && cell.getValue().getOrder().isStar()) {
                    selectedContinent.setValue("*");
                } else {
                    selectedContinent.setValue("");
                }
                return selectedContinent;
            }
        });
        tblZones.getColumns().addAll(zoneCol, commandCol, starCol);
        updateZonesTable();
    }

    private void updateZonesTable() {
        List<Zone> zones = new ArrayList<Zone>(GameContext.getMyGameContext()
                .getZones().values());
        tblZoneData.clear();
        for (Zone zone : zones) {
            if (zone.getControlledBy() != null && zone.getControlledBy().isMe()) {
                tblZoneData.add(zone);
            }
        }
        tblZones.setItems(tblZoneData);
        tblZones.setDisable(false);
    }

    @FXML
    protected void resetClicked() {
        if (currentTurn.getType().equals(Turn.RAVEN)) {
            modifiedZone.setOrder(currentOrder);
            currentOrder = Order.NONE_ORDER;
        } else {
            for (Zone zone : tblZoneData) {
                zone.setOrder(Order.NONE_ORDER);
            }
        }
        tblZones.setDisable(false);
        updateZonesTable();
    }

    public void submitClicked() {
        // Validate no duplicates
        PlanningPhase planningPhase = new PlanningPhase(mainscreen);
        List<String> availableOrders = Order.getOrderNames();
        List<String> selectedOrders = new ArrayList<String>();
        for (Zone zone : tblZoneData) {
            if (!zone.getOrder().getType().equals(Order.NONE)) {
                selectedOrders.add(zone.getOrder().getName());
            }
        }
        for (String order : selectedOrders) {
            if (!availableOrders.remove(order)) {
                JavaFXLauncher.showInformationDialog("Duplicate Command",
                        "You have selected too many  " + order + " commands.");
                return;
            }
        }
        if (validateKingsCourt()) {
            // Dont submit if testing
            Client.getLogger().info("Planning Phase validated, submitting commands.");
            if (GameContext.getMyGameContext().getTurns() != null) {
                planningPhase.submitCommands(this);
            }
        }
    }

    private boolean validateKingsCourt() {
        // Validate Commands against kings court
        List<Player> players = GameContext.getMyGameContext().getPlayers();
        int kingCourtTrack = 10;
        for (Player player : players) {
            if (player.isMe()) {
                kingCourtTrack = player.getKingsCourtTrack();
                break;
            }
        }
        int stars = 0;
        for (Zone zone : tblZoneData) {
            if (zone.getOrder().isStar()) {
                stars++;
            }
        }
        if (players.size() <= 4) {
            if (kingCourtTrack == 1) {
                if (stars > 3) {
                    JavaFXLauncher.showInformationDialog(
                            "Kings Court Violation",
                            "You can only use 3 star commands, you have selected "
                                    + stars);
                    return false;
                }
            }
            if (kingCourtTrack == 2) {
                if (stars > 2) {
                    JavaFXLauncher.showInformationDialog(
                            "Kings Court Violation",
                            "You can only use 2 star commands, you have selected "
                                    + stars);
                    return false;
                }
            }
            if (kingCourtTrack == 3) {
                if (stars > 1) {
                    JavaFXLauncher.showInformationDialog(
                            "Kings Court Violation",
                            "You can only use 1 star commands, you have selected "
                                    + stars);
                    return false;
                }
            }
            if (kingCourtTrack > 3) {
                if (stars > 0) {
                    JavaFXLauncher.showInformationDialog(
                            "Kings Court Violation",
                            "You can't use star commands, you have selected "
                                    + stars);
                    return false;
                }
            }
        } else {
            if (kingCourtTrack == 1) {
                if (stars > 3) {
                    JavaFXLauncher.showInformationDialog(
                            "Kings Court Violation",
                            "You can only use 3 star commands, you have selected "
                                    + stars);
                    return false;
                }
            }
            if (kingCourtTrack == 2) {
                if (stars > 3) {
                    JavaFXLauncher.showInformationDialog(
                            "Kings Court Violation",
                            "You can only use 3 star commands, you have selected "
                                    + stars);
                    return false;
                }
            }
            if (kingCourtTrack == 3) {
                if (stars > 2) {
                    JavaFXLauncher.showInformationDialog(
                            "Kings Court Violation",
                            "You can only use 2 star commands, you have selected "
                                    + stars);
                    return false;
                }
            }
            if (kingCourtTrack == 4) {
                if (stars > 1) {
                    JavaFXLauncher.showInformationDialog(
                            "Kings Court Violation",
                            "You can only use 1 star commands, you have selected "
                                    + stars);
                    return false;
                }
            }
            if (kingCourtTrack > 4) {
                if (stars > 0) {
                    JavaFXLauncher.showInformationDialog(
                            "Kings Court Violation",
                            "You can't use star commands, you have selected "
                                    + stars);
                    return false;
                }
            }
        }
        return true;
    }

    public TableView getZonesTable() {
        return tblZones;
    }
}