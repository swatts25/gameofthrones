package controllers;

import javafx.scene.Scene;
import client.model.Unit;

public abstract class JavaFXController {

    private String fxmlPanel;

    private Scene scene;

    public JavaFXController(String fxmlPanel) {
        this.fxmlPanel = fxmlPanel;
    }

    public String getFxmlPanel() {
        return fxmlPanel;
    }

    public Scene getScene() {
        return scene;
    }

    public void setScene(Scene scene) {
        this.scene = scene;
    }

    public void markUnit(Unit unit, Boolean newValue) {
        return;
    }
}
