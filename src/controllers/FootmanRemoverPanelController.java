package controllers;

import java.net.URL;
import java.util.List;
import java.util.Map;
import java.util.ResourceBundle;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.Stage;
import javafx.util.Callback;
import client.Client;
import client.HouseCardSpecialAbility;
import client.TurnHandler;
import client.model.Footman;
import client.model.GameContext;
import client.model.MarchMovement;
import client.model.Player;
import client.model.Turn;
import client.model.Unit;
import client.model.zones.Zone;

@SuppressWarnings({ "unchecked", "rawtypes" })
public class FootmanRemoverPanelController extends JavaFXController implements
        Initializable {

    private ObservableList<Zone> tblZoneData = FXCollections
            .observableArrayList();

    @FXML
    private TableView tblFootmen;

    private String targetZoneName;

    private String opponentName;

    private HouseCardSpecialAbility maceHouseCardAbility;

    private MainScreenPanelController mainScreenPanelController;

    private Turn currentTurn;

    public FootmanRemoverPanelController(String targetZoneName,
            String opponentName, HouseCardSpecialAbility queenHouseCardAbility,
            MainScreenPanelController mainScreenPanelController,
            Turn currentTurn) {
        super("/forms/BaseUnitEditorPanel.fxml");
        this.targetZoneName = targetZoneName;
        this.opponentName = opponentName;
        this.maceHouseCardAbility = queenHouseCardAbility;
        this.mainScreenPanelController = mainScreenPanelController;
        this.currentTurn = currentTurn;
    }

    @Override
    public void initialize(URL arg0, ResourceBundle arg1) {
        initTableView();
    }

    private void initTableView() {
        tblFootmen.setEditable(true);
        TableColumn zoneCol = new TableColumn("Zone");
        zoneCol.setPrefWidth(356);
        zoneCol.setCellValueFactory(new PropertyValueFactory<Zone, String>(
                "name"));
        TableColumn actionCol = new TableColumn("Action");
        actionCol.setCellValueFactory(new PropertyValueFactory<>("DUMMY"));
        actionCol
                .setCellFactory(new Callback<TableColumn<Zone, String>, TableCell<Zone, String>>() {

                    @Override
                    public TableCell call(final TableColumn<Zone, String> param) {
                        final TableCell<Zone, String> cell = new TableCell<Zone, String>() {

                            final Button btn = new Button("Remove");

                            @Override
                            public void updateItem(String item, boolean empty) {
                                super.updateItem(item, empty);
                                if (empty) {
                                    setGraphic(null);
                                    setText(null);
                                } else {
                                    btn.setOnAction((ActionEvent event) -> {
                                        Zone zone = getTableView().getItems()
                                                .get(getIndex());
                                        selectionMade(zone);
                                    });
                                    setGraphic(btn);
                                    setText(null);
                                }
                            }
                        };
                        return cell;
                    }
                });
        actionCol.setPrefWidth(65);
        tblFootmen.getColumns().addAll(zoneCol, actionCol);
        updateZonesTable(targetZoneName, opponentName);
    }

    private boolean hasFootmen(Zone zone) {
        List<Unit> units = zone.getUnits();
        for (Unit unit : units) {
            if (unit instanceof Footman) {
                return true;
            }
        }
        List<MarchMovement> marches = zone.getMarches();
        for (MarchMovement marchMovement : marches) {
            Unit unit = marchMovement.getUnit();
            if (unit.getPlayerName().equalsIgnoreCase(opponentName)
                    && unit instanceof Footman) {
                return true;
            }
        }
        return false;
    }

    private void updateZonesTable(String targetZoneName, String opponentName) {
        GameContext context = GameContext.getMyGameContext();
        Map<String, Zone> zones = context.getZones();
        Zone targetZone = context.getZones().get(targetZoneName);
        Player opponent = null;
        for (Player player : context.getPlayers()) {
            if (player.getName().equalsIgnoreCase(opponentName)) {
                opponent = player;
            }
        }
        tblZoneData.clear();
        for (String adajacentZoneName : targetZone.getAdjacentZones()) {
            // Need to add some filters here, check house card
            Zone adjacentZone = zones.get(adajacentZoneName);
            if (adjacentZone.getControlledBy() != null
                    && adjacentZone.getControlledBy().equals(opponent)
                    && hasFootmen(adjacentZone)) {
                tblZoneData.add(adjacentZone);
            }
        }
        tblFootmen.setItems(tblZoneData);
    }

    private void selectionMade(Zone zone) {
        GameContext context = GameContext.getMyGameContext();
        if (zone != null) {
            Client.getLogger().info("Removed Footman from " + zone.getName());
            removeFootman(zone);
        } else {
            Client.getLogger().info("No Footman has been removed");
        }
        if (maceHouseCardAbility != null && mainScreenPanelController != null
                && currentTurn != null) {
            if (maceHouseCardAbility.isAttacker()) {
                maceHouseCardAbility.getCombat().setAttackerHouseCardPlayed(
                        true);
            } else {
                maceHouseCardAbility.getCombat().setDefenderHouseCardPlayed(
                        true);
            }
            context.getTurns().remove(0);
            if (context.getTurns().isEmpty()) {
                maceHouseCardAbility.getCombat().resolveCombatBetweenPlayers(
                        mainScreenPanelController);
            }
            TurnHandler.saveAndDistributeGameContext(currentTurn, context);
            TurnHandler.UpdateUI(mainScreenPanelController);
        }
        Stage stage = (Stage) tblFootmen.getScene().getWindow();
        stage.close();
    }

    private void removeFootman(Zone zone) {
        List<Unit> aclimatedUnits = zone.getUnits();
        for (Unit unit : aclimatedUnits) {
            if (unit.getPlayerName().equalsIgnoreCase(opponentName)
                    && unit instanceof Footman) {
                aclimatedUnits.remove(unit);
                return;
            }
        }
        List<MarchMovement> marches = zone.getMarches();
        for (MarchMovement marchMovement : marches) {
            Unit unit = marchMovement.getUnit();
            if (unit.getPlayerName().equalsIgnoreCase(opponentName)
                    && unit instanceof Footman) {
                marches.remove(marchMovement);
                return;
            }
        }
    }

    public void doneClicked() {
        selectionMade(null);
    }
}
