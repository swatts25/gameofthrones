package controllers;

import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableColumn.CellDataFeatures;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.util.Callback;
import client.model.GameContext;
import client.model.Order;
import client.model.Player;
import client.model.zones.Zone;

@SuppressWarnings({ "unchecked", "rawtypes" })
public class OrderViewerPanelController extends JavaFXController
        implements Initializable {

    private ObservableList<Zone> tblZoneData = FXCollections
            .observableArrayList();

    private ObservableList<Player> cboPlayerData = FXCollections
            .observableArrayList();

    @FXML
    private TableView tblZones;

    @FXML
    private ComboBox cboPlayer;

    @FXML
    private Button btnEditCommand;

    @FXML
    private Button btnWildlingCard;

    public OrderViewerPanelController() {
        super("/forms/BaseOrderViewerPanel.fxml");
    }

    @Override
    public void initialize(URL arg0, ResourceBundle arg1) {
        initTableView();
        initializePlayerComboBox();
    }

    private void initTableView() {
        tblZones.setEditable(true);
        TableColumn zoneCol = new TableColumn("Name");
        zoneCol.setPrefWidth(128);
        zoneCol.setCellValueFactory(
                new PropertyValueFactory<Zone, String>("name"));
        TableColumn commandCol = new TableColumn("Order");
        commandCol.setPrefWidth(159);
        commandCol.setCellValueFactory(
                new PropertyValueFactory<Order, String>("Order"));
        TableColumn starCol = new TableColumn("Star");
        starCol.setPrefWidth(39);
        starCol.setStyle("-fx-alignment: CENTER;");
        starCol.setCellValueFactory(
                new Callback<CellDataFeatures<Zone, String>, ObservableValue<String>>() {

                    public ObservableValue<String> call(
                            CellDataFeatures<Zone, String> cell) {
                        SimpleStringProperty selectedContinent = new SimpleStringProperty();
                        if (cell.getValue().getOrder() != null
                                && cell.getValue().getOrder().isStar()) {
                            selectedContinent.setValue("*");
                        } else {
                            selectedContinent.setValue("");
                        }
                        return selectedContinent;
                    }
                });
        tblZones.getColumns().addAll(zoneCol, commandCol, starCol);
        updateZonesTable(Player.getMyName());
    }

    private void initializePlayerComboBox() {
        cboPlayer.setCellFactory(
                new Callback<ListView<Player>, ListCell<Player>>() {

                    @Override
                    public ListCell<Player> call(ListView<Player> param) {
                        final ListCell<Player> cell = new ListCell<Player>() {

                            @Override
                            public void updateItem(Player player,
                                    boolean empty) {
                                super.updateItem(player, empty);
                                if (player != null) {
                                    setText(player.getName());
                                } else {
                                    setText(null);
                                }
                            }
                        };
                        return cell;
                    }
                });
        cboPlayer.valueProperty().addListener(new ChangeListener<Player>() {

            @Override
            public void changed(ObservableValue<? extends Player> arg0,
                    Player oldSelection, Player newSelection) {
                updateZonesTable(newSelection.getName());
            }
        });
        updatePlayerCombobox();
    }

    private void updateZonesTable(String playerName) {
        List<Zone> zones = new ArrayList<Zone>(
                GameContext.getMyGameContext().getZones().values());
        tblZoneData.clear();
        for (Zone zone : zones) {
            if (zone.getControlledBy() != null && zone.getControlledBy()
                    .getName().equalsIgnoreCase(playerName)) {
                tblZoneData.add(zone);
            }
        }
        tblZones.setItems(tblZoneData);
        tblZones.setDisable(false);
    }

    private void updatePlayerCombobox() {
        List<Player> players = GameContext.getMyGameContext().getPlayers();
        cboPlayerData.addAll(players);
        cboPlayer.setItems(cboPlayerData);
        for (Player player : players) {
            if (player.isMe()) {
                // Updates zones table
                cboPlayer.getSelectionModel().select(player);
            }
        }
        cboPlayer.setDisable(false);
    }
}
