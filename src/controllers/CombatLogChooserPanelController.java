package controllers;

import java.net.URL;
import java.util.List;
import java.util.ResourceBundle;

import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.stage.Stage;
import client.JavaFXLauncher;
import client.model.CombatHistory;

public class CombatLogChooserPanelController extends JavaFXController implements
        Initializable {

    private int index;

    private List<CombatHistory> combats;

    @FXML
    Label lblId;

    @FXML
    Label lblZone;

    @FXML
    Label lblAttacker;

    @FXML
    Label lblDefender;

    public CombatLogChooserPanelController(List<CombatHistory> combats) {
        super("/forms/BaseCombatLogChooserPanel.fxml");
        this.combats = combats;
        index = 0;
    }

    @Override
    public void initialize(URL arg0, ResourceBundle arg1) {
        nextClicked();
    }

    public void selectClicked() {
        JavaFXLauncher.showPanel("/forms/BaseCombatLogPanel.fxml",
                "Combat Log", new CombatLogPanelController(combats.get(index)));
        closePanel();
    }

    private void closePanel() {
        Stage stage = (Stage) lblId.getScene().getWindow();
        stage.close();
    }

    public void previousClicked() {
        CombatHistory combat;
        if (index > 0) {
            combat = combats.get(--index);
        } else {
            index = (combats.size() - 1);
            combat = combats.get(index);
        }
        updatePanel(combat);
    }

    private void updatePanel(CombatHistory combat) {
        lblId.setText("Id: " + String.valueOf(combat.getCombatId()));
        lblZone.setText("Zone: " + combat.getTargetZoneName());
        lblAttacker.setText("Attacker: " + combat.getAttackerName());
        lblDefender.setText("Defender: " + combat.getDefenderName());
    }

    public void nextClicked() {
        CombatHistory combat;
        if (index < combats.size() - 1) {
            combat = combats.get(++index);
        } else {
            index = 0;
            combat = combats.get(index);
        }
        updatePanel(combat);
    }

    public void keyPressed(KeyEvent event) {
        if (event.getCode().equals(KeyCode.RIGHT)
                || event.getCode().equals(KeyCode.UP)) {
            nextClicked();
        } else if (event.getCode().equals(KeyCode.LEFT)
                || event.getCode().equals(KeyCode.DOWN)) {
            previousClicked();
        } else if (event.getCode().equals(KeyCode.ESCAPE)) {
            closePanel();
        }
    }
}
