package controllers;

import java.net.URL;
import java.util.ResourceBundle;

import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.image.ImageView;
import client.model.CombatHistory;

public class CombatLogPanelController extends JavaFXController implements
        Initializable {

    private CombatHistory combatHistory;

    public CombatLogPanelController(CombatHistory combatHistory) {
        super("/forms/BaseCombatLogPanel.fxml");
        this.combatHistory = combatHistory;
    }

    @FXML
    ImageView imvAttackingCardImage;

    @FXML
    ImageView imvDefendingCardImage;

    @FXML
    Label lblZone;

    @FXML
    Label lblAttackingPlayer;

    @FXML
    Label lblDefendingPlayer;

    @FXML
    Label lblMarchingPower;

    @FXML
    Label lblAttackingSupportPower;

    @FXML
    Label lblAttackingSpecialAbility;

    @FXML
    Label lblDefendingPower;

    @FXML
    Label lblDefendingSupportPower;

    @FXML
    Label lblDefendingSpecialAbility;

    @FXML
    TextArea txtLog;

    @FXML
    @Override
    public void initialize(URL arg0, ResourceBundle arg1) {
        imvAttackingCardImage.setImage(combatHistory.getAttackerHouseCard()
                .getImage());
        imvDefendingCardImage.setImage(combatHistory.getDefenderHouseCard()
                .getImage());
        lblZone.setText("Battle for " + combatHistory.getTargetZoneName());
        lblAttackingPlayer.setText("ATTACKER: "
                + combatHistory.getAttackerName());
        lblDefendingPlayer.setText("DEFENDER: "
                + combatHistory.getDefenderName());
        lblMarchingPower.setText("Marching Power: +"
                + combatHistory.getAttackerMarchingPower());
        lblAttackingSupportPower.setText("Support Power: +"
                + combatHistory.getAttackerSupportPower());
        lblAttackingSpecialAbility.setText("Special Ability: +"
                + combatHistory.getAttackerSpecialAbility());
        lblDefendingPower.setText("Defending Power: +"
                + combatHistory.getDefenderDefensePower());
        lblDefendingSupportPower.setText("Support Power: +"
                + combatHistory.getDefenderSupportPower());
        lblDefendingSpecialAbility.setText("Special Ability: +"
                + combatHistory.getDefenderSpecialAbility());
        txtLog.setText(combatHistory.getLog());
    }
}
