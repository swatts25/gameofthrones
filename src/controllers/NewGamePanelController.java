package controllers;

import java.io.File;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;
import java.util.logging.Logger;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableColumn.CellEditEvent;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.control.cell.TextFieldTableCell;
import javafx.stage.Stage;

import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;

import utils.EmailMessage;
import utils.GameOfThronesMarshaller;
import utils.SavedGameData;
import utils.StringHelper;
import client.Board;
import client.Client;
import client.JavaFXLauncher;
import client.model.GameContext;
import client.model.House;
import client.model.Player;
import client.model.Turn;
import client.model.cards.AgeOneWesterosCard;
import client.model.cards.AgeThreeWesterosCard;
import client.model.cards.AgeTwoWesterosCard;
import client.model.cards.BaratheonHouseCard;
import client.model.cards.Deck;
import client.model.cards.GreyjoyHouseCard;
import client.model.cards.LannisterHouseCard;
import client.model.cards.MartellHouseCard;
import client.model.cards.StarkHouseCard;
import client.model.cards.TyrellHouseCard;
import client.model.cards.WildlingCard;

@SuppressWarnings({ "rawtypes", "unchecked" })
public class NewGamePanelController extends JavaFXController implements
        Initializable {

    private MainScreenPanelController mainScreen;

    private final ObservableList<Player> data = FXCollections
            .observableArrayList(new Player(), new Player(), new Player(),
                    new Player(), new Player(), new Player());

    public NewGamePanelController(MainScreenPanelController mainScreen) {
        super("/forms/BaseNewGamePanel.fxml");
        this.mainScreen = mainScreen;
    }

    @FXML
    private TableView tblPlayers;

    @FXML
    public void startClicked() {
        List<Player> newPlayers = new ArrayList<Player>();
        for (Player player : data) {
            if (StringHelper.isNullOrEmpty(player.getName())
                    && StringHelper.isNotNullOrEmpty(player.getEmailAddress())) {
                JavaFXLauncher.showInformationDialog("Oops",
                        "Oops! One of the players is missing a name.");
                return;
            } else if (StringHelper.isNotNullOrEmpty(player.getName())
                    && StringHelper.isNullOrEmpty(player.getEmailAddress())) {
                JavaFXLauncher.showInformationDialog("Oops", player.getName()
                        + " is missing a email address.");
                return;
            } else if (StringHelper.isNotNullOrEmpty(player.getName())) {
                newPlayers.add(player);
            }
        }
        boolean iAmPlaying = false;
        for (Player player : newPlayers) {
            if (player.isMe()) {
                iAmPlaying = true;
            }
        }
        if (!iAmPlaying) {
            JavaFXLauncher.showInformationDialog(
                    "Oops!",
                    "You left yourself out! Don't you want to play "
                            + Player.getMyName() + "?");
            return;
        }
        if (newPlayers.size() < 3) {
            JavaFXLauncher.showInformationDialog("Oops!",
                    "You must have 3 players to start a game.");
            return;
        }
        if (newPlayers.size() > 6) {
            JavaFXLauncher.showInformationDialog("Oops!",
                    "You cannot play with more than 6 players.");
            return;
        }
        File newGameFile = startGame(newPlayers);
        Stage stage = (Stage) tblPlayers.getScene().getWindow();
        stage.close();
        GameContext newGameContext = GameOfThronesMarshaller
                .unMarshallGameContext(newGameFile);
        mainScreen.loadGame(newGameContext);
        JavaFXLauncher.showInformationDialog("Confirmation",
                "Game invitations have been sent.");
    }

    private File startGame(List<Player> newPlayers) {
        GameContext context = new GameContext();
        GameContext.setMyGameContext(context);
        for (int i = 0; i < newPlayers.size(); i++) {
            newPlayers.get(i).setId(i);
        }
        // Remove random gameId for debugging
        // int randomNum = 10000 + (int) (Math.random() * ((90000 - 10000) +
        // 1));
        int randomNum = 1;
        context.setGameid(randomNum);
        SavedGameData.saveProperty(SavedGameData.GAME_ID_PROPERTY,
                Integer.toString(randomNum));
        context.setContextid(1);
        // Init Houses
        House.setHouses(newPlayers);
        context.setPlayers(newPlayers);
        // Init Zones
        Board.initializeZones(newPlayers);
        context.setZones(Board.zones);
        // Init Card Decks
        Deck<WildlingCard> wildlingDeck = Deck.getNewWildlingDeck();
        Deck<AgeOneWesterosCard> ageOneDeck = Deck.getNewAgeOneDeck();
        Deck<AgeTwoWesterosCard> ageTwoDeck = Deck.getNewAgeTwoDeck();
        Deck<AgeThreeWesterosCard> ageThreeDeck = Deck.getNewAgeThreeDeck();
        context.setWildlingDeck(wildlingDeck.getCardsAsList());
        context.setAgeOneDeck(ageOneDeck.getCardsAsList());
        context.setAgeTwoDeck(ageTwoDeck.getCardsAsList());
        context.setAgeThreeDeck(ageThreeDeck.getCardsAsList());
        // Add house decks
        if (newPlayers.size() >= 3) {
            Deck<StarkHouseCard> starkHouseDeck = Deck.getNewStarkHouseDeck();
            Deck<BaratheonHouseCard> baratheonHouseDeck = Deck
                    .getNewBaratheonHouseDeck();
            Deck<LannisterHouseCard> lannisterHouseDeck = Deck
                    .getNewLannisterHouseDeck();
            context.setStarkHouseDeck(starkHouseDeck.getCardsAsList());
            context.setBaratheonHouseDeck(baratheonHouseDeck.getCardsAsList());
            context.setLannisterHouseDeck(lannisterHouseDeck.getCardsAsList());
        }
        if (newPlayers.size() >= 4) {
            Deck<GreyjoyHouseCard> greyjoyHouseDeck = Deck
                    .getNewGreyjoyHouseDeck();
            context.setGreyjoyHouseDeck(greyjoyHouseDeck.getCardsAsList());
        }
        if (newPlayers.size() >= 5) {
            Deck<TyrellHouseCard> tyrellHouseDeck = Deck
                    .getNewTyrellHouseDeck();
            context.setTyrellHouseDeck(tyrellHouseDeck.getCardsAsList());
        }
        if (newPlayers.size() >= 6) {
            Deck<MartellHouseCard> martellHouseDeck = Deck
                    .getNewMartellHouseDeck();
            context.setMartellHouseDeck(martellHouseDeck.getCardsAsList());
        }
        // Set next turn (Planning Phase)
        List<Turn> turns = new ArrayList<Turn>();
        for (Player player : newPlayers) {
            Turn turn = new Turn(player);
            turn.setType(Turn.PLANNING_PHASE);
            turns.add(turn);
        }
        context.setTurns(turns);
        File newGameFile = GameOfThronesMarshaller.marshall(context);
        EmailMessage message = new EmailMessage(
                StringHelper.concatinateEmailAddresses(newPlayers),
                "New Game from " + Player.getMyName(),
                "You have been invited to play A Game of Thrones board game!\n\nIn order to play you must first download the latest version of the game client available here: "
                        + MainScreenPanelController.LATEST_GAME_URL
                        + "\n\nAfter downloading the game run the .exe file and load in the attached NewGameContext.xml file.\n\nWhen starting the game, you must use your own name which must be one of the following for this game: "
                        + StringHelper.getNameList(newPlayers)
                        + "In order to help us improve the game, please report isssues (bugs, improvements, wildest desires) here: "
                        + MainScreenPanelController.ISSUE_TRACKING_URL
                        + "\n\nIn order to run the game, you will need a Java 8 (or later) runtime enviormnet, if you get an error starting the game download the latest version of Java here: https://java.com/en/download/"
                        + "\n\nAll of us here at the Game of Thrones board game team (mainly just Sean) thank you for playing. Be sure to make a donation! LOL JK, but not really #collegeloans\n\n\nSean Watts\nLead/Sole Developer and President",
                "NewGameContext.xml", newGameFile.getAbsolutePath());
        message.send();
        return newGameFile;
    }

    @Override
    public void initialize(URL arg0, ResourceBundle arg1) {
        initTableView();
    }

    private void initTableView() {
        tblPlayers.setEditable(true);
        TableColumn nameCol = new TableColumn("Name");
        tblPlayers.setFixedCellSize(30);
        nameCol.setCellValueFactory(new PropertyValueFactory<Player, String>(
                "name"));
        nameCol.setCellFactory(TextFieldTableCell.<Player> forTableColumn());
        nameCol.setMinWidth(100);
        nameCol.setResizable(false);
        nameCol.setOnEditCommit(new EventHandler<CellEditEvent<Player, String>>() {

            @Override
            public void handle(CellEditEvent<Player, String> t) {
                String desiredName = t.getNewValue();
                for (Player player : data) {
                    if (player.getName() != null
                            && player.getName().equalsIgnoreCase(desiredName)) {
                        JavaFXLauncher.showInformationDialog("Oops!",
                                "A player with that name already exists.");
                        Player emptyNamePlayer = new Player();
                        emptyNamePlayer.setEmailAddress(((Player) t
                                .getTableView().getItems()
                                .get(t.getTablePosition().getRow()))
                                .getEmailAddress());
                        t.getTableView()
                                .getItems()
                                .set(t.getTablePosition().getRow(),
                                        emptyNamePlayer);
                        return;
                    }
                }
                ((Player) t.getTableView().getItems()
                        .get(t.getTablePosition().getRow()))
                        .setName(desiredName);
            }
        });
        TableColumn emailCol = new TableColumn("Email Address");
        emailCol.setCellValueFactory(new PropertyValueFactory<Player, String>(
                "emailAddress"));
        emailCol.setCellFactory(TextFieldTableCell.<Player> forTableColumn());
        emailCol.setMinWidth(281);
        emailCol.setResizable(false);
        emailCol.setOnEditCommit(new EventHandler<CellEditEvent<Player, String>>() {

            @Override
            public void handle(CellEditEvent<Player, String> t) {
                if (isValidEmailAddress(t.getNewValue())) {
                    ((Player) t.getTableView().getItems()
                            .get(t.getTablePosition().getRow()))
                            .setEmailAddress(t.getNewValue());
                } else {
                    JavaFXLauncher.showInformationDialog("Oops!",
                            "The email address you entered is not valid.");
                    Player emptyEmailPlayer = new Player();
                    emptyEmailPlayer.setName(((Player) t.getTableView()
                            .getItems().get(t.getTablePosition().getRow()))
                            .getName());
                    t.getTableView()
                            .getItems()
                            .set(t.getTablePosition().getRow(),
                                    emptyEmailPlayer);
                }
            }
        });
        tblPlayers.setItems(data);
        tblPlayers.getColumns().addAll(nameCol, emailCol);
    }

    private boolean isValidEmailAddress(String email) {
        boolean result = true;
        try {
            InternetAddress emailAddr = new InternetAddress(email);
            emailAddr.validate();
        } catch (AddressException ex) {
            Client.getLogger().info(
                    "Player entered invalid email, exception caught");
            result = false;
        }
        return result;
    }
}
