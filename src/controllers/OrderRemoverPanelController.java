package controllers;

import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.ResourceBundle;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.Stage;
import javafx.util.Callback;
import client.Client;
import client.HouseCardSpecialAbility;
import client.JavaFXLauncher;
import client.TurnHandler;
import client.model.GameContext;
import client.model.Order;
import client.model.Player;
import client.model.Turn;
import client.model.zones.Zone;

@SuppressWarnings({ "unchecked", "rawtypes" })
public class OrderRemoverPanelController extends JavaFXController implements
        Initializable {

    private ObservableList<Zone> tblZoneData = FXCollections
            .observableArrayList();

    @FXML
    private TableView tblOrders;

    private String targetZoneName;

    private String opponentName;

    private HouseCardSpecialAbility houseCardAbility;

    private MainScreenPanelController mainScreenPanelController;

    private Turn currentTurn;

    public OrderRemoverPanelController(String targetZoneName,
            String opponentName, HouseCardSpecialAbility houseCardAbility,
            MainScreenPanelController mainScreenPanelController,
            Turn currentTurn) {
        super("/forms/BaseOrderRemoverPanel.fxml");
        this.targetZoneName = targetZoneName;
        this.opponentName = opponentName;
        this.houseCardAbility = houseCardAbility;
        this.mainScreenPanelController = mainScreenPanelController;
        this.currentTurn = currentTurn;
    }

    @Override
    public void initialize(URL arg0, ResourceBundle arg1) {
        initTableView();
    }

    private void initTableView() {
        tblOrders.setEditable(true);
        TableColumn zoneCol = new TableColumn("Zone");
        zoneCol.setPrefWidth(225);
        zoneCol.setCellValueFactory(new PropertyValueFactory<Zone, String>(
                "name"));
        TableColumn orderCol = new TableColumn("Order");
        orderCol.setPrefWidth(131);
        orderCol.setCellValueFactory(new PropertyValueFactory<Order, String>(
                "Order"));
        TableColumn actionCol = new TableColumn("Action");
        actionCol.setCellValueFactory(new PropertyValueFactory<>("DUMMY"));
        actionCol
                .setCellFactory(new Callback<TableColumn<Zone, String>, TableCell<Zone, String>>() {

                    @Override
                    public TableCell call(final TableColumn<Zone, String> param) {
                        final TableCell<Zone, String> cell = new TableCell<Zone, String>() {

                            final Button btn = new Button("Remove");

                            @Override
                            public void updateItem(String item, boolean empty) {
                                super.updateItem(item, empty);
                                if (empty) {
                                    setGraphic(null);
                                    setText(null);
                                } else {
                                    btn.setOnAction((ActionEvent event) -> {
                                        Zone zone = getTableView().getItems()
                                                .get(getIndex());
                                        selectionMade(zone);
                                    });
                                    setGraphic(btn);
                                    setText(null);
                                }
                            }
                        };
                        return cell;
                    }
                });
        actionCol.setPrefWidth(65);
        tblOrders.getColumns().addAll(zoneCol, orderCol, actionCol);
        if (currentTurn.getType().equals(Turn.QUEEN_OF_THORNS)) {
            updateZonesTableWithAdjacentZones(targetZoneName, opponentName);
        } else if (currentTurn.getType().equals(Turn.CERSEI_LANNISTER)) {
            updateZonesTableWithAllZones(opponentName);
        }
    }

    private void updateZonesTableWithAdjacentZones(String targetZoneName,
            String opponentName) {
        GameContext context = GameContext.getMyGameContext();
        Map<String, Zone> zones = context.getZones();
        Zone targetZone = context.getZones().get(targetZoneName);
        Player opponent = null;
        for (Player player : context.getPlayers()) {
            if (player.getName().equalsIgnoreCase(opponentName)) {
                opponent = player;
            }
        }
        tblZoneData.clear();
        for (String adajacentZoneName : targetZone.getAdjacentZones()) {
            // Need to add some filters here, check house card
            Zone adjacentZone = zones.get(adajacentZoneName);
            if (adjacentZone.getControlledBy() != null
                    && adjacentZone.getControlledBy().equals(opponent)
                    && adjacentZone.getOrder() != null
                    && !adjacentZone.getOrder().getType().equals(Order.NONE)) {
                tblZoneData.add(adjacentZone);
            }
        }
        if (tblZoneData.isEmpty()) {
            JavaFXLauncher.showInformationDialog("No Orders",
                    "Your opponent has no orders in any zone adjacent to "
                            + targetZone.getName());
        } else {
            tblOrders.setItems(tblZoneData);
        }
    }

    private void updateZonesTableWithAllZones(String opponentName) {
        GameContext context = GameContext.getMyGameContext();
        List<Zone> zones = new ArrayList<Zone>(context.getZones().values());
        Player opponent = null;
        for (Player player : context.getPlayers()) {
            if (player.getName().equalsIgnoreCase(opponentName)) {
                opponent = player;
            }
        }
        tblZoneData.clear();
        for (Zone zone : zones) {
            // Need to add some filters here, check house card
            if (zone.getControlledBy() != null
                    && zone.getControlledBy().equals(opponent)
                    && zone.getOrder() != null
                    && !zone.getOrder().getType().equals(Order.NONE)) {
                tblZoneData.add(zone);
            }
        }
        if (tblZoneData.isEmpty()) {
            JavaFXLauncher.showInformationDialog("No Orders",
                    "Your opponent has no orders in any zone");
        } else {
            tblOrders.setItems(tblZoneData);
        }
    }

    private void selectionMade(Zone zone) {
        GameContext context = GameContext.getMyGameContext();
        if (zone != null) {
            Client.getLogger().info("Removed Order " + zone.getOrder().getName()
                    + " from " + zone.getName());
            zone.setOrder(Order.NONE_ORDER);
        } else {
            Client.getLogger().info("No Order has been removed");
        }
        if (houseCardAbility != null && mainScreenPanelController != null
                && currentTurn != null) {
            if (houseCardAbility.isAttacker()) {
                houseCardAbility.getCombat().setAttackerHouseCardPlayed(true);
            } else {
                houseCardAbility.getCombat().setDefenderHouseCardPlayed(true);
            }
            context.getTurns().remove(0);
            if (context.getTurns().isEmpty()) {
                houseCardAbility.getCombat().resolveCombatBetweenPlayers(
                        mainScreenPanelController);
            }
            TurnHandler.saveAndDistributeGameContext(currentTurn, context);
            TurnHandler.UpdateUI(mainScreenPanelController);
        }
        Stage stage = (Stage) tblOrders.getScene().getWindow();
        stage.close();
    }

    public void doneClicked() {
        selectionMade(null);
    }
}
