package controllers;

import java.net.URL;
import java.util.List;
import java.util.ResourceBundle;

import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.stage.Stage;
import client.Client;
import client.Combat;
import client.TurnHandler;
import client.model.GameContext;
import client.model.House;
import client.model.Turn;
import client.model.cards.HouseCard;
import client.model.zones.Zone;

public class HouseCardChooserPanelController extends JavaFXController
        implements Initializable {

    private List<HouseCard> cards;

    private int index;

    private MainScreenPanelController mainscreen;

    private Turn turn;

    public HouseCardChooserPanelController(List houseCards,
            MainScreenPanelController mainscreen, Turn turn) {
        super("/forms/BaseHouseCardChooserPanel.fxml");
        this.mainscreen = mainscreen;
        this.turn = turn;
        if (houseCards == null || houseCards.isEmpty()) {
            Client.error("House deck is empty!");
        }
        this.cards = houseCards;
        index = 0;
    }

    @FXML
    ImageView imvCardImage;

    @FXML
    Label lblContestedZone;

    @FXML
    Label lblOpponentCombatPower;

    @FXML
    Label lblYourCombatPower;

    @Override
    public void initialize(URL arg0, ResourceBundle arg1) {
        GameContext context = GameContext.getMyGameContext();
        imvCardImage.setImage(cards.get(index).getImage());
        Combat combat = Combat.getCombat(turn.getCombatId());
        if (combat != null) {
            combat.setContext(context);
            Zone targetZone = context.getZones()
                    .get(combat.getTargetZoneName());
            lblContestedZone
                    .setText("Contested Zone: " + combat.getTargetZoneName());
            if (!combat.getAttackingPlayerName()
                    .equalsIgnoreCase(turn.getPlayerName())) {
                lblOpponentCombatPower.setText("Opponent Initial Combat Power: "
                        + combat.getAttackingPlayerCombatPower(
                                combat.getAttackingPlayerName(), targetZone));
                lblYourCombatPower.setText("Your Initial Combat Power: "
                        + combat.getDefendingPlayerCombatPower(
                                combat.getDefendingPlayerName(), targetZone));
            } else {
                lblYourCombatPower.setText("Opponent Initial Combat Power: "
                        + combat.getAttackingPlayerCombatPower(
                                combat.getAttackingPlayerName(), targetZone));
                lblOpponentCombatPower.setText("Your Initial Combat Power: "
                        + combat.getDefendingPlayerCombatPower(
                                combat.getDefendingPlayerName(), targetZone));
            }
        }
    }

    public void selectClicked() {
        GameContext context = GameContext.getMyGameContext();
        Combat combat = turn.getCombat();
        HouseCard selectedCard = cards.get(index);
        switch (turn.getType()) {
            case Turn.AERON_DAMPHAIR:
                if (combat.getAttackingPlayerName().equals(turn.getPlayer())) {
                    combat.setAttackingHouseCard(selectedCard);
                } else {
                    combat.setDefendingHouseCard(selectedCard);
                }
                cards.remove(selectedCard);
                context.getTurns().remove(turn);
                if (context.getTurns().isEmpty()) {
                    combat.setContext(context);
                    combat.resolveCombatBetweenPlayers(mainscreen);
                }
                TurnHandler.saveAndDistributeGameContext(turn, context);
                TurnHandler.UpdateUI(mainscreen);
                break;
            case Turn.TYRION_LANNISTER_OPPONENT:
                if (combat.getAttackingPlayerName()
                        .equals(turn.getPlayer().getName())) {
                    combat.setAttackingHouseCard(selectedCard);
                } else {
                    combat.setDefendingHouseCard(selectedCard);
                }
                // remove card from CONTEXT, not cards list. Tyrion card
                // uses a copy
                switch (turn.getPlayer().getHouse()) {
                    case House.BARATHEON:
                        removeCard(selectedCard,
                                context.getBaratheonHouseDeck());
                        break;
                    case House.GREYJOY:
                        removeCard(selectedCard, context.getGreyjoyHouseDeck());
                        break;
                    case House.LANNISTER:
                        removeCard(selectedCard,
                                context.getLannisterHouseDeck());
                        break;
                    case House.MARTELL:
                        removeCard(selectedCard, context.getMartellHouseDeck());
                        break;
                    case House.STARK:
                        removeCard(selectedCard, context.getStarkHouseDeck());
                        break;
                    case House.TYRELL:
                        removeCard(selectedCard, context.getTyrellHouseDeck());
                        break;
                    default:
                        break;
                }
                context.getTurns().remove(0);
                combat.setContext(context);
                combat.resolveCombatBetweenPlayers(mainscreen);
                break;
            case Turn.PATCHFACE:
                cards.remove(selectedCard);
                context.getTurns().remove(turn);
                if (combat.getAttackingPlayerName().equals(turn.getPlayer())) {
                    combat.setAttackingHouseCard(selectedCard);
                } else {
                    combat.setDefendingHouseCard(selectedCard);
                }
                if (context.getTurns().isEmpty()) {
                    combat.setContext(context);
                    combat.resolveCombatBetweenPlayers(mainscreen);
                }
                TurnHandler.saveAndDistributeGameContext(turn, context);
                TurnHandler.UpdateUI(mainscreen);
                break;
            case Turn.CHOOSE_HOUSE_CARD:
                cards.remove(selectedCard);
                context.getTurns().remove(turn);
                if (combat.getAttackingPlayerName()
                        .equals(turn.getPlayer().getName())) {
                    combat.setAttackingHouseCard(selectedCard);
                } else {
                    combat.setDefendingHouseCard(selectedCard);
                }
                // Check if combat is ready to be resolved
                if (combat.getAttackingHouseCard() != null
                        && combat.getDefendingHouseCard() != null) {
                    combat.setContext(context);
                    combat.resolveCombatBetweenPlayers(mainscreen);
                } else {
                    TurnHandler.saveAndDistributeGameContext(turn, context);
                    TurnHandler.UpdateUI(mainscreen);
                }
                break;
            default:
                break;
        }
        closePanel();
    }

    private void closePanel() {
        Stage stage = (Stage) imvCardImage.getScene().getWindow();
        stage.close();
    }

    private void removeCard(HouseCard selectedCard, List houseDeck) {
        for (Object card : houseDeck) {
            if (card instanceof HouseCard) {
                HouseCard houseCard = (HouseCard) card;
                if (selectedCard.getName().equals(houseCard.getName())) {
                    houseDeck.remove(card);
                    return;
                }
            }
        }
    }

    public void previousClicked() {
        if (index > 0) {
            imvCardImage.setImage(cards.get(--index).getImage());
        } else {
            index = (cards.size() - 1);
            imvCardImage.setImage(cards.get(index).getImage());
        }
    }

    public void nextClicked() {
        if (index < cards.size() - 1) {
            imvCardImage.setImage(cards.get(++index).getImage());
        } else {
            index = 0;
            imvCardImage.setImage(cards.get(index).getImage());
        }
    }

    public void keyPressed(KeyEvent event) {
        if (event.getCode().equals(KeyCode.RIGHT)
                || event.getCode().equals(KeyCode.UP)) {
            nextClicked();
        } else if (event.getCode().equals(KeyCode.LEFT)
                || event.getCode().equals(KeyCode.DOWN)) {
            previousClicked();
        } else if (event.getCode().equals(KeyCode.ESCAPE)) {
            closePanel();
        }
    }
}
