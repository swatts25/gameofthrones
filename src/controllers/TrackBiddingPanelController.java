package controllers;

import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.ResourceBundle;

import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableRow;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.ClipboardContent;
import javafx.scene.input.DataFormat;
import javafx.scene.input.Dragboard;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.TransferMode;
import javafx.scene.paint.Color;
import javafx.scene.paint.Paint;
import javafx.stage.Stage;
import utils.StringHelper;
import client.TurnHandler;
import client.model.GameContext;
import client.model.Player;
import client.model.TrackBiddingData;
import client.model.Turn;

@SuppressWarnings({ "rawtypes", "unchecked" })
public class TrackBiddingPanelController extends JavaFXController implements
        Initializable {

    private MainScreenPanelController mainScreen;

    private static final DataFormat SERIALIZED_MIME_TYPE = new DataFormat(
            "application/x-java-serialized-object");

    private Turn currentTurn;

    private int remamingInfluenceTokens;

    public TrackBiddingPanelController(Turn currentTurn,
            MainScreenPanelController mainScreen) {
        super("/forms/BaseTrackBiddingPanel.fxml");
        this.mainScreen = mainScreen;
        this.currentTurn = currentTurn;
    }

    @FXML
    private TextField txtIronThrone;

    @FXML
    private TextField txtFiefdoms;

    @FXML
    private TextField txtKingsCourt;

    @FXML
    private Button btnSubmit;

    @FXML
    private Button btnReset;

    @FXML
    private TableView tblPlayers;

    @FXML
    private Label lblRemainingInfluenceTokens;

    private Paint defaultColor;

    private ObservableList<Player> tblPlayerData = FXCollections
            .observableArrayList();

    @Override
    public void initialize(URL arg0, ResourceBundle arg1) {
        defaultColor = lblRemainingInfluenceTokens.getTextFill();
        txtIronThrone.addEventFilter(KeyEvent.KEY_TYPED, maxLength(3));
        txtFiefdoms.addEventFilter(KeyEvent.KEY_TYPED, maxLength(3));
        txtKingsCourt.addEventFilter(KeyEvent.KEY_TYPED, maxLength(3));
        ChangeListener<String> numericChangeListener = new ChangeListener<String>() {

            @Override
            public void changed(ObservableValue<? extends String> observable,
                    String oldValue, String newValue) {
                if (!newValue.matches("\\d*")) {
                    txtIronThrone.setText(newValue.replaceAll("[^\\d]", ""));
                }
                int newVal;
                if (StringHelper.isNotNullOrEmpty(newValue)) {
                    newVal = Integer.parseInt(newValue);
                } else {
                    newVal = 0;
                }
                int oldVal;
                if (StringHelper.isNotNullOrEmpty(oldValue)) {
                    oldVal = Integer.parseInt(oldValue);
                } else {
                    oldVal = 0;
                }
                remamingInfluenceTokens += oldVal - newVal;
                lblRemainingInfluenceTokens
                        .setText("Remaining Influence Tokens: "
                                + remamingInfluenceTokens);
                if (remamingInfluenceTokens < 0) {
                    lblRemainingInfluenceTokens.setTextFill(Color.RED);
                    btnSubmit.setDisable(true);
                } else {
                    lblRemainingInfluenceTokens.setTextFill(defaultColor);
                    btnSubmit.setDisable(false);
                }
            }
        };
        txtIronThrone.textProperty().addListener(numericChangeListener);
        txtFiefdoms.textProperty().addListener(numericChangeListener);
        txtKingsCourt.textProperty().addListener(numericChangeListener);
        remamingInfluenceTokens = Player.getPlayer(currentTurn.getPlayerName())
                .getInfluence();
        lblRemainingInfluenceTokens.setText("Remaining Influence Tokens: "
                + remamingInfluenceTokens);
        initializePlayerTable();
        updateTracksTable();
    }

    private void initializePlayerTable() {
        TableColumn playerCol = new TableColumn("Player");
        playerCol.setPrefWidth(128);
        playerCol.setCellValueFactory(new PropertyValueFactory<Player, String>(
                "name"));
        TableColumn houseCol = new TableColumn("House");
        houseCol.setPrefWidth(65);
        houseCol.setCellValueFactory(new PropertyValueFactory<Player, String>(
                "house"));
        tblPlayers.getColumns().addAll(playerCol, houseCol);
        tblPlayers.setRowFactory(tv -> {
            TableRow<Player> row = new TableRow<Player>();
            row.setOnDragDetected(event -> {
                if (!row.isEmpty()) {
                    Integer index = row.getIndex();
                    Dragboard db = row.startDragAndDrop(TransferMode.MOVE);
                    db.setDragView(row.snapshot(null, null));
                    ClipboardContent cc = new ClipboardContent();
                    cc.put(SERIALIZED_MIME_TYPE, index);
                    db.setContent(cc);
                    event.consume();
                }
            });
            row.setOnDragOver(event -> {
                Dragboard db = event.getDragboard();
                if (db.hasContent(SERIALIZED_MIME_TYPE)) {
                    if (row.getIndex() != ((Integer) db
                            .getContent(SERIALIZED_MIME_TYPE)).intValue()) {
                        event.acceptTransferModes(TransferMode.COPY_OR_MOVE);
                        event.consume();
                    }
                }
            });
            row.setOnDragDropped(event -> {
                Dragboard db = event.getDragboard();
                if (db.hasContent(SERIALIZED_MIME_TYPE)) {
                    int draggedIndex = (Integer) db
                            .getContent(SERIALIZED_MIME_TYPE);
                    Player draggedPerson = (Player) tblPlayers.getItems()
                            .remove(draggedIndex);
                    int dropIndex;
                    if (row.isEmpty()) {
                        dropIndex = tblPlayers.getItems().size();
                    } else {
                        dropIndex = row.getIndex();
                    }
                    tblPlayers.getItems().add(dropIndex, draggedPerson);
                    event.setDropCompleted(true);
                    tblPlayers.getSelectionModel().select(dropIndex);
                    event.consume();
                }
            });
            return row;
        });
    }

    public void updateTracksTable() {
        List<Player> players = GameContext.getMyGameContext().getPlayers();
        tblPlayerData.clear();
        for (Player player : players) {
            if (!player.isMe()) {
                tblPlayerData.add(player);
            }
        }
        tblPlayers.setItems(tblPlayerData);
    }

    public void resetClicked() {
        txtIronThrone.setText("0");
        txtFiefdoms.setText("0");
        txtKingsCourt.setText("0");
    }

    public void submitClicked() {
        GameContext context = GameContext.getMyGameContext();
        updateBidData(context);
        context.getTurns().remove(currentTurn);
        if (context.getBiddingData().getBidsMap().size() == context
                .getPlayers().size()) {
            resolveBidding(context);
        }
        TurnHandler.saveAndDistributeGameContext(currentTurn, context);
        TurnHandler.UpdateUI(mainScreen);
        Stage stage = (Stage) tblPlayers.getScene().getWindow();
        stage.close();
    }

    private void resolveBidding(GameContext context) {
        for (int trackIndex = 0; trackIndex < 3; trackIndex++) {
            String highestBidder = getHighestBidder(context, trackIndex);
            List<String> track = getTrack(context, highestBidder, trackIndex);
            for (String playerName : track) {
                if (trackIndex == 0) {
                    Player.getPlayer(playerName).setIronThroneTrack(
                            track.indexOf(playerName) + 1);
                } else if (trackIndex == 1) {
                    Player.getPlayer(playerName).setFieldomsTrack(
                            track.indexOf(playerName) + 1);
                } else {
                    Player.getPlayer(playerName).setKingsCourtTrack(
                            track.indexOf(playerName) + 1);
                }
            }
        }
    }

    private List<String> getTrack(GameContext context, String highestBidder,
            int trackIndex) {
        Map<String, int[]> bids = context.getBiddingData().getBidsMap();
        List<String> playerOrderPreference = context.getBiddingData()
                .getPlayerOrderMap().get(highestBidder);
        List<String> track = new ArrayList<String>();
        track.add(highestBidder);
        for (Player player : context.getPlayers()) {
            if (!player.getName().equalsIgnoreCase(highestBidder)) {
                int i;
                for (i = 1; i < track.size(); i++) {
                    if (bids.get(player.getName())[trackIndex] > bids.get(track
                            .get(i))[trackIndex]) {
                        track.add(i, player.getName());
                        break;
                    } else if (bids.get(player.getName())[trackIndex] == bids
                            .get(track.get(i))[trackIndex]) {
                        if (playerOrderPreference.indexOf(player.getName()) < playerOrderPreference
                                .indexOf(track.get(i))) {
                            track.add(i, player.getName());
                            break;
                        }
                    }
                }
                if (i == track.size()) {
                    track.add(player.getName());
                }
            }
        }
        return track;
    }

    private String getHighestBidder(GameContext context, int track) {
        Map<String, int[]> bids = context.getBiddingData().getBidsMap();
        String highestBidder = null;
        int highestBid = 0;
        for (Player player : context.getPlayers()) {
            if (highestBidder == null
                    || bids.get(player.getName())[track] > highestBid) {
                highestBidder = player.getName();
                highestBid = bids.get(player.getName())[track];
            } else if (bids.get(player.getName())[track] == highestBid
                    && player.getIronThroneTrack() < Player.getPlayer(
                            highestBidder).getIronThroneTrack()) {
                highestBidder = player.getName();
            }
        }
        return highestBidder;
    }

    private void updateBidData(GameContext context) {
        TrackBiddingData data;
        if (context.getBiddingData() == null) {
            data = new TrackBiddingData();
            context.setBiddingData(data);
        } else {
            data = context.getBiddingData();
        }
        int[] bids = new int[3];
        if (StringHelper.isNotNullOrEmpty(txtIronThrone.getText())) {
            bids[0] = Integer.parseInt(txtIronThrone.getText());
        } else {
            bids[0] = 0;
        }
        if (StringHelper.isNotNullOrEmpty(txtFiefdoms.getText())) {
            bids[1] = Integer.parseInt(txtFiefdoms.getText());
        } else {
            bids[1] = 0;
        }
        if (StringHelper.isNotNullOrEmpty(txtKingsCourt.getText())) {
            bids[2] = Integer.parseInt(txtKingsCourt.getText());
        } else {
            bids[2] = 0;
        }
        data.getBidsMap().put(currentTurn.getPlayerName(), bids);
        ArrayList<String> playerOrder = new ArrayList<String>();
        for (Player player : tblPlayerData) {
            playerOrder.add(player.getName());
        }
        data.getPlayerOrderMap().put(currentTurn.getPlayerName(), playerOrder);
    }

    public EventHandler<KeyEvent> maxLength(final Integer i) {
        return new EventHandler<KeyEvent>() {

            @Override
            public void handle(KeyEvent arg0) {
                TextField tx = (TextField) arg0.getSource();
                if (tx.getText().length() >= i) {
                    arg0.consume();
                }
            }
        };
    }
}
