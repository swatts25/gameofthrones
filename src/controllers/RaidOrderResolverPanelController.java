package controllers;

import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;

import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.ComboBoxTableCell;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.Stage;
import javafx.util.Callback;
import javafx.util.StringConverter;
import client.Client;
import client.JavaFXLauncher;
import client.PlanningPhase;
import client.TurnHandler;
import client.WesterosPhase;
import client.model.GameContext;
import client.model.Order;
import client.model.Turn;
import client.model.zones.Zone;

@SuppressWarnings({ "rawtypes", "unchecked" })
public class RaidOrderResolverPanelController extends JavaFXController
        implements Initializable {

    private ObservableList<Zone> tblZoneData = FXCollections
            .observableArrayList();

    private ObservableList<Zone> cboZoneData = FXCollections
            .observableArrayList();

    private MainScreenPanelController mainscreen;

    private Turn currentTurn;

    public RaidOrderResolverPanelController(
            MainScreenPanelController mainScreenPanelController, Turn turn) {
        super("/forms/BaseRaidOrderResolverPanel.fxml");
        this.mainscreen = mainScreenPanelController;
        this.currentTurn = turn;
    }

    @FXML
    private TableView tblZones;

    @FXML
    private Button btnSubmit;

    @FXML
    private ComboBox cboZone;

    private TableColumn targetCol;

    protected Zone selectedTargetZone;

    @Override
    public void initialize(URL arg0, ResourceBundle arg1) {
        initTableView();
        initializeZoneComboBox();
    }

    private void initTableView() {
        tblZones.setEditable(true);
        TableColumn zoneCol = new TableColumn("Zone");
        zoneCol.setPrefWidth(150);
        zoneCol.setCellValueFactory(new PropertyValueFactory<Zone, String>(
                "name"));
        TableColumn raidOrderCol = new TableColumn("Raid Order");
        raidOrderCol.setPrefWidth(80);
        raidOrderCol
                .setCellValueFactory(new PropertyValueFactory<Order, String>(
                        "order"));
        targetCol = new TableColumn("Target");
        targetCol.setPrefWidth(277);
        tblZones.getColumns().addAll(zoneCol, raidOrderCol, targetCol);
    }

    private void updateZonesTable(Zone zone) {
        tblZoneData.clear();
        tblZoneData.add(zone);
        ObservableList<Zone> targetChoice = FXCollections
                .observableArrayList(new ArrayList<Zone>());
        List<String> adjacentZones = zone.getAdjacentZones();
        GameContext context = GameContext.getMyGameContext();
        for (String zoneName : adjacentZones) {
            Zone adjacentZone = context.getZones().get(zoneName);
            if (adjacentZone.getOrder() != null
                    && adjacentZone.getControlledBy() != null
                    && !adjacentZone.getControlledBy().isMe()
                    && (adjacentZone.getOrder().getType()
                            .equals(Order.CONSOLODATE_ORDER)
                            || adjacentZone.getOrder().getType()
                                    .equals(Order.DEFENSE_ORDER)
                            || adjacentZone.getOrder().getType()
                                    .equals(Order.RAID_ORDER) || adjacentZone
                            .getOrder().getType().equals(Order.SUPPORT_ORDER))) {
                targetChoice.add(adjacentZone);
            }
        }
        targetCol
                .setCellFactory(new Callback<TableColumn<Zone, String>, TableCell<Zone, String>>() {

                    @Override
                    public TableCell<Zone, String> call(
                            TableColumn<Zone, String> p) {
                        ComboBoxTableCell cell = new ComboBoxTableCell(
                                FXCollections.observableArrayList(targetChoice)) {

                            @Override
                            public void updateItem(Object item, boolean empty) {
                                super.updateItem(item, empty);
                                if (item != null && item instanceof Zone) {
                                    Zone zone = ((Zone) item);
                                    Order order = zone.getOrder();
                                    setText(zone.getControlledBy().getName()
                                            + " @ " + zone.getName() + " - "
                                            + order.getName());
                                    selectedTargetZone = zone;
                                }
                            }
                        };
                        cell.setConverter(new StringConverter() {

                            @Override
                            public Object fromString(String arg0) {
                                // TODO Auto-generated method stub
                                return null;
                            }

                            @Override
                            public String toString(Object arg0) {
                                if (arg0 != null) {
                                    if (arg0 instanceof Zone) {
                                        Zone zone = ((Zone) arg0);
                                        Order order = zone.getOrder();
                                        return zone.getControlledBy().getName()
                                                + " @ " + zone.getName()
                                                + " - " + order.getName();
                                    }
                                }
                                return null;
                            }
                        });
                        return cell;
                    }
                });
        tblZones.setItems(tblZoneData);
        tblZones.setDisable(false);
    }

    private void initializeZoneComboBox() {
        cboZone.setCellFactory(new Callback<ListView<Zone>, ListCell<Zone>>() {

            @Override
            public ListCell<Zone> call(ListView<Zone> param) {
                final ListCell<Zone> cell = new ListCell<Zone>() {

                    @Override
                    public void updateItem(Zone zone, boolean empty) {
                        super.updateItem(zone, empty);
                        if (zone != null) {
                            setText(zone.getName());
                        } else {
                            setText(null);
                        }
                    }
                };
                return cell;
            }
        });
        cboZone.valueProperty().addListener(new ChangeListener<Zone>() {

            @Override
            public void changed(ObservableValue<? extends Zone> arg0,
                    Zone oldSelection, Zone newSelection) {
                updateZonesTable(newSelection);
            }
        });
        updateZoneCombobox();
    }

    private void updateZoneCombobox() {
        List<Zone> zones = new ArrayList<Zone>(GameContext.getMyGameContext()
                .getZones().values());
        List<Zone> zonesWithRaidOrders = new ArrayList<Zone>();
        for (Zone zone : zones) {
            if (zone.getControlledBy() != null && zone.getControlledBy().isMe()
                    && zone.getOrder() != null
                    && zone.getOrder().getType().equals(Order.RAID_ORDER)) {
                zonesWithRaidOrders.add(zone);
            }
        }
        cboZoneData.addAll(zonesWithRaidOrders);
        cboZone.setItems(cboZoneData);
        if (zonesWithRaidOrders != null && !zonesWithRaidOrders.isEmpty()) {
            cboZone.getSelectionModel().select(zonesWithRaidOrders.get(0));
        }
    }

    public void submitClicked() {
        GameContext context = GameContext.getMyGameContext();
        if (!tblZoneData.isEmpty()) {
            if (selectedTargetZone != null) {
                Order orderToRemove = selectedTargetZone.getOrder();
                Order myRaidOrder = tblZoneData.get(0).getOrder();
                if (orderToRemove.getType().equals(Order.DEFENSE_ORDER)
                        && !myRaidOrder.isStar()) {
                    JavaFXLauncher
                            .showInformationDialog(
                                    "Raid Order Validation Error",
                                    "You can only remove a Defense order with a Raid(*) order");
                } else {
                    Zone targetZone = context.getZones().get(
                            selectedTargetZone.getName());
                    targetZone.setOrder(Order.NONE_ORDER);
                }
            }
            tblZoneData.get(0).setOrder(null);
        }
        if (mainscreen != null && currentTurn != null) {
            context.getTurns().remove(currentTurn);
            if (context.getTurns().isEmpty()) {
                PlanningPhase.resolveConsolodatePowerOrders(context);
                Client.getLogger().info("No march orders, skipping to westeros phase");
                WesterosPhase newPhase = new WesterosPhase(mainscreen);
                newPhase.drawWesterosCards();
            }
            TurnHandler.saveAndDistributeGameContext(currentTurn, context);
            TurnHandler.UpdateUI(mainscreen);
            Stage stage = (Stage) tblZones.getScene().getWindow();
            stage.close();
        }
    }
}