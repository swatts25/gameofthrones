package controllers;

import java.awt.Desktop;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;

import client.Client;
import client.JavaFXLauncher;
import client.TurnHandler;
import client.model.Footman;
import client.model.GameContext;
import client.model.House;
import client.model.Knight;
import client.model.Player;
import client.model.Ship;
import client.model.SiegeEngine;
import client.model.Turn;
import client.model.zones.Land;
import client.model.zones.Sea;
import client.model.zones.Zone;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.geometry.Insets;
import javafx.geometry.Point2D;
import javafx.geometry.Pos;
import javafx.geometry.Rectangle2D;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableColumn.CellDataFeatures;
import javafx.scene.control.TableView;
import javafx.scene.control.ToggleGroup;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.Priority;
import javafx.scene.layout.VBox;
import javafx.scene.media.Media;
import javafx.scene.media.MediaPlayer;
import javafx.stage.FileChooser;
import javafx.stage.FileChooser.ExtensionFilter;
import javafx.stage.Stage;
import javafx.util.Callback;
import utils.GameOfThronesMarshaller;
import utils.MapBuilder;
import utils.SavedGameData;
import utils.StringHelper;

@SuppressWarnings({ "unchecked", "rawtypes" })
public class MainScreenPanelController extends JavaFXController implements
        Initializable {

    public static final String VERSION = "1.1.4";

    public static final String LATEST_GAME_URL = "https://drive.google.com/folderview?id=0B7cqRAh9MBTLbVF6TmpUM0dwcU0&usp=sharing";

    public static final String ISSUE_TRACKING_URL = "https://bitbucket.org/swatts25/gameofthrones/issues/new";

    private static final double MIN_PIXELS = 200;

    final FileChooser fileChooser = new FileChooser();

    private ObservableList<Player> tblTrackData = FXCollections
            .observableArrayList();

    private ObservableList<Zone> tblZoneData = FXCollections
            .observableArrayList();

    private ObservableList<Player> cboPlayerData = FXCollections
            .observableArrayList();

    @FXML
    ImageView imvInfluenceToken;

    @FXML
    RadioButton rdbOn;

    @FXML
    RadioButton rdbOff;

    @FXML
    TableView tblZones;

    @FXML
    ComboBox cboPlayer;

    @FXML
    Button btnBeginTurn;

    @FXML
    Label lblStatus;

    @FXML
    Label lblInfluenceToken;

    @FXML
    Button btnResendTurn;

    @FXML
    Button btnViewWesterosCards;

    @FXML
    Button btnCombats;

    @FXML
    VBox vboxMap;

    private MediaPlayer mediaPlayer;

    private Button btnResetMap;

    public MainScreenPanelController() {
        super("/forms/BaseMainScreenPanel.fxml");
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        String myName = SavedGameData
                .getProperty(SavedGameData.MYNAME_PROPERTY);
        if (StringHelper.isNullOrEmpty(myName)) {
            JavaFXLauncher.showPromptForName(myName);
        } else {
            Player.setMyName(myName);
        }
        initializeZonesTable();
        initializePlayerComboBox();
        updateButtons();
        ToggleGroup group = new ToggleGroup();
        rdbOn.setToggleGroup(group);
        rdbOff.setToggleGroup(group);
    }

    private void initializeZonesTable() {
        tblZones.setDisable(true);
        TableColumn zoneCol = new TableColumn("Zone");
        zoneCol.setPrefWidth(145);
        zoneCol.setCellValueFactory(new PropertyValueFactory<Zone, String>(
                "name"));
        TableColumn footmenCol = new TableColumn("Footmen");
        footmenCol.setPrefWidth(65);
        footmenCol
                .setCellValueFactory(new Callback<CellDataFeatures<Zone, String>, ObservableValue<String>>() {

                    public ObservableValue<String> call(
                            CellDataFeatures<Zone, String> cell) {
                        SimpleStringProperty selectedContinent = new SimpleStringProperty();
                        if (cell.getValue() instanceof Land) {
                            Land land = (Land) cell.getValue();
                            List<Footman> myFootmen = new ArrayList<Footman>();
                            List<Footman> myRoutedFootmen = new ArrayList<Footman>();
                            for (Footman footman : land.getFootmen()) {
                                if (footman.getPlayerName().equalsIgnoreCase(
                                        land.getControlledByName())) {
                                    if (!footman.isRouted()) {
                                        myFootmen.add(footman);
                                    } else {
                                        myRoutedFootmen.add(footman);
                                    }
                                }
                            }
                            selectedContinent.setValue(Integer
                                    .toString(myFootmen.size())
                                    + " / "
                                    + Integer.toString(myRoutedFootmen.size()
                                            + myFootmen.size()));
                        } else {
                            selectedContinent.setValue("-");
                        }
                        return selectedContinent;
                    }
                });
        TableColumn knightsCol = new TableColumn("Knights");
        knightsCol.setPrefWidth(65);
        knightsCol
                .setCellValueFactory(new Callback<CellDataFeatures<Zone, String>, ObservableValue<String>>() {

                    public ObservableValue<String> call(
                            CellDataFeatures<Zone, String> cell) {
                        SimpleStringProperty selectedContinent = new SimpleStringProperty();
                        if (cell.getValue() instanceof Land) {
                            Land land = (Land) cell.getValue();
                            List<Knight> myKnights = new ArrayList<Knight>();
                            List<Knight> myRoutedKnights = new ArrayList<Knight>();
                            for (Knight knight : land.getKnights()) {
                                if (knight.getPlayerName().equalsIgnoreCase(
                                        land.getControlledByName())) {
                                    if (!knight.isRouted()) {
                                        myKnights.add(knight);
                                    } else {
                                        myRoutedKnights.add(knight);
                                    }
                                }
                            }
                            selectedContinent.setValue(Integer
                                    .toString(myKnights.size())
                                    + " / "
                                    + Integer.toString(myRoutedKnights.size()
                                            + myKnights.size()));
                        } else {
                            selectedContinent.setValue("-");
                        }
                        return selectedContinent;
                    }
                });
        TableColumn siegeEnginesCol = new TableColumn("Siege Eng");
        siegeEnginesCol.setPrefWidth(65);
        siegeEnginesCol
                .setCellValueFactory(new Callback<CellDataFeatures<Zone, String>, ObservableValue<String>>() {

                    public ObservableValue<String> call(
                            CellDataFeatures<Zone, String> cell) {
                        SimpleStringProperty selectedContinent = new SimpleStringProperty();
                        if (cell.getValue() instanceof Land) {
                            Land land = (Land) cell.getValue();
                            List<SiegeEngine> mySiegeEngines = new ArrayList<SiegeEngine>();
                            for (SiegeEngine siegeEngine : land
                                    .getSiegeEngines()) {
                                if (siegeEngine.getPlayerName()
                                        .equalsIgnoreCase(
                                                land.getControlledByName())) {
                                    mySiegeEngines.add(siegeEngine);
                                }
                            }
                            selectedContinent.setValue(Integer
                                    .toString(mySiegeEngines.size()));
                        } else {
                            selectedContinent.setValue("-");
                        }
                        return selectedContinent;
                    }
                });
        TableColumn shipsCol = new TableColumn("Ships");
        shipsCol.setPrefWidth(65);
        shipsCol.setCellValueFactory(new Callback<CellDataFeatures<Zone, String>, ObservableValue<String>>() {

            public ObservableValue<String> call(
                    CellDataFeatures<Zone, String> cell) {
                SimpleStringProperty selectedContinent = new SimpleStringProperty();
                List<Ship> myShips = new ArrayList<Ship>();
                List<Ship> myRoutedShips = new ArrayList<Ship>();
                if (cell.getValue() instanceof Sea) {
                    Sea sea = (Sea) cell.getValue();
                    for (Ship ship : sea.getShips()) {
                        if (ship.getPlayerName().equalsIgnoreCase(
                                sea.getControlledByName())) {
                            if (!ship.isRouted()) {
                                myShips.add(ship);
                            } else {
                                myRoutedShips.add(ship);
                            }
                        }
                    }
                } else if (cell.getValue() instanceof Land) {
                    Land land = (Land) cell.getValue();
                    for (Ship ship : land.getShips()) {
                        if (ship.getPlayerName().equalsIgnoreCase(
                                land.getControlledByName())) {
                            if (!ship.isRouted()) {
                                myShips.add(ship);
                            } else {
                                myRoutedShips.add(ship);
                            }
                        }
                    }
                }
                selectedContinent.setValue(Integer.toString(myShips.size())
                        + " / "
                        + Integer.toString(myRoutedShips.size()
                                + myShips.size()));
                return selectedContinent;
            }
        });
        tblZones.getColumns().addAll(zoneCol, footmenCol, knightsCol,
                siegeEnginesCol, shipsCol);
    }

    @FXML
    protected void loadGameClicked() {
        Stage stage = new Stage();
        fileChooser.getExtensionFilters().addAll(
                new ExtensionFilter("XML Files", "*.xml"));
        File file = fileChooser.showOpenDialog(stage);
        if (file != null) {
            if (validateFile(file)) {
                GameContext newGameContext = GameOfThronesMarshaller
                        .unMarshallGameContext(file);
                loadGame(newGameContext);
            }
        }
    }

    private void viewMapClicked() {
        try {
            final Path destination = Paths
                    .get(SavedGameData.BASE_BOARD_FILENAME);
            InputStream is = getClass().getResourceAsStream(
                    SavedGameData.BASE_BOARD_LOCATION);
            File map = destination.toFile();
            if (!map.exists()) {
                Files.copy(is, destination);
            }
            if (GameContext.getMyGameContext() != null
                    && GameContext.getMyGameContext().getZones() != null) {
                MapBuilder boardmap = new MapBuilder(map);
                File image = buildMap(boardmap);
                Desktop.getDesktop().open(image);
            } else {
                Desktop.getDesktop().open(map);
            }
        } catch (IOException e) {
            Client.error(e);
        }
    }

    private File buildMap(MapBuilder boardmap) {
        GameContext context = GameContext.getMyGameContext();
        List<Zone> zones = new ArrayList<Zone>(GameContext.getMyGameContext()
                .getZones().values());
        for (Zone zone : zones) {
            if (zone.getControlledBy() != null) {
                if (zone.getControlledBy() != null) {
                    if (zone instanceof Sea
                            && ((Sea) zone).getShips().size() > 0) {
                        boardmap.addShipTokenToSea(zone.getControlledBy()
                                .getHouse(), zone, ((Sea) zone).getShips()
                                .size());
                    } else if (zone instanceof Land
                            && ((Land) zone).getShips().size() > 0) {
                        boardmap.addShipTokenToPort(zone.getControlledBy()
                                .getHouse(), zone, ((Land) zone).getShips()
                                .size());
                    } else {
                        boardmap.addInfluenceTokenToMap(zone.getControlledBy()
                                .getHouse(), zone);
                    }
                }
                if (context != null
                        && !context.getTurns().isEmpty()
                        && !context.getTurns().get(0).getType()
                                .equals(Turn.PLANNING_PHASE)) {
                    boardmap.addOrderTokenToMap(zone.getOrder(), zone);
                }
            }
        }
        List<Player> players = GameContext.getMyGameContext().getPlayers();
        if (players.size() >= 4) {
            boardmap.placeKingsCourt();
        }
        for (Player player : players) {
            boardmap.addPlayerTokenToIronThroneTrack(player.getHouse(),
                    player.getIronThroneTrack());
            boardmap.addPlayerTokenToFiefdomsTrack(player.getHouse(),
                    player.getFieldomsTrack());
            boardmap.addPlayerTokenToKingsCourtTrack(player.getHouse(),
                    player.getKingsCourtTrack());
            boardmap.addSupplyTokenToSupplyTrack(player.getHouse(),
                    player.getSupplyTrack());
            boardmap.addVictoryTokenToVictoryTrack(player.getHouse(),
                    player.getVictoryTrack());
        }
        return boardmap.generateImage();
    }

    @FXML
    protected void viewInstructionsClicked() {
        String inputPdf = SavedGameData.INSTRUCTIONS_LOCATION;
        try {
            Path tempOutput = Files.createTempFile("TempManual", ".pdf");
            tempOutput.toFile().deleteOnExit();
            InputStream is = getClass().getClassLoader().getResourceAsStream(
                    inputPdf);
            Files.copy(is, tempOutput, StandardCopyOption.REPLACE_EXISTING);
            Desktop.getDesktop().open(tempOutput.toFile());
        } catch (IOException e) {
            Client.error(e);
        }
    }

    public void loadGame(GameContext newGameContext) {
        GameContext.setMyGameContext(newGameContext);
        updateMyGUI();
        refreshMap(newGameContext);
        updatePlayerCombobox();
        updateInfluenceTokens();
        updateButtons();
        List<Turn> turns = GameContext.getMyGameContext().getTurns();
        if (!turns.isEmpty()) {
            Turn turn = turns.get(0);
            if (turn.getType().equals(Turn.RAVEN)
                    || turn.getType().equals(Turn.RESOLVE_RAID_ORDER)
                    || turn.getType().equals(Turn.RESOLVE_MARCH_ORDER)) {
            }
            Player turnPlayer = turn.getPlayer();
            if (turnPlayer.isMe()) {
                btnBeginTurn.setDisable(false);
                lblStatus.setText("Next Turn: Waiting for "
                        + newGameContext.getTurns().get(0).getPlayer()
                                .getName() + " to complete "
                        + newGameContext.getTurns().get(0).getType() + ".");
            } else if (turn.getType().equals(Turn.GAME_OVER)) {
                lblStatus.setText("Game has ended");
                btnBeginTurn.setText("New King");
            } else {
                lblStatus.setText("Next Turn: Waiting for "
                        + newGameContext.getTurns().get(0).getPlayer()
                                .getName() + " to complete "
                        + newGameContext.getTurns().get(0).getType()
                        + ". Load GameContext_"
                        + (newGameContext.getContextid() + 1));
            }
        }
        if (VERSION.compareTo(newGameContext.getGreatesVersion()) < 0) {
            int choice = JavaFXLauncher
                    .showYesNoDialog(
                            "New Version Available",
                            "New Release: "
                                    + newGameContext.getGreatesVersion(),
                            "Your game version ("
                                    + VERSION
                                    + ") is out of date, would you like to update now?");
            if (choice > 0) {
                try {
                    Desktop.getDesktop().browse(new URI(LATEST_GAME_URL));
                    System.exit(0);
                } catch (IOException e) {
                    Client.error(e);
                } catch (URISyntaxException e) {
                    Client.error(e);
                }
            }
        }
        playMusic();
    }

    public void updateButtons() {
        updateWesterosPhaseButton();
        updateCombatsButton();
        updateResendTurnButton();
    }

    private void playMusic() {
        final URL resource;
        switch (Player.getMyHouse()) {
            case House.BARATHEON:
                resource = getClass().getResource(
                        "/resources/audio/baratheon.mp3");
                break;
            case House.GREYJOY:
                resource = getClass().getResource(
                        "/resources/audio/greyjoy.mp3");
                break;
            case House.LANNISTER:
                resource = getClass().getResource(
                        "/resources/audio/lannister.mp3");
                break;
            case House.STARK:
                resource = getClass().getResource("/resources/audio/stark.mp3");
                break;
            case House.TYRELL:
                resource = getClass().getResource(
                        "/resources/audio/generic.mp3");
                break;
            case House.MARTELL:
                resource = getClass().getResource(
                        "/resources/audio/generic.mp3");
                break;
            default:
                return;
        }
        if (mediaPlayer != null) {
            mediaPlayer.stop();
        }
        final Media media = new Media(resource.toString());
        mediaPlayer = new MediaPlayer(media);
        String playMusic = SavedGameData
                .getProperty(SavedGameData.PLAYMUSIC_PROPERTY);
        if (playMusic != null && playMusic.equals("no")) {
            mediaPlayer.pause();;
            rdbOff.setSelected(true);
        } else {
            mediaPlayer.play();
            rdbOn.setSelected(true);
        }
    }

    @FXML
    public void beginTurnClicked() {
        handleTurn();
    }

    private void handleTurn() {
        List<Turn> turns = GameContext.getMyGameContext().getTurns();
        Turn turn = turns.get(0);
        Player turnPlayer = turn.getPlayer();
        if (turnPlayer.isMe()) {
            TurnHandler.takeTurn(turn, this);
        }
    }

    private void updatePlayerCombobox() {
        List<Player> players = GameContext.getMyGameContext().getPlayers();
        cboPlayerData.clear();
        cboPlayerData.addAll(players);
        cboPlayer.setItems(cboPlayerData);
        for (Player player : players) {
            if (player.isMe()) {
                // Updates zones table
                cboPlayer.getSelectionModel().select(player);
            }
        }
        cboPlayer.setDisable(false);
    }

    private void initializePlayerComboBox() {
        cboPlayer.setDisable(true);
        cboPlayer
                .setCellFactory(new Callback<ListView<Player>, ListCell<Player>>() {

                    @Override
                    public ListCell<Player> call(ListView<Player> param) {
                        final ListCell<Player> cell = new ListCell<Player>() {

                            @Override
                            public void updateItem(Player player, boolean empty) {
                                super.updateItem(player, empty);
                                if (player != null) {
                                    setText(player.getName() + " "
                                            + player.getHouse());
                                } else {
                                    setText(null);
                                }
                            }
                        };
                        return cell;
                    }
                });
        cboPlayer.valueProperty().addListener(new ChangeListener<Player>() {

            @Override
            public void changed(ObservableValue<? extends Player> arg0,
                    Player oldSelection, Player newSelection) {
                if (newSelection != null) {
                    updateZonesTable(newSelection.getName());
                }
            }
        });
    }

    public void updateZonesTable(String playerName) {
        List<Zone> zones = new ArrayList<Zone>(GameContext.getMyGameContext()
                .getZones().values());
        tblZoneData.clear();
        for (Zone zone : zones) {
            if (zone.getControlledBy() != null
                    && zone.getControlledBy().getName()
                            .equalsIgnoreCase(playerName)) {
                tblZoneData.add(zone);
            }
        }
        tblZones.setItems(tblZoneData);
        tblZones.setDisable(false);
    }

    public Label getStatusLabel() {
        return lblStatus;
    }

    public Button getBeginTurnButton() {
        return btnBeginTurn;
    }

    @FXML
    protected void viewOrdersClicked() {
        JavaFXLauncher.showPanel("/forms/BaseOrderViewerPanel.fxml",
                "View Orders", new OrderViewerPanelController());
    }

    @FXML
    protected void newGameClicked() {
        Parent root;
        try {
            FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource(
                    "/forms/BaseNewGamePanel.fxml"));
            fxmlLoader.setController(new NewGamePanelController(this));
            root = (Parent) fxmlLoader.load();
            Scene scene = new Scene(root);
            Stage stage = new Stage();
            stage.setTitle("New Game");
            stage.setScene(scene);
            stage.setResizable(false);
            stage.show();
            if (Player.getMyHouse() != null) {
                scene.getStylesheets().add(House.getStyle(Player.getMyHouse()));
            }
        } catch (IOException e) {
            Client.error(e);
        }
    }

    private boolean validateFile(File file) {
        return true;
    }

    public void updateMyGUI() {
        if (Player.getMyHouse() != null) {
            Image img = null;
            switch (Player.getMyHouse()) {
                case House.BARATHEON:
                    getScene().getStylesheets().add(
                            House.getStyle(House.BARATHEON));
                    img = new Image(getClass().getResourceAsStream(
                            "/resources/banners/baratheon.jpg"));
                    break;
                case House.STARK:
                    getScene().getStylesheets()
                            .add(House.getStyle(House.STARK));
                    img = new Image(getClass().getResourceAsStream(
                            "/resources/banners/stark.jpg"));
                    break;
                case House.LANNISTER:
                    getScene().getStylesheets().add(
                            House.getStyle(House.LANNISTER));
                    img = new Image(getClass().getResourceAsStream(
                            "/resources/banners/lannister.jpg"));
                    break;
                case House.GREYJOY:
                    getScene().getStylesheets().add(
                            House.getStyle(House.GREYJOY));
                    img = new Image(getClass().getResourceAsStream(
                            "/resources/banners/greyjoy.jpg"));
                    break;
                case House.TYRELL:
                    getScene().getStylesheets().add(
                            House.getStyle(House.TYRELL));
                    img = new Image(getClass().getResourceAsStream(
                            "/resources/banners/tyrell.jpg"));
                    break;
                case House.MARTELL:
                    getScene().getStylesheets().add(
                            House.getStyle(House.MARTELL));
                    img = new Image(getClass().getResourceAsStream(
                            "/resources/banners/martell.jpg"));
                    break;
                default:
                    break;
            }
            imvInfluenceToken.setImage(img);
        }
    }

    public void keyPressed(KeyEvent event) {
        if (event.isControlDown()) {
            String sendEmails;
            switch (event.getCode()) {
                case M:
                    viewMapClicked();
                    break;
                case L:
                    loadGameClicked();
                    break;
                case O:
                    loadGameClicked();
                    break;
                case H:
                    if (SavedGameData.getProperty(SavedGameData.DISABLE_EMAILS) == null) {
                        sendEmails = "Emails are enabled";
                    } else {
                        sendEmails = "Emails are disabled";
                    }
                    JavaFXLauncher
                            .showInformationDialog(
                                    "Help",
                                    "Version: "
                                            + VERSION
                                            + "\nYour Name: "
                                            + Player.getMyName()
                                            + "\nYour House: "
                                            + Player.getMyHouse()
                                            + "\n"
                                            + sendEmails
                                            + "\nAuthor: Sean Watts\n\n\u00a9 2016 Zggis Inc. All Right Reserved");
                    break;
                default:
                    break;
            }
            return;
        }
    }

    public void soundClicked() {
        if (rdbOn.isSelected()) {
            mediaPlayer.play();
            SavedGameData.saveProperty(SavedGameData.PLAYMUSIC_PROPERTY, "yes");
        } else {
            SavedGameData.saveProperty(SavedGameData.PLAYMUSIC_PROPERTY, "no");
            mediaPlayer.pause();
        }
    }

    public void resendTurnClicked() {
        btnResendTurn.setDisable(true);
        TurnHandler.resendGameContext(GameContext.getMyGameContext());
        btnResendTurn.setDisable(false);
    }

    public void updateInfluenceTokens() {
        lblInfluenceToken.setText("Influence Tokens: "
                + Player.getPlayer(Player.getMyName()).getInfluence());
    }

    public void updateWesterosPhaseButton() {
        GameContext context = GameContext.getMyGameContext();
        if (context != null && context.getCurrentAgeOneCard() != null
                && context.getCurrentAgeTwoCard() != null
                && context.getCurrentAgeThreeCard() != null) {
            btnViewWesterosCards.setDisable(false);
        } else {
            btnViewWesterosCards.setDisable(true);
        }
    }

    public void updateCombatsButton() {
        GameContext context = GameContext.getMyGameContext();
        if (context != null && context.getCombatHistory() != null
                && !context.getCombatHistory().isEmpty()) {
            btnCombats.setDisable(false);
        } else {
            btnCombats.setDisable(true);
        }
    }

    public void updateResendTurnButton() {
        GameContext context = GameContext.getMyGameContext();
        if (context != null) {
            btnResendTurn.setDisable(false);
        } else {
            btnResendTurn.setDisable(true);
        }
    }

    public void viewWesterosCardsClicked() {
        GameContext context = GameContext.getMyGameContext();
        JavaFXLauncher.showPanel(
                "/forms/BaseWesterosCardsViewerPanel.fxml",
                "Westeros Cards this Phase",
                new WesterosCardsViewerPanelController(context
                        .getCurrentAgeOneCard(),
                        context.getCurrentAgeTwoCard(), context
                                .getCurrentAgeThreeCard()));
    }

    public void combatsClicked() {
        JavaFXLauncher.showPanel("/forms/BaseCombatLogChooserPanel.fxml",
                "Combats", new CombatLogChooserPanelController(GameContext
                        .getMyGameContext().getCombatHistory()));
    }

    public void refreshMap(GameContext context) {
        if (context != null) {
            vboxMap.getChildren().clear();
            MapBuilder mapBuilder = getMapBuilder();
            Image image = getImage(mapBuilder);
            double width = image.getWidth();
            double height = image.getHeight();
            ImageView imageView = new ImageView(image);
            imageView.setPreserveRatio(true);
            reset(imageView, width / 2, height / 2);
            ObjectProperty<Point2D> mouseDown = new SimpleObjectProperty<>();
            imageView.setOnMousePressed(e -> {
                Point2D mousePress = imageViewToImage(imageView,
                        new Point2D(e.getX(), e.getY()));
                mouseDown.set(mousePress);
            });
            imageView.setOnMouseDragged(e -> {
                Point2D dragPoint = imageViewToImage(imageView,
                        new Point2D(e.getX(), e.getY()));
                shift(imageView, dragPoint.subtract(mouseDown.get()));
                mouseDown.set(imageViewToImage(imageView, new Point2D(e.getX(),
                        e.getY())));
            });
            imageView.setOnScroll(e -> {
                double delta = e.getDeltaY();
                Rectangle2D viewport = imageView.getViewport();
                double scale = clamp(Math.pow(1.01, delta),
                // don't scale so we're zoomed in to fewer than
                // MIN_PIXELS
                // in any direction:
                        Math.min(MIN_PIXELS / viewport.getWidth(), MIN_PIXELS
                                / viewport.getHeight()),
                        // don't scale so that we're bigger than image
                        // dimensions:
                        Math.max(width / viewport.getWidth(),
                                height / viewport.getHeight()));
                Point2D mouse = imageViewToImage(imageView,
                        new Point2D(e.getX(), e.getY()));
                double newWidth = viewport.getWidth() * scale;
                double newHeight = viewport.getHeight() * scale;
                double newMinX = clamp(
                        mouse.getX() - (mouse.getX() - viewport.getMinX())
                                * scale, 0, width - newWidth);
                double newMinY = clamp(
                        mouse.getY() - (mouse.getY() - viewport.getMinY())
                                * scale, 0, height - newHeight);
                imageView.setViewport(new Rectangle2D(newMinX, newMinY,
                        newWidth, newHeight));
            });
            imageView.setOnMouseClicked(e -> {
                if (e.getClickCount() == 2) {
                    reset(imageView, width, height);
                }
            });
            HBox buttons = createButtons(width, height, imageView);
            Pane container = new Pane(imageView);
            container.setPrefSize(800, 600);
            imageView.fitWidthProperty().bind(container.widthProperty());
            imageView.fitHeightProperty().bind(container.heightProperty());
            vboxMap.getChildren().add(container);
            vboxMap.getChildren().add(buttons);
            vboxMap.setFillWidth(true);
            VBox.setVgrow(container, Priority.ALWAYS);
        }
    }

    private Image getImage(MapBuilder mapBuilder) {
        InputStream is;
        try {
            is = new FileInputStream(buildMap(mapBuilder));
            return new Image(is);
        } catch (FileNotFoundException e1) {
            e1.printStackTrace();
        }
        return null;
    }

    private MapBuilder getMapBuilder() {
        final Path destination = Paths.get(SavedGameData.BASE_BOARD_FILENAME);
        InputStream is = getClass().getResourceAsStream(
                SavedGameData.BASE_BOARD_LOCATION);
        File map = destination.toFile();
        if (!map.exists()) {
            try {
                Files.copy(is, destination);
            } catch (IOException e1) {
                e1.printStackTrace();
            }
        }
        MapBuilder mb = new MapBuilder(map);
        return mb;
    }

    private HBox createButtons(double width, double height, ImageView imageView) {
        btnResetMap = new Button("Home Territory");
        btnResetMap.setOnAction(e -> reset(imageView, width / 2, height / 2));
        btnResetMap.setDisable(true);
        HBox buttons = new HBox(10, btnResetMap);
        buttons.setAlignment(Pos.CENTER);
        buttons.setPadding(new Insets(10));
        return buttons;
    }

    private void reset(ImageView imageView, double width, double height) {
        if (btnResetMap != null) {
            btnResetMap.setDisable(true);
        }
        switch (Player.getMyHouse()) {
            case House.BARATHEON:
                imageView
                        .setViewport(new Rectangle2D(704, 1076, width, height));
                break;
            case House.GREYJOY:
                imageView.setViewport(new Rectangle2D(0, 669, width, height));
                break;
            case House.LANNISTER:
                imageView.setViewport(new Rectangle2D(0, 901, width, height));
                break;
            case House.MARTELL:
                imageView
                        .setViewport(new Rectangle2D(634, 1632, width, height));
                break;
            case House.STARK:
                imageView.setViewport(new Rectangle2D(251, 0, width, height));
                break;
            case House.TYRELL:
                imageView.setViewport(new Rectangle2D(0, 1631, width, height));
                break;
            default:
                break;
        }
    }

    // shift the viewport of the imageView by the specified delta, clamping so
    // the viewport does not move off the actual image:
    private void shift(ImageView imageView, Point2D delta) {
        btnResetMap.setDisable(false);
        Rectangle2D viewport = imageView.getViewport();
        double width = imageView.getImage().getWidth();
        double height = imageView.getImage().getHeight();
        double maxX = width - viewport.getWidth();
        double maxY = height - viewport.getHeight();
        double minX = clamp(viewport.getMinX() - delta.getX(), 0, maxX);
        double minY = clamp(viewport.getMinY() - delta.getY(), 0, maxY);
        imageView.setViewport(new Rectangle2D(minX, minY, viewport.getWidth(),
                viewport.getHeight()));
    }

    private double clamp(double value, double min, double max) {
        if (value < min) return min;
        if (value > max) return max;
        return value;
    }

    // convert mouse coordinates in the imageView to coordinates in the actual
    // image:
    private Point2D imageViewToImage(ImageView imageView,
            Point2D imageViewCoordinates) {
        double xProportion = imageViewCoordinates.getX()
                / imageView.getBoundsInLocal().getWidth();
        double yProportion = imageViewCoordinates.getY()
                / imageView.getBoundsInLocal().getHeight();
        Rectangle2D viewport = imageView.getViewport();
        return new Point2D(viewport.getMinX() + xProportion
                * viewport.getWidth(), viewport.getMinY() + yProportion
                * viewport.getHeight());
    }
}
