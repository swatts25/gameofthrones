package utils;

import java.util.List;

import client.model.Player;

public class StringHelper {
	public static boolean isNullOrEmpty(String s) {
		if (s == null || s.isEmpty()) {
			return true;
		} else {
			return false;
		}
	}

	public static boolean isNotNullOrEmpty(String s) {
		return !isNullOrEmpty(s);
	}

	public static String concatinateEmailAddresses(List<Player> players) {
		String result = "";
		for (Player player : players) {
			result += player.getEmailAddress() + ",";
		}
		return result;
	}

    public static String getNameList(List<Player> newPlayers) {
        String result = "\n";
        for(Player player : newPlayers){
            result+=player.getName() + "\n";
        }
        result += "\n";
        return result;
    }
}
