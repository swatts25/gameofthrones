package utils;

import client.model.zones.Land;
import client.model.zones.Zone;

/**
 * @author swatts
 */
public class ShipTokenPortCoordinates {

    public static final int[] DRAGONSTONE_PORT = { 1372, 1901 };

    public static final int[] LANNISPORT_PORT = { 266, 1716 };

    public static final int[] OLDTOWN_PORT = { 179, 2379 };

    public static final int[] PYKE_PORT = { 344, 1325 };

    public static final int[] STORMS_END_PORT = { 1155, 2252 };

    public static final int[] SUNSPEAR_PORT = { 1327, 2628 };

    public static final int[] WHITE_HARBOR_PORT = { 896, 1046 };

    public static final int[] WINTERFELL_PORT = { 394, 481 };

    public static int[] getPortCoordinates(Zone zone) {
        switch (zone.getName()) {
            case Land.WINTERFELL:
                return WINTERFELL_PORT;
            case Land.PYKE:
                return PYKE_PORT;
            case Land.LANNISPORT:
                return LANNISPORT_PORT;
            case Land.OLDTOWN:
                return OLDTOWN_PORT;
            case Land.SUNSPEAR:
                return SUNSPEAR_PORT;
            case Land.STORMS_END:
                return STORMS_END_PORT;
            case Land.DRAGONSTONE:
                return DRAGONSTONE_PORT;
            case Land.WHITE_HARBOR:
                return WHITE_HARBOR_PORT;
            default:
                return null;
        }
    }
}