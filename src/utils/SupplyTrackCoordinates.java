package utils;

import client.model.House;

/**
 * @author Will
 */
public class SupplyTrackCoordinates {

    static final int[][] SUPPLY_ZERO = { { 1619, 1827 }, { 1654, 1827 },
            { 1689, 1827 }, { 1619, 1873 }, { 1654, 1873 }, { 1689, 1873 } };

    static final int[][] SUPPLY_ONE = { { 1619, 1731 }, { 1654, 1731 },
            { 1689, 1731 }, { 1619, 1777 }, { 1654, 1777 }, { 1689, 1777 } };

    static final int[][] SUPPLY_TWO = { { 1619, 1633 }, { 1654, 1633 },
            { 1689, 1633 }, { 1619, 1678 }, { 1654, 1678 }, { 1689, 1678 } };

    static final int[][] SUPPLY_THREE = { { 1619, 1538 }, { 1654, 1538 },
            { 1689, 1538 }, { 1619, 1582 }, { 1654, 1582 }, { 1689, 1582 } };

    static final int[][] SUPPLY_FOUR = { { 1619, 1442 }, { 1654, 1442 },
            { 1689, 1442 }, { 1619, 1487 }, { 1654, 1487 }, { 1689, 1487 } };

    static final int[][] SUPPLY_FIVE = { { 1619, 1348 }, { 1654, 1348 },
            { 1689, 1348 }, { 1619, 1392 }, { 1654, 1392 }, { 1689, 1392 } };

    static final int[][] SUPPLY_SIX = { { 1619, 1252 }, { 1654, 1252 },
            { 1689, 1252 }, { 1619, 1298 }, { 1654, 1298 }, { 1689, 1298 } };

    // Add additional constants for each supply track, for each track there
    // should be potentially 6 spots one for each house
    // Supply_X = { {p0X,p0Y}, {p1X,p1Y} ... {p5X,p5Y}}
    // Allocate spots middle out in this fashion [p5][p3][p1][p0][p2][p4], these
    // spots will be filled in order 0 thru 6 for cleanest look
    public static int[] getSupplyCoordinates(int supplyTrack, int position) {
        switch (supplyTrack) {
            case 0:
                return SUPPLY_ZERO[position];
            case 1:
                return SUPPLY_ONE[position];
            case 2:
                return SUPPLY_TWO[position];
            case 3:
                return SUPPLY_THREE[position];
            case 4:
                return SUPPLY_FOUR[position];
            case 5:
                return SUPPLY_FIVE[position];
            case 6:
                return SUPPLY_SIX[position];
            default:
                return null;
        }
    }

    public static String getSupplyTokenResource(String house) {
        String resource = null;
        switch (house) {
            case House.BARATHEON:
                resource = "/resources/supplytokens/baratheon.png";
                break;
            case House.GREYJOY:
                resource = "/resources/supplytokens/greyjoy.png";
                break;
            case House.LANNISTER:
                resource = "/resources/supplytokens/lannister.png";
                break;
            case House.MARTELL:
                resource = "/resources/supplytokens/martell.png";
                break;
            case House.STARK:
                resource = "/resources/supplytokens/stark.png";
                break;
            case House.TYRELL:
                resource = "/resources/supplytokens/tyrell.png";
                break;
            default:
                break;
        }
        return resource;
    }
}
