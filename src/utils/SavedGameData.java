package utils;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Properties;

import client.Client;

public class SavedGameData {

    public static final String MYNAME_PROPERTY = "myname";

    public static final String PLAYMUSIC_PROPERTY = "playmusic";

    public static final String GAME_ID_PROPERTY = "gameid";
    
    public static final String DISABLE_EMAILS = "disableemails";

    public static final String PROPERTIES_FILENAME = "config.properties";

    public static final String BASE_BOARD_FILENAME = "base_board.jpg";

    public static final String INSTRUCTIONS_FILENAME = "manual.pdf";

    public static final String BOARD_FILENAME = "board.jpg";

    public static final String BASE_BOARD_LOCATION = "/resources/base_board.jpg";

    public static final String INSTRUCTIONS_LOCATION = "resources/manual.pdf";

    public static void saveProperty(String property, String value) {
        Properties prop = new Properties();
        OutputStream output = null;
        FileInputStream in;
        try {
            in = new FileInputStream(PROPERTIES_FILENAME);
            prop.load(in);
            in.close();
        } catch (IOException e1) {
            Client.getLogger().info("Creating new properties file");
        }
        try {
            output = new FileOutputStream(PROPERTIES_FILENAME);
            prop.setProperty(property, value);
            Client.getLogger().info("Property set, key=" + property);
            prop.store(output, null);
        } catch (IOException io) {
            Client.error(io);
        } finally {
            if (output != null) {
                try {
                    output.close();
                } catch (IOException e) {
                    Client.error(e);
                }
            }
        }
    }

    public static String getProperty(String property) {
        String userName = null;
        Properties prop = new Properties();
        InputStream input = null;
        try {
            input = new FileInputStream(PROPERTIES_FILENAME);
            prop.load(input);
            userName = prop.getProperty(property);
        } catch (IOException ex) {
            Client.getLogger().info(
                    "Properties file does not exist, key=" + property);
        } finally {
            if (input != null) {
                try {
                    input.close();
                } catch (IOException e) {
                    Client.error(e);
                }
            }
        }
        return userName;
    }
}
