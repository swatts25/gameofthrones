package utils;

import client.Client;
import client.model.Order;
import client.model.zones.Land;
import client.model.zones.Sea;
import client.model.zones.Zone;

/**
 * @author swatts
 */
public class OrderTokenCoordinates {

    public static final int[] BLACKWATER = { 499, 1967 };

    public static final int[] CASTLE_BLACK = { 759, 237 };

    public static final int[] CRACKLAW_POINT = { 881, 1669 };

    public static final int[] DORNISH_MARCHES = { 441, 2393 };

    public static final int[] DRAGONSTONE = { 1421, 1730 };

    public static final int[] FLINTS_FINGER = { 207, 1144 };

    public static final int[] GREYWATER_WATCH = { 538, 1063 };

    public static final int[] HARRENHAL = { 686, 1647 };

    public static final int[] HIGHGARDEN = { 400, 2162 };

    public static final int[] KARHOLD = { 1108, 577 };

    public static final int[] KINGS_LANDING = { 999, 1878 };

    public static final int[] KINGSWOOD = { 866, 2206 };

    public static final int[] LANNISPORT = { 353, 1677 };

    public static final int[] MOAT_CALIN = { 621, 1099 };

    public static final int[] OLDTOWN = { 288, 2357 };

    public static final int[] PRINCES_PASS = { 632, 2388 };

    public static final int[] PYKE = { 220, 1349 };

    public static final int[] RIVERRUN = { 583, 1544 };

    public static final int[] SALT_SHORE = { 1123, 2693 };

    public static final int[] SEAGARD = { 580, 1377 };

    public static final int[] SEAROADMARCHES = { 272, 2060 };

    public static final int[] STARFALL = { 716, 2731 };

    public static final int[] STONEY_SEPT = { 619, 1702 };

    public static final int[] STORMS_END = { 1143, 2354 };

    public static final int[] SUNSPEAR = { 1114, 2614 };

    public static final int[] THE_ARBOR = { 67, 2793 };

    public static final int[] THE_BONEWAY = { 889, 2288 };

    public static final int[] THE_EYRIE = { 1178, 1518 };

    public static final int[] THE_FINGERS = { 1080, 1260 };

    public static final int[] THE_MOUNTAINS_OF_THE_MOON = { 1005, 1426 };

    public static final int[] THE_REACH = { 750, 2241 };

    public static final int[] THE_STONY_SHORE = { 262, 924 };

    public static final int[] THE_TWINS = { 783, 1308 };

    public static final int[] THREE_TOWERS = { 321, 2654 };

    public static final int[] WHITE_HARBOR = { 945, 692 };

    public static final int[] WIDOWS_WATCH = { 1031, 794 };

    public static final int[] WINTERFELL = { 732, 548 };

    public static final int[] YRONWOOD = { 660, 2615 };

    // Sea zone
    public static final int[] BAY_OF_ICE = { 186, 364 };

    public static final int[] BLACKWATER_BAY = { 1068, 1914 };

    public static final int[] EAST_SUMMER_SEA = { 903, 2852 };

    public static final int[] IRONMANS_BAY = { 109, 1269 };

    public static final int[] REDWYNE_STRAIGHTS = { 225, 2308 };

    public static final int[] SEA_OF_DORNE = { 898, 2467 };

    public static final int[] SHIPBREAKER_BAY = { 1257, 1950 };

    public static final int[] SUNSET_SEA = { 19, 1192 };

    public static final int[] THE_GOLDEN_SOUND = { 166, 1677 };

    public static final int[] THE_NARROW_SEA = { 1200, 1049 };

    public static final int[] THE_SHIVERING_SEA = { 1384, 821 };

    public static final int[] WEST_SUMMER_SEA = { 544, 2892 };

    public static int[] getZoneCoordinates(Zone zone) {
        int[] boardCoordinates = null;
        switch (zone.getName()) {
            case Land.BLACKWATER:
                boardCoordinates = BLACKWATER;
                break;
            case Land.CASTLE_BLACK:
                boardCoordinates = CASTLE_BLACK;
                break;
            case Land.CRACKLAW_POINT:
                boardCoordinates = CRACKLAW_POINT;
                break;
            case Land.DORNISH_MARCHES:
                boardCoordinates = DORNISH_MARCHES;
                break;
            case Land.DRAGONSTONE:
                boardCoordinates = DRAGONSTONE;
                break;
            case Land.FLINTS_FINGER:
                boardCoordinates = FLINTS_FINGER;
                break;
            case Land.GREYWATER_WATCH:
                boardCoordinates = GREYWATER_WATCH;
                break;
            case Land.HARRENHAL:
                boardCoordinates = HARRENHAL;
                break;
            case Land.HIGHGARDEN:
                boardCoordinates = HIGHGARDEN;
                break;
            case Land.KARHOLD:
                boardCoordinates = KARHOLD;
                break;
            case Land.KINGS_LANDING:
                boardCoordinates = KINGS_LANDING;
                break;
            case Land.KINGSWOOD:
                boardCoordinates = KINGSWOOD;
                break;
            case Land.LANNISPORT:
                boardCoordinates = LANNISPORT;
                break;
            case Land.OLDTOWN:
                boardCoordinates = OLDTOWN;
                break;
            case Land.PRINCES_PASS:
                boardCoordinates = PRINCES_PASS;
                break;
            case Land.PYKE:
                boardCoordinates = PYKE;
                break;
            case Land.RIVERRUN:
                boardCoordinates = RIVERRUN;
                break;
            case Land.SALT_SHORE:
                boardCoordinates = SALT_SHORE;
                break;
            case Land.SEAGARD:
                boardCoordinates = SEAGARD;
                break;
            case Land.SEAROADMARCHES:
                boardCoordinates = SEAROADMARCHES;
                break;
            case Land.STARFALL:
                boardCoordinates = STARFALL;
                break;
            case Land.STONEY_SEPT:
                boardCoordinates = STONEY_SEPT;
                break;
            case Land.STORMS_END:
                boardCoordinates = STORMS_END;
                break;
            case Land.SUNSPEAR:
                boardCoordinates = SUNSPEAR;
                break;
            case Land.THE_ARBOR:
                boardCoordinates = THE_ARBOR;
                break;
            case Land.THE_BONEWAY:
                boardCoordinates = THE_BONEWAY;
                break;
            case Land.THE_EYRIE:
                boardCoordinates = THE_EYRIE;
                break;
            case Land.THE_FINGERS:
                boardCoordinates = THE_FINGERS;
                break;
            case Land.THE_MOUNTAINS_OF_THE_MOON:
                boardCoordinates = THE_MOUNTAINS_OF_THE_MOON;
                break;
            case Land.THE_REACH:
                boardCoordinates = THE_REACH;
                break;
            case Land.THE_STONY_SHORE:
                boardCoordinates = THE_STONY_SHORE;
                break;
            case Land.THE_TWINS:
                boardCoordinates = THE_TWINS;
                break;
            case Land.THREE_TOWERS:
                boardCoordinates = THREE_TOWERS;
                break;
            case Land.WHITE_HARBOR:
                boardCoordinates = WHITE_HARBOR;
                break;
            case Land.WIDOWS_WATCH:
                boardCoordinates = WIDOWS_WATCH;
                break;
            case Land.WINTERFELL:
                boardCoordinates = WINTERFELL;
                break;
            case Land.YRONWOOD:
                boardCoordinates = YRONWOOD;
                break;
            case Sea.BAY_OF_ICE:
                boardCoordinates = BAY_OF_ICE;
                break;
            case Sea.BLACKWATER_BAY:
                boardCoordinates = BLACKWATER_BAY;
                break;
            case Sea.EAST_SUMMER_SEA:
                boardCoordinates = EAST_SUMMER_SEA;
                break;
            case Sea.IRONMANS_BAY:
                boardCoordinates = IRONMANS_BAY;
                break;
            case Sea.REDWYNE_STRAIGHTS:
                boardCoordinates = REDWYNE_STRAIGHTS;
                break;
            case Sea.SEA_OF_DORNE:
                boardCoordinates = SEA_OF_DORNE;
                break;
            case Sea.SHIPBREAKER_BAY:
                boardCoordinates = SHIPBREAKER_BAY;
                break;
            case Sea.SUNSET_SEA:
                boardCoordinates = SUNSET_SEA;
                break;
            case Sea.THE_GOLDEN_SOUND:
                boardCoordinates = THE_GOLDEN_SOUND;
                break;
            case Sea.THE_NARROW_SEA:
                boardCoordinates = THE_NARROW_SEA;
                break;
            case Sea.THE_SHIVERING_SEA:
                boardCoordinates = THE_SHIVERING_SEA;
                break;
            case Sea.WEST_SUMMER_SEA:
                boardCoordinates = WEST_SUMMER_SEA;
                break;
            case Land.MOAT_CALIN:
                boardCoordinates = MOAT_CALIN;
                break;
            default:
                Client.error("Can't find MapBuilder mapping for "
                        + zone.getName());
                return null;
        }
        return boardCoordinates;
    }

    public static String getOrderTokenResource(Order order) {
        String resource = null;
        switch (order.getType()) {
            case Order.CONSOLODATE_ORDER:
                if (order.isStar()) {
                    resource = "/resources/ordertokens/ConsolodateStar.png";
                } else {
                    resource = "/resources/ordertokens/Cosolodate.png";
                }
                break;
            case Order.DEFENSE_ORDER:
                if (order.isStar()) {
                    resource = "/resources/ordertokens/DefenseStar.png";
                } else {
                    resource = "/resources/ordertokens/Defense.png";
                }
                break;
            case Order.MARCH_ORDER:
                if (order.isStar()) {
                    resource = "/resources/ordertokens/MarchStar.png";
                } else if (order.getBonus() == 0) {
                    resource = "/resources/ordertokens/March.png";
                } else {
                    resource = "/resources/ordertokens/MarchMinusOne.png";
                }
                break;
            case Order.RAID_ORDER:
                if (order.isStar()) {
                    resource = "/resources/ordertokens/RaidStar.png";
                } else {
                    resource = "/resources/ordertokens/Raid.png";
                }
                break;
            case Order.SUPPORT_ORDER:
                if (order.isStar()) {
                    resource = "/resources/ordertokens/SupportStar.png";
                } else {
                    resource = "/resources/ordertokens/Support.png";
                }
                break;
            default:
                break;
        }
        return resource;
    }
}