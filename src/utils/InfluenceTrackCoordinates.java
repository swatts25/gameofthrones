package utils;

import client.model.House;

/**
 * @author Will
 */
public class InfluenceTrackCoordinates {

    static final int[] IRON_THRONE_ONE = { 1558, 864 };

    static final int[] IRON_THRONE_TWO = { 1558, 715 };

    static final int[] IRON_THRONE_THREE = { 1558, 564 };

    static final int[] IRON_THRONE_FOUR = { 1558, 414 };

    static final int[] IRON_THRONE_FIVE = { 1558, 265 };

    static final int[] IRON_THRONE_SIX = { 1558, 115 };

    static final int[] FIEFDOMS_ONE = { 1706, 864 };

    static final int[] FIEFDOMS_TWO = { 1705, 715 };

    static final int[] FIEFDOMS_THREE = { 1706, 564 };

    static final int[] FIEFDOMS_FOUR = { 1705, 414 };

    static final int[] FIEFDOMS_FIVE = { 1706, 265 };

    static final int[] FIEFDOMS_SIX = { 1706, 114 };

    static final int[] KINGS_COURT_ONE = { 1852, 865 };

    static final int[] KINGS_COURT_TWO = { 1852, 716 };

    static final int[] KINGS_COURT_THREE = { 1852, 564 };

    static final int[] KINGS_COURT_FOUR = { 1852, 415 };

    static final int[] KINGS_COURT_FIVE = { 1852, 265 };

    static final int[] KINGS_COURT_SIX = { 1852, 114 };

    public static int[] getIronThroneCoordinates(int position) {
        switch (position) {
            case 1:
                return IRON_THRONE_ONE;
            case 2:
                return IRON_THRONE_TWO;
            case 3:
                return IRON_THRONE_THREE;
            case 4:
                return IRON_THRONE_FOUR;
            case 5:
                return IRON_THRONE_FIVE;
            case 6:
                return IRON_THRONE_SIX;
            default:
                return null;
        }
    }

    public static int[] getFiefdomsCoordinates(int position) {
        switch (position) {
            case 1:
                return FIEFDOMS_ONE;
            case 2:
                return FIEFDOMS_TWO;
            case 3:
                return FIEFDOMS_THREE;
            case 4:
                return FIEFDOMS_FOUR;
            case 5:
                return FIEFDOMS_FIVE;
            case 6:
                return FIEFDOMS_SIX;
            default:
                return null;
        }
    }

    public static int[] getKingsCourtCoordinates(int position) {
        switch (position) {
            case 1:
                return KINGS_COURT_ONE;
            case 2:
                return KINGS_COURT_TWO;
            case 3:
                return KINGS_COURT_THREE;
            case 4:
                return KINGS_COURT_FOUR;
            case 5:
                return KINGS_COURT_FIVE;
            case 6:
                return KINGS_COURT_SIX;
            default:
                return null;
        }
    }

    public static String getHouseTokenResource(String house) {
        String resource = null;
        switch (house) {
            case House.BARATHEON:
                resource = "/resources/housetokens/baratheon.png";
                break;
            case House.GREYJOY:
                resource = "/resources/housetokens/greyjoy.png";
                break;
            case House.LANNISTER:
                resource = "/resources/housetokens/lannister.png";
                break;
            case House.MARTELL:
                resource = "/resources/housetokens/martell.png";
                break;
            case House.STARK:
                resource = "/resources/housetokens/stark.png";
                break;
            case House.TYRELL:
                resource = "/resources/housetokens/tyrell.png";
                break;
            default:
                break;
        }
        return resource;
    }
}
