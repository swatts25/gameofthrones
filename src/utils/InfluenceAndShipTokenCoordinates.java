package utils;

import java.util.logging.Level;

import client.Client;
import client.model.House;
import client.model.zones.Land;
import client.model.zones.Sea;
import client.model.zones.Zone;

/**
 * @author swatts
 */
public class InfluenceAndShipTokenCoordinates {

    public static final int[] BLACKWATER = { 744, 1889 };

    public static final int[] CASTLE_BLACK = { 710, 366 };

    public static final int[] CRACKLAW_POINT = { 1041, 1733 };

    public static final int[] DORNISH_MARCHES = { 682, 2302 };

    public static final int[] DRAGONSTONE = { 1293, 1764 };

    public static final int[] FLINTS_FINGER = { 363, 1095 };

    public static final int[] GREYWATER_WATCH = { 521, 1161 };

    public static final int[] HARRENHAL = { 804, 1745 };

    public static final int[] HIGHGARDEN = { 393, 2311 };

    public static final int[] KARHOLD = { 1205, 446 };

    public static final int[] KINGS_LANDING = { 1025, 1994 };

    public static final int[] KINGSWOOD = { 1131, 2003 };

    public static final int[] LANNISPORT = { 394, 1775 };

    public static final int[] MOAT_CALIN = { 639, 1210 };

    public static final int[] OLDTOWN = { 332, 2459 };

    public static final int[] PRINCES_PASS = { 551, 2477 };

    public static final int[] PYKE = { 237, 1416 };

    public static final int[] RIVERRUN = { 680, 1523 };

    public static final int[] SALT_SHORE = { 808, 2695 };

    public static final int[] SEAGARD = { 499, 1335 };

    public static final int[] SEAROADMARCHES = { 395, 2015 };

    public static final int[] STARFALL = { 626, 2687 };

    public static final int[] STONEY_SEPT = { 496, 1822 };

    public static final int[] STORMS_END = { 1014, 2239 };
    
    public static final int[] SUNSPEAR = { 1245, 2558 };
    
    public static final int[] THE_ARBOR = { 166, 2759 };

    public static final int[] THE_BONEWAY = { 790, 2360 };

    public static final int[] THE_EYRIE = { 1110, 1617 };

    public static final int[] THE_FINGERS = { 1010, 1318 };

    public static final int[] THE_MOUNTAINS_OF_THE_MOON = { 1110, 1394 };

    public static final int[] THE_REACH = { 580, 2151 };

    public static final int[] THE_STONY_SHORE = { 418, 854 };

    public static final int[] THE_TWINS = { 807, 1363 };

    public static final int[] THREE_TOWERS = { 423, 2597 };

    public static final int[] WHITE_HARBOR = { 846, 813 };
    
    public static final int[] WIDOWS_WATCH = { 1118, 872 };

    public static final int[] WINTERFELL = { 593, 584 };
    
    public static final int[] YRONWOOD = { 765, 2582 };

    // Sea Zones
    public static final int[] BAY_OF_ICE = { 118, 549 };

    public static final int[] BLACKWATER_BAY = { 1105, 1816 };

    public static final int[] EAST_SUMMER_SEA = { 1278, 2815 };

    public static final int[] IRONMANS_BAY = { 101, 1570 };

    public static final int[] REDWYNE_STRAIGHTS = { 100, 2553 };

    public static final int[] SEA_OF_DORNE = { 1118, 2479 };

    public static final int[] SHIPBREAKER_BAY = { 1371, 2179 };

    public static final int[] SUNSET_SEA = { 19, 1725 };

    public static final int[] THE_GOLDEN_SOUND = { 151, 1767 };

    public static final int[] THE_NARROW_SEA = { 1362, 1198 };

    public static final int[] THE_SHIVERING_SEA = { 1359, 615 };

    public static final int[] WEST_SUMMER_SEA = { 288, 2897 };

    public static int[] getZoneCoordinates(Zone zone) {
        int[] boardCoordinates = null;
        switch (zone.getName()) {
            case Land.BLACKWATER:
                boardCoordinates = BLACKWATER;
                break;
            case Land.CASTLE_BLACK:
                boardCoordinates = CASTLE_BLACK;
                break;
            case Land.CRACKLAW_POINT:
                boardCoordinates = CRACKLAW_POINT;
                break;
            case Land.DORNISH_MARCHES:
                boardCoordinates = DORNISH_MARCHES;
                break;
            case Land.DRAGONSTONE:
                if (zone.getControlledBy() == null
                        || !zone.getControlledBy().getHouse()
                                .equals(House.BARATHEON)) {
                    boardCoordinates = DRAGONSTONE;
                } else {
                    return null;
                }
                break;
            case Land.FLINTS_FINGER:
                boardCoordinates = FLINTS_FINGER;
                break;
            case Land.GREYWATER_WATCH:
                boardCoordinates = GREYWATER_WATCH;
                break;
            case Land.HARRENHAL:
                boardCoordinates = HARRENHAL;
                break;
            case Land.HIGHGARDEN:
                if (zone.getControlledBy() == null
                        || !zone.getControlledBy().getHouse()
                                .equals(House.TYRELL)) {
                    boardCoordinates = HIGHGARDEN;
                } else {
                    return null;
                }
                break;
            case Land.KARHOLD:
                boardCoordinates = KARHOLD;
                break;
            case Land.KINGS_LANDING:
                boardCoordinates = KINGS_LANDING;
                break;
            case Land.KINGSWOOD:
                boardCoordinates = KINGSWOOD;
                break;
            case Land.LANNISPORT:
                if (zone.getControlledBy() == null
                        || !zone.getControlledBy().getHouse()
                                .equals(House.LANNISTER)) {
                    boardCoordinates = LANNISPORT;
                } else {
                    return null;
                }
                break;
            case Land.OLDTOWN:
                boardCoordinates = OLDTOWN;
                break;
            case Land.PRINCES_PASS:
                boardCoordinates = PRINCES_PASS;
                break;
            case Land.PYKE:
                if (zone.getControlledBy() == null
                        || !zone.getControlledBy().getHouse()
                                .equals(House.GREYJOY)) {
                    boardCoordinates = PYKE;
                } else {
                    return null;
                }
                break;
            case Land.RIVERRUN:
                boardCoordinates = RIVERRUN;
                break;
            case Land.SALT_SHORE:
                boardCoordinates = SALT_SHORE;
                break;
            case Land.SEAGARD:
                boardCoordinates = SEAGARD;
                break;
            case Land.SEAROADMARCHES:
                boardCoordinates = SEAROADMARCHES;
                break;
            case Land.STARFALL:
                boardCoordinates = STARFALL;
                break;
            case Land.STONEY_SEPT:
                boardCoordinates = STONEY_SEPT;
                break;
            case Land.STORMS_END:
                boardCoordinates = STORMS_END;
                break;
            case Land.SUNSPEAR:
                if (zone.getControlledBy() == null
                        || !zone.getControlledBy().getHouse()
                                .equals(House.MARTELL)) {
                    boardCoordinates = SUNSPEAR;
                } else {
                    return null;
                }
                break;
            case Land.THE_ARBOR:
                boardCoordinates = THE_ARBOR;
                break;
            case Land.THE_BONEWAY:
                boardCoordinates = THE_BONEWAY;
                break;
            case Land.THE_EYRIE:
                boardCoordinates = THE_EYRIE;
                break;
            case Land.THE_FINGERS:
                boardCoordinates = THE_FINGERS;
                break;
            case Land.THE_MOUNTAINS_OF_THE_MOON:
                boardCoordinates = THE_MOUNTAINS_OF_THE_MOON;
                break;
            case Land.THE_REACH:
                boardCoordinates = THE_REACH;
                break;
            case Land.THE_STONY_SHORE:
                boardCoordinates = THE_STONY_SHORE;
                break;
            case Land.THE_TWINS:
                boardCoordinates = THE_TWINS;
                break;
            case Land.THREE_TOWERS:
                boardCoordinates = THREE_TOWERS;
                break;
            case Land.WHITE_HARBOR:
                boardCoordinates = WHITE_HARBOR;
                break;
            case Land.WIDOWS_WATCH:
                boardCoordinates = WIDOWS_WATCH;
                break;
            case Land.WINTERFELL:
                if (zone.getControlledBy() == null
                        || !zone.getControlledBy().getHouse()
                                .equals(House.STARK)) {
                    boardCoordinates = WINTERFELL;
                } else {
                    return null;
                }
                break;
            case Land.YRONWOOD:
                boardCoordinates = YRONWOOD;
                break;
            case Sea.BAY_OF_ICE:
                boardCoordinates = BAY_OF_ICE;
                break;
            case Sea.BLACKWATER_BAY:
                boardCoordinates = BLACKWATER_BAY;
                break;
            case Sea.EAST_SUMMER_SEA:
                boardCoordinates = EAST_SUMMER_SEA;
                break;
            case Sea.IRONMANS_BAY:
                boardCoordinates = IRONMANS_BAY;
                break;
            case Sea.REDWYNE_STRAIGHTS:
                boardCoordinates = REDWYNE_STRAIGHTS;
                break;
            case Sea.SEA_OF_DORNE:
                boardCoordinates = SEA_OF_DORNE;
                break;
            case Sea.SHIPBREAKER_BAY:
                boardCoordinates = SHIPBREAKER_BAY;
                break;
            case Sea.SUNSET_SEA:
                boardCoordinates = SUNSET_SEA;
                break;
            case Sea.THE_GOLDEN_SOUND:
                boardCoordinates = THE_GOLDEN_SOUND;
                break;
            case Sea.THE_NARROW_SEA:
                boardCoordinates = THE_NARROW_SEA;
                break;
            case Sea.THE_SHIVERING_SEA:
                boardCoordinates = THE_SHIVERING_SEA;
                break;
            case Sea.WEST_SUMMER_SEA:
                boardCoordinates = WEST_SUMMER_SEA;
                break;
            case Land.MOAT_CALIN:
                boardCoordinates = MOAT_CALIN;
                break;
            default:
                Client.error("Can't find MapBuilder mapping for "
                        + zone.getName());
                return null;
        }
        return boardCoordinates;
    }

    public static String getInfluenceTokenResource(String house) {
        String resource = null;
        switch (house) {
            case House.STARK:
                resource = "/resources/influencetokens/StarkInfluenceToken.png";
                break;
            case House.BARATHEON:
                resource = "/resources/influencetokens/BaratheonInfluenceToken.png";
                break;
            case House.GREYJOY:
                resource = "/resources/influencetokens/GreyjoyInfluenceToken.png";
                break;
            case House.LANNISTER:
                resource = "/resources/influencetokens/LannisterInfluenceToken.png";
                break;
            case House.MARTELL:
                resource = "/resources/influencetokens/MartellInfluenceToken.png";
                break;
            case House.TYRELL:
                resource = "/resources/influencetokens/TyrellInfluenceToken.png";
                break;
            default:
                break;
        }
        return resource;
    }

    public static String getShipTokenResource(String house, int quantity) {
        String resource = null;
        String folder = null;
        switch (quantity) {
            case 1:
                folder = "/resources/shiptokens/one/";
                break;
            case 2:
                folder = "/resources/shiptokens/two/";
                break;
            case 3:
                folder = "/resources/shiptokens/three/";
                break;
            case 4:
                folder = "/resources/shiptokens/four/";
                break;
            default:
                Client.getLogger().log(Level.WARNING,
                        "Invalid ship quantity: " + quantity);
                return null;
        }
        switch (house) {
            case House.STARK:
                resource = folder + "stark.png";
                break;
            case House.BARATHEON:
                resource = folder + "baratheon.png";
                break;
            case House.GREYJOY:
                resource = folder + "greyjoy.png";
                break;
            case House.LANNISTER:
                resource = folder + "lannister.png";
                break;
            case House.MARTELL:
                resource = folder + "martell.png";
                break;
            case House.TYRELL:
                resource = folder + "tyrell.png";
                break;
            default:
                Client.getLogger().log(Level.WARNING,
                        "Invalid ship house: " + house);
                return null;
        }
        return resource;
    }
}