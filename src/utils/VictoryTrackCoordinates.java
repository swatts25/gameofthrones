package utils;

import client.model.House;

public class VictoryTrackCoordinates {

    static final int[][] VICTORY_ONE = { { 1770, 2810 }, { 1825, 2810 },
            { 1880, 2810 }, { 1770, 2875 }, { 1825, 2875 }, { 1880, 2875 } };

    static final int[][] VICTORY_TWO = { { 1770, 2680 }, { 1825, 2680 },
            { 1880, 2680 }, { 1770, 2745 }, { 1825, 2745 }, { 1880, 2745 } };

    static final int[][] VICTORY_THREE = { { 1770, 2550 }, { 1825, 2550 },
            { 1880, 2550 }, { 1770, 2615 }, { 1825, 2615 }, { 1880, 2615 } };

    static final int[][] VICTORY_FOUR = { { 1770, 2420 }, { 1825, 2420 },
            { 1880, 2420 }, { 1770, 2485 }, { 1825, 2485 }, { 1880, 2485 } };

    static final int[][] VICTORY_FIVE = { { 1770, 2285 }, { 1825, 2285 },
            { 1880, 2285 }, { 1770, 2350 }, { 1825, 2350 }, { 1880, 2350 } };

    static final int[][] VICTORY_SIX = { { 1770, 2150 }, { 1825, 2150 },
            { 1880, 2150 }, { 1770, 2215 }, { 1825, 2215 }, { 1880, 2215 } };

    static final int[][] VICTORY_SEVEN = { { 1770, 2020 }, { 1825, 2020 },
            { 1880, 2020 }, { 1770, 2085 }, { 1825, 2085 }, { 1880, 2085 } };

    // Add additional constants for each victory track, for each track there
    // should be potentially 6 spots one for each house
    // Victory_X = { {p0X,p0Y}, {p1X,p1Y} ... {p5X,p5Y}}
    // Allocate spots in two rows like supply, or another configuration you
    // think is best
    public static int[] getVictoryTrackCoordinates(int victoryTrack,
            int position) {
        switch (victoryTrack) {
            case 1:
                return VICTORY_ONE[position];
            // implement additional cases
            case 2:
                return VICTORY_TWO[position];
            case 3:
                return VICTORY_THREE[position];
            case 4:
                return VICTORY_FOUR[position];
            case 5:
                return VICTORY_FIVE[position];
            case 6:
                return VICTORY_SIX[position];
            case 7:
                return VICTORY_SEVEN[position];
            default:
                return null;
        }
    }

    public static String getVictoryTokenResource(String house) {
        String resource = null;
        switch (house) {
            case House.BARATHEON:
                resource = "/resources/victorytokens/baratheon.png";
                break;
            case House.GREYJOY:
                resource = "/resources/victorytokens/greyjoy.png";
                break;
            case House.LANNISTER:
                resource = "/resources/victorytokens/lannister.png";
                break;
            case House.MARTELL:
                resource = "/resources/victorytokens/martell.png";
                break;
            case House.STARK:
                resource = "/resources/victorytokens/stark.png";
                break;
            case House.TYRELL:
                resource = "/resources/victorytokens/tyrell.png";
                break;
            default:
                break;
        }
        return resource;
    }
}
