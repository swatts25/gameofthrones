package utils;

import java.util.List;
import java.util.Properties;

import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.activation.FileDataSource;
import javax.mail.BodyPart;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import javax.swing.JOptionPane;

import client.Client;
import client.model.Player;

public class EmailMessage {

    private String recepients;

    private String subject;

    private String filename;

    private String file;

    private String body;

    public EmailMessage(String recepients, String subject, String body,
            String filename, String file) {
        this.recepients = recepients;
        this.subject = subject;
        this.filename = filename;
        this.file = file;
        this.body = body;
    }

    public EmailMessage(List<Player> recepients, String subject,
            String filename, String file, String body) {
        this.recepients = "";
        for (Player player : recepients) {
            this.recepients += player.getEmailAddress() + ",";
        }
        this.subject = subject;
        this.filename = filename;
        this.file = file;
        this.body = body;
    }

    public String getRecepients() {
        return recepients;
    }

    public String getSubject() {
        return subject;
    }

    public String getFilename() {
        return filename;
    }

    public String getFile() {
        return file;
    }

    public void send() {
        new Thread(new Runnable() {

            public void run() {
                Properties props = new Properties();
                props.put("mail.smtp.host", "smtp.gmail.com");
                props.put("mail.smtp.socketFactory.port", "465");
                props.put("mail.smtp.socketFactory.class",
                        "javax.net.ssl.SSLSocketFactory");
                props.put("mail.smtp.auth", "true");
                props.put("mail.smtp.port", "465");
                Session session = Session.getDefaultInstance(props,
                        new javax.mail.Authenticator() {

                            protected PasswordAuthentication getPasswordAuthentication() {
                                return new PasswordAuthentication(
                                        "gotonlineboardgame@gmail.com",
                                        "nedstarklives92");
                            }
                        });
                try {
                    // Create a default MimeMessage object.
                    Message message = new MimeMessage(session);
                    // Set From: header field of the header.
                    message.setFrom(new InternetAddress(
                            "gotonlineboardgame@gmail.com"));
                    // Set To: header field of the header.
                    message.setRecipients(Message.RecipientType.TO,
                            InternetAddress.parse(recepients));
                    // Set Subject: header field
                    message.setSubject(subject);
                    // Create the message part
                    BodyPart messageBodyPart = new MimeBodyPart();
                    // Now set the actual message
                    messageBodyPart.setText(body);
                    // Create a multipar message
                    Multipart multipart = new MimeMultipart();
                    // Set text message part
                    multipart.addBodyPart(messageBodyPart);
                    // Part two is attachment
                    if (StringHelper.isNotNullOrEmpty(file)
                            && StringHelper.isNotNullOrEmpty(filename)) {
                        messageBodyPart = new MimeBodyPart();
                        DataSource source = new FileDataSource(file);
                        messageBodyPart.setDataHandler(new DataHandler(source));
                        messageBodyPart.setFileName(filename);
                        multipart.addBodyPart(messageBodyPart);
                    }
                    // Send the complete message parts
                    message.setContent(multipart);
                    Transport.send(message);
                } catch (MessagingException e) {
                    Client.error(e);
                    JOptionPane
                            .showMessageDialog(
                                    null,
                                    "An error occured trying to send the email.\nPlease make sure you are connected to the internet, and select 'Resend Turn'",
                                    "Send Error", JOptionPane.ERROR_MESSAGE);
                }
                Client.getLogger().info(
                        "Email sent to: " + recepients.toString()
                                + " File attached: " + filename);
            }
        }).start();
    }
}
