package utils;

import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.logging.Level;

import javax.imageio.ImageIO;

import client.Client;
import client.model.Order;
import client.model.zones.Zone;

/**
 * @author swatts
 */
public class MapBuilder {

    private static final int NUM_MAX_PLAYERS = 6;

    private static final int NUM_SUPPLY_TRACKS = 7;

    private static final int NUM_VICTORY_TRACKS = 7;

    private BufferedImage boardImg;

    private BufferedImage newImage;

    private Graphics2D graphic;

    private int[][] occupiedSupplyPositions;

    private int[][] occupiedVictoryPositions;

    public MapBuilder(File baseBoardFile) {
        occupiedSupplyPositions = new int[NUM_SUPPLY_TRACKS][NUM_MAX_PLAYERS];
        occupiedVictoryPositions = new int[NUM_VICTORY_TRACKS][NUM_MAX_PLAYERS];
        File map = null;
        try {
            final Path destination = Paths.get(SavedGameData.BOARD_FILENAME);
            FileInputStream is = new FileInputStream(baseBoardFile);
            map = destination.toFile();
            if (!map.exists()) {
                Files.copy(is, destination);
            }
        } catch (IOException e) {
            Client.error(e);
        }
        try {
            boardImg = ImageIO.read(map);
            newImage = new BufferedImage(boardImg.getWidth(),
                    boardImg.getHeight(), BufferedImage.TYPE_INT_RGB);
            graphic = newImage.createGraphics();
            graphic.drawImage(boardImg, 0, 0, null);
        } catch (IOException e) {
            Client.error(e);
        }
    }

    private void placeInfluenceToken(String house, int[] boardCoordinates) {
        BufferedImage img;
        try {
            img = ImageIO.read(getClass()
                    .getResourceAsStream(InfluenceAndShipTokenCoordinates
                            .getInfluenceTokenResource(house)));
            graphic.drawImage(img, boardCoordinates[0], boardCoordinates[1], 48,
                    70, null);
        } catch (IOException e) {
            Client.getLogger().log(Level.SEVERE,
                    "Can't find influence token resource", e);
        }
    }

    private void placeShipToken(String house, int[] boardCoordinates,
            int quantity) {
        BufferedImage img;
        try {
            img = ImageIO.read(getClass()
                    .getResourceAsStream(InfluenceAndShipTokenCoordinates
                            .getShipTokenResource(house, quantity)));
            graphic.drawImage(img, boardCoordinates[0], boardCoordinates[1], 35,
                    82, null);
        } catch (IOException e) {
            Client.getLogger().log(Level.SEVERE,
                    "Can't find ship token resource", e);
        }
    }

    private void placeHouseToken(String house, int[] boardCoordinates) {
        BufferedImage img;
        try {
            img = ImageIO.read(getClass().getResourceAsStream(
                    InfluenceTrackCoordinates.getHouseTokenResource(house)));
            graphic.drawImage(img, boardCoordinates[0], boardCoordinates[1], 50,
                    50, null);
        } catch (IOException e) {
            Client.getLogger().log(Level.SEVERE,
                    "Can't find house token resource", e);
        }
    }

    public void placeKingsCourt() {
        BufferedImage img;
        try {
            img = ImageIO.read(getClass().getResourceAsStream(
                    "/resources/maptokens/kingsCourt.png"));
            graphic.drawImage(img, 1816, 365, 125, 604, null);
        } catch (IOException e) {
            Client.getLogger().log(Level.SEVERE,
                    "Can't find house token resource", e);
        }
    }

    private void placeOrderToken(Order order, int[] boardCoordinates) {
        BufferedImage img;
        try {
            img = ImageIO.read(getClass().getResourceAsStream(
                    OrderTokenCoordinates.getOrderTokenResource(order)));
            graphic.drawImage(img, boardCoordinates[0], boardCoordinates[1], 50,
                    50, null);
        } catch (IOException e) {
            Client.getLogger().log(Level.SEVERE,
                    "Can't find order token resource", e);
        }
    }

    private void placeSupplyToken(String house, int[] boardCoordinates) {
        BufferedImage img;
        try {
            img = ImageIO.read(getClass().getResourceAsStream(
                    SupplyTrackCoordinates.getSupplyTokenResource(house)));
            graphic.drawImage(img, boardCoordinates[0], boardCoordinates[1], 35,
                    46, null);
        } catch (IOException e) {
            Client.getLogger().log(Level.SEVERE,
                    "Can't find supply token resource", e);
        }
    }

    private void placeVictoryToken(String house, int[] boardCoordinates) {
        BufferedImage img;
        try {
            img = ImageIO.read(getClass().getResourceAsStream(
                    VictoryTrackCoordinates.getVictoryTokenResource(house)));
            graphic.drawImage(img, boardCoordinates[0], boardCoordinates[1], 50,
                    46, null);
        } catch (IOException e) {
            Client.getLogger().log(Level.SEVERE,
                    "Can't find victory token resource", e);
        }
    }

    public void addInfluenceTokenToMap(String house, Zone zone) {
        int[] boardCoordinates = InfluenceAndShipTokenCoordinates
                .getZoneCoordinates(zone);
        if (boardCoordinates != null) {
            placeInfluenceToken(house, boardCoordinates);
        }
    }

    public void addShipTokenToSea(String house, Zone zone, int quantity) {
        int[] boardCoordinates = InfluenceAndShipTokenCoordinates
                .getZoneCoordinates(zone);
        if (boardCoordinates != null) {
            placeShipToken(house, boardCoordinates, quantity);
        }
    }

    public void addShipTokenToPort(String house, Zone zone, int quantity) {
        int[] boardCoordinates = ShipTokenPortCoordinates
                .getPortCoordinates(zone);
        if (boardCoordinates != null) {
            placeShipToken(house, boardCoordinates, quantity);
        }
    }

    public void addPlayerTokenToIronThroneTrack(String house, int position) {
        int[] boardCoordinates = InfluenceTrackCoordinates
                .getIronThroneCoordinates(position);
        if (boardCoordinates != null) {
            placeHouseToken(house, boardCoordinates);
        }
    }

    public void addSupplyTokenToSupplyTrack(String house, int supplyTrack) {
        int[] boardCoordinates = null;
        for (int i = 0; i < NUM_MAX_PLAYERS; i++) {
            if (occupiedSupplyPositions[supplyTrack][i] == 0) {
                occupiedSupplyPositions[supplyTrack][i] = 1;
                boardCoordinates = SupplyTrackCoordinates
                        .getSupplyCoordinates(supplyTrack, i);
                break;
            }
        }
        if (boardCoordinates != null) {
            placeSupplyToken(house, boardCoordinates);
        }
    }

    public void addVictoryTokenToVictoryTrack(String house, int victoryTrack) {
        if (victoryTrack > 0 && victoryTrack <= NUM_VICTORY_TRACKS) {
            int[] boardCoordinates = null;
            for (int i = 0; i < NUM_MAX_PLAYERS; i++) {
                if (occupiedVictoryPositions[victoryTrack - 1][i] == 0) {
                    occupiedVictoryPositions[victoryTrack - 1][i] = 1;
                    boardCoordinates = VictoryTrackCoordinates
                            .getVictoryTrackCoordinates(victoryTrack, i);
                    break;
                }
            }
            if (boardCoordinates != null) {
                placeVictoryToken(house, boardCoordinates);
            }
        } else {
            Client.error("Victory track out of range: " + victoryTrack);
        }
    }

    public void addPlayerTokenToFiefdomsTrack(String house, int position) {
        int[] boardCoordinates = InfluenceTrackCoordinates
                .getFiefdomsCoordinates(position);
        if (boardCoordinates != null) {
            placeHouseToken(house, boardCoordinates);
        }
    }

    public void addPlayerTokenToKingsCourtTrack(String house, int position) {
        int[] boardCoordinates = InfluenceTrackCoordinates
                .getKingsCourtCoordinates(position);
        if (boardCoordinates != null) {
            placeHouseToken(house, boardCoordinates);
        }
    }

    public void addOrderTokenToMap(Order order, Zone zone) {
        if (order != null && !order.getType().equals(Order.NONE)) {
            int[] boardCoordinates = OrderTokenCoordinates
                    .getZoneCoordinates(zone);
            if (boardCoordinates != null) {
                placeOrderToken(order, boardCoordinates);
            }
        }
    }

    public File generateImage() {
        graphic.dispose();
        File result = null;
        try {
            result = File.createTempFile("Map", ".jpg");
            if (result.exists()) {
                result.delete();
            }
            if (ImageIO.write(newImage, "jpg", result)) {
                Client.getLogger().info("New board image has been generated.");
            }
        } catch (IOException e) {
            Client.error(e);
        }
        return result;
    }
}
