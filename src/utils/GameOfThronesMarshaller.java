package utils;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.StringWriter;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;

import client.Client;
import client.model.GameContext;

public class GameOfThronesMarshaller {
    public static final String DEFAULT_GAME_COTEXT_FILENAME = "GameContext.xml";
	public GameOfThronesMarshaller() {
		// TODO Auto-generated constructor stub
	}

	public static File marshall(GameContext context) {
		StringWriter sw = new StringWriter();
		try {
			JAXBContext jaxbContext = JAXBContext.newInstance(GameContext.class);
			Marshaller jaxbMarshaller;
			jaxbMarshaller = jaxbContext.createMarshaller();
			jaxbMarshaller.marshal(context, sw);
			String xmlString = sw.toString();
			File testXmlFile = new File(DEFAULT_GAME_COTEXT_FILENAME);
			FileWriter fw = new FileWriter(testXmlFile);
			fw.write(xmlString);
			fw.close();
			return testXmlFile;
		} catch (JAXBException e) {
			Client.error(e);
		} catch (IOException e) {
			Client.error(e);
		}
		return null;
	}

	public static GameContext unMarshallGameContext(File xml) {
		JAXBContext jaxbContext;
		try {
			jaxbContext = JAXBContext.newInstance(GameContext.class);
			Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();
			GameContext result = (GameContext) unmarshaller.unmarshal(xml);
			return result;
		} catch (JAXBException e) {
			Client.error(e);
		}
		return null;
	}
}
